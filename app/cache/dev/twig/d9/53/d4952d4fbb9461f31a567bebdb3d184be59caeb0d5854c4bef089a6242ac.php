<?php

/* ResearchProjectMyProjectBundle:Doctor:edit.html.twig */
class __TwigTemplate_d953d4952d4fbb9461f31a567bebdb3d184be59caeb0d5854c4bef089a6242ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'pagetitle' => array($this, 'block_pagetitle'),
            'doctor' => array($this, 'block_doctor'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pagetitle($context, array $blocks = array())
    {
        echo "DOCTOR";
    }

    // line 4
    public function block_doctor($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "<link href=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/select/select2.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
<center><h1><b>Edit Doctor Information</b></h1></center>
    <div class=\"x_content\">
        <br />
        <div id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "editForm")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >First Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "firstName"), 'widget', array("attr" => array("id" => "firstName", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 17
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "firstName"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Last Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "lastName"), 'widget', array("attr" => array("name" => "lastName", "placeholder" => "Last Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "text")));
        // line 26
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "lastName"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Middle Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "middleName"), 'widget', array("attr" => array("name" => "middleName", "placeholder" => "Middle Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "text")));
        // line 35
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "middleName"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Email Address <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "emailAddress"), 'widget', array("attr" => array("name" => "emailAddress", "placeholder" => "Email Address", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "email")));
        // line 44
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "emailAddress"), 'errors');
        echo "</div></i>
            <div class=\"item form-group \">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Username <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "username"), 'widget', array("attr" => array("name" => "username", "placeholder" => "Username", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "6", "required" => "required", "type" => "text")));
        // line 53
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "username"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Date of Birth <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                   ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "dateOfBirth"), 'widget', array("attr" => array("name" => "dateOfBirth", "placeholder" => "Date of Birth", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "6", "required" => "required", "type" => "password")));
        // line 62
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "dateOfBirth"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Field of Practice <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "fieldOfPractice"), 'widget', array("attr" => array("name" => "fieldOfPractice", "placeholder" => "Field of Practice", "class" => "form-control col-md-7 col-xs-12")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "fieldOfPractice"), 'errors');
        echo "</div></i>
            <div id =\"specialtyblock\" class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Specialty <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "specialties"), 'widget', array("attr" => array("name" => "specialty", "placeholder" => "Specialty", "class" => "select2_multiple form-control")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "specialties"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\" id=\"fellowshipblock\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Fellowship/s <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "fellowships"), 'widget', array("attr" => array("name" => "fellowships", "class" => "select2_multiple form-control", "required" => "required")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "fellowships"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">PRC Number <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 91
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "prcNumber"), 'widget', array("attr" => array("name" => "prcNumber", "placeholder" => "PRC Number", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "7,7", "required" => "required", "type" => "number")));
        // line 92
        echo "  
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "prcNumber"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Original Issue Date <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                   ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "origIssueDate"), 'widget', array("attr" => array("name" => "origIssueDate", "placeholder" => "Original Issue Date:", "class" => "form-control col-md-7 col-xs-12")));
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "origIssueDate"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">About My Practice <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "aboutMyPractice"), 'widget', array("attr" => array("name" => "aboutMyPractice", "placeholder" => "About My Practice", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "6", "required" => "required", "type" => "text")));
        // line 108
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 111
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "aboutMyPractice"), 'errors');
        echo "</div></i>
                
            <div class=\"ln_solid\"></div>
            
            <div class =\"form-group\">
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
        echo "\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "submit"), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button class=\"btn btn-dark\" onclick=\"goBack()\">Cancel</button>
                </div>
            </div>
                
            ";
        // line 122
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
            </div>
    </div>
    
    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>

    <script>
        function goBack() {
        window.history.back();
        }
    </script>
    <!-- icheck -->
    <script src=";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "></script>

    <script src=";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "></script>
    <!-- form validation -->
    <script src=";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
     <script src=";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/select/select2.full.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#editForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#editForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script type=\"text/javascript\">
            var choice = document.getElementById(\"researchproject_myprojectbundle_doctor_fieldOfPractice\");
            choice.onchange=newChoice;
            function newChoice()
            {   
                var choice = document.getElementById(\"researchproject_myprojectbundle_doctor_fieldOfPractice\");
                var selectedValue = choice.options[choice.selectedIndex].value;


                if (selectedValue == \"Specialized\")
                {   document.getElementById(\"specialtyblock\").style.display = \"block\";
                }
                else
                {
                   document.getElementById(\"specialtyblock\").style.display = \"none\";
                }
            }
    </script>
     <script>
            \$(document).ready(function () {
                \$(\".select2_single\").select2({
                    allowClear: true
                });
                \$(\".select2_group\").select2({});
                \$(\".select2_multiple\").select2({
                    allowClear: true
                });
                
                
            });
        </script>

";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Doctor:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 144,  277 => 143,  272 => 141,  267 => 139,  247 => 122,  237 => 117,  228 => 111,  223 => 108,  221 => 107,  214 => 103,  208 => 100,  200 => 95,  195 => 92,  193 => 91,  185 => 86,  179 => 83,  172 => 79,  166 => 76,  159 => 72,  153 => 69,  146 => 65,  141 => 62,  139 => 61,  131 => 56,  126 => 53,  124 => 52,  116 => 47,  111 => 44,  109 => 43,  101 => 38,  96 => 35,  94 => 34,  86 => 29,  81 => 26,  79 => 25,  71 => 20,  66 => 17,  64 => 16,  56 => 11,  47 => 6,  44 => 5,  38 => 4,  32 => 3,  27 => 2,);
    }
}
