<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_fedcb4c50466c3acc12e5f2f1e574001fd2c51fa140a1cdaaf13072b4eb91eaf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e25a073b1e3bbd07a13a45321166c2869f0fe89a5a855c87b797bb291492d9c = $this->env->getExtension("native_profiler");
        $__internal_7e25a073b1e3bbd07a13a45321166c2869f0fe89a5a855c87b797bb291492d9c->enter($__internal_7e25a073b1e3bbd07a13a45321166c2869f0fe89a5a855c87b797bb291492d9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7e25a073b1e3bbd07a13a45321166c2869f0fe89a5a855c87b797bb291492d9c->leave($__internal_7e25a073b1e3bbd07a13a45321166c2869f0fe89a5a855c87b797bb291492d9c_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2b7792d09b99a6bf2d5eda1da8e3a1511f3fcaf917f2ba04fe8c4d3ede8345d7 = $this->env->getExtension("native_profiler");
        $__internal_2b7792d09b99a6bf2d5eda1da8e3a1511f3fcaf917f2ba04fe8c4d3ede8345d7->enter($__internal_2b7792d09b99a6bf2d5eda1da8e3a1511f3fcaf917f2ba04fe8c4d3ede8345d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_2b7792d09b99a6bf2d5eda1da8e3a1511f3fcaf917f2ba04fe8c4d3ede8345d7->leave($__internal_2b7792d09b99a6bf2d5eda1da8e3a1511f3fcaf917f2ba04fe8c4d3ede8345d7_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_3c9be1a374a1727a0d80a6caf31ff5d8d95b80cbe3fe3aa33d5124ca534ee21a = $this->env->getExtension("native_profiler");
        $__internal_3c9be1a374a1727a0d80a6caf31ff5d8d95b80cbe3fe3aa33d5124ca534ee21a->enter($__internal_3c9be1a374a1727a0d80a6caf31ff5d8d95b80cbe3fe3aa33d5124ca534ee21a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_3c9be1a374a1727a0d80a6caf31ff5d8d95b80cbe3fe3aa33d5124ca534ee21a->leave($__internal_3c9be1a374a1727a0d80a6caf31ff5d8d95b80cbe3fe3aa33d5124ca534ee21a_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_ec36a704ad4ebfa615fee8903b6f7382d927eba5d60e4a386fd987ae60b6e3fc = $this->env->getExtension("native_profiler");
        $__internal_ec36a704ad4ebfa615fee8903b6f7382d927eba5d60e4a386fd987ae60b6e3fc->enter($__internal_ec36a704ad4ebfa615fee8903b6f7382d927eba5d60e4a386fd987ae60b6e3fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_ec36a704ad4ebfa615fee8903b6f7382d927eba5d60e4a386fd987ae60b6e3fc->leave($__internal_ec36a704ad4ebfa615fee8903b6f7382d927eba5d60e4a386fd987ae60b6e3fc_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
