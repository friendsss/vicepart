<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_575ab54e326c7e4df5295a85822668f56a74536e1173f4820fe5c13e88bdb61c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_37eef767c959e1d616bb75867b625fb18281ca16040e8684e8688065a735afe0 = $this->env->getExtension("native_profiler");
        $__internal_37eef767c959e1d616bb75867b625fb18281ca16040e8684e8688065a735afe0->enter($__internal_37eef767c959e1d616bb75867b625fb18281ca16040e8684e8688065a735afe0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_37eef767c959e1d616bb75867b625fb18281ca16040e8684e8688065a735afe0->leave($__internal_37eef767c959e1d616bb75867b625fb18281ca16040e8684e8688065a735afe0_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c598f6ed477c9ede1c8ccd20f24bd1904550259a26c86442d77d398dce42428f = $this->env->getExtension("native_profiler");
        $__internal_c598f6ed477c9ede1c8ccd20f24bd1904550259a26c86442d77d398dce42428f->enter($__internal_c598f6ed477c9ede1c8ccd20f24bd1904550259a26c86442d77d398dce42428f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_c598f6ed477c9ede1c8ccd20f24bd1904550259a26c86442d77d398dce42428f->leave($__internal_c598f6ed477c9ede1c8ccd20f24bd1904550259a26c86442d77d398dce42428f_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e1b35691de905bd44f5834636fbe7e3e5e25af6e3fc97dce085f6ef693d3187c = $this->env->getExtension("native_profiler");
        $__internal_e1b35691de905bd44f5834636fbe7e3e5e25af6e3fc97dce085f6ef693d3187c->enter($__internal_e1b35691de905bd44f5834636fbe7e3e5e25af6e3fc97dce085f6ef693d3187c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_e1b35691de905bd44f5834636fbe7e3e5e25af6e3fc97dce085f6ef693d3187c->leave($__internal_e1b35691de905bd44f5834636fbe7e3e5e25af6e3fc97dce085f6ef693d3187c_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_ae9d16bfc3a514fa16b7fc45b39df447a5d5f3d2ff0f03ae9ce1044f9d18d47d = $this->env->getExtension("native_profiler");
        $__internal_ae9d16bfc3a514fa16b7fc45b39df447a5d5f3d2ff0f03ae9ce1044f9d18d47d->enter($__internal_ae9d16bfc3a514fa16b7fc45b39df447a5d5f3d2ff0f03ae9ce1044f9d18d47d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_ae9d16bfc3a514fa16b7fc45b39df447a5d5f3d2ff0f03ae9ce1044f9d18d47d->leave($__internal_ae9d16bfc3a514fa16b7fc45b39df447a5d5f3d2ff0f03ae9ce1044f9d18d47d_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 33,  114 => 32,  108 => 28,  106 => 27,  102 => 25,  96 => 24,  88 => 21,  82 => 17,  80 => 16,  75 => 14,  70 => 13,  64 => 12,  54 => 9,  48 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     {% if collector.hasexception %}*/
/*         <style>*/
/*             {{ render(path('_profiler_exception_css', { token: token })) }}*/
/*         </style>*/
/*     {% endif %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
/* {% block menu %}*/
/*     <span class="label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}">*/
/*         <span class="icon">{{ include('@WebProfiler/Icon/exception.svg') }}</span>*/
/*         <strong>Exception</strong>*/
/*         {% if collector.hasexception %}*/
/*             <span class="count">*/
/*                 <span>1</span>*/
/*             </span>*/
/*         {% endif %}*/
/*     </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     <h2>Exceptions</h2>*/
/* */
/*     {% if not collector.hasexception %}*/
/*         <div class="empty">*/
/*             <p>No exception was thrown and caught during the request.</p>*/
/*         </div>*/
/*     {% else %}*/
/*         <div class="sf-reset">*/
/*             {{ render(path('_profiler_exception', { token: token })) }}*/
/*         </div>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
