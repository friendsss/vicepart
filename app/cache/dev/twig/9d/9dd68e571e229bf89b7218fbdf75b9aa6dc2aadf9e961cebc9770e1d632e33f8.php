<?php

/* ResearchProjectMyProjectBundle:Admin:editDoctorSearch.html.twig */
class __TwigTemplate_daae70dacc69e93bd56922d3aa1e243f3a3fb9bb7594ecf3a44734faadc8572e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:editDoctorSearch.html.twig", 1);
        $this->blocks = array(
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8313ac8537a3f2ff774f4a12c7dd6d82a49681caa9d318223c8849d4a6d5bd67 = $this->env->getExtension("native_profiler");
        $__internal_8313ac8537a3f2ff774f4a12c7dd6d82a49681caa9d318223c8849d4a6d5bd67->enter($__internal_8313ac8537a3f2ff774f4a12c7dd6d82a49681caa9d318223c8849d4a6d5bd67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:editDoctorSearch.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8313ac8537a3f2ff774f4a12c7dd6d82a49681caa9d318223c8849d4a6d5bd67->leave($__internal_8313ac8537a3f2ff774f4a12c7dd6d82a49681caa9d318223c8849d4a6d5bd67_prof);

    }

    // line 2
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_46ada794e4659ae1c94e915a3af8c4034db85b294f96a1c4d7d9ff9821833fac = $this->env->getExtension("native_profiler");
        $__internal_46ada794e4659ae1c94e915a3af8c4034db85b294f96a1c4d7d9ff9821833fac->enter($__internal_46ada794e4659ae1c94e915a3af8c4034db85b294f96a1c4d7d9ff9821833fac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "Edit Account";
        
        $__internal_46ada794e4659ae1c94e915a3af8c4034db85b294f96a1c4d7d9ff9821833fac->leave($__internal_46ada794e4659ae1c94e915a3af8c4034db85b294f96a1c4d7d9ff9821833fac_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_f621579bb52c623cb55944dc1b6e0033fe0867a7690a6ce43503bfd0186b5244 = $this->env->getExtension("native_profiler");
        $__internal_f621579bb52c623cb55944dc1b6e0033fe0867a7690a6ce43503bfd0186b5244->enter($__internal_f621579bb52c623cb55944dc1b6e0033fe0867a7690a6ce43503bfd0186b5244_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    
                        <h3>Edit Account</h3>
                       
                    <div class=\"x_content\">
                        <input class=\"form-control\" id='search' type=\"text\" name=\"query\" placeholder=\"Type a name\" />
                         
                                                    <center><img hidden id=\"loading\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("charisma/img/ajax-loaders/ajax-loader-1.gif"), "html", null, true);
        echo "\" ></center>
                                                    <table class=\"table table-striped\" id=\"resulttable\" class=\"hidden\">

                                                        <tbody id=\"resultbody\">

                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- form validation -->
<script>
                                   
   </script>
    <script>
\$('#search').keyup(function() {
    
     searchText = \$(this).val();
     if (this.value != ''){
         \$(\"#resultbody\").empty();
     }
     
     
     if (this.value.length >= 3 && this.value != '')
  {
     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 53
        echo $this->env->getExtension('routing')->getPath("admin_edit_doctor_search");
        echo "',
        data: {searchText : searchText},
        beforeSend: function(){
        \$('#loading').show();
        },
        complete: function(){
        \$('#loading').hide();
        },
        success : function(data) 
           
           {if(data){
                \$(\"#resultbody\").empty();
                var len = data.length;
                var txt = \"\";
                  if(len > 0){
                    for(var i=0;i<len;i++){
                            //var button= \$('<button type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\"></i> Invite to Group </button>');
                            var entityId = data[i].id;
                            var showPath = '";
        // line 71
        echo $this->env->getExtension('routing')->getPath("admin_doctor_edit", array("id" => "entityId"));
        echo "';
                             var showPath2 = '";
        // line 72
        echo $this->env->getExtension('routing')->getPath("doctor_show", array("id" => "entityId"));
        echo "';
                            showPath = showPath.replace(\"entityId\", data[i].id);
                            showPath2 = showPath2.replace(\"entityId\", data[i].id);
                            txt += \"<tr><td>\"+data[i].firstName+\" \"+data[i].middleName+\" \"+data[i].lastName +\"</td>\";
                            txt+= '<td><a href=\"'+showPath+'\" ><button id=\"edit-'+ data[i].id+'\" type=\"button\" class=\"btn btn-success btn-xs btn-edit\"> <i class=\"fa fa-edit\"></i> Edit Account </button><td><a href=\"'+showPath2+'\" ><button id=\"view-'+ data[i].id+'\" type=\"button\" class=\"btn btn-primary btn-xs btn-view\"> <i class=\"fa fa-user\"></i> View Account </button>';
                        
                    }
                    if(txt != \"\"){
                        \$(\"#resultbody\").append(txt).removeClass(\"hidden\");
                         
                    }
                }
            }
        },error: function() {
         alert('error');
          }
        });
    }
});
       
    </script>
    ";
        
        $__internal_f621579bb52c623cb55944dc1b6e0033fe0867a7690a6ce43503bfd0186b5244->leave($__internal_f621579bb52c623cb55944dc1b6e0033fe0867a7690a6ce43503bfd0186b5244_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:editDoctorSearch.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 72,  128 => 71,  107 => 53,  70 => 19,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% block pagetitle %}Edit Account{% endblock %}*/
/* {% block body -%}*/
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                     */
/*                         <h3>Edit Account</h3>*/
/*                        */
/*                     <div class="x_content">*/
/*                         <input class="form-control" id='search' type="text" name="query" placeholder="Type a name" />*/
/*                          */
/*                                                     <center><img hidden id="loading" src="{{asset('charisma/img/ajax-loaders/ajax-loader-1.gif')}}" ></center>*/
/*                                                     <table class="table table-striped" id="resulttable" class="hidden">*/
/* */
/*                                                         <tbody id="resultbody">*/
/* */
/*                                                             </tr>*/
/*                                                         </tbody>*/
/*                                                     </table>*/
/* */
/*                                                 </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/* <script>*/
/*                                    */
/*    </script>*/
/*     <script>*/
/* $('#search').keyup(function() {*/
/*     */
/*      searchText = $(this).val();*/
/*      if (this.value != ''){*/
/*          $("#resultbody").empty();*/
/*      }*/
/*      */
/*      */
/*      if (this.value.length >= 3 && this.value != '')*/
/*   {*/
/*      $.ajax({*/
/*         type: "POST",*/
/*         dataType: "json",*/
/*         url: '{{ path('admin_edit_doctor_search') }}',*/
/*         data: {searchText : searchText},*/
/*         beforeSend: function(){*/
/*         $('#loading').show();*/
/*         },*/
/*         complete: function(){*/
/*         $('#loading').hide();*/
/*         },*/
/*         success : function(data) */
/*            */
/*            {if(data){*/
/*                 $("#resultbody").empty();*/
/*                 var len = data.length;*/
/*                 var txt = "";*/
/*                   if(len > 0){*/
/*                     for(var i=0;i<len;i++){*/
/*                             //var button= $('<button type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user"></i> Invite to Group </button>');*/
/*                             var entityId = data[i].id;*/
/*                             var showPath = '{{ path("admin_doctor_edit", {'id' : 'entityId' })}}';*/
/*                              var showPath2 = '{{ path("doctor_show", {'id' : 'entityId' })}}';*/
/*                             showPath = showPath.replace("entityId", data[i].id);*/
/*                             showPath2 = showPath2.replace("entityId", data[i].id);*/
/*                             txt += "<tr><td>"+data[i].firstName+" "+data[i].middleName+" "+data[i].lastName +"</td>";*/
/*                             txt+= '<td><a href="'+showPath+'" ><button id="edit-'+ data[i].id+'" type="button" class="btn btn-success btn-xs btn-edit"> <i class="fa fa-edit"></i> Edit Account </button><td><a href="'+showPath2+'" ><button id="view-'+ data[i].id+'" type="button" class="btn btn-primary btn-xs btn-view"> <i class="fa fa-user"></i> View Account </button>';*/
/*                         */
/*                     }*/
/*                     if(txt != ""){*/
/*                         $("#resultbody").append(txt).removeClass("hidden");*/
/*                          */
/*                     }*/
/*                 }*/
/*             }*/
/*         },error: function() {*/
/*          alert('error');*/
/*           }*/
/*         });*/
/*     }*/
/* });*/
/*        */
/*     </script>*/
/*     {% endblock %}*/
/* */
