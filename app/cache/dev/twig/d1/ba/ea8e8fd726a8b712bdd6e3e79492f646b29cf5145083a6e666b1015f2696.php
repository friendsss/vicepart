<?php

/* ResearchProjectMyProjectBundle:Search:searchSpecialty.html.twig */
class __TwigTemplate_d1baea8e8fd726a8b712bdd6e3e79492f646b29cf5145083a6e666b1015f2696 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'doctor' => array($this, 'block_doctor'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_doctor($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " ";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                <h3>&nbsp;DOCTOR - Specialty
                </h3>
            </div>
        </div>
        <div class=\"clearfix\"></div>
        
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                        <h3 style=\"font-size: 20px;\">";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["specialty"]) ? $context["specialty"] : $this->getContext($context, "specialty")), "html", null, true);
        echo "</h3>
                        <a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("doctorhomepage");
        echo "\"><button class=\"btn btn-primary btn-xs\">Back to Specialties</button></a> 
                    <div class=\"x_content\">
                        ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["doctorsBySpecialty"]) ? $context["doctorsBySpecialty"] : $this->getContext($context, "doctorsBySpecialty")));
        foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
            // line 21
            echo "                            <div class=\"col-md-6 col-sm-6 col-xs-6\">
                                <center>
                                    <h3 style=\"font-size: 20px;\"> <a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id"))), "html", null, true);
            echo "\"><i class=\"fa fa-user-md\"></i> &emsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "firstName"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "lastName"), "html", null, true);
            echo "</a></h3>
                                </center>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "                    </div>
                </div>
            </div>
        </div>
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Search:searchSpecialty.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 27,  76 => 23,  72 => 21,  68 => 20,  63 => 18,  59 => 17,  45 => 5,  42 => 4,  36 => 3,  30 => 2,);
    }
}
