<?php

/* ResearchProjectMyProjectBundle:Search:searchResult.html.twig */
class __TwigTemplate_74b0db0e146f11c73637f71c84f116aea19120768ad46aea4cf2aa33dc40c091 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "
";
        // line 4
        $context["doctorcount"] = "";
        // line 5
        $context["cliniccount"] = "";
        // line 6
        $context["illnesscount"] = "";
        // line 7
        $context["doctorgroupcount"] = "";
        // line 8
        echo "<div>
\t<div class=\"row clearfix\">
\t\t<div class=\"col-md-12 column\">
                        
\t\t\t<div class=\"jumbotron\">
                                
                                                ";
        // line 14
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 15
            echo "                                                    <center><h1 style=\"font-family: Century Gothic; font-size: 50px\">Search results for <b style=\"color:#379DFF;\"><i>________</i></b></h1></center>
                                                ";
        } else {
            // line 17
            echo "                                                    <center><h1 style=\"font-family: Century Gothic; font-size: 50px\">Search results for <b style=\"color:#379DFF;\"><i>";
            echo twig_escape_filter($this->env, (isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")), "html", null, true);
            echo "</i></b></h1></center>
                                                ";
        }
        // line 19
        echo "                        </div>
                      
                        
                        <div class=\"x_content\">
                            
                            <form action=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("search");
        echo "\" method=\"POST\" role=\"search\">
                                <div class=\"col-md-10 col-sm-10 col-xs-12 form-group top_search\" style='margin-left: 60px;'>
                                    
                                    <div class=\"input-group\">
                                        <input action=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("search");
        echo "\" method=\"POST\" role=\"search\" id=\"searchfield\" name=\"searchfield\" type=\"text\" class=\"form-control\" placeholder=\"Search something.\">
                                            <span class=\"input-group-btn\">
                                                <button  action=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("search");
        echo "\" method=\"POST\" role=\"search\" class=\"btn btn-default\" type=\"submit\">Go!</button>
                                            </span>
                                    </div>
                                    
                                </div>
                                    </form>
                                    <div class=\"col-xs-12\">
                                        <!-- required for floating -->
                                        <!-- Nav tabs -->
                                        <ul class=\"nav nav-tabs tabs-right\">
                                            
                                            ";
        // line 41
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 42
            echo "                                                     ";
            $context["doctorcount"] = "0";
            // line 43
            echo "                                            ";
        } elseif (twig_test_empty((isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")))) {
            // line 44
            echo "                                                    ";
            $context["doctorcount"] = "0";
            // line 45
            echo "                                            ";
        } else {
            // line 46
            echo "                                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")));
            foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
                // line 47
                echo "                                                    ";
                $context["doctorcount"] = twig_length_filter($this->env, (isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")));
                // line 48
                echo "                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 49
            echo "                                            ";
        }
        // line 50
        echo "                                            <li class=\"active\"><a href=\"#home-r\" data-toggle=\"tab\"><button class=\"btn-lg btn-info\" type=\"button\"><center><i class =\"fa fa-stethoscope\"></i> Doctors (";
        echo twig_escape_filter($this->env, (isset($context["doctorcount"]) ? $context["doctorcount"] : $this->getContext($context, "doctorcount")), "html", null, true);
        echo ")</center></button></a>
                                            </li>
                                            
                                            ";
        // line 53
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 54
            echo "                                                     ";
            $context["cliniccount"] = "0";
            // line 55
            echo "                                            ";
        } elseif (twig_test_empty((isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")))) {
            // line 56
            echo "                                                    ";
            $context["cliniccount"] = "0";
            // line 57
            echo "                                            ";
        } else {
            // line 58
            echo "                                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")));
            foreach ($context['_seq'] as $context["_key"] => $context["clinic"]) {
                // line 59
                echo "                                                    ";
                $context["cliniccount"] = twig_length_filter($this->env, (isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")));
                // line 60
                echo "                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clinic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "                                            ";
        }
        // line 62
        echo "                                            <li><a href=\"#profile-r\" data-toggle=\"tab\"><button class=\"btn-lg btn-info\" type=\"button\"><center><i class =\"fa fa-hospital-o\"></i> Clinics (";
        echo twig_escape_filter($this->env, (isset($context["cliniccount"]) ? $context["cliniccount"] : $this->getContext($context, "cliniccount")), "html", null, true);
        echo ")</center></button></a>
                                            </li>
                                            
                                            ";
        // line 65
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 66
            echo "                                                     ";
            $context["illnesscount"] = "0";
            // line 67
            echo "                                            ";
        } elseif (twig_test_empty((isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")))) {
            // line 68
            echo "                                                    ";
            $context["illnesscount"] = "0";
            // line 69
            echo "                                            ";
        } else {
            // line 70
            echo "                                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")));
            foreach ($context['_seq'] as $context["_key"] => $context["illness"]) {
                // line 71
                echo "                                                    ";
                $context["illnesscount"] = twig_length_filter($this->env, (isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")));
                // line 72
                echo "                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['illness'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "                                            ";
        }
        // line 74
        echo "                                            <li><a href=\"#messages-r\" data-toggle=\"tab\"><button class=\"btn-lg btn-info\" type=\"button\"><center><i class =\"fa fa-wheelchair\"></i> Illnesses (";
        echo twig_escape_filter($this->env, (isset($context["illnesscount"]) ? $context["illnesscount"] : $this->getContext($context, "illnesscount")), "html", null, true);
        echo ")</center></button></a>
                                            </li>
                                            ";
        // line 76
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 77
            echo "                                            ";
            if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
                // line 78
                echo "                                                     ";
                $context["doctorgroupcount"] = "0";
                // line 79
                echo "                                            ";
            } elseif (twig_test_empty((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")))) {
                // line 80
                echo "                                                    ";
                $context["doctorgroupcount"] = "0";
                // line 81
                echo "                                            ";
            } else {
                // line 82
                echo "                                                ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")));
                foreach ($context['_seq'] as $context["_key"] => $context["doctorgroup"]) {
                    // line 83
                    echo "                                                    ";
                    $context["doctorgroupcount"] = twig_length_filter($this->env, (isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")));
                    // line 84
                    echo "                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctorgroup'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 85
                echo "                                            ";
            }
            // line 86
            echo "                                            <li><a href=\"#doctorgroup-r\" data-toggle=\"tab\"><button class=\"btn-lg btn-info\" type=\"button\"><center><i class =\"fa fa-users\"></i> Doctor Groups (";
            echo twig_escape_filter($this->env, (isset($context["doctorgroupcount"]) ? $context["doctorgroupcount"] : $this->getContext($context, "doctorgroupcount")), "html", null, true);
            echo ")</center></button></a>
                                            </li>
                                            ";
        }
        // line 89
        echo "                                        </ul>
                                    </div>

                                    <div class=\"col-xs-12\">
                                        <!-- Tab panes -->
                                        <div class=\"tab-content\">
                                            <div class=\"tab-pane active\" id=\"home-r\">
                                                </br>
                                                </br>
                                                
                                                ";
        // line 99
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 100
            echo "                                                     <center><i><h3>Search something... A doctor maybe?</h3></i></center>
                                                     </br>
                                                     </br>
                                                ";
        } elseif (twig_test_empty((isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")))) {
            // line 104
            echo "                                                   <center><i><h3>No Doctor Found</h3></i></center>
                                                   </br>
                                                   </br>
                                                ";
        } else {
            // line 108
            echo "                                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")));
            foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
                // line 109
                echo "                                                        <center><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id"))), "html", null, true);
                echo "\"><i style=\"font-size: 25px; color: gray\"class =\"fa fa-user-md\"></i><h3>
                                                                    ";
                // line 110
                if (($this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "fieldOfPractice") == "Specialized")) {
                    // line 111
                    echo "                                                                        <b style=\"color:#379DFF;\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "firstName"), "html", null, true);
                    echo "  ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "middleName"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "lastName"), "html", null, true);
                    echo "</b> </a>
                                                                        <br>";
                    // line 112
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "fieldOfPractice"), "html", null, true);
                    echo " - 
                                                                            ";
                    // line 113
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "specialties"));
                    foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                        // line 114
                        echo "                                                                                ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["specialty"]) ? $context["specialty"] : $this->getContext($context, "specialty")), "specialtyName"), "html", null, true);
                        echo "
                                                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 116
                    echo "                                                                    ";
                } else {
                    // line 117
                    echo "                                                                        <b style=\"color:#379DFF;\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "firstName"), "html", null, true);
                    echo "  ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "middleName"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "lastName"), "html", null, true);
                    echo "</b> </a>
                                                                        <br>";
                    // line 118
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "fieldOfPractice"), "html", null, true);
                    echo "
                                                                            
                                                                    ";
                }
                // line 121
                echo "                                                           </h3></center>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 123
            echo "                                                ";
        }
        // line 124
        echo "                                            </div>
                                            <div class=\"tab-pane\" id=\"profile-r\">
                                                </br>
                                                </br>
                                                ";
        // line 128
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 129
            echo "                                                     <center><i><h3>Search something... A clinic maybe?</h3></i></center>
                                                     </br>
                                                     </br>
                                                ";
        } elseif (twig_test_empty((isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")))) {
            // line 133
            echo "                                                   <center><i><h3>No Clinic Found</h3></i></center>
                                                   </br>
                                                   </br>
                                                ";
        } else {
            // line 137
            echo "                                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")));
            foreach ($context['_seq'] as $context["_key"] => $context["clinic"]) {
                echo "     
                                                        <center><a href=\"";
                // line 138
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["clinic"]) ? $context["clinic"] : $this->getContext($context, "clinic")), "id"))), "html", null, true);
                echo "\"><i style=\"font-size: 25px; color: gray\" class =\"fa fa-map-marker\"></i><h3>
                                                                    <b style=\"color:#379DFF;\"> ";
                // line 139
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["clinic"]) ? $context["clinic"] : $this->getContext($context, "clinic")), "clinicName"), "html", null, true);
                echo "</b></a>
                                                                    <br/>";
                // line 140
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["clinic"]) ? $context["clinic"] : $this->getContext($context, "clinic")), "clinicAddress"), "html", null, true);
                echo "</h3></center>                  
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clinic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 142
            echo "                                                ";
        }
        // line 143
        echo "                                            </div>
                                            <div class=\"tab-pane\" id=\"messages-r\">
                                                </br>
                                                </br>
                                                ";
        // line 147
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 148
            echo "                                                     <center><i><h3>Search something... An illness maybe?</h3></i></center>
                                                     </br>
                                                     </br>
                                                ";
        } elseif (twig_test_empty((isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")))) {
            // line 152
            echo "                                                   <center><i><h3>No Illness Found</h3></i></center>
                                                   </br>
                                                   </br>
                                                ";
        } else {
            // line 156
            echo "                                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")));
            foreach ($context['_seq'] as $context["_key"] => $context["illness"]) {
                // line 157
                echo "                                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("illness_show", array("id" => $this->getAttribute((isset($context["illness"]) ? $context["illness"] : $this->getContext($context, "illness")), "id"))), "html", null, true);
                echo "\"><center><i style=\" font-size: 25px; color: gray\" class =\"fa fa-tint\"></i><h3> 
                                                                    <b style=\"color:#379DFF;\">";
                // line 158
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["illness"]) ? $context["illness"] : $this->getContext($context, "illness")), "illnessName"), "html", null, true);
                echo "</b></a>
                                                                    <br/>\"";
                // line 159
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["illness"]) ? $context["illness"] : $this->getContext($context, "illness")), "illnessCommonName"), "html", null, true);
                echo "\"
                                                                    <br/>";
                // line 160
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["illness"]) ? $context["illness"] : $this->getContext($context, "illness")), "illnessDefinition"), "html", null, true);
                echo " </h3></center>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['illness'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 162
            echo "                                                ";
        }
        echo "    
                                            </div>
                                            ";
        // line 164
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 165
            echo "                                            <div class=\"tab-pane\" id=\"doctorgroup-r\">
                                                </br>
                                                </br>
                                                
                                                ";
            // line 169
            if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
                // line 170
                echo "                                                     <center><i><h3>Search something... A doctor group maybe?</h3></i></center>
                                                     </br>
                                                     </br>
                                                ";
            } elseif (twig_test_empty((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")))) {
                // line 174
                echo "                                                   <center><i><h3>No Doctor Group Found</h3></i></center>
                                                   </br>
                                                   </br>
                                                ";
            } else {
                // line 178
                echo "                                                ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")));
                foreach ($context['_seq'] as $context["_key"] => $context["doctorgroup"]) {
                    // line 179
                    echo "                                                        <center><a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute((isset($context["doctorgroup"]) ? $context["doctorgroup"] : $this->getContext($context, "doctorgroup")), "id"))), "html", null, true);
                    echo "\"><i style=\"font-size: 25px; color: gray\"class =\"fa fa-users\"></i>
                                                            <h3>
                                                                <b style=\"color:#379DFF;\">";
                    // line 181
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctorgroup"]) ? $context["doctorgroup"] : $this->getContext($context, "doctorgroup")), "groupName"), "html", null, true);
                    echo " </b><br/>
                                                                ";
                    // line 182
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctorgroup"]) ? $context["doctorgroup"] : $this->getContext($context, "doctorgroup")), "description"), "html", null, true);
                    echo "
                                                            </h3>
                                                        </a></center>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctorgroup'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 186
                echo "                                                ";
            }
            // line 187
            echo "                                            </div>
                                            ";
        }
        // line 189
        echo "                                        </div>
                                    </div>

                                    <div class=\"clearfix\"></div>
                        </div>
                        
                </div>
\t</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Search:searchResult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  493 => 189,  489 => 187,  486 => 186,  476 => 182,  472 => 181,  466 => 179,  461 => 178,  455 => 174,  449 => 170,  447 => 169,  441 => 165,  439 => 164,  433 => 162,  425 => 160,  421 => 159,  417 => 158,  412 => 157,  407 => 156,  401 => 152,  395 => 148,  393 => 147,  387 => 143,  384 => 142,  376 => 140,  372 => 139,  368 => 138,  361 => 137,  355 => 133,  349 => 129,  347 => 128,  341 => 124,  338 => 123,  331 => 121,  325 => 118,  316 => 117,  313 => 116,  304 => 114,  300 => 113,  296 => 112,  287 => 111,  285 => 110,  280 => 109,  275 => 108,  269 => 104,  263 => 100,  261 => 99,  249 => 89,  242 => 86,  239 => 85,  233 => 84,  230 => 83,  225 => 82,  222 => 81,  219 => 80,  216 => 79,  213 => 78,  210 => 77,  208 => 76,  202 => 74,  199 => 73,  193 => 72,  190 => 71,  185 => 70,  182 => 69,  179 => 68,  176 => 67,  173 => 66,  171 => 65,  164 => 62,  161 => 61,  155 => 60,  152 => 59,  147 => 58,  144 => 57,  141 => 56,  138 => 55,  135 => 54,  133 => 53,  126 => 50,  123 => 49,  117 => 48,  114 => 47,  109 => 46,  106 => 45,  103 => 44,  100 => 43,  97 => 42,  95 => 41,  81 => 30,  76 => 28,  69 => 24,  62 => 19,  56 => 17,  52 => 15,  50 => 14,  42 => 8,  40 => 7,  38 => 6,  36 => 5,  34 => 4,  31 => 3,  28 => 2,);
    }
}
