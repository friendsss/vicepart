<?php

/* ResearchProjectMyProjectBundle:Specialty:show.html.twig */
class __TwigTemplate_0373218483cf4de9a30e4dcd5de1589ee00fc79f4b8aaa7d4c2dcb5f10f89630 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "ResearchProjectMyProjectBundle:Specialty:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71cb0db971feb369d609d470ab79506ed0aadce26ef34aee2b01055bec412f71 = $this->env->getExtension("native_profiler");
        $__internal_71cb0db971feb369d609d470ab79506ed0aadce26ef34aee2b01055bec412f71->enter($__internal_71cb0db971feb369d609d470ab79506ed0aadce26ef34aee2b01055bec412f71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Specialty:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_71cb0db971feb369d609d470ab79506ed0aadce26ef34aee2b01055bec412f71->leave($__internal_71cb0db971feb369d609d470ab79506ed0aadce26ef34aee2b01055bec412f71_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b9fdc70b9726ce7210933d70dcdb6db50a90a9bec7e0c1d5dc84df4e6c518d01 = $this->env->getExtension("native_profiler");
        $__internal_b9fdc70b9726ce7210933d70dcdb6db50a90a9bec7e0c1d5dc84df4e6c518d01->enter($__internal_b9fdc70b9726ce7210933d70dcdb6db50a90a9bec7e0c1d5dc84df4e6c518d01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>Specialty</h1>

    <table class=\"record_properties\">
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Specialtyname</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "specialtyName", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("specialty");
        echo "\">
            Back to the list
        </a>
    </li>
    <li>
        <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("specialty_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">
            Edit
        </a>
    </li>
    <li>";
        // line 30
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</li>
</ul>
";
        
        $__internal_b9fdc70b9726ce7210933d70dcdb6db50a90a9bec7e0c1d5dc84df4e6c518d01->leave($__internal_b9fdc70b9726ce7210933d70dcdb6db50a90a9bec7e0c1d5dc84df4e6c518d01_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Specialty:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 30,  73 => 26,  65 => 21,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Specialty</h1>*/
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <td>{{ entity.id }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Specialtyname</th>*/
/*                 <td>{{ entity.specialtyName }}</td>*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('specialty') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/*     <li>*/
/*         <a href="{{ path('specialty_edit', { 'id': entity.id }) }}">*/
/*             Edit*/
/*         </a>*/
/*     </li>*/
/*     <li>{{ form(delete_form) }}</li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
