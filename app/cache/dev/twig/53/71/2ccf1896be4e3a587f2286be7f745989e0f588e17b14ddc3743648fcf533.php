<?php

/* ResearchProjectMyProjectBundle:Default:illness.html.twig */
class __TwigTemplate_53712ccf1896be4e3a587f2286be7f745989e0f588e17b14ddc3743648fcf533 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'illness' => array($this, 'block_illness'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " ";
    }

    // line 3
    public function block_illness($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    <div>
    <div class=\"page-title\">
        <div class=\"title\">
            <h3>&nbsp;ILLNESS</h3>
        </div>
    </div>
    <div class=\"clearfix\"></div>
    
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            ";
        // line 15
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 16
            echo "                    <a href=\"";
            echo $this->env->getExtension('routing')->getPath("illness_new");
            echo "\">
                        <button class=\"btn btn-success right\" style=\"margin-bottom: 5px;\"><i class=\"fa fa-plus\"></i> Define New Illness</button>
                    </a>
                    ";
        }
        // line 20
        echo "            <div class=\"x_panel\">
                <h3>&nbsp;Health Topics</h3>
                    ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 23
            echo "                        <a href='";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("illness_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "'>
                        <div class='col-md-5 col-sm-5 col-xs-5' style='box-shadow: 4px 4px 2px #888888; border-radius: 5px 25px 10px; background-color: #404040; margin-right: 30px; margin-left: 30px;
                             margin-bottom: 20px;' >
                            <center>
                                <h4 style ='color:#379DFF; font-family: Century Gothic; font-size:20px;'>
                                    ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessName"), "html", null, true);
            echo "</h4> 
                                <h4 style ='color: white; font-family:Century Gothic; font-size:15px'>
                                    <i class='fa fa-quote-left'> </i> ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessCommonName"), "html", null, true);
            echo " <i class='fa fa-quote-right'> </i></h4>
                            </center>
                        </div>
                        </a>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "            </div>
        </div>
    </div>
            
    </div>

";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:illness.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 35,  89 => 30,  84 => 28,  75 => 23,  71 => 22,  67 => 20,  59 => 16,  57 => 15,  45 => 5,  42 => 4,  36 => 3,  30 => 2,);
    }
}
