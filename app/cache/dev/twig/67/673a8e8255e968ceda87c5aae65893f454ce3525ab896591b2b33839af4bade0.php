<?php

/* ResearchProjectMyProjectBundle:Clinic:new.html.twig */
class __TwigTemplate_2376eb11859936f1a5b2aa9c65f96e83cd61560c1e70eec22c6474b42136fcbe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Clinic:new.html.twig", 1);
        $this->blocks = array(
            'clinic' => array($this, 'block_clinic'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bf663a48e127649c41eee896ccede407a3cf44b3b596b335b50e4e6cbd423263 = $this->env->getExtension("native_profiler");
        $__internal_bf663a48e127649c41eee896ccede407a3cf44b3b596b335b50e4e6cbd423263->enter($__internal_bf663a48e127649c41eee896ccede407a3cf44b3b596b335b50e4e6cbd423263_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Clinic:new.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field_dropdown.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bf663a48e127649c41eee896ccede407a3cf44b3b596b335b50e4e6cbd423263->leave($__internal_bf663a48e127649c41eee896ccede407a3cf44b3b596b335b50e4e6cbd423263_prof);

    }

    // line 3
    public function block_clinic($context, array $blocks = array())
    {
        $__internal_4af3b5c701fdc0fdf3085082fa3cc9927ef4a95ae3e0cb461d0111f5a4664411 = $this->env->getExtension("native_profiler");
        $__internal_4af3b5c701fdc0fdf3085082fa3cc9927ef4a95ae3e0cb461d0111f5a4664411->enter($__internal_4af3b5c701fdc0fdf3085082fa3cc9927ef4a95ae3e0cb461d0111f5a4664411_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "clinic"));

        echo " class=\"active\" ";
        
        $__internal_4af3b5c701fdc0fdf3085082fa3cc9927ef4a95ae3e0cb461d0111f5a4664411->leave($__internal_4af3b5c701fdc0fdf3085082fa3cc9927ef4a95ae3e0cb461d0111f5a4664411_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_1d7e0943d6d67fe2a03fa234b7544470a44c08e81dd621c6b98c747b0f986b0c = $this->env->getExtension("native_profiler");
        $__internal_1d7e0943d6d67fe2a03fa234b7544470a44c08e81dd621c6b98c747b0f986b0c->enter($__internal_1d7e0943d6d67fe2a03fa234b7544470a44c08e81dd621c6b98c747b0f986b0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<style>
#clinicmap{
    width: 500px;
    height: 400px;
}
</style>
<center><h1><b>Register New Clinic</b></h1>
  <div id=\"overlay\">
    <div id=\"progstat\"></div>
    <div id=\"progress\"></div>
  </div>

    <div class=\"x_content\">
        <br/>
        <div data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "createClinicForm")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Clinic Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clinicName", array()), 'widget', array("attr" => array("name" => "clinicName", "placeholder" => "Clinic Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 26
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clinicName", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Clinic Description <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clinicDescription", array()), 'widget', array("attr" => array("name" => "clinicName", "placeholder" => "Clinic Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "10", "required" => "required", "type" => "text")));
        // line 35
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clinicDescription", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Clinic Address <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clinicAddress", array()), 'widget', array("attr" => array("name" => "clinicAddress", "placeholder" => "Clinic Address", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 44
        echo "
                </div>
            </div>
            <div hidden class=\"form-group\">
               
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clinicLong", array()), 'widget');
        echo "
                </div>
            </div> 
              <div hidden class=\"form-group\">
               
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clinicLat", array()), 'widget');
        echo "
                </div>
            </div>     
               
                <div id=\"clinicmap\" class =\"form-group\">
                </div>
                <div>Drag the red marker to indicate the location of your clinic.
                </div><br>    
                <div hidden id=\"current\">Nothing yet...</div>
                <div hidden id=\"long\"></div>
                <div hidden id=\"lat\"></div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "clinicAddress", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Clinic Contact <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contactNumber", array()), 'widget', array("attr" => array("name" => "contactNumber", "placeholder" => "Contact Number", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 73
        echo "
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-12 col-sm-12 col-xs-12\"><h4><b><center>Clinic Schedule</center></b></h4></label>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Sunday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "sunOpenTime", array()), 'widget');
        echo " 
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                    ";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "sunCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"sunday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Monday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 99
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "monOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "monCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"monday\" type=\"checkbox\" value=\"\"> Closed
                   </label>                             
                </div>
            </div>
               <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Tuesday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 115
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tueOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tueCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"tuesday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Wednesday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "wedOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 135
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "wedCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"wednesday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Thursday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 147
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "thuOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 151
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "thuCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"thursday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Friday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "friOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 167
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "friCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"friday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Saturday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 179
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "satOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 183
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "satCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"saturday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Clinic Display Picture <span class=\"required\">*</span>
                </label>
                    ";
        // line 194
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'widget');
        echo "
            </div>    
            <div class=\"ln_solid\"></div>
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"\">";
        // line 198
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
                </div>
             </div> 
            ";
        // line 202
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>

    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
<!-- form validation -->
    <script src=";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validatgor.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#createClinicForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#createClinicForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
    <script async defer src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDRJhfHphrYhN10mIoK_5CtgJQ0M9qbO14&callback=initMap&libraries=places&sensor=false\"></script> 
    

    <script type=\"text/javascript\" src=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/jquery.geocomplete.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "\"></script>

    <script>

  
        
            
function initMap() {
  var myLatLng = {lat: 10.7167, lng: 122.5667};
  \$('#researchproject_myprojectbundle_clinic_clinicLat').val(\"10.7167\");
  \$('#researchproject_myprojectbundle_clinic_clinicLong').val(\"122.5667\");
  var map = new google.maps.Map(document.getElementById('clinicmap'), {
    zoom: 16,
    center: myLatLng
  });
  
   var input = /** @type {!HTMLInputElement} */(
      document.getElementById('researchproject_myprojectbundle_clinic_clinicAddress'));
    var options = {
  
   componentRestrictions: {country: 'ph'}
};
 
  var autocomplete = new google.maps.places.Autocomplete(input, options);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!',
    draggable: true
  });

  google.maps.event.addListener(marker, 'dragend', function(evt){
    document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';
    \$('#researchproject_myprojectbundle_clinic_clinicLat').val(evt.latLng.lat().toFixed(3));
    \$('#researchproject_myprojectbundle_clinic_clinicLong').val(evt.latLng.lng().toFixed(3));
    
});

google.maps.event.addListener(marker, 'dragstart', function(evt){
    document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
}); 


  autocomplete.addListener('place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      window.alert(\"Autocomplete's returned place contains no geometry\");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
     // map.setZoom(17);  // Why 17? Because it looks good.
    }
    \$('#researchproject_myprojectbundle_clinic_clinicLat').val(place.geometry.location.lat());
    \$('#researchproject_myprojectbundle_clinic_clinicLong').val(place.geometry.location.lng());
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
    radioButton.addEventListener('click', function() {
      autocomplete.setTypes(types);
    });
  }
  
  

  setupClickListener('changetype-all', []);
  setupClickListener('changetype-address', ['address']);
  setupClickListener('changetype-establishment', ['establishment']);
  setupClickListener('changetype-geocode', ['geocode']);
}
    </script>
    </script>
    <script>
    
    document.getElementById('sunday').onclick = function() {
    // access properties using this keyword
     sundayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_hour');
     sundayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_minute');
     sundayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_hour');
     sundayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_minute');
     
     if ( this.checked ) {
        sundayOpenH.disabled = true;
        sundayOpenM.disabled = true;
        sundayOpenH.value = 00;
        sundayOpenM.value = 00;
        sundayCloseH.disabled = true;
        sundayCloseM.disabled = true;
        sundayCloseH.value = 00;
        sundayCloseM.value = 00;
        
         } else {
        sundayOpenH.disabled = false;
        sundayOpenM.disabled = false;
        sundayCloseH.disabled = false;
        sundayCloseM.disabled = false;
         }
         
        
};

    document.getElementById('monday').onclick = function() {
    // access properties using this keyword
     mondayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_hour');
     mondayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_minute');
     mondayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_hour');
     mondayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_minute');
     
     if ( this.checked ) {
         mondayOpenH.disabled = true;
        mondayOpenM.disabled = true;
        mondayOpenH.value = 00;
        mondayOpenM.value = 00;
        mondayCloseH.disabled = true;
        mondayCloseM.disabled = true;
        mondayCloseH.value = 00;
        mondayCloseM.value = 00;
        
         } else {
        mondayOpenH.disabled = false;
        mondayOpenM.disabled = false;
        mondayCloseH.disabled = false;
        mondayCloseM.disabled = false;
         }
         
        
};
      document.getElementById('tuesday').onclick = function() {
    // access properties using this keyword
    tuesdayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_hour');
     tuesdayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_minute');
     tuesdayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_hour');
     tuesdayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_minute');
     
     if ( this.checked ) {
        
        tuesdayOpenH.disabled = true;
        tuesdayOpenM.disabled = true;
        tuesdayOpenH.value = 00;
        tuesdayOpenM.value = 00;
        tuesdayCloseH.disabled = true;
        tuesdayCloseM.disabled = true;
        tuesdayCloseH.value = 00;
        tuesdayCloseM.value = 00;
        
         } else {
         
        tuesdayOpenH.disabled = false;
        tuesdayOpenM.disabled = false;
        tuesdayCloseH.disabled = false;
        tuesdayCloseM.disabled = false;
       
         }
         
        
};

 document.getElementById('wednesday').onclick = function() {
    // access properties using this keyword
     wedOpenH = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_hour');
     wedOpenM = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_minute');
     wedCloseH = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_hour');
     wedCloseM = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_minute');
     
     if ( this.checked ) {
         wedOpenH.disabled = true;
        wedOpenM.disabled = true;
        wedOpenH.value = 00;
        wedOpenM.value = 00;
        wedCloseH.disabled = true;
        wedCloseM.disabled = true;
        wedCloseH.value = 00;
        wedCloseM.value = 00;
        
       
        
         } else {
         
        wedOpenH.disabled = false;
        wedOpenM.disabled = false;
        wedCloseH.disabled = false;
        wedCloseM.disabled = false;
        
        
         }
         
        
};

 document.getElementById('thursday').onclick = function() {
    // access properties using this keyword
    thOpenH = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_hour');
     thOpenM = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_minute');
     thCloseH = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_hour');
     thCloseM = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_minute');
     
     if ( this.checked ) {
         thOpenH.disabled = true;
        thOpenM.disabled = true;
        thOpenH.value = 00;
        thOpenM.value = 00;
        thCloseH.disabled = true;
        thCloseM.disabled = true;
        thCloseH.value = 00;
        thCloseM.value = 00;
        
         } else {
        thOpenH.disabled = false;
        thOpenM.disabled = false;
        thCloseH.disabled = false;
        thCloseM.disabled = false;
       
         }
         
        
};

 document.getElementById('friday').onclick = function() {
    // access properties using this keyword
     
     friOpenH = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_hour');
     friOpenM = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_minute');
     friCloseH = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_hour');
     friCloseM = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_minute');
     
     if ( this.checked ) {
          friOpenH.disabled = true;
        friOpenM.disabled = true;
        friOpenH.value = 00;
        friOpenM.value = 00;
        friCloseH.disabled = true;
        friCloseM.disabled = true;
        friCloseH.value = 00;
        friCloseM.value = 00;
        
        
        
         } else {
         
        friOpenH.disabled = false;
        friOpenM.disabled = false;
        friCloseH.disabled = false;
        friCloseM.disabled = false;
       
         }
         
        
};

 document.getElementById('saturday').onclick = function() {
    // access properties using this keyword
     
     satOpenH = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_hour');
     satOpenM = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_minute');
     satCloseH = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_hour');
     satCloseM = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_minute');
     
     
     if ( this.checked ) {
       satOpenH.disabled = true;
        satOpenM.disabled = true;
        satOpenH.value = 00;
        satOpenM.value = 00;
        satCloseH.disabled = true;
        satCloseM.disabled = true;
        satCloseH.value = 00;
        satCloseM.value = 00;
        
         } else {
        
        satOpenH.disabled = false;
        satOpenM.disabled = false;
        satCloseH.disabled = false;
        satCloseM.disabled = false;
         }
         
        
};

</script>


";
        
        $__internal_1d7e0943d6d67fe2a03fa234b7544470a44c08e81dd621c6b98c747b0f986b0c->leave($__internal_1d7e0943d6d67fe2a03fa234b7544470a44c08e81dd621c6b98c747b0f986b0c_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Clinic:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  393 => 250,  389 => 249,  385 => 248,  347 => 213,  333 => 202,  326 => 198,  319 => 194,  305 => 183,  298 => 179,  283 => 167,  276 => 163,  261 => 151,  254 => 147,  239 => 135,  232 => 131,  217 => 119,  210 => 115,  195 => 103,  188 => 99,  173 => 87,  166 => 83,  154 => 73,  152 => 72,  144 => 67,  130 => 56,  121 => 50,  113 => 44,  111 => 43,  103 => 38,  98 => 35,  96 => 34,  88 => 29,  83 => 26,  81 => 25,  73 => 20,  56 => 5,  50 => 4,  38 => 3,  31 => 1,  29 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% form_theme form 'ResearchProjectMyProjectBundle:Form:field_dropdown.html.twig' %}*/
/* {%block clinic%} class="active" {% endblock %}*/
/* {% block body -%}*/
/* <style>*/
/* #clinicmap{*/
/*     width: 500px;*/
/*     height: 400px;*/
/* }*/
/* </style>*/
/* <center><h1><b>Register New Clinic</b></h1>*/
/*   <div id="overlay">*/
/*     <div id="progstat"></div>*/
/*     <div id="progress"></div>*/
/*   </div>*/
/* */
/*     <div class="x_content">*/
/*         <br/>*/
/*         <div data-parsley-validate class="form-horizontal form-label-left">*/
/*             {{ form_start(form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'createClinicForm' } }) }}*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Clinic Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.clinicName, { 'attr' : { 'name': 'clinicName', 'placeholder' : 'Clinic Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.clinicName)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Clinic Description <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.clinicDescription, { 'attr' : { 'name': 'clinicName', 'placeholder' : 'Clinic Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                          'data-validate-length-range' : '10', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.clinicDescription)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Clinic Address <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.clinicAddress, { 'attr' : { 'name': 'clinicAddress', 'placeholder' : 'Clinic Address', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                          'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*             <div hidden class="form-group">*/
/*                */
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.clinicLong) }}*/
/*                 </div>*/
/*             </div> */
/*               <div hidden class="form-group">*/
/*                */
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.clinicLat) }}*/
/*                 </div>*/
/*             </div>     */
/*                */
/*                 <div id="clinicmap" class ="form-group">*/
/*                 </div>*/
/*                 <div>Drag the red marker to indicate the location of your clinic.*/
/*                 </div><br>    */
/*                 <div hidden id="current">Nothing yet...</div>*/
/*                 <div hidden id="long"></div>*/
/*                 <div hidden id="lat"></div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.clinicAddress)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Clinic Contact <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.contactNumber, { 'attr' : { 'name': 'contactNumber', 'placeholder' : 'Contact Number', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-12 col-sm-12 col-xs-12"><h4><b><center>Clinic Schedule</center></b></h4></label>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Sunday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.sunOpenTime)}} */
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                     {{ form_widget(form.sunCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="sunday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Monday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.monOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.monCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="monday" type="checkbox" value=""> Closed*/
/*                    </label>                             */
/*                 </div>*/
/*             </div>*/
/*                <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Tuesday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.tueOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.tueCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="tuesday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Wednesday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.wedOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.wedCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="wednesday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Thursday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.thuOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.thuCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="thursday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Friday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.friOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.friCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="friday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Saturday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.satOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(form.satCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="saturday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Clinic Display Picture <span class="required">*</span>*/
/*                 </label>*/
/*                     {{form_widget(form.file)}}*/
/*             </div>    */
/*             <div class="ln_solid"></div>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                     <a href="">{{ form_widget(form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn'} }) }}</a>*/
/*                     <button type="reset" class="btn btn-dark" onclick="goBack()" >Cancel</button>*/
/*                 </div>*/
/*              </div> */
/*             {{ form_end(form)}}*/
/*     </div>*/
/* */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validatgor.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#createClinicForm')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#createClinicForm').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/*     <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRJhfHphrYhN10mIoK_5CtgJQ0M9qbO14&callback=initMap&libraries=places&sensor=false"></script> */
/*     */
/* */
/*     <script type="text/javascript" src="{{asset('gentelella/js/jquery.geocomplete.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/icheck/icheck.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/custom.js')}}"></script>*/
/* */
/*     <script>*/
/* */
/*   */
/*         */
/*             */
/* function initMap() {*/
/*   var myLatLng = {lat: 10.7167, lng: 122.5667};*/
/*   $('#researchproject_myprojectbundle_clinic_clinicLat').val("10.7167");*/
/*   $('#researchproject_myprojectbundle_clinic_clinicLong').val("122.5667");*/
/*   var map = new google.maps.Map(document.getElementById('clinicmap'), {*/
/*     zoom: 16,*/
/*     center: myLatLng*/
/*   });*/
/*   */
/*    var input = /** @type {!HTMLInputElement} *//* (*/
/*       document.getElementById('researchproject_myprojectbundle_clinic_clinicAddress'));*/
/*     var options = {*/
/*   */
/*    componentRestrictions: {country: 'ph'}*/
/* };*/
/*  */
/*   var autocomplete = new google.maps.places.Autocomplete(input, options);*/
/*   autocomplete.bindTo('bounds', map);*/
/* */
/*   var infowindow = new google.maps.InfoWindow();*/
/*   var marker = new google.maps.Marker({*/
/*     position: myLatLng,*/
/*     map: map,*/
/*     title: 'Hello World!',*/
/*     draggable: true*/
/*   });*/
/* */
/*   google.maps.event.addListener(marker, 'dragend', function(evt){*/
/*     document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';*/
/*     $('#researchproject_myprojectbundle_clinic_clinicLat').val(evt.latLng.lat().toFixed(3));*/
/*     $('#researchproject_myprojectbundle_clinic_clinicLong').val(evt.latLng.lng().toFixed(3));*/
/*     */
/* });*/
/* */
/* google.maps.event.addListener(marker, 'dragstart', function(evt){*/
/*     document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';*/
/* }); */
/* */
/* */
/*   autocomplete.addListener('place_changed', function() {*/
/*     infowindow.close();*/
/*     marker.setVisible(false);*/
/*     var place = autocomplete.getPlace();*/
/*     if (!place.geometry) {*/
/*       window.alert("Autocomplete's returned place contains no geometry");*/
/*       return;*/
/*     }*/
/* */
/*     // If the place has a geometry, then present it on a map.*/
/*     if (place.geometry.viewport) {*/
/*       map.fitBounds(place.geometry.viewport);*/
/*     } else {*/
/*       map.setCenter(place.geometry.location);*/
/*      // map.setZoom(17);  // Why 17? Because it looks good.*/
/*     }*/
/*     $('#researchproject_myprojectbundle_clinic_clinicLat').val(place.geometry.location.lat());*/
/*     $('#researchproject_myprojectbundle_clinic_clinicLong').val(place.geometry.location.lng());*/
/*     marker.setPosition(place.geometry.location);*/
/*     marker.setVisible(true);*/
/* */
/*     var address = '';*/
/*     if (place.address_components) {*/
/*       address = [*/
/*         (place.address_components[0] && place.address_components[0].short_name || ''),*/
/*         (place.address_components[1] && place.address_components[1].short_name || ''),*/
/*         (place.address_components[2] && place.address_components[2].short_name || '')*/
/*       ].join(' ');*/
/*     }*/
/* */
/*     infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);*/
/*     infowindow.open(map, marker);*/
/*   });*/
/* */
/*   // Sets a listener on a radio button to change the filter type on Places*/
/*   // Autocomplete.*/
/*   function setupClickListener(id, types) {*/
/*     var radioButton = document.getElementById(id);*/
/*     radioButton.addEventListener('click', function() {*/
/*       autocomplete.setTypes(types);*/
/*     });*/
/*   }*/
/*   */
/*   */
/* */
/*   setupClickListener('changetype-all', []);*/
/*   setupClickListener('changetype-address', ['address']);*/
/*   setupClickListener('changetype-establishment', ['establishment']);*/
/*   setupClickListener('changetype-geocode', ['geocode']);*/
/* }*/
/*     </script>*/
/*     </script>*/
/*     <script>*/
/*     */
/*     document.getElementById('sunday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      sundayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_hour');*/
/*      sundayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_minute');*/
/*      sundayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_hour');*/
/*      sundayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*         sundayOpenH.disabled = true;*/
/*         sundayOpenM.disabled = true;*/
/*         sundayOpenH.value = 00;*/
/*         sundayOpenM.value = 00;*/
/*         sundayCloseH.disabled = true;*/
/*         sundayCloseM.disabled = true;*/
/*         sundayCloseH.value = 00;*/
/*         sundayCloseM.value = 00;*/
/*         */
/*          } else {*/
/*         sundayOpenH.disabled = false;*/
/*         sundayOpenM.disabled = false;*/
/*         sundayCloseH.disabled = false;*/
/*         sundayCloseM.disabled = false;*/
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*     document.getElementById('monday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      mondayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_hour');*/
/*      mondayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_minute');*/
/*      mondayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_hour');*/
/*      mondayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*          mondayOpenH.disabled = true;*/
/*         mondayOpenM.disabled = true;*/
/*         mondayOpenH.value = 00;*/
/*         mondayOpenM.value = 00;*/
/*         mondayCloseH.disabled = true;*/
/*         mondayCloseM.disabled = true;*/
/*         mondayCloseH.value = 00;*/
/*         mondayCloseM.value = 00;*/
/*         */
/*          } else {*/
/*         mondayOpenH.disabled = false;*/
/*         mondayOpenM.disabled = false;*/
/*         mondayCloseH.disabled = false;*/
/*         mondayCloseM.disabled = false;*/
/*          }*/
/*          */
/*         */
/* };*/
/*       document.getElementById('tuesday').onclick = function() {*/
/*     // access properties using this keyword*/
/*     tuesdayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_hour');*/
/*      tuesdayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_minute');*/
/*      tuesdayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_hour');*/
/*      tuesdayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*         */
/*         tuesdayOpenH.disabled = true;*/
/*         tuesdayOpenM.disabled = true;*/
/*         tuesdayOpenH.value = 00;*/
/*         tuesdayOpenM.value = 00;*/
/*         tuesdayCloseH.disabled = true;*/
/*         tuesdayCloseM.disabled = true;*/
/*         tuesdayCloseH.value = 00;*/
/*         tuesdayCloseM.value = 00;*/
/*         */
/*          } else {*/
/*          */
/*         tuesdayOpenH.disabled = false;*/
/*         tuesdayOpenM.disabled = false;*/
/*         tuesdayCloseH.disabled = false;*/
/*         tuesdayCloseM.disabled = false;*/
/*        */
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*  document.getElementById('wednesday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      wedOpenH = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_hour');*/
/*      wedOpenM = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_minute');*/
/*      wedCloseH = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_hour');*/
/*      wedCloseM = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*          wedOpenH.disabled = true;*/
/*         wedOpenM.disabled = true;*/
/*         wedOpenH.value = 00;*/
/*         wedOpenM.value = 00;*/
/*         wedCloseH.disabled = true;*/
/*         wedCloseM.disabled = true;*/
/*         wedCloseH.value = 00;*/
/*         wedCloseM.value = 00;*/
/*         */
/*        */
/*         */
/*          } else {*/
/*          */
/*         wedOpenH.disabled = false;*/
/*         wedOpenM.disabled = false;*/
/*         wedCloseH.disabled = false;*/
/*         wedCloseM.disabled = false;*/
/*         */
/*         */
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*  document.getElementById('thursday').onclick = function() {*/
/*     // access properties using this keyword*/
/*     thOpenH = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_hour');*/
/*      thOpenM = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_minute');*/
/*      thCloseH = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_hour');*/
/*      thCloseM = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*          thOpenH.disabled = true;*/
/*         thOpenM.disabled = true;*/
/*         thOpenH.value = 00;*/
/*         thOpenM.value = 00;*/
/*         thCloseH.disabled = true;*/
/*         thCloseM.disabled = true;*/
/*         thCloseH.value = 00;*/
/*         thCloseM.value = 00;*/
/*         */
/*          } else {*/
/*         thOpenH.disabled = false;*/
/*         thOpenM.disabled = false;*/
/*         thCloseH.disabled = false;*/
/*         thCloseM.disabled = false;*/
/*        */
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*  document.getElementById('friday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      */
/*      friOpenH = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_hour');*/
/*      friOpenM = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_minute');*/
/*      friCloseH = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_hour');*/
/*      friCloseM = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*           friOpenH.disabled = true;*/
/*         friOpenM.disabled = true;*/
/*         friOpenH.value = 00;*/
/*         friOpenM.value = 00;*/
/*         friCloseH.disabled = true;*/
/*         friCloseM.disabled = true;*/
/*         friCloseH.value = 00;*/
/*         friCloseM.value = 00;*/
/*         */
/*         */
/*         */
/*          } else {*/
/*          */
/*         friOpenH.disabled = false;*/
/*         friOpenM.disabled = false;*/
/*         friCloseH.disabled = false;*/
/*         friCloseM.disabled = false;*/
/*        */
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*  document.getElementById('saturday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      */
/*      satOpenH = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_hour');*/
/*      satOpenM = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_minute');*/
/*      satCloseH = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_hour');*/
/*      satCloseM = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_minute');*/
/*      */
/*      */
/*      if ( this.checked ) {*/
/*        satOpenH.disabled = true;*/
/*         satOpenM.disabled = true;*/
/*         satOpenH.value = 00;*/
/*         satOpenM.value = 00;*/
/*         satCloseH.disabled = true;*/
/*         satCloseM.disabled = true;*/
/*         satCloseH.value = 00;*/
/*         satCloseM.value = 00;*/
/*         */
/*          } else {*/
/*         */
/*         satOpenH.disabled = false;*/
/*         satOpenM.disabled = false;*/
/*         satCloseH.disabled = false;*/
/*         satCloseM.disabled = false;*/
/*          }*/
/*          */
/*         */
/* };*/
/* */
/* </script>*/
/* */
/* */
/* {% endblock %}*/
/* */
