<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_e71494fe4acb84166845890afa34dcc55fbab367feb9bae77a7cfb5123452a05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c376a8d81abbff7ccf2968ba95a5e75f3ff75400046ef7aa8006faca60d210fc = $this->env->getExtension("native_profiler");
        $__internal_c376a8d81abbff7ccf2968ba95a5e75f3ff75400046ef7aa8006faca60d210fc->enter($__internal_c376a8d81abbff7ccf2968ba95a5e75f3ff75400046ef7aa8006faca60d210fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c376a8d81abbff7ccf2968ba95a5e75f3ff75400046ef7aa8006faca60d210fc->leave($__internal_c376a8d81abbff7ccf2968ba95a5e75f3ff75400046ef7aa8006faca60d210fc_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_a9a8eb387289741d77c4d527eb7f22c8aafba7aa6cd316dcf035b0966716681a = $this->env->getExtension("native_profiler");
        $__internal_a9a8eb387289741d77c4d527eb7f22c8aafba7aa6cd316dcf035b0966716681a->enter($__internal_a9a8eb387289741d77c4d527eb7f22c8aafba7aa6cd316dcf035b0966716681a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_a9a8eb387289741d77c4d527eb7f22c8aafba7aa6cd316dcf035b0966716681a->leave($__internal_a9a8eb387289741d77c4d527eb7f22c8aafba7aa6cd316dcf035b0966716681a_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_668cbd7a9a592c078017a82239c1988bb85534379ad3818f3ba3c267bd646c3d = $this->env->getExtension("native_profiler");
        $__internal_668cbd7a9a592c078017a82239c1988bb85534379ad3818f3ba3c267bd646c3d->enter($__internal_668cbd7a9a592c078017a82239c1988bb85534379ad3818f3ba3c267bd646c3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_668cbd7a9a592c078017a82239c1988bb85534379ad3818f3ba3c267bd646c3d->leave($__internal_668cbd7a9a592c078017a82239c1988bb85534379ad3818f3ba3c267bd646c3d_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9a0f989d7c96d0e163819fd104d1cbc6a99767c19312c6fc5e271b8092f05d0c = $this->env->getExtension("native_profiler");
        $__internal_9a0f989d7c96d0e163819fd104d1cbc6a99767c19312c6fc5e271b8092f05d0c->enter($__internal_9a0f989d7c96d0e163819fd104d1cbc6a99767c19312c6fc5e271b8092f05d0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_9a0f989d7c96d0e163819fd104d1cbc6a99767c19312c6fc5e271b8092f05d0c->leave($__internal_9a0f989d7c96d0e163819fd104d1cbc6a99767c19312c6fc5e271b8092f05d0c_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
