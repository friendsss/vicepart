<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_122c8a4453620462b2cb16fcf2c251e5ea97177800944d05b0fbd87b4265b74d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1371e3f1744936fd5cde52f90a212c863e3989435aae92fcb64b68602d462eb6 = $this->env->getExtension("native_profiler");
        $__internal_1371e3f1744936fd5cde52f90a212c863e3989435aae92fcb64b68602d462eb6->enter($__internal_1371e3f1744936fd5cde52f90a212c863e3989435aae92fcb64b68602d462eb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_1371e3f1744936fd5cde52f90a212c863e3989435aae92fcb64b68602d462eb6->leave($__internal_1371e3f1744936fd5cde52f90a212c863e3989435aae92fcb64b68602d462eb6_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_8d219a6be9fe7bd9ee8b9be87d2317597b19f421260a7516c1f2983a2ffce1b4 = $this->env->getExtension("native_profiler");
        $__internal_8d219a6be9fe7bd9ee8b9be87d2317597b19f421260a7516c1f2983a2ffce1b4->enter($__internal_8d219a6be9fe7bd9ee8b9be87d2317597b19f421260a7516c1f2983a2ffce1b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_8d219a6be9fe7bd9ee8b9be87d2317597b19f421260a7516c1f2983a2ffce1b4->leave($__internal_8d219a6be9fe7bd9ee8b9be87d2317597b19f421260a7516c1f2983a2ffce1b4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
