<?php

/* ResearchProjectMyProjectBundle:Clinic:index.html.twig */
class __TwigTemplate_77d92920f0c91276ae8d4305abd473a150cfe45d9c6bc856528f7bbad2fda8b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basesidebar.html.twig", "ResearchProjectMyProjectBundle:Clinic:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basesidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b71e33f1f2b583ab868d0720e83e4df670ae50f73fe9a60485656bc2b76914e = $this->env->getExtension("native_profiler");
        $__internal_6b71e33f1f2b583ab868d0720e83e4df670ae50f73fe9a60485656bc2b76914e->enter($__internal_6b71e33f1f2b583ab868d0720e83e4df670ae50f73fe9a60485656bc2b76914e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Clinic:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6b71e33f1f2b583ab868d0720e83e4df670ae50f73fe9a60485656bc2b76914e->leave($__internal_6b71e33f1f2b583ab868d0720e83e4df670ae50f73fe9a60485656bc2b76914e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_75a026040e59717e09fb0303a1743a05c0aec81046d9709bb4d30e467f8b2f65 = $this->env->getExtension("native_profiler");
        $__internal_75a026040e59717e09fb0303a1743a05c0aec81046d9709bb4d30e467f8b2f65->enter($__internal_75a026040e59717e09fb0303a1743a05c0aec81046d9709bb4d30e467f8b2f65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>Clinic list</h1>

    <table class=\"records_list\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Clinicname</th>
                <th>Clinictime</th>
                <th>Clinicaddress</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 18
            echo "            <tr>
                <td><a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "clinicName", array()), "html", null, true);
            echo "</td>
              
                <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "clinicAddress", array()), "html", null, true);
            echo "</td>
                <td>
                <ul>
                    <li>
                        <a href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">show</a>
                    </li>
                    <li>
                        <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                    </li>
                </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "        </tbody>
    </table>

        <ul>
        <li>
            <a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("clinic_new");
        echo "\">
                Create a new entry
            </a>
        </li>
    </ul>
    ";
        
        $__internal_75a026040e59717e09fb0303a1743a05c0aec81046d9709bb4d30e467f8b2f65->leave($__internal_75a026040e59717e09fb0303a1743a05c0aec81046d9709bb4d30e467f8b2f65_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Clinic:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 40,  98 => 35,  86 => 29,  80 => 26,  73 => 22,  68 => 20,  62 => 19,  59 => 18,  55 => 17,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::basesidebar.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Clinic list</h1>*/
/* */
/*     <table class="records_list">*/
/*         <thead>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <th>Clinicname</th>*/
/*                 <th>Clinictime</th>*/
/*                 <th>Clinicaddress</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*         {% for entity in entities %}*/
/*             <tr>*/
/*                 <td><a href="{{ path('clinic_show', { 'id': entity.id }) }}">{{ entity.id }}</a></td>*/
/*                 <td>{{ entity.clinicName }}</td>*/
/*               */
/*                 <td>{{ entity.clinicAddress }}</td>*/
/*                 <td>*/
/*                 <ul>*/
/*                     <li>*/
/*                         <a href="{{ path('clinic_show', { 'id': entity.id }) }}">show</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ path('clinic_edit', { 'id': entity.id }) }}">edit</a>*/
/*                     </li>*/
/*                 </ul>*/
/*                 </td>*/
/*             </tr>*/
/*         {% endfor %}*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul>*/
/*         <li>*/
/*             <a href="{{ path('clinic_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/*     {% endblock %}*/
/* */
