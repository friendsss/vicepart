<?php

/* ResearchProjectMyProjectBundle:GroupPost:show.html.twig */
class __TwigTemplate_9c6134c1f27b25e6d00fd0dfabc7defd38c056cd87f4ebc01da278a1affba4a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:GroupPost:show.html.twig", 1);
        $this->blocks = array(
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd39ee0dcf36c4e8db60daba18ce6d60ffd1146d39423642470fe70c23d7469e = $this->env->getExtension("native_profiler");
        $__internal_bd39ee0dcf36c4e8db60daba18ce6d60ffd1146d39423642470fe70c23d7469e->enter($__internal_bd39ee0dcf36c4e8db60daba18ce6d60ffd1146d39423642470fe70c23d7469e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:GroupPost:show.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bd39ee0dcf36c4e8db60daba18ce6d60ffd1146d39423642470fe70c23d7469e->leave($__internal_bd39ee0dcf36c4e8db60daba18ce6d60ffd1146d39423642470fe70c23d7469e_prof);

    }

    // line 3
    public function block_doctorgroup($context, array $blocks = array())
    {
        $__internal_9fdc7996b7be8fceb0446549dfe0ef0ef0248313068610a3fd3e11a33447c493 = $this->env->getExtension("native_profiler");
        $__internal_9fdc7996b7be8fceb0446549dfe0ef0ef0248313068610a3fd3e11a33447c493->enter($__internal_9fdc7996b7be8fceb0446549dfe0ef0ef0248313068610a3fd3e11a33447c493_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctorgroup"));

        echo " class=\"active\" ";
        
        $__internal_9fdc7996b7be8fceb0446549dfe0ef0ef0248313068610a3fd3e11a33447c493->leave($__internal_9fdc7996b7be8fceb0446549dfe0ef0ef0248313068610a3fd3e11a33447c493_prof);

    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_af1a1797e5a2a7ca1b92d96d498ab693ee16ddf60fdec41aedb511f98ef5e6f5 = $this->env->getExtension("native_profiler");
        $__internal_af1a1797e5a2a7ca1b92d96d498ab693ee16ddf60fdec41aedb511f98ef5e6f5->enter($__internal_af1a1797e5a2a7ca1b92d96d498ab693ee16ddf60fdec41aedb511f98ef5e6f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "DOCTOR GROUP";
        
        $__internal_af1a1797e5a2a7ca1b92d96d498ab693ee16ddf60fdec41aedb511f98ef5e6f5->leave($__internal_af1a1797e5a2a7ca1b92d96d498ab693ee16ddf60fdec41aedb511f98ef5e6f5_prof);

    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        $__internal_caf9712d947edb25e3b94d8fef61c9df00440ae2c2baab9fb58ae57953936efb = $this->env->getExtension("native_profiler");
        $__internal_caf9712d947edb25e3b94d8fef61c9df00440ae2c2baab9fb58ae57953936efb->enter($__internal_caf9712d947edb25e3b94d8fef61c9df00440ae2c2baab9fb58ae57953936efb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                <h3>&nbsp;DOCTOR GROUP
                <a class=\"right\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getGroup", array()), "getId", array()))), "html", null, true);
        echo "\"><button class=\"btn btn-primary\">Back to Group</button></a></h3>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                        <h3>Group Post</h3>
                    <div class=\"x_content\">
                    <h3 style=\"font-size: 20px;\">
                        <b>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "postTitle", array()), "html", null, true);
        echo "</b>
                        <br/>
                        <i style=\"font-size: 11px;\">";
        // line 23
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "datePosted", array()), "Y-m-d H:i:s"), "html", null, true);
        echo "</i>
                    </h3>
                     <div style=\"word-wrap: break-word;\">
                         <h3 style=\"font-size: 14px;\"><i class='fa fa-circle'></i> &emsp; ";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "postContent", array()), "html", null, true);
        echo " </h3>
                     </div>
                    
                    <h3 style='margin-bottom: 3px;'>Comments:</h3><br/>
                    ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getPostComments", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 31
            echo "                                <div class=\"alert alert-info\" style=\"margin-bottom: 3px;\">
                                    <div style=\"word-wrap: break-word;\"><h4 style=\"font-family: Century Gothic; font-size: 15px;\"><i class=\"fa fa-circle-o\"></i> &emsp;";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "getCommentContent", array()), "html", null, true);
            echo "<br/></div>
                                     &emsp;&emsp;<i style=\"font-size: 11px;\"> <b>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "getCommentAuthor", array()), "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "getCommentAuthor", array()), "lastName", array()), "html", null, true);
            echo "</b>
                                         (";
            // line 34
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "dateCommented", array()), "Y-m-d H:i:s"), "html", null, true);
            echo ")
                                     
                                     </i>
                                 ";
            // line 37
            if ((($this->getAttribute($context["comment"], "commentAuthor", array()) == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) || ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctorAuthor", array()) == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())))) {
                echo " 
                                            <a  id=\"dlt_comment-";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "id", array()), "html", null, true);
                echo "\" title=\"Remove comment\" data-toggle=\"tooltip\" data-placement=\"left\" class=\"btn btn-danger btn-xs btn-delete-comment right\" href=\"#\">
                                                            <i class=\"glyphicon glyphicon-trash icon-white\"></i>

                                            </a>
                                 ";
            }
            // line 43
            echo "                                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                    
                    ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity1"]) ? $context["entity1"] : $this->getContext($context, "entity1")), "getDoctors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["doc"]) {
            // line 47
            echo "                        ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()) == $context["doc"])) {
                // line 48
                echo "                            <button type=\"button\" class=\"btn btn-info right\" data-toggle=\"modal\" data-target=\"#commentmodal\"><i class=\"glyphicon glyphicon-plus-sign icon-white\"></i> Comment</button>
                        ";
            } else {
                // line 50
                echo "                            <div></div>
                        ";
            }
            // line 52
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doc'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "                        
                                <div id=\"commentmodal\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog modal-lg\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                                </button>
                                                <h3 class=\"modal-title\" id=\"myModalLabel\">New Comment</h3>
                                            </div>
                                            <div class=\"modal-body\">
                                                ";
        // line 64
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "newComment", "class" => "form-horizontal form-label-left", "onsubmit" => "setTimeout(function () { window.location.reload(); }, 10)")));
        echo "
                                                <div class=\"item form-group\">
                                                <label class=\"control-label col-md-2 col-sm-2 col-xs-2 left-align\" >Content: </label>
                                                    <div class=\"col-md-7\">
                                                        ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentContent", array()), 'widget', array("attr" => array("name" => "commentContent", "placeholder" => "Post your comment here", "class" => "form-control col-md-5 col-xs-5", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 69
        echo "
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=\"modal-footer\">
                                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                <a href=\"\">";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Comment", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn", "onClick" => "submitClicked()")));
        echo "</a> 
                                                
                                            </div>
                                            ";
        // line 78
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

 <div id=\"delete\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" >
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 style=\"font-family: Century Gothic; font-size: 16px;\"class=\"modal-title\" id=\"deleteModal\">Delete Post</h4>
                                            </div>

                                            <div class=\"modal-body\">
                                                <center><h4 style=\"font-family: Century Gothic; font-size: 16px;\">
                                                        <i class=\"fa fa-exclamation-triangle\" style=\"color:#ff4d4d; font-size: 30px;\"></i>&emsp;Are you sure you want to remove this comment?</h4></center>
                                            </div>
                                            
                                            <div class=\"modal-footer\">
                                                    <button type=\"button\" id='con_delete_comment' class=\"btn btn-primary\" >Yes</button>
                                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
                                            </div>
                                           </div>
                                        </div>

                                        </div>
   <script src=";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#newComment')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#newComment').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>

<script>
        function goBack() {
        window.history.back();
        }
    </script>
    <script>
        \$('#commentmodal').find('form')[0].reset();
    </script>
    
    <script>
                                    var dltCommentBtnId;
                                    \$(\".btn-delete-comment\").click(function(x) {
                                        x.preventDefault();
                                        dltCommentBtnId = this.id;
                                        dltCommentBtnId = dltCommentBtnId.replace('dlt_comment-', '');
                                        \$('#delete').modal();
                                    });
                                    \$(\"#con_delete_comment\").click(function(d) {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 157
        echo $this->env->getExtension('routing')->getPath("comment_delete");
        echo "',
                                            data: {id: dltCommentBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                                
                                                console.log(response);
                                                 \$(\"#newComment\")[0].reset();
                                                window.location.reload(true);
                                               
                                            },
                                            error: function() {
                                                alert('error');
                                                
                                            }
                                        });
                                    });
                                   
                                   
</script>
    
";
        
        $__internal_caf9712d947edb25e3b94d8fef61c9df00440ae2c2baab9fb58ae57953936efb->leave($__internal_caf9712d947edb25e3b94d8fef61c9df00440ae2c2baab9fb58ae57953936efb_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:GroupPost:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 157,  241 => 109,  207 => 78,  201 => 75,  193 => 69,  191 => 68,  184 => 64,  171 => 53,  165 => 52,  161 => 50,  157 => 48,  154 => 47,  150 => 46,  147 => 45,  140 => 43,  132 => 38,  128 => 37,  122 => 34,  116 => 33,  112 => 32,  109 => 31,  105 => 30,  98 => 26,  92 => 23,  87 => 21,  75 => 12,  69 => 8,  63 => 6,  51 => 4,  39 => 3,  32 => 1,  30 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% form_theme form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {% block doctorgroup %} class="active" {% endblock %}*/
/* {% block pagetitle %}DOCTOR GROUP{% endblock %}*/
/* */
/* {% block body -%}*/
/* */
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 <h3>&nbsp;DOCTOR GROUP*/
/*                 <a class="right" href="{{path('doctorgroup_show', {'id' : entity.getGroup.getId})}}"><button class="btn btn-primary">Back to Group</button></a></h3>*/
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                         <h3>Group Post</h3>*/
/*                     <div class="x_content">*/
/*                     <h3 style="font-size: 20px;">*/
/*                         <b>{{ entity.postTitle }}</b>*/
/*                         <br/>*/
/*                         <i style="font-size: 11px;">{{ entity.datePosted|date('Y-m-d H:i:s') }}</i>*/
/*                     </h3>*/
/*                      <div style="word-wrap: break-word;">*/
/*                          <h3 style="font-size: 14px;"><i class='fa fa-circle'></i> &emsp; {{ entity.postContent }} </h3>*/
/*                      </div>*/
/*                     */
/*                     <h3 style='margin-bottom: 3px;'>Comments:</h3><br/>*/
/*                     {% for comment in entity.getPostComments %}*/
/*                                 <div class="alert alert-info" style="margin-bottom: 3px;">*/
/*                                     <div style="word-wrap: break-word;"><h4 style="font-family: Century Gothic; font-size: 15px;"><i class="fa fa-circle-o"></i> &emsp;{{comment.getCommentContent}}<br/></div>*/
/*                                      &emsp;&emsp;<i style="font-size: 11px;"> <b>{{comment.getCommentAuthor.firstName}} {{comment.getCommentAuthor.lastName}}</b>*/
/*                                          ({{comment.dateCommented|date('Y-m-d H:i:s')}})*/
/*                                      */
/*                                      </i>*/
/*                                  {% if (comment.commentAuthor == app.user) or (entity.getDoctorAuthor == app.user) %} */
/*                                             <a  id="dlt_comment-{{comment.id}}" title="Remove comment" data-toggle="tooltip" data-placement="left" class="btn btn-danger btn-xs btn-delete-comment right" href="#">*/
/*                                                             <i class="glyphicon glyphicon-trash icon-white"></i>*/
/* */
/*                                             </a>*/
/*                                  {% endif%}*/
/*                                     </div>*/
/*                     {% endfor %}*/
/*                     */
/*                     {% for doc in entity1.getDoctors %}*/
/*                         {% if app.user == doc%}*/
/*                             <button type="button" class="btn btn-info right" data-toggle="modal" data-target="#commentmodal"><i class="glyphicon glyphicon-plus-sign icon-white"></i> Comment</button>*/
/*                         {%else%}*/
/*                             <div></div>*/
/*                         {% endif %}*/
/*                     {% endfor %}*/
/*                         */
/*                                 <div id="commentmodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">*/
/*                                     <div class="modal-dialog modal-lg">*/
/*                                         <div class="modal-content">*/
/* */
/*                                             <div class="modal-header">*/
/*                                                 <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>*/
/*                                                 </button>*/
/*                                                 <h3 class="modal-title" id="myModalLabel">New Comment</h3>*/
/*                                             </div>*/
/*                                             <div class="modal-body">*/
/*                                                 {{ form_start(form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'newComment', 'class': 'form-horizontal form-label-left', 'onsubmit':'setTimeout(function () { window.location.reload(); }, 10)' } }) }}*/
/*                                                 <div class="item form-group">*/
/*                                                 <label class="control-label col-md-2 col-sm-2 col-xs-2 left-align" >Content: </label>*/
/*                                                     <div class="col-md-7">*/
/*                                                         {{ form_widget(form.commentContent, { 'attr' : { 'name': 'commentContent', 'placeholder' : 'Post your comment here', 'class' : 'form-control col-md-5 col-xs-5', */
/*                                                         'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <div class="modal-footer">*/
/*                                                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>*/
/*                                                 <a href="">{{ form_widget(form.Comment, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn', 'onClick' : 'submitClicked()'} }) }}</a> */
/*                                                 */
/*                                             </div>*/
/*                                             {{ form_end(form) }}*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*  <div id="delete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">*/
/*                                     <div class="modal-dialog" >*/
/*                                         <div class="modal-content">*/
/*                                             <div class="modal-header">*/
/*                                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 16px;"class="modal-title" id="deleteModal">Delete Post</h4>*/
/*                                             </div>*/
/* */
/*                                             <div class="modal-body">*/
/*                                                 <center><h4 style="font-family: Century Gothic; font-size: 16px;">*/
/*                                                         <i class="fa fa-exclamation-triangle" style="color:#ff4d4d; font-size: 30px;"></i>&emsp;Are you sure you want to remove this comment?</h4></center>*/
/*                                             </div>*/
/*                                             */
/*                                             <div class="modal-footer">*/
/*                                                     <button type="button" id='con_delete_comment' class="btn btn-primary" >Yes</button>*/
/*                                                     <button type="button" class="btn btn-default" data-dismiss="modal">No</button>*/
/*                                             </div>*/
/*                                            </div>*/
/*                                         </div>*/
/* */
/*                                         </div>*/
/*    <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#newComment')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#newComment').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/* */
/* <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/*     <script>*/
/*         $('#commentmodal').find('form')[0].reset();*/
/*     </script>*/
/*     */
/*     <script>*/
/*                                     var dltCommentBtnId;*/
/*                                     $(".btn-delete-comment").click(function(x) {*/
/*                                         x.preventDefault();*/
/*                                         dltCommentBtnId = this.id;*/
/*                                         dltCommentBtnId = dltCommentBtnId.replace('dlt_comment-', '');*/
/*                                         $('#delete').modal();*/
/*                                     });*/
/*                                     $("#con_delete_comment").click(function(d) {*/
/*                                         $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('comment_delete') }}',*/
/*                                             data: {id: dltCommentBtnId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                 */
/*                                                 console.log(response);*/
/*                                                  $("#newComment")[0].reset();*/
/*                                                 window.location.reload(true);*/
/*                                                */
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                                 */
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*                                    */
/*                                    */
/* </script>*/
/*     */
/* {% endblock %}*/
/* */
