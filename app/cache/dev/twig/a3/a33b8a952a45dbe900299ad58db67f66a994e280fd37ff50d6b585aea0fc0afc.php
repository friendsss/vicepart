<?php

/* ResearchProjectMyProjectBundle:Admin:newFellowship.html.twig */
class __TwigTemplate_a3a741092031a99a73e54470e067a47eb2b583af2f78bfccb5f7f190ea6505dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:newFellowship.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_49553ba2109835e3dc22e60022c7c81b9361bdb49de07f788af7fdb61df0eff9 = $this->env->getExtension("native_profiler");
        $__internal_49553ba2109835e3dc22e60022c7c81b9361bdb49de07f788af7fdb61df0eff9->enter($__internal_49553ba2109835e3dc22e60022c7c81b9361bdb49de07f788af7fdb61df0eff9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:newFellowship.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_49553ba2109835e3dc22e60022c7c81b9361bdb49de07f788af7fdb61df0eff9->leave($__internal_49553ba2109835e3dc22e60022c7c81b9361bdb49de07f788af7fdb61df0eff9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_d6a86bb9e194ebb6c918d03a70517415ca7b6aa8407236dd114eea8b507db1e4 = $this->env->getExtension("native_profiler");
        $__internal_d6a86bb9e194ebb6c918d03a70517415ca7b6aa8407236dd114eea8b507db1e4->enter($__internal_d6a86bb9e194ebb6c918d03a70517415ca7b6aa8407236dd114eea8b507db1e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<link href=";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/select/select2.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
</br>

    <center><h1><b>Register New Fellowship</b></h1>
    <div>
        <br/>
        <br/>
        <div >
            ";
        // line 12
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "registerForm", "class" => "form-horizontal form-label-left")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Specialty Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fellowshipName", array()), 'widget', array("attr" => array("placeholder" => "Fellowship Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 18
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fellowshipName", array()), 'errors');
        echo "</div></i>
            
            <div class=\"ln_solid\"></div>
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"\">";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
                </div>
            </div>
           ";
        // line 29
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>

    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
<script type=\"text/javascript\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/external/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
     <script type=\"text/javascript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
     <script src=";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/bootstrap.min.js"), "html", null, true);
        echo "></script>
    <!-- icheck -->
    <script src=";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "></script>

    <script src=";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "></script>
    <!-- form validation -->
    <script src=";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/select/select2.full.js"), "html", null, true);
        echo "></script>
    <!-- tags -->
        <script src=";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/tags/jquery.tagsinput.min.js"), "html", null, true);
        echo "></script>
    <script type=\"text/javascript\" src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("notify/notify.js"), "html", null, true);
        echo "\"></script>
    <!-- input tags -->
        <script>
            function onAddTag(tag) {
                alert(\"Added a tag: \" + tag);
            }

            function onRemoveTag(tag) {
                alert(\"Removed a tag: \" + tag);
            }

            function onChangeTag(input, tag) {
                alert(\"Changed a tag: \" + tag);
            }

            \$(function () {
                \$('#researchproject_myprojectbundle_doctor_contactNumbers').tagsInput({
                    width: 'auto'
                });
            });
        </script>
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
 
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#registerForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

       

        \$('#registerForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }
            

            if (submit)
                this.submit();
            return false;
        });
    </script>
   
";
        
        $__internal_d6a86bb9e194ebb6c918d03a70517415ca7b6aa8407236dd114eea8b507db1e4->leave($__internal_d6a86bb9e194ebb6c918d03a70517415ca7b6aa8407236dd114eea8b507db1e4_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:newFellowship.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 50,  128 => 49,  123 => 47,  119 => 46,  114 => 44,  109 => 42,  104 => 40,  100 => 39,  96 => 38,  84 => 29,  77 => 25,  70 => 21,  65 => 18,  63 => 17,  55 => 12,  43 => 4,  37 => 3,  30 => 1,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% form_theme form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {% block body -%}*/
/*     <link href={{asset('gentelella/css/select/select2.min.css')}} rel="stylesheet">*/
/* </br>*/
/* */
/*     <center><h1><b>Register New Fellowship</b></h1>*/
/*     <div>*/
/*         <br/>*/
/*         <br/>*/
/*         <div >*/
/*             {{ form_start(form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'registerForm', 'class':'form-horizontal form-label-left' } }) }}*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Specialty Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.fellowshipName, { 'attr' : { 'placeholder' : 'Fellowship Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '1',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.fellowshipName)}}</div></i>*/
/*             */
/*             <div class="ln_solid"></div>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                     <a href="">{{ form_widget(form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn'} }) }}</a>*/
/*                     <button type="reset" class="btn btn-dark" onclick="goBack()" >Cancel</button>*/
/*                 </div>*/
/*             </div>*/
/*            {{ form_end(form)}}*/
/*     </div>*/
/* */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/* <script type="text/javascript" src="{{asset('gentelella/jquery-ui/external/jquery/jquery.js')}}"></script>*/
/*      <script type="text/javascript" src="{{asset('gentelella/jquery-ui/jquery-ui.min.js')}}"></script>*/
/*      <script src={{asset('gentelella/js/bootstrap.min.js')}}></script>*/
/*     <!-- icheck -->*/
/*     <script src={{asset('gentelella/js/icheck/icheck.min.js')}}></script>*/
/* */
/*     <script src={{asset('gentelella/js/custom.js')}}></script>*/
/*     <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script src={{asset('gentelella/js/select/select2.full.js')}}></script>*/
/*     <!-- tags -->*/
/*         <script src={{asset('gentelella/js/tags/jquery.tagsinput.min.js')}}></script>*/
/*     <script type="text/javascript" src="{{asset('notify/notify.js')}}"></script>*/
/*     <!-- input tags -->*/
/*         <script>*/
/*             function onAddTag(tag) {*/
/*                 alert("Added a tag: " + tag);*/
/*             }*/
/* */
/*             function onRemoveTag(tag) {*/
/*                 alert("Removed a tag: " + tag);*/
/*             }*/
/* */
/*             function onChangeTag(input, tag) {*/
/*                 alert("Changed a tag: " + tag);*/
/*             }*/
/* */
/*             $(function () {*/
/*                 $('#researchproject_myprojectbundle_doctor_contactNumbers').tagsInput({*/
/*                     width: 'auto'*/
/*                 });*/
/*             });*/
/*         </script>*/
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/*  */
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#registerForm')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*        */
/* */
/*         $('#registerForm').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/*             */
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*    */
/* {% endblock %}*/
/* */
