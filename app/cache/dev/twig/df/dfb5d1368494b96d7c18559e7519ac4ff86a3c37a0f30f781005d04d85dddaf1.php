<?php

/* ::basesidebar.html.twig */
class __TwigTemplate_e8c94b327c0d2a058c509bdc78feafe296cd0e088c889f75a100e26097b71a7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eae0e3375e6b47790446b307000cdcf7b8592cc0c4228e866d12ddd0a82dbe9b = $this->env->getExtension("native_profiler");
        $__internal_eae0e3375e6b47790446b307000cdcf7b8592cc0c4228e866d12ddd0a82dbe9b->enter($__internal_eae0e3375e6b47790446b307000cdcf7b8592cc0c4228e866d12ddd0a82dbe9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::basesidebar.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    <!-- Bootstrap core CSS -->

    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/fonts/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom styling plus plugins -->
   
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/maps/jquery-jvectormap-2.0.1.css"), "html", null, true);
        echo "\" />
    <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/icheck/flat/green.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/floatexamples.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

    <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/nprogress.js"), "html", null, true);
        echo "\"></script>
    <script>
        NProgress.start();
    </script>
    
    <!--[if lt IE 9]>
        <script src=\"../assets/js/ie8-responsive-file-warning.js\"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
          <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->

</head>


<body class=\"nav-md\">

    <div class=\"container body\">


        <div class=\"main_container\">

            <div class=\"col-md-3 left_col\">
                <div class=\"left_col scroll-view\">

                    <div class=\"navbar nav_title\" style=\"border: 0;\">
                        <a href=\"";
        // line 56
        echo $this->env->getExtension('routing')->getPath("doctor_home");
        echo "\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>FIMADILO</span></a>
                    </div>
                    <div class=\"clearfix\"></div>

                    <!-- menu prile quick info -->
                    <div class=\"profile\">
                        <div class=\"profile_pic\">
                            <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
        echo "\" alt=\"...\" class=\"img-circle profile_img\">
                        </div>
                        <div class=\"profile_info\">
                            <span>Welcome,</span>
                            <h2></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">

                        <div class=\"menu_section\">
                            <h3>General</h3>
                            <ul class=\"nav side-menu\">
                                <li><a><i class=\"fa fa-home\"></i> Profile <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"\">View Profile</a>
                                        </li>
                                        <li><a href=\"\">Edit Profile</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-edit\"></i> Groups <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"form.html\">General Form</a>
                                        </li>
                                        <li><a href=\"form_advanced.html\">Advanced Components</a>
                                        </li>
                                        <li><a href=\"form_validation.html\">Form Validation</a>
                                        </li>
                                        <li><a href=\"form_wizards.html\">Form Wizard</a>
                                        </li>
                                        <li><a href=\"form_upload.html\">Form Upload</a>
                                        </li>
                                        <li><a href=\"form_buttons.html\">Form Buttons</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-desktop\"></i> UI Elements <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"general_elements.html\">General Elements</a>
                                        </li>
                                        <li><a href=\"media_gallery.html\">Media Gallery</a>
                                        </li>
                                        <li><a href=\"typography.html\">Typography</a>
                                        </li>
                                        <li><a href=\"icons.html\">Icons</a>
                                        </li>
                                        <li><a href=\"glyphicons.html\">Glyphicons</a>
                                        </li>
                                        <li><a href=\"widgets.html\">Widgets</a>
                                        </li>
                                        <li><a href=\"invoice.html\">Invoice</a>
                                        </li>
                                        <li><a href=\"inbox.html\">Inbox</a>
                                        </li>
                                        <li><a href=\"calender.html\">Calender</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-table\"></i> Tables <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"tables.html\">Tables</a>
                                        </li>
                                        <li><a href=\"tables_dynamic.html\">Table Dynamic</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-bar-chart-o\"></i> Data Presentation <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"chartjs.html\">Chart JS</a>
                                        </li>
                                        <li><a href=\"chartjs2.html\">Chart JS2</a>
                                        </li>
                                        <li><a href=\"morisjs.html\">Moris JS</a>
                                        </li>
                                        <li><a href=\"echarts.html\">ECharts </a>
                                        </li>
                                        <li><a href=\"other_charts.html\">Other Charts </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class=\"menu_section\">
                            <h3>Live On</h3>
                            <ul class=\"nav side-menu\">
                                <li><a><i class=\"fa fa-bug\"></i> Additional Pages <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"e_commerce.html\">E-commerce</a>
                                        </li>
                                        <li><a href=\"projects.html\">Projects</a>
                                        </li>
                                        <li><a href=\"project_detail.html\">Project Detail</a>
                                        </li>
                                        <li><a href=\"contacts.html\">Contacts</a>
                                        </li>
                                        <li><a href=\"profile.html\">Profile</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-windows\"></i> Extras <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"page_404.html\">404 Error</a>
                                        </li>
                                        <li><a href=\"page_500.html\">500 Error</a>
                                        </li>
                                        <li><a href=\"plain_page.html\">Plain Page</a>
                                        </li>
                                        <li><a href=\"login.html\">Login Page</a>
                                        </li>
                                        <li><a href=\"pricing_tables.html\">Pricing Tables</a>
                                        </li>

                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-laptop\"></i> Landing Page <span class=\"label label-success pull-right\">Coming Soon</span></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class=\"sidebar-footer hidden-small\">
                        <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">
                            <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
                        </a>
                        <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
                            <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>
                        </a>
                        <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">
                            <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
                        </a>
                        <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\">
                            <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class=\"top_nav\">

                <div class=\"nav_menu\">
                    <nav class=\"\" role=\"navigation\">
                        <div class=\"nav toggle\">
                            <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
                        </div>

                        <ul class=\"nav navbar-nav navbar-right\">
                            <li class=\"\">
                                <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                    <img src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
        echo "\" alt=\"\">
                                    <span class=\" fa fa-angle-down\"></span>
                                </a>
                                <ul class=\"dropdown-menu dropdown-usermenu animated fadeInDown pull-right\">
                                    <li><a href=\"javascript:;\">  Profile</a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:;\">
                                            <span class=\"badge bg-red pull-right\">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:;\">Help</a>
                                    </li>
                                    <li><a href=\"";
        // line 236
        echo $this->env->getExtension('routing')->getPath("logout");
        echo "\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>

                            <li role=\"presentation\" class=\"dropdown\">
                                <a href=\"javascript:;\" class=\"dropdown-toggle info-number\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                    <i class=\"fa fa-envelope-o\"></i>
                                    <span class=\"badge bg-green\">6</span>
                                </a>
                                <ul id=\"menu1\" class=\"dropdown-menu list-unstyled msg_list animated fadeInDown\" role=\"menu\">
                                    <li>
                                        <a>
                                            <span class=\"image\">
                                        <img src=\"images/img.jpg\" alt=\"Profile Image\" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class=\"time\">3 mins ago</span>
                                            </span>
                                            <span class=\"message\">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class=\"image\">
                                        <img src=\"images/img.jpg\" alt=\"Profile Image\" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class=\"time\">3 mins ago</span>
                                            </span>
                                            <span class=\"message\">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class=\"image\">
                                        <img src=\"images/img.jpg\" alt=\"Profile Image\" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class=\"time\">3 mins ago</span>
                                            </span>
                                            <span class=\"message\">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class=\"image\">
                                        <img src=\"images/img.jpg\" alt=\"Profile Image\" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class=\"time\">3 mins ago</span>
                                            </span>
                                            <span class=\"message\">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class=\"text-center\">
                                                <strong><a href=\"inbox.html\">See All Alerts</strong>
                                                <i class=\"fa fa-angle-right\"></i>
                                        </div>
                                <ul class=\"dropdown-menu dropdown-usermenu animated fadeInDown pull-right\">
                                    <li><a href=\"javascript:;\">  Profile</a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:;\">
                                            <span class=\"badge bg-red pull-right\">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:;\">Help</a>
                                    </li>
                                    <li><a href=\"login.html\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a>
                                    </li>
                                </ul>
                                    </li>
                                </ul>
                                
                            </li>
                            
                            <li>
                                <a>
                                 <div class=\"form-group top_search\">
                                    <form class=\"col-md-5 col-sm-5 col-xs-12 navbar-form navbar-right\" action=\"";
        // line 331
        echo $this->env->getExtension('routing')->getPath("search");
        echo "\" role=\"form\"  method=\"POST\">
                                    <div class=\"input-group\">
                                        <input type=\"text\"  class=\"form-control\" id=\"search\" name=\"search\" placeholder=\"Search\" >
                                        <span class=\"input-group-btn\">
                                        <button class=\"btn btn-default\" type=\"submit\">Go!</button>
                                        </span>
                                    </div>
                                    
                                 </form>
                                     </div>
                                </a>     
                                
                            </li>

                        </ul>
                    </nav>
                </div>
                                        
                                        

            </div>
            <!-- /top navigation -->
            
<!-- page content -->
            <div class=\"right_col\" role=\"main\" style=\"height:800px;\">
                <div class=\"\">
                    <div class=\"page-title\">
                       
                    </div>
                    <div class=\"clearfix\"></div>

                    <div class=\"row\">

                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\" style=\"height:725px;\">
                                <div class=\"x_title\">
                                    <h2>";
        // line 367
        $this->displayBlock('pagetitle', $context, $blocks);
        echo "</h2>
                                    
                                    <div class=\"clearfix\"></div>
                                </div>
                                ";
        // line 371
        $this->displayBlock('body', $context, $blocks);
        // line 372
        echo "                            </div>
                        </div>
                    </div>
                    
                                       
                </div>

                

            </div>
            <!-- /page content -->


                
           
        </div>

    </div>

    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>

    <script src=\"";
        // line 398
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

    <!-- gauge js -->
    <script type=\"text/javascript\" src=\"";
        // line 401
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/gauge/gauge.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 402
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/gauge/gauge_demo.js"), "html", null, true);
        echo "\"></script>
    <!-- chart js -->
    <script src=\"";
        // line 404
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/chartjs/chart.min.js"), "html", null, true);
        echo "\"></script>
    <!-- bootstrap progress js -->
    <script src=\"";
        // line 406
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/progressbar/bootstrap-progressbar.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 407
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/nicescroll/jquery.nicescroll.min.js"), "html", null, true);
        echo "\"></script>
    <!-- icheck -->
    <script src=\"";
        // line 409
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
    <!-- daterangepicker -->
    <script type=\"text/javascript\" src=\"";
        // line 411
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/moment.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 412
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/datepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 414
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "\"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type=\"text/javascript\" src=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/excanvas.min.js"), "html", null, true);
        echo "\"></script><![endif]-->
    <script type=\"text/javascript\" src=\"";
        // line 418
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 419
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.pie.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 420
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.orderBars.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 421
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.time.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 422
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/date.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 423
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.spline.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.stack.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 425
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/curvedLines.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 426
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(document).ready(function () {
            // [17, 74, 6, 39, 20, 85, 7]
            //[82, 23, 66, 9, 99, 6, 2]
            var data1 = [[gd(2012, 1, 1), 17], [gd(2012, 1, 2), 74], [gd(2012, 1, 3), 6], [gd(2012, 1, 4), 39], [gd(2012, 1, 5), 20], [gd(2012, 1, 6), 85], [gd(2012, 1, 7), 7]];

            var data2 = [[gd(2012, 1, 1), 82], [gd(2012, 1, 2), 23], [gd(2012, 1, 3), 66], [gd(2012, 1, 4), 9], [gd(2012, 1, 5), 119], [gd(2012, 1, 6), 6], [gd(2012, 1, 7), 9]];
            \$(\"#canvas_dahs\").length && \$.plot(\$(\"#canvas_dahs\"), [
                data1, data2
            ], {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    verticalLines: true,
                    hoverable: true,
                    clickable: true,
                    tickColor: \"#d5d5d5\",
                    borderWidth: 1,
                    color: '#fff'
                },
                colors: [\"rgba(38, 185, 154, 0.38)\", \"rgba(3, 88, 106, 0.38)\"],
                xaxis: {
                    tickColor: \"rgba(51, 51, 51, 0.06)\",
                    mode: \"time\",
                    tickSize: [1, \"day\"],
                    //tickLength: 10,
                    axisLabel: \"Date\",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                        //mode: \"time\", timeformat: \"%m/%d/%y\", minTickSize: [1, \"day\"]
                },
                yaxis: {
                    ticks: 8,
                    tickColor: \"rgba(51, 51, 51, 0.06)\",
                },
                tooltip: false
            });

            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }
        });
    </script>

    <!-- worldmap -->
    <script type=\"text/javascript\" src=\"";
        // line 489
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/maps/jquery-jvectormap-2.0.1.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 490
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/maps/gdp-data.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 491
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/maps/jquery-jvectormap-world-mill-en.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 492
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/maps/jquery-jvectormap-us-aea-en.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#world-map-gdp').vectorMap({
                map: 'world_mill_en',
                backgroundColor: 'transparent',
                zoomOnScroll: false,
                series: {
                    regions: [{
                        values: gdpData,
                        scale: ['#E6F2F0', '#149B7E'],
                        normalizeFunction: 'polynomial'
                    }]
                },
                onRegionTipShow: function (e, el, code) {
                    el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
                }
            });
        });
    </script>
    <!-- skycons -->
    <script src=\"js/skycons/skycons.js\"></script>
    <script>
        var icons = new Skycons({
                \"color\": \"#73879C\"
            }),
            list = [
                \"clear-day\", \"clear-night\", \"partly-cloudy-day\",
                \"partly-cloudy-night\", \"cloudy\", \"rain\", \"sleet\", \"snow\", \"wind\",
                \"fog\"
            ],
            i;

        for (i = list.length; i--;)
            icons.set(list[i], list[i]);

        icons.play();
    </script>

    <!-- dashbord linegraph -->
    <script>
        var doughnutData = [
            {
                value: 30,
                color: \"#455C73\"
            },
            {
                value: 30,
                color: \"#9B59B6\"
            },
            {
                value: 60,
                color: \"#BDC3C7\"
            },
            {
                value: 100,
                color: \"#26B99A\"
            },
            {
                value: 120,
                color: \"#3498DB\"
            }
    ];
        var myDoughnut = new Chart(document.getElementById(\"canvas1\").getContext(\"2d\")).Doughnut(doughnutData);
    </script>
    <!-- /dashbord linegraph -->
    <!-- datepicker -->
    <script type=\"text/javascript\">
        \$(document).ready(function () {

            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                \$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                //alert(\"Callback has fired: [\" + start.format('MMMM D, YYYY') + \" to \" + end.format('MMMM D, YYYY') + \", label = \" + label + \"]\");
            }

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };
            \$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            \$('#reportrange').daterangepicker(optionSet1, cb);
            \$('#reportrange').on('show.daterangepicker', function () {
                console.log(\"show event fired\");
            });
            \$('#reportrange').on('hide.daterangepicker', function () {
                console.log(\"hide event fired\");
            });
            \$('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                console.log(\"apply event fired, start/end dates are \" + picker.startDate.format('MMMM D, YYYY') + \" to \" + picker.endDate.format('MMMM D, YYYY'));
            });
            \$('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                console.log(\"cancel event fired\");
            });
            \$('#options1').click(function () {
                \$('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
            });
            \$('#options2').click(function () {
                \$('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
            });
            \$('#destroy').click(function () {
                \$('#reportrange').data('daterangepicker').remove();
            });
        });
    </script>
    <script>
        NProgress.done();
    </script>
    <!-- /datepicker -->
    <!-- /footer content -->
</body>

</html>

";
        
        $__internal_eae0e3375e6b47790446b307000cdcf7b8592cc0c4228e866d12ddd0a82dbe9b->leave($__internal_eae0e3375e6b47790446b307000cdcf7b8592cc0c4228e866d12ddd0a82dbe9b_prof);

    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        $__internal_99749e84cfc679e8ffe50807d609e3a918858b3435078b6c5c8957c60753d561 = $this->env->getExtension("native_profiler");
        $__internal_99749e84cfc679e8ffe50807d609e3a918858b3435078b6c5c8957c60753d561->enter($__internal_99749e84cfc679e8ffe50807d609e3a918858b3435078b6c5c8957c60753d561_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "FIMADILO";
        
        $__internal_99749e84cfc679e8ffe50807d609e3a918858b3435078b6c5c8957c60753d561->leave($__internal_99749e84cfc679e8ffe50807d609e3a918858b3435078b6c5c8957c60753d561_prof);

    }

    // line 367
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_493aa890c14933a3f2125d62e8df7862d69385f15f85f8fcf8abce7bd9a32441 = $this->env->getExtension("native_profiler");
        $__internal_493aa890c14933a3f2125d62e8df7862d69385f15f85f8fcf8abce7bd9a32441->enter($__internal_493aa890c14933a3f2125d62e8df7862d69385f15f85f8fcf8abce7bd9a32441_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        
        $__internal_493aa890c14933a3f2125d62e8df7862d69385f15f85f8fcf8abce7bd9a32441->leave($__internal_493aa890c14933a3f2125d62e8df7862d69385f15f85f8fcf8abce7bd9a32441_prof);

    }

    // line 371
    public function block_body($context, array $blocks = array())
    {
        $__internal_d061c5b57349d30ab2eb4fff990e6d65efc4911a7bf1dc6449c7a1d478e0e0b8 = $this->env->getExtension("native_profiler");
        $__internal_d061c5b57349d30ab2eb4fff990e6d65efc4911a7bf1dc6449c7a1d478e0e0b8->enter($__internal_d061c5b57349d30ab2eb4fff990e6d65efc4911a7bf1dc6449c7a1d478e0e0b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_d061c5b57349d30ab2eb4fff990e6d65efc4911a7bf1dc6449c7a1d478e0e0b8->leave($__internal_d061c5b57349d30ab2eb4fff990e6d65efc4911a7bf1dc6449c7a1d478e0e0b8_prof);

    }

    public function getTemplateName()
    {
        return "::basesidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  814 => 371,  803 => 367,  791 => 11,  635 => 492,  631 => 491,  627 => 490,  623 => 489,  557 => 426,  553 => 425,  549 => 424,  545 => 423,  541 => 422,  537 => 421,  533 => 420,  529 => 419,  525 => 418,  521 => 417,  515 => 414,  510 => 412,  506 => 411,  501 => 409,  496 => 407,  492 => 406,  487 => 404,  482 => 402,  478 => 401,  472 => 398,  444 => 372,  442 => 371,  435 => 367,  396 => 331,  298 => 236,  280 => 221,  119 => 63,  109 => 56,  77 => 27,  73 => 26,  68 => 24,  64 => 23,  60 => 22,  53 => 18,  49 => 17,  44 => 15,  37 => 11,  25 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* */
/* <head>*/
/*     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">*/
/*     <!-- Meta, title, CSS, favicons, etc. -->*/
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* */
/*     <title>{% block title %}FIMADILO{% endblock %}</title>*/
/* */
/*     <!-- Bootstrap core CSS -->*/
/* */
/*     <link href="{{asset('gentelella/css/bootstrap.min.css')}}" rel="stylesheet">*/
/* */
/*     <link href="{{asset('gentelella/fonts/css/font-awesome.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('gentelella/css/animate.min.css')}}" rel="stylesheet">*/
/* */
/*     <!-- Custom styling plus plugins -->*/
/*    */
/*     <link rel="stylesheet" type="text/css" href="{{asset('gentelella/css/maps/jquery-jvectormap-2.0.1.css')}}" />*/
/*     <link href="{{asset('gentelella/css/icheck/flat/green.css')}}" rel="stylesheet" />*/
/*     <link href="{{asset('gentelella/css/floatexamples.css')}}" rel="stylesheet" type="text/css" />*/
/* */
/*     <script src="{{asset('gentelella/js/jquery.min.js')}}"></script>*/
/*     <script src="{{asset('gentelella/js/nprogress.js')}}"></script>*/
/*     <script>*/
/*         NProgress.start();*/
/*     </script>*/
/*     */
/*     <!--[if lt IE 9]>*/
/*         <script src="../assets/js/ie8-responsive-file-warning.js"></script>*/
/*         <![endif]-->*/
/* */
/*     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->*/
/*     <!--[if lt IE 9]>*/
/*           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>*/
/*           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>*/
/*         <![endif]-->*/
/* */
/* </head>*/
/* */
/* */
/* <body class="nav-md">*/
/* */
/*     <div class="container body">*/
/* */
/* */
/*         <div class="main_container">*/
/* */
/*             <div class="col-md-3 left_col">*/
/*                 <div class="left_col scroll-view">*/
/* */
/*                     <div class="navbar nav_title" style="border: 0;">*/
/*                         <a href="{{ path('doctor_home') }}" class="site_title"><i class="fa fa-paw"></i> <span>FIMADILO</span></a>*/
/*                     </div>*/
/*                     <div class="clearfix"></div>*/
/* */
/*                     <!-- menu prile quick info -->*/
/*                     <div class="profile">*/
/*                         <div class="profile_pic">*/
/*                             <img src="{{asset('gentelella/images/img.jpg')}}" alt="..." class="img-circle profile_img">*/
/*                         </div>*/
/*                         <div class="profile_info">*/
/*                             <span>Welcome,</span>*/
/*                             <h2></h2>*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- /menu prile quick info -->*/
/* */
/*                     <br />*/
/* */
/*                     <!-- sidebar menu -->*/
/*                     <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">*/
/* */
/*                         <div class="menu_section">*/
/*                             <h3>General</h3>*/
/*                             <ul class="nav side-menu">*/
/*                                 <li><a><i class="fa fa-home"></i> Profile <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                         <li><a href="">View Profile</a>*/
/*                                         </li>*/
/*                                         <li><a href="">Edit Profile</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-edit"></i> Groups <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                         <li><a href="form.html">General Form</a>*/
/*                                         </li>*/
/*                                         <li><a href="form_advanced.html">Advanced Components</a>*/
/*                                         </li>*/
/*                                         <li><a href="form_validation.html">Form Validation</a>*/
/*                                         </li>*/
/*                                         <li><a href="form_wizards.html">Form Wizard</a>*/
/*                                         </li>*/
/*                                         <li><a href="form_upload.html">Form Upload</a>*/
/*                                         </li>*/
/*                                         <li><a href="form_buttons.html">Form Buttons</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                         <li><a href="general_elements.html">General Elements</a>*/
/*                                         </li>*/
/*                                         <li><a href="media_gallery.html">Media Gallery</a>*/
/*                                         </li>*/
/*                                         <li><a href="typography.html">Typography</a>*/
/*                                         </li>*/
/*                                         <li><a href="icons.html">Icons</a>*/
/*                                         </li>*/
/*                                         <li><a href="glyphicons.html">Glyphicons</a>*/
/*                                         </li>*/
/*                                         <li><a href="widgets.html">Widgets</a>*/
/*                                         </li>*/
/*                                         <li><a href="invoice.html">Invoice</a>*/
/*                                         </li>*/
/*                                         <li><a href="inbox.html">Inbox</a>*/
/*                                         </li>*/
/*                                         <li><a href="calender.html">Calender</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                         <li><a href="tables.html">Tables</a>*/
/*                                         </li>*/
/*                                         <li><a href="tables_dynamic.html">Table Dynamic</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                         <li><a href="chartjs.html">Chart JS</a>*/
/*                                         </li>*/
/*                                         <li><a href="chartjs2.html">Chart JS2</a>*/
/*                                         </li>*/
/*                                         <li><a href="morisjs.html">Moris JS</a>*/
/*                                         </li>*/
/*                                         <li><a href="echarts.html">ECharts </a>*/
/*                                         </li>*/
/*                                         <li><a href="other_charts.html">Other Charts </a>*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                             </ul>*/
/*                         </div>*/
/*                         <div class="menu_section">*/
/*                             <h3>Live On</h3>*/
/*                             <ul class="nav side-menu">*/
/*                                 <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                         <li><a href="e_commerce.html">E-commerce</a>*/
/*                                         </li>*/
/*                                         <li><a href="projects.html">Projects</a>*/
/*                                         </li>*/
/*                                         <li><a href="project_detail.html">Project Detail</a>*/
/*                                         </li>*/
/*                                         <li><a href="contacts.html">Contacts</a>*/
/*                                         </li>*/
/*                                         <li><a href="profile.html">Profile</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                         <li><a href="page_404.html">404 Error</a>*/
/*                                         </li>*/
/*                                         <li><a href="page_500.html">500 Error</a>*/
/*                                         </li>*/
/*                                         <li><a href="plain_page.html">Plain Page</a>*/
/*                                         </li>*/
/*                                         <li><a href="login.html">Login Page</a>*/
/*                                         </li>*/
/*                                         <li><a href="pricing_tables.html">Pricing Tables</a>*/
/*                                         </li>*/
/* */
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a>*/
/*                                 </li>*/
/*                             </ul>*/
/*                         </div>*/
/* */
/*                     </div>*/
/*                     <!-- /sidebar menu -->*/
/* */
/*                     <!-- /menu footer buttons -->*/
/*                     <div class="sidebar-footer hidden-small">*/
/*                         <a data-toggle="tooltip" data-placement="top" title="Settings">*/
/*                             <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>*/
/*                         </a>*/
/*                         <a data-toggle="tooltip" data-placement="top" title="FullScreen">*/
/*                             <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>*/
/*                         </a>*/
/*                         <a data-toggle="tooltip" data-placement="top" title="Lock">*/
/*                             <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>*/
/*                         </a>*/
/*                         <a data-toggle="tooltip" data-placement="top" title="Logout">*/
/*                             <span class="glyphicon glyphicon-off" aria-hidden="true"></span>*/
/*                         </a>*/
/*                     </div>*/
/*                     <!-- /menu footer buttons -->*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <!-- top navigation -->*/
/*             <div class="top_nav">*/
/* */
/*                 <div class="nav_menu">*/
/*                     <nav class="" role="navigation">*/
/*                         <div class="nav toggle">*/
/*                             <a id="menu_toggle"><i class="fa fa-bars"></i></a>*/
/*                         </div>*/
/* */
/*                         <ul class="nav navbar-nav navbar-right">*/
/*                             <li class="">*/
/*                                 <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">*/
/*                                     <img src="{{asset('gentelella/images/img.jpg')}}" alt="">*/
/*                                     <span class=" fa fa-angle-down"></span>*/
/*                                 </a>*/
/*                                 <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">*/
/*                                     <li><a href="javascript:;">  Profile</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="javascript:;">*/
/*                                             <span class="badge bg-red pull-right">50%</span>*/
/*                                             <span>Settings</span>*/
/*                                         </a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="javascript:;">Help</a>*/
/*                                     </li>*/
/*                                     <li><a href="{{ path('logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>*/
/*                                     </li>*/
/*                                 </ul>*/
/*                             </li>*/
/* */
/*                             <li role="presentation" class="dropdown">*/
/*                                 <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">*/
/*                                     <i class="fa fa-envelope-o"></i>*/
/*                                     <span class="badge bg-green">6</span>*/
/*                                 </a>*/
/*                                 <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">*/
/*                                     <li>*/
/*                                         <a>*/
/*                                             <span class="image">*/
/*                                         <img src="images/img.jpg" alt="Profile Image" />*/
/*                                     </span>*/
/*                                             <span>*/
/*                                         <span>John Smith</span>*/
/*                                             <span class="time">3 mins ago</span>*/
/*                                             </span>*/
/*                                             <span class="message">*/
/*                                         Film festivals used to be do-or-die moments for movie makers. They were where... */
/*                                     </span>*/
/*                                         </a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a>*/
/*                                             <span class="image">*/
/*                                         <img src="images/img.jpg" alt="Profile Image" />*/
/*                                     </span>*/
/*                                             <span>*/
/*                                         <span>John Smith</span>*/
/*                                             <span class="time">3 mins ago</span>*/
/*                                             </span>*/
/*                                             <span class="message">*/
/*                                         Film festivals used to be do-or-die moments for movie makers. They were where... */
/*                                     </span>*/
/*                                         </a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a>*/
/*                                             <span class="image">*/
/*                                         <img src="images/img.jpg" alt="Profile Image" />*/
/*                                     </span>*/
/*                                             <span>*/
/*                                         <span>John Smith</span>*/
/*                                             <span class="time">3 mins ago</span>*/
/*                                             </span>*/
/*                                             <span class="message">*/
/*                                         Film festivals used to be do-or-die moments for movie makers. They were where... */
/*                                     </span>*/
/*                                         </a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a>*/
/*                                             <span class="image">*/
/*                                         <img src="images/img.jpg" alt="Profile Image" />*/
/*                                     </span>*/
/*                                             <span>*/
/*                                         <span>John Smith</span>*/
/*                                             <span class="time">3 mins ago</span>*/
/*                                             </span>*/
/*                                             <span class="message">*/
/*                                         Film festivals used to be do-or-die moments for movie makers. They were where... */
/*                                     </span>*/
/*                                         </a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <div class="text-center">*/
/*                                                 <strong><a href="inbox.html">See All Alerts</strong>*/
/*                                                 <i class="fa fa-angle-right"></i>*/
/*                                         </div>*/
/*                                 <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">*/
/*                                     <li><a href="javascript:;">  Profile</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="javascript:;">*/
/*                                             <span class="badge bg-red pull-right">50%</span>*/
/*                                             <span>Settings</span>*/
/*                                         </a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="javascript:;">Help</a>*/
/*                                     </li>*/
/*                                     <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a>*/
/*                                     </li>*/
/*                                 </ul>*/
/*                                     </li>*/
/*                                 </ul>*/
/*                                 */
/*                             </li>*/
/*                             */
/*                             <li>*/
/*                                 <a>*/
/*                                  <div class="form-group top_search">*/
/*                                     <form class="col-md-5 col-sm-5 col-xs-12 navbar-form navbar-right" action="{{path('search')}}" role="form"  method="POST">*/
/*                                     <div class="input-group">*/
/*                                         <input type="text"  class="form-control" id="search" name="search" placeholder="Search" >*/
/*                                         <span class="input-group-btn">*/
/*                                         <button class="btn btn-default" type="submit">Go!</button>*/
/*                                         </span>*/
/*                                     </div>*/
/*                                     */
/*                                  </form>*/
/*                                      </div>*/
/*                                 </a>     */
/*                                 */
/*                             </li>*/
/* */
/*                         </ul>*/
/*                     </nav>*/
/*                 </div>*/
/*                                         */
/*                                         */
/* */
/*             </div>*/
/*             <!-- /top navigation -->*/
/*             */
/* <!-- page content -->*/
/*             <div class="right_col" role="main" style="height:800px;">*/
/*                 <div class="">*/
/*                     <div class="page-title">*/
/*                        */
/*                     </div>*/
/*                     <div class="clearfix"></div>*/
/* */
/*                     <div class="row">*/
/* */
/*                         <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                             <div class="x_panel" style="height:725px;">*/
/*                                 <div class="x_title">*/
/*                                     <h2>{% block pagetitle %}{% endblock %}</h2>*/
/*                                     */
/*                                     <div class="clearfix"></div>*/
/*                                 </div>*/
/*                                 {% block body %}{% endblock %}*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     */
/*                                        */
/*                 </div>*/
/* */
/*                 */
/* */
/*             </div>*/
/*             <!-- /page content -->*/
/* */
/* */
/*                 */
/*            */
/*         </div>*/
/* */
/*     </div>*/
/* */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/* */
/*     <script src="{{asset('gentelella/js/bootstrap.min.js')}}"></script>*/
/* */
/*     <!-- gauge js -->*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/gauge/gauge.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/gauge/gauge_demo.js')}}"></script>*/
/*     <!-- chart js -->*/
/*     <script src="{{asset('gentelella/js/chartjs/chart.min.js')}}"></script>*/
/*     <!-- bootstrap progress js -->*/
/*     <script src="{{asset('gentelella/js/progressbar/bootstrap-progressbar.min.js')}}"></script>*/
/*     <script src="{{asset('gentelella/js/nicescroll/jquery.nicescroll.min.js')}}"></script>*/
/*     <!-- icheck -->*/
/*     <script src="{{asset('gentelella/js/icheck/icheck.min.js')}}"></script>*/
/*     <!-- daterangepicker -->*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/moment.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/datepicker/daterangepicker.js')}}"></script>*/
/* */
/*     <script src="{{asset('gentelella/js/custom.js')}}"></script>*/
/* */
/*     <!-- flot js -->*/
/*     <!--[if lte IE 8]><script type="text/javascript" src="{{asset('gentelella/js/excanvas.min.js')}}"></script><![endif]-->*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.pie.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.orderBars.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.time.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/date.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.spline.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.stack.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/curvedLines.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.resize.js')}}"></script>*/
/*     <script>*/
/*         $(document).ready(function () {*/
/*             // [17, 74, 6, 39, 20, 85, 7]*/
/*             //[82, 23, 66, 9, 99, 6, 2]*/
/*             var data1 = [[gd(2012, 1, 1), 17], [gd(2012, 1, 2), 74], [gd(2012, 1, 3), 6], [gd(2012, 1, 4), 39], [gd(2012, 1, 5), 20], [gd(2012, 1, 6), 85], [gd(2012, 1, 7), 7]];*/
/* */
/*             var data2 = [[gd(2012, 1, 1), 82], [gd(2012, 1, 2), 23], [gd(2012, 1, 3), 66], [gd(2012, 1, 4), 9], [gd(2012, 1, 5), 119], [gd(2012, 1, 6), 6], [gd(2012, 1, 7), 9]];*/
/*             $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [*/
/*                 data1, data2*/
/*             ], {*/
/*                 series: {*/
/*                     lines: {*/
/*                         show: false,*/
/*                         fill: true*/
/*                     },*/
/*                     splines: {*/
/*                         show: true,*/
/*                         tension: 0.4,*/
/*                         lineWidth: 1,*/
/*                         fill: 0.4*/
/*                     },*/
/*                     points: {*/
/*                         radius: 0,*/
/*                         show: true*/
/*                     },*/
/*                     shadowSize: 2*/
/*                 },*/
/*                 grid: {*/
/*                     verticalLines: true,*/
/*                     hoverable: true,*/
/*                     clickable: true,*/
/*                     tickColor: "#d5d5d5",*/
/*                     borderWidth: 1,*/
/*                     color: '#fff'*/
/*                 },*/
/*                 colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],*/
/*                 xaxis: {*/
/*                     tickColor: "rgba(51, 51, 51, 0.06)",*/
/*                     mode: "time",*/
/*                     tickSize: [1, "day"],*/
/*                     //tickLength: 10,*/
/*                     axisLabel: "Date",*/
/*                     axisLabelUseCanvas: true,*/
/*                     axisLabelFontSizePixels: 12,*/
/*                     axisLabelFontFamily: 'Verdana, Arial',*/
/*                     axisLabelPadding: 10*/
/*                         //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]*/
/*                 },*/
/*                 yaxis: {*/
/*                     ticks: 8,*/
/*                     tickColor: "rgba(51, 51, 51, 0.06)",*/
/*                 },*/
/*                 tooltip: false*/
/*             });*/
/* */
/*             function gd(year, month, day) {*/
/*                 return new Date(year, month - 1, day).getTime();*/
/*             }*/
/*         });*/
/*     </script>*/
/* */
/*     <!-- worldmap -->*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/maps/jquery-jvectormap-2.0.1.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/maps/gdp-data.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/maps/jquery-jvectormap-world-mill-en.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/maps/jquery-jvectormap-us-aea-en.js')}}"></script>*/
/*     <script>*/
/*         $(function () {*/
/*             $('#world-map-gdp').vectorMap({*/
/*                 map: 'world_mill_en',*/
/*                 backgroundColor: 'transparent',*/
/*                 zoomOnScroll: false,*/
/*                 series: {*/
/*                     regions: [{*/
/*                         values: gdpData,*/
/*                         scale: ['#E6F2F0', '#149B7E'],*/
/*                         normalizeFunction: 'polynomial'*/
/*                     }]*/
/*                 },*/
/*                 onRegionTipShow: function (e, el, code) {*/
/*                     el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');*/
/*                 }*/
/*             });*/
/*         });*/
/*     </script>*/
/*     <!-- skycons -->*/
/*     <script src="js/skycons/skycons.js"></script>*/
/*     <script>*/
/*         var icons = new Skycons({*/
/*                 "color": "#73879C"*/
/*             }),*/
/*             list = [*/
/*                 "clear-day", "clear-night", "partly-cloudy-day",*/
/*                 "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",*/
/*                 "fog"*/
/*             ],*/
/*             i;*/
/* */
/*         for (i = list.length; i--;)*/
/*             icons.set(list[i], list[i]);*/
/* */
/*         icons.play();*/
/*     </script>*/
/* */
/*     <!-- dashbord linegraph -->*/
/*     <script>*/
/*         var doughnutData = [*/
/*             {*/
/*                 value: 30,*/
/*                 color: "#455C73"*/
/*             },*/
/*             {*/
/*                 value: 30,*/
/*                 color: "#9B59B6"*/
/*             },*/
/*             {*/
/*                 value: 60,*/
/*                 color: "#BDC3C7"*/
/*             },*/
/*             {*/
/*                 value: 100,*/
/*                 color: "#26B99A"*/
/*             },*/
/*             {*/
/*                 value: 120,*/
/*                 color: "#3498DB"*/
/*             }*/
/*     ];*/
/*         var myDoughnut = new Chart(document.getElementById("canvas1").getContext("2d")).Doughnut(doughnutData);*/
/*     </script>*/
/*     <!-- /dashbord linegraph -->*/
/*     <!-- datepicker -->*/
/*     <script type="text/javascript">*/
/*         $(document).ready(function () {*/
/* */
/*             var cb = function (start, end, label) {*/
/*                 console.log(start.toISOString(), end.toISOString(), label);*/
/*                 $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));*/
/*                 //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");*/
/*             }*/
/* */
/*             var optionSet1 = {*/
/*                 startDate: moment().subtract(29, 'days'),*/
/*                 endDate: moment(),*/
/*                 minDate: '01/01/2012',*/
/*                 maxDate: '12/31/2015',*/
/*                 dateLimit: {*/
/*                     days: 60*/
/*                 },*/
/*                 showDropdowns: true,*/
/*                 showWeekNumbers: true,*/
/*                 timePicker: false,*/
/*                 timePickerIncrement: 1,*/
/*                 timePicker12Hour: true,*/
/*                 ranges: {*/
/*                     'Today': [moment(), moment()],*/
/*                     'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],*/
/*                     'Last 7 Days': [moment().subtract(6, 'days'), moment()],*/
/*                     'Last 30 Days': [moment().subtract(29, 'days'), moment()],*/
/*                     'This Month': [moment().startOf('month'), moment().endOf('month')],*/
/*                     'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]*/
/*                 },*/
/*                 opens: 'left',*/
/*                 buttonClasses: ['btn btn-default'],*/
/*                 applyClass: 'btn-small btn-primary',*/
/*                 cancelClass: 'btn-small',*/
/*                 format: 'MM/DD/YYYY',*/
/*                 separator: ' to ',*/
/*                 locale: {*/
/*                     applyLabel: 'Submit',*/
/*                     cancelLabel: 'Clear',*/
/*                     fromLabel: 'From',*/
/*                     toLabel: 'To',*/
/*                     customRangeLabel: 'Custom',*/
/*                     daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],*/
/*                     monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],*/
/*                     firstDay: 1*/
/*                 }*/
/*             };*/
/*             $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));*/
/*             $('#reportrange').daterangepicker(optionSet1, cb);*/
/*             $('#reportrange').on('show.daterangepicker', function () {*/
/*                 console.log("show event fired");*/
/*             });*/
/*             $('#reportrange').on('hide.daterangepicker', function () {*/
/*                 console.log("hide event fired");*/
/*             });*/
/*             $('#reportrange').on('apply.daterangepicker', function (ev, picker) {*/
/*                 console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));*/
/*             });*/
/*             $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {*/
/*                 console.log("cancel event fired");*/
/*             });*/
/*             $('#options1').click(function () {*/
/*                 $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);*/
/*             });*/
/*             $('#options2').click(function () {*/
/*                 $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);*/
/*             });*/
/*             $('#destroy').click(function () {*/
/*                 $('#reportrange').data('daterangepicker').remove();*/
/*             });*/
/*         });*/
/*     </script>*/
/*     <script>*/
/*         NProgress.done();*/
/*     </script>*/
/*     <!-- /datepicker -->*/
/*     <!-- /footer content -->*/
/* </body>*/
/* */
/* </html>*/
/* */
/* */
