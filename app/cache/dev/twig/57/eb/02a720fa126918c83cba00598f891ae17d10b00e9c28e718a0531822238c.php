<?php

/* ResearchProjectMyProjectBundle:Clinic:edit.html.twig */
class __TwigTemplate_57eb02a720fa126918c83cba00598f891ae17d10b00e9c28e718a0531822238c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'clinic' => array($this, 'block_clinic'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_clinic($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<center><h1><b>Edit Clinic Information</b></h1>
  <div id=\"overlay\">
    <div id=\"progstat\"></div>
    <div id=\"progress\"></div>
  </div>

    <div class=\"x_content\">
        <br />
        <div data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 14
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "editClinicForm")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Clinic Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicName"), 'widget', array("attr" => array("name" => "clinicName", "placeholder" => "Clinic Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 20
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicName"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Clinic Description <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicDescription"), 'widget', array("attr" => array("name" => "clinicName", "placeholder" => "Clinic Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "10", "required" => "required", "type" => "text")));
        // line 29
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicDescription"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Clinic Address <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicAddress"), 'widget', array("attr" => array("name" => "clinicAddress", "placeholder" => "Clinic Address", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 38
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicAddress"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Clinic Contact <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicContact"), 'widget', array("attr" => array("name" => "clinicContact", "placeholder" => "Clinic Contact", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "7,11", "required" => "required", "type" => "number")));
        // line 47
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicContact"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-12 col-sm-12 col-xs-12\"><h4><b><center>Clinic Schedule</center></b></h4></label>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Sunday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "sunOpenTime"), 'widget');
        echo " 
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                    ";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "sunCloseTime"), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"sunday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Monday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "monOpenTime"), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "monCloseTime"), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"monday\" type=\"checkbox\" value=\"\"> Closed
                   </label>                             
                </div>
            </div>
               <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Tuesday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "tueOpenTime"), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "tueCloseTime"), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"tuesday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Wednesday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "wedOpenTime"), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 110
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "wedCloseTime"), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"wednesday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Thursday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "thuOpenTime"), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "thuCloseTime"), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"thursday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Friday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "friOpenTime"), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 142
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "friCloseTime"), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"friday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Saturday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 154
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "satOpenTime"), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 158
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "satCloseTime"), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"saturday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"form-group\">
            <div class=\"ln_solid\"></div>
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
        echo "\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "submit"), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
                </div>
             </div> 
            ";
        // line 173
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
                </div>
    </div>

    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
<!-- form validation -->
    <script src=";
        // line 185
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#editClinicForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#editClinicForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
    
    <script src=\"http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/jquery.geocomplete.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "\"></script>
    <script>
            var autocomplete = new google.maps.places.Autocomplete(\$(\"#researchproject_myprojectbundle_clinic_clinicAddress\")[0], {});

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                console.log(place.address_components);
            });

    </script>
    <script>
    
    document.getElementById('sunday').onclick = function() {
    // access properties using this keyword
     sundayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_hour');
     sundayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_minute');
     sundayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_hour');
     sundayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_minute');
     
     if ( this.checked ) {
        sundayOpenH.disabled = true;
        sundayOpenM.disabled = true;
        sundayOpenH.value = 00;
        sundayOpenM.value = 00;
        sundayCloseH.disabled = true;
        sundayCloseM.disabled = true;
        sundayCloseH.value = 00;
        sundayCloseM.value = 00;
        
         } else {
        sundayOpenH.disabled = false;
        sundayOpenM.disabled = false;
        sundayCloseH.disabled = false;
        sundayCloseM.disabled = false;
         }
         
        
};

    document.getElementById('monday').onclick = function() {
    // access properties using this keyword
     mondayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_hour');
     mondayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_minute');
     mondayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_hour');
     mondayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_minute');
     
     if ( this.checked ) {
         mondayOpenH.disabled = true;
        mondayOpenM.disabled = true;
        mondayOpenH.value = 00;
        mondayOpenM.value = 00;
        mondayCloseH.disabled = true;
        mondayCloseM.disabled = true;
        mondayCloseH.value = 00;
        mondayCloseM.value = 00;
        
         } else {
        mondayOpenH.disabled = false;
        mondayOpenM.disabled = false;
        mondayCloseH.disabled = false;
        mondayCloseM.disabled = false;
         }
         
        
};
      document.getElementById('tuesday').onclick = function() {
    // access properties using this keyword
    tuesdayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_hour');
     tuesdayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_minute');
     tuesdayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_hour');
     tuesdayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_minute');
     
     if ( this.checked ) {
        
        tuesdayOpenH.disabled = true;
        tuesdayOpenM.disabled = true;
        tuesdayOpenH.value = 00;
        tuesdayOpenM.value = 00;
        tuesdayCloseH.disabled = true;
        tuesdayCloseM.disabled = true;
        tuesdayCloseH.value = 00;
        tuesdayCloseM.value = 00;
        
         } else {
         
        tuesdayOpenH.disabled = false;
        tuesdayOpenM.disabled = false;
        tuesdayCloseH.disabled = false;
        tuesdayCloseM.disabled = false;
       
         }
         
        
};

 document.getElementById('wednesday').onclick = function() {
    // access properties using this keyword
     wedOpenH = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_hour');
     wedOpenM = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_minute');
     wedCloseH = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_hour');
     wedCloseM = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_minute');
     
     if ( this.checked ) {
         wedOpenH.disabled = true;
        wedOpenM.disabled = true;
        wedOpenH.value = 00;
        wedOpenM.value = 00;
        wedCloseH.disabled = true;
        wedCloseM.disabled = true;
        wedCloseH.value = 00;
        wedCloseM.value = 00;
        
       
        
         } else {
         
        wedOpenH.disabled = false;
        wedOpenM.disabled = false;
        wedCloseH.disabled = false;
        wedCloseM.disabled = false;
        
        
         }
         
        
};

 document.getElementById('thursday').onclick = function() {
    // access properties using this keyword
    thOpenH = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_hour');
     thOpenM = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_minute');
     thCloseH = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_hour');
     thCloseM = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_minute');
     
     if ( this.checked ) {
         thOpenH.disabled = true;
        thOpenM.disabled = true;
        thOpenH.value = 00;
        thOpenM.value = 00;
        thCloseH.disabled = true;
        thCloseM.disabled = true;
        thCloseH.value = 00;
        thCloseM.value = 00;
        
         } else {
        thOpenH.disabled = false;
        thOpenM.disabled = false;
        thCloseH.disabled = false;
        thCloseM.disabled = false;
       
         }
         
        
};

 document.getElementById('friday').onclick = function() {
    // access properties using this keyword
     
     friOpenH = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_hour');
     friOpenM = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_minute');
     friCloseH = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_hour');
     friCloseM = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_minute');
     
     if ( this.checked ) {
          friOpenH.disabled = true;
        friOpenM.disabled = true;
        friOpenH.value = 00;
        friOpenM.value = 00;
        friCloseH.disabled = true;
        friCloseM.disabled = true;
        friCloseH.value = 00;
        friCloseM.value = 00;
        
        
        
         } else {
         
        friOpenH.disabled = false;
        friOpenM.disabled = false;
        friCloseH.disabled = false;
        friCloseM.disabled = false;
       
         }
         
        
};

 document.getElementById('saturday').onclick = function() {
    // access properties using this keyword
     
     satOpenH = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_hour');
     satOpenM = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_minute');
     satCloseH = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_hour');
     satCloseM = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_minute');
     
     
     if ( this.checked ) {
       satOpenH.disabled = true;
        satOpenM.disabled = true;
        satOpenH.value = 00;
        satOpenM.value = 00;
        satCloseH.disabled = true;
        satCloseM.disabled = true;
        satCloseH.value = 00;
        satCloseM.value = 00;
        
         } else {
        
        satOpenH.disabled = false;
        satOpenM.disabled = false;
        satCloseH.disabled = false;
        satCloseM.disabled = false;
         }
         
        
};
     
     
    
</script>




";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Clinic:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 222,  341 => 221,  337 => 220,  299 => 185,  284 => 173,  275 => 169,  261 => 158,  254 => 154,  239 => 142,  232 => 138,  217 => 126,  210 => 122,  195 => 110,  188 => 106,  173 => 94,  166 => 90,  151 => 78,  144 => 74,  129 => 62,  122 => 58,  111 => 50,  106 => 47,  104 => 46,  96 => 41,  91 => 38,  89 => 37,  81 => 32,  76 => 29,  74 => 28,  66 => 23,  61 => 20,  59 => 19,  51 => 14,  40 => 5,  37 => 4,  31 => 3,  26 => 2,);
    }
}
