<?php

/* ResearchProjectMyProjectBundle:Default:doctorgroup.html.twig */
class __TwigTemplate_23d1752a55ba2b34fa9a56f8a32e6284460fbf800fe8b3b3ad68135e1e97194b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Default:doctorgroup.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3bc88bbc4e41b25e99bca95f45fa7b5d86e9be35f33a7288ca0329a22d8cf413 = $this->env->getExtension("native_profiler");
        $__internal_3bc88bbc4e41b25e99bca95f45fa7b5d86e9be35f33a7288ca0329a22d8cf413->enter($__internal_3bc88bbc4e41b25e99bca95f45fa7b5d86e9be35f33a7288ca0329a22d8cf413_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Default:doctorgroup.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3bc88bbc4e41b25e99bca95f45fa7b5d86e9be35f33a7288ca0329a22d8cf413->leave($__internal_3bc88bbc4e41b25e99bca95f45fa7b5d86e9be35f33a7288ca0329a22d8cf413_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_087a00a1b5afd7d922eae91ed885da4dfe8538a7db7f93d38d094056e72dadc0 = $this->env->getExtension("native_profiler");
        $__internal_087a00a1b5afd7d922eae91ed885da4dfe8538a7db7f93d38d094056e72dadc0->enter($__internal_087a00a1b5afd7d922eae91ed885da4dfe8538a7db7f93d38d094056e72dadc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_087a00a1b5afd7d922eae91ed885da4dfe8538a7db7f93d38d094056e72dadc0->leave($__internal_087a00a1b5afd7d922eae91ed885da4dfe8538a7db7f93d38d094056e72dadc0_prof);

    }

    // line 3
    public function block_doctorgroup($context, array $blocks = array())
    {
        $__internal_c244cd2019fec1418b87e2d2b798aec84cb3f26aafc9bf354cbe515d9dada7b3 = $this->env->getExtension("native_profiler");
        $__internal_c244cd2019fec1418b87e2d2b798aec84cb3f26aafc9bf354cbe515d9dada7b3->enter($__internal_c244cd2019fec1418b87e2d2b798aec84cb3f26aafc9bf354cbe515d9dada7b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctorgroup"));

        echo " class=\"active\" ";
        
        $__internal_c244cd2019fec1418b87e2d2b798aec84cb3f26aafc9bf354cbe515d9dada7b3->leave($__internal_c244cd2019fec1418b87e2d2b798aec84cb3f26aafc9bf354cbe515d9dada7b3_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_c6cc4e39d90b4a6cb03227e08d44617c1688dc85af802aa6fd3b2fc1b14c615c = $this->env->getExtension("native_profiler");
        $__internal_c6cc4e39d90b4a6cb03227e08d44617c1688dc85af802aa6fd3b2fc1b14c615c->enter($__internal_c6cc4e39d90b4a6cb03227e08d44617c1688dc85af802aa6fd3b2fc1b14c615c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    <div class=\"\">
            <div class=\"page-title\">
                <div class=\"title\">
                    <h3>&nbsp;DOCTOR GROUP</h3>
                </div>
            </div>
            <div class=\"clearfix\"></div>
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12\">
                    
            <a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("doctorgroup_new");
        echo "\">
                <button class=\"btn btn-success right\" style=\"margin-bottom: 5px;\"><i class=\"fa fa-plus\"></i> Create New Group</button>
            </a>
                        
            
            <div class=\"x_panel\">
            <br/>            
            <form action=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("doctorgrouphomepage");
        echo "\" method=\"POST\" role=\"search\">
                <div class=\"col-md-10 col-sm-10 col-xs-12 form-group top_search\" style='margin-left: 60px;'>
                    <div class=\"input-group\">
                        
                        <input action=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("doctorgrouphomepage");
        echo "\" method=\"POST\" role=\"search\" id=\"searchfield\" name=\"searchfield\" type=\"text\" class=\"form-control\" placeholder=\"Find a doctor group.\">
                            <span class=\"input-group-btn\">
                                <button  action=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("doctorgrouphomepage");
        echo "\" method=\"POST\" role=\"search\" class=\"btn btn-default\" type=\"submit\">Go!</button>
                            </span>
                    </div>
                </div>
            </form>
            <h3></h3>
                            
            ";
        // line 35
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 36
            echo "                <center><i><h3>Find a doctor group maybe?</h3></i></center>
                <br/>
                ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 39
                echo "                <div class=\"col-md-3 col-sm-3 col-xs-3\">
                <div class=\"item\" style=\"vertical-align: top; display: inline-block; text-align: center; width: 120px;\">
                    <span style=\"display: block;\">
                        <a href=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\">
                            <img src=\"";
                // line 43
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/homepage/Group.png"), "html", null, true);
                echo "\" alt=\"Doctor Group\">
                            <h3 style=\"color:#379DFF;\"><i class=\"fa fa-circle\"></i><b> &emsp;";
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "groupName", array()), "html", null, true);
                echo "</b></h3>
                        </a>
                        ";
                // line 46
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["entity"], "getAdminDoctors", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
                    // line 47
                    echo "                            <h4 style=\"font-family: Century Gothic; font-size: 12px;\">Created by: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "firstName", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "lastName", array()), "html", null, true);
                    echo "</h4>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo "                    </span>
                </div>
                </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "                <br/>
            ";
        } elseif (twig_test_empty(        // line 54
(isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")))) {
            // line 55
            echo "                <center><i><h3>No Doctor Group Found. Find another one.</h3></i></center>
                <br/>
            ";
        } else {
            // line 58
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")));
            foreach ($context['_seq'] as $context["_key"] => $context["doctorgroup"]) {
                // line 59
                echo "                <center>
                <div class=\"col-md-3 col-sm-3 col-xs-3\">
                <div class=\"item\" style=\"vertical-align: top; display: inline-block; text-align: center; width: 120px;\">
                    <span style=\"display: block;\">
                        <a href=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute($context["doctorgroup"], "id", array()))), "html", null, true);
                echo "\">
                            <img src=\"";
                // line 64
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/homepage/Group.png"), "html", null, true);
                echo "\" alt=\"Doctor Group\">
                            <h3 style=\"color:#379DFF;\"><i class=\"fa fa-circle\"></i><b> &emsp;";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctorgroup"], "groupName", array()), "html", null, true);
                echo "</b></h3>
                        </a>
                        ";
                // line 67
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["doctorgroup"], "getAdminDoctors", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
                    // line 68
                    echo "                            <h4 style=\"font-family: Century Gothic; font-size: 12px;\">Created by: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "firstName", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "lastName", array()), "html", null, true);
                    echo "</h4>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 70
                echo "                    </span>
                </div>
                </div>
                </center>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctorgroup'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "            ";
        }
        // line 76
        echo "            
            </div>
        </div>
    </div>
";
        
        $__internal_c6cc4e39d90b4a6cb03227e08d44617c1688dc85af802aa6fd3b2fc1b14c615c->leave($__internal_c6cc4e39d90b4a6cb03227e08d44617c1688dc85af802aa6fd3b2fc1b14c615c_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:doctorgroup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 76,  221 => 75,  211 => 70,  200 => 68,  196 => 67,  191 => 65,  187 => 64,  183 => 63,  177 => 59,  172 => 58,  167 => 55,  165 => 54,  162 => 53,  153 => 49,  142 => 47,  138 => 46,  133 => 44,  129 => 43,  125 => 42,  120 => 39,  116 => 38,  112 => 36,  110 => 35,  100 => 28,  95 => 26,  88 => 22,  78 => 15,  66 => 5,  60 => 4,  48 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block title %} {% endblock %}*/
/* {% block doctorgroup %} class="active" {% endblock %}*/
/* {% block body %}*/
/*     <div class="">*/
/*             <div class="page-title">*/
/*                 <div class="title">*/
/*                     <h3>&nbsp;DOCTOR GROUP</h3>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="clearfix"></div>*/
/*             <div class="row">*/
/*                 <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                     */
/*             <a href="{{path('doctorgroup_new')}}">*/
/*                 <button class="btn btn-success right" style="margin-bottom: 5px;"><i class="fa fa-plus"></i> Create New Group</button>*/
/*             </a>*/
/*                         */
/*             */
/*             <div class="x_panel">*/
/*             <br/>            */
/*             <form action="{{path('doctorgrouphomepage')}}" method="POST" role="search">*/
/*                 <div class="col-md-10 col-sm-10 col-xs-12 form-group top_search" style='margin-left: 60px;'>*/
/*                     <div class="input-group">*/
/*                         */
/*                         <input action="{{path('doctorgrouphomepage')}}" method="POST" role="search" id="searchfield" name="searchfield" type="text" class="form-control" placeholder="Find a doctor group.">*/
/*                             <span class="input-group-btn">*/
/*                                 <button  action="{{path('doctorgrouphomepage')}}" method="POST" role="search" class="btn btn-default" type="submit">Go!</button>*/
/*                             </span>*/
/*                     </div>*/
/*                 </div>*/
/*             </form>*/
/*             <h3></h3>*/
/*                             */
/*             {% if searchword == ""%}*/
/*                 <center><i><h3>Find a doctor group maybe?</h3></i></center>*/
/*                 <br/>*/
/*                 {% for entity in entities %}*/
/*                 <div class="col-md-3 col-sm-3 col-xs-3">*/
/*                 <div class="item" style="vertical-align: top; display: inline-block; text-align: center; width: 120px;">*/
/*                     <span style="display: block;">*/
/*                         <a href="{{path('doctorgroup_show', {'id': entity.id})}}">*/
/*                             <img src="{{asset('images/homepage/Group.png')}}" alt="Doctor Group">*/
/*                             <h3 style="color:#379DFF;"><i class="fa fa-circle"></i><b> &emsp;{{entity.groupName}}</b></h3>*/
/*                         </a>*/
/*                         {% for admin in entity.getAdminDoctors%}*/
/*                             <h4 style="font-family: Century Gothic; font-size: 12px;">Created by: {{admin.firstName}} {{admin.lastName}}</h4>*/
/*                         {% endfor %}*/
/*                     </span>*/
/*                 </div>*/
/*                 </div>*/
/*                 {% endfor %}*/
/*                 <br/>*/
/*             {%elseif doctorgroups is empty%}*/
/*                 <center><i><h3>No Doctor Group Found. Find another one.</h3></i></center>*/
/*                 <br/>*/
/*             {%else%}*/
/*             {% for doctorgroup in doctorgroups %}*/
/*                 <center>*/
/*                 <div class="col-md-3 col-sm-3 col-xs-3">*/
/*                 <div class="item" style="vertical-align: top; display: inline-block; text-align: center; width: 120px;">*/
/*                     <span style="display: block;">*/
/*                         <a href="{{path('doctorgroup_show', {'id': doctorgroup.id})}}">*/
/*                             <img src="{{asset('images/homepage/Group.png')}}" alt="Doctor Group">*/
/*                             <h3 style="color:#379DFF;"><i class="fa fa-circle"></i><b> &emsp;{{doctorgroup.groupName}}</b></h3>*/
/*                         </a>*/
/*                         {% for admin in doctorgroup.getAdminDoctors%}*/
/*                             <h4 style="font-family: Century Gothic; font-size: 12px;">Created by: {{admin.firstName}} {{admin.lastName}}</h4>*/
/*                         {% endfor %}*/
/*                     </span>*/
/*                 </div>*/
/*                 </div>*/
/*                 </center>*/
/*             {% endfor %}*/
/*             {% endif %}*/
/*             */
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
