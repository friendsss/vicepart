<?php

/* ::admin.html.twig */
class __TwigTemplate_ab41745877b26134bd6fdd8b854653194529869bb6130f6aa03f2c5c7936c52a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f404ea41c7389d2552ab1b4f6cd79a2f711d4b1617cc555f2bb043031a6f64f7 = $this->env->getExtension("native_profiler");
        $__internal_f404ea41c7389d2552ab1b4f6cd79a2f711d4b1617cc555f2bb043031a6f64f7->enter($__internal_f404ea41c7389d2552ab1b4f6cd79a2f711d4b1617cc555f2bb043031a6f64f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::admin.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
<head>
\t<meta charset=\"utf-8\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\" />
\t<title>Find Me A Doctor Iloilo</title>
\t
\t
\t<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>
\t
\t<!--[if lt IE 9]>
\t\t<script src=\"js/modernizr.custom.js\"></script>
\t<![endif]-->
\t
        <!-- Bootstrap core CSS -->
        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("retina/default/js/jquery-1.8.0.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("retina/default/js/functions.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
 <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/jquery-ui.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
     <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/jquery-ui.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
     <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/jquery-ui.structure.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
     <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/fonts/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom styling plus plugins -->
    <link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/maps/jquery-jvectormap-2.0.1.css"), "html", null, true);
        echo "\" />
    <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/icheck/flat/green.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/floatexamples.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

    <script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/nprogress.js"), "html", null, true);
        echo "\"></script>
</head>

<body class=\"nav-md\">

    <div class=\"container body\">


        <div class=\"main_container\">

            <div class=\"col-md-3 left_col\">
                <div class=\"left_col scroll-view\">

                    <div class=\"navbar nav_title\" style=\"border: 0;\">
                        <a href=\"index.html\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>FIMADILO ADMIN</span></a>
                    </div>
                    <div class=\"clearfix\"></div>

                   
                    <br />

                    <!-- sidebar menu -->
                    <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">

                        <div class=\"menu_section\">
                            <h3>General</h3>
                            <ul class=\"nav side-menu\">
                                <li><a href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("admin");
        echo "\"><i class=\"fa fa-home\"></i> Home </a>
                                    
                                </li>
                                <li><a><i class=\"fa fa-user-md\"></i> Doctor <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"";
        // line 66
        echo $this->env->getExtension('routing')->getPath("admin_doctor_new");
        echo "\">Register Doctor</a>
                                        <li><a href=\"";
        // line 67
        echo $this->env->getExtension('routing')->getPath("admin_edit_doctor_search");
        echo "\">Edit Doctor</a>
                                         <li><a href=\"";
        // line 68
        echo $this->env->getExtension('routing')->getPath("admin_delete_doctor");
        echo "\">Remove Doctor</a>    
                                        </li>
                                    </ul>
                                </li>
                                 <li><a><i class=\"fa fa-hospital-o\"></i> Clinic <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                         <li><a href=\"";
        // line 74
        echo $this->env->getExtension('routing')->getPath("admin_edit_clinic_search");
        echo "\">Edit Clinic</a>
                                         <li><a href=\"";
        // line 75
        echo $this->env->getExtension('routing')->getPath("admin_delete_clinic");
        echo "\">Remove Clinic</a>  
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-stethoscope\"></i> Specialty <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                         <li><a href=\"";
        // line 80
        echo $this->env->getExtension('routing')->getPath("admin_specialty_create");
        echo "\">Add Specialty</a>
                                         <li><a href=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("admin_delete_specialty");
        echo "\">Remove Specialty</a>  
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-eyedropper\"></i> Illness <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                       <li><a href=\"";
        // line 86
        echo $this->env->getExtension('routing')->getPath("admin_edit_illness_search");
        echo "\">Add Illness</a>
                                       <li><a href=\"";
        // line 87
        echo $this->env->getExtension('routing')->getPath("admin_edit_illness_search");
        echo "\">Edit Illness</a>
                                       <li><a href=\"";
        // line 88
        echo $this->env->getExtension('routing')->getPath("admin_delete_illness");
        echo "\">Delete Illness</a>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-users\"></i> Fellowship <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"";
        // line 93
        echo $this->env->getExtension('routing')->getPath("admin_fellowship_create");
        echo "\">Add Fellowship</a>
                                       <li><a href=\"";
        // line 94
        echo $this->env->getExtension('routing')->getPath("admin_delete_fellowship");
        echo "\">Delete Fellowship</a>
                                    </ul>
                                </li>
                                 <li><a href=\"";
        // line 97
        echo $this->env->getExtension('routing')->getPath("admin_logout");
        echo "\"> Logout </a>
                            </ul>
                        </div>
                        

                    </div>
                    <!-- /sidebar menu -->

                   
                </div>
            </div>

            <!-- top navigation -->
            <div class=\"top_nav\">

                <div class=\"nav_menu\">
                    <nav class=\"\" role=\"navigation\">
                        <div class=\"nav toggle\">
                            <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
                        </div>

                        <ul class=\"nav navbar-nav navbar-right\">
                            <li class=\"\">
                                <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                    ";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo "
                                    <span class=\" fa fa-angle-down\"></span>
                                </a>
                                <ul class=\"dropdown-menu dropdown-usermenu animated fadeInDown pull-right\">
                                    
                                    <li><a href=\"";
        // line 126
        echo $this->env->getExtension('routing')->getPath("admin_logout");
        echo "\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>


                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
            <div class=\"right_col\" role=\"main\">
               ";
        // line 142
        $this->displayBlock('body', $context, $blocks);
        // line 144
        echo "                
                                  
                           
             </div>








                <!-- footer content -->

                <footer>
                    
                    <div class=\"clearfix\"></div>
                </footer>
                <!-- /footer content -->
            </div>
            <!-- /page content -->

        </div>

    </div>
<!-- end of wrapper -->
<script src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("notify/notify.js"), "html", null, true);
        echo "\"></script>
    <!-- gauge js -->
    <script type=\"text/javascript\" src=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/gauge/gauge.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/gauge/gauge_demo.js"), "html", null, true);
        echo "\"></script>
    <!-- chart js -->
    <script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/chartjs/chart.min.js"), "html", null, true);
        echo "\"></script>
    <!-- bootstrap progress js -->
    <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/progressbar/bootstrap-progressbar.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/nicescroll/jquery.nicescroll.min.js"), "html", null, true);
        echo "\"></script>
    <!-- icheck -->
    <script src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/icheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
    <!-- daterangepicker -->
    <script type=\"text/javascript\" src=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/moment.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/datepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/custom.js"), "html", null, true);
        echo "\"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type=\"text/javascript\" src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/excanvas.min.js"), "html", null, true);
        echo "\"></script><![endif]-->
    <script type=\"text/javascript\" src=\"";
        // line 190
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/flot/jquery.flot.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/flot/jquery.flot.pie.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/flot/jquery.flot.orderBars.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/flot/jquery.flot.time.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/flot/date.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/flot/jquery.flot.spline.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/flot/jquery.flot.stack.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/flot/curvedLines.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/flot/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/notify/pnotify.core.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/notify/pnotify.buttons.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/notify/pnotify.nonblock.js"), "html", null, true);
        echo "\"></script>
</body>
</html>";
        
        $__internal_f404ea41c7389d2552ab1b4f6cd79a2f711d4b1617cc555f2bb043031a6f64f7->leave($__internal_f404ea41c7389d2552ab1b4f6cd79a2f711d4b1617cc555f2bb043031a6f64f7_prof);

    }

    // line 142
    public function block_body($context, array $blocks = array())
    {
        $__internal_b180172aa551c76e8e00cd20c18a673dce3b08ba1f864ee881e0ff30e92422d8 = $this->env->getExtension("native_profiler");
        $__internal_b180172aa551c76e8e00cd20c18a673dce3b08ba1f864ee881e0ff30e92422d8->enter($__internal_b180172aa551c76e8e00cd20c18a673dce3b08ba1f864ee881e0ff30e92422d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 143
        echo "                ";
        
        $__internal_b180172aa551c76e8e00cd20c18a673dce3b08ba1f864ee881e0ff30e92422d8->leave($__internal_b180172aa551c76e8e00cd20c18a673dce3b08ba1f864ee881e0ff30e92422d8_prof);

    }

    public function getTemplateName()
    {
        return "::admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  402 => 143,  396 => 142,  386 => 201,  382 => 200,  378 => 199,  374 => 198,  370 => 197,  366 => 196,  362 => 195,  358 => 194,  354 => 193,  350 => 192,  346 => 191,  342 => 190,  338 => 189,  332 => 186,  327 => 184,  323 => 183,  318 => 181,  313 => 179,  309 => 178,  304 => 176,  299 => 174,  295 => 173,  290 => 171,  286 => 170,  258 => 144,  256 => 142,  237 => 126,  229 => 121,  202 => 97,  196 => 94,  192 => 93,  184 => 88,  180 => 87,  176 => 86,  168 => 81,  164 => 80,  156 => 75,  152 => 74,  143 => 68,  139 => 67,  135 => 66,  127 => 61,  97 => 34,  93 => 33,  88 => 31,  84 => 30,  80 => 29,  76 => 28,  70 => 25,  66 => 24,  61 => 22,  57 => 21,  53 => 20,  49 => 19,  45 => 18,  41 => 17,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/*     */
/* <head>*/
/* 	<meta charset="utf-8" />*/
/* 	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />*/
/* 	<title>Find Me A Doctor Iloilo</title>*/
/* 	*/
/* 	*/
/* 	<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>*/
/* 	*/
/* 	<!--[if lt IE 9]>*/
/* 		<script src="js/modernizr.custom.js"></script>*/
/* 	<![endif]-->*/
/* 	*/
/*         <!-- Bootstrap core CSS -->*/
/*         <script src="{{asset('retina/default/js/jquery-1.8.0.min.js')}}" type="text/javascript"></script>*/
/*         <script src="{{asset('retina/default/js/functions.js')}}" type="text/javascript"></script>*/
/*  <link href="{{asset('gentelella/jquery-ui/jquery-ui.min.css')}}" rel="stylesheet">*/
/*      <link href="{{asset('gentelella/jquery-ui/jquery-ui.theme.css')}}" rel="stylesheet">*/
/*      <link href="{{asset('gentelella/jquery-ui/jquery-ui.structure.css')}}" rel="stylesheet">*/
/*      <link href="{{asset('gentelellaoriginal/css/bootstrap.min.css')}}" rel="stylesheet">*/
/* */
/*     <link href="{{asset('gentelellaoriginal/fonts/css/font-awesome.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('gentelellaoriginal/css/animate.min.css')}}" rel="stylesheet">*/
/* */
/*     <!-- Custom styling plus plugins -->*/
/*     <link href="{{asset('gentelellaoriginal/css/custom.css')}}" rel="stylesheet">*/
/*     <link rel="stylesheet" type="text/css" href="{{asset('gentelellaoriginal/css/maps/jquery-jvectormap-2.0.1.css')}}" />*/
/*     <link href="{{asset('gentelellaoriginal/css/icheck/flat/green.css')}}" rel="stylesheet" />*/
/*     <link href="{{asset('gentelellaoriginal/css/floatexamples.css')}}" rel="stylesheet" type="text/css" />*/
/* */
/*     <script src="{{asset('gentelella/js/jquery.min.js')}}"></script>*/
/*     <script src="{{asset('gentelellaoriginal/js/nprogress.js')}}"></script>*/
/* </head>*/
/* */
/* <body class="nav-md">*/
/* */
/*     <div class="container body">*/
/* */
/* */
/*         <div class="main_container">*/
/* */
/*             <div class="col-md-3 left_col">*/
/*                 <div class="left_col scroll-view">*/
/* */
/*                     <div class="navbar nav_title" style="border: 0;">*/
/*                         <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>FIMADILO ADMIN</span></a>*/
/*                     </div>*/
/*                     <div class="clearfix"></div>*/
/* */
/*                    */
/*                     <br />*/
/* */
/*                     <!-- sidebar menu -->*/
/*                     <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">*/
/* */
/*                         <div class="menu_section">*/
/*                             <h3>General</h3>*/
/*                             <ul class="nav side-menu">*/
/*                                 <li><a href="{{path('admin')}}"><i class="fa fa-home"></i> Home </a>*/
/*                                     */
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-user-md"></i> Doctor <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                         <li><a href="{{path('admin_doctor_new')}}">Register Doctor</a>*/
/*                                         <li><a href="{{path('admin_edit_doctor_search')}}">Edit Doctor</a>*/
/*                                          <li><a href="{{path('admin_delete_doctor')}}">Remove Doctor</a>    */
/*                                         </li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                                  <li><a><i class="fa fa-hospital-o"></i> Clinic <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                          <li><a href="{{path('admin_edit_clinic_search')}}">Edit Clinic</a>*/
/*                                          <li><a href="{{path('admin_delete_clinic')}}">Remove Clinic</a>  */
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-stethoscope"></i> Specialty <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                          <li><a href="{{path('admin_specialty_create')}}">Add Specialty</a>*/
/*                                          <li><a href="{{path('admin_delete_specialty')}}">Remove Specialty</a>  */
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-eyedropper"></i> Illness <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                        <li><a href="{{path('admin_edit_illness_search')}}">Add Illness</a>*/
/*                                        <li><a href="{{path('admin_edit_illness_search')}}">Edit Illness</a>*/
/*                                        <li><a href="{{path('admin_delete_illness')}}">Delete Illness</a>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li><a><i class="fa fa-users"></i> Fellowship <span class="fa fa-chevron-down"></span></a>*/
/*                                     <ul class="nav child_menu" style="display: none">*/
/*                                         <li><a href="{{path('admin_fellowship_create')}}">Add Fellowship</a>*/
/*                                        <li><a href="{{path('admin_delete_fellowship')}}">Delete Fellowship</a>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                                  <li><a href="{{path('admin_logout')}}"> Logout </a>*/
/*                             </ul>*/
/*                         </div>*/
/*                         */
/* */
/*                     </div>*/
/*                     <!-- /sidebar menu -->*/
/* */
/*                    */
/*                 </div>*/
/*             </div>*/
/* */
/*             <!-- top navigation -->*/
/*             <div class="top_nav">*/
/* */
/*                 <div class="nav_menu">*/
/*                     <nav class="" role="navigation">*/
/*                         <div class="nav toggle">*/
/*                             <a id="menu_toggle"><i class="fa fa-bars"></i></a>*/
/*                         </div>*/
/* */
/*                         <ul class="nav navbar-nav navbar-right">*/
/*                             <li class="">*/
/*                                 <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">*/
/*                                     {{app.user.username}}*/
/*                                     <span class=" fa fa-angle-down"></span>*/
/*                                 </a>*/
/*                                 <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">*/
/*                                     */
/*                                     <li><a href="{{path('admin_logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>*/
/*                                     </li>*/
/*                                 </ul>*/
/*                             </li>*/
/* */
/* */
/*                         </ul>*/
/*                     </nav>*/
/*                 </div>*/
/* */
/*             </div>*/
/*             <!-- /top navigation -->*/
/* */
/* */
/*             <!-- page content -->*/
/*             <div class="right_col" role="main">*/
/*                {%block body%}*/
/*                 {%endblock%}*/
/*                 */
/*                                   */
/*                            */
/*              </div>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*                 <!-- footer content -->*/
/* */
/*                 <footer>*/
/*                     */
/*                     <div class="clearfix"></div>*/
/*                 </footer>*/
/*                 <!-- /footer content -->*/
/*             </div>*/
/*             <!-- /page content -->*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* <!-- end of wrapper -->*/
/* <script src="{{asset('gentelellaoriginal/js/bootstrap.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('notify/notify.js')}}"></script>*/
/*     <!-- gauge js -->*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/gauge/gauge.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/gauge/gauge_demo.js')}}"></script>*/
/*     <!-- chart js -->*/
/*     <script src="{{asset('gentelellaoriginal/js/chartjs/chart.min.js')}}"></script>*/
/*     <!-- bootstrap progress js -->*/
/*     <script src="{{asset('gentelellaoriginal/js/progressbar/bootstrap-progressbar.min.js')}}"></script>*/
/*     <script src="{{asset('gentelellaoriginal/js/nicescroll/jquery.nicescroll.min.js')}}"></script>*/
/*     <!-- icheck -->*/
/*     <script src="{{asset('gentelellaoriginal/js/icheck/icheck.min.js')}}"></script>*/
/*     <!-- daterangepicker -->*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/moment.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/datepicker/daterangepicker.js')}}"></script>*/
/* */
/*     <script src="{{asset('gentelellaoriginal/js/custom.js')}}"></script>*/
/* */
/*     <!-- flot js -->*/
/*     <!--[if lte IE 8]><script type="text/javascript" src="{{asset('gentelellaoriginal/js/excanvas.min.js')}}"></script><![endif]-->*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/flot/jquery.flot.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/flot/jquery.flot.pie.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/flot/jquery.flot.orderBars.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/flot/jquery.flot.time.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/flot/date.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/flot/jquery.flot.spline.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/flot/jquery.flot.stack.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/flot/curvedLines.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/flot/jquery.flot.resize.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/notify/pnotify.core.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/notify/pnotify.buttons.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelellaoriginal/js/notify/pnotify.nonblock.js')}}"></script>*/
/* </body>*/
/* </html>*/
