<?php

/* ResearchProjectMyProjectBundle:Illness:show.html.twig */
class __TwigTemplate_c1c03b8ce144b4131fcf51af129e9052339718f9234de0178814c61a6f9f916a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"\">
            <div class=\"page-title\">
                <div class=\"title\">
                    <h3>&nbsp;ILLNESS</h3>
                </div>
            </div>
                    <div class=\"clearfix\"></div>

                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\">
                                <div class=\"x_title\">
                                    <h3>Illness Information</h3>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div class=\"x_content\">
                                    <center>
                                        <h3 style=\"font-size: 50px;\"><b>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessName"), "html", null, true);
        echo "<b></h3>       
                                        <h3 style=\"font-size: 20px; font-family: Century Gothic;\"><i class =\"fa fa-quote-left\"></i>&emsp; <b><i>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessCommonName"), "html", null, true);
        echo "</i></b> &emsp;<i class =\"fa fa-quote-right\"></i></h3>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class =\"fa fa-file-text\"></i>&emsp;<b>Illness Definition:</b> ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessDefinition"), "html", null, true);
        echo "</h4>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class =\"fa fa-clipboard\"></i>&emsp;<b>Illness Description:</b> ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessDescription"), "html", null, true);
        echo "</h4>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class =\"fa fa-exclamation-triangle\"></i>&emsp;<b>Symptoms:</b> ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "symptoms"), "html", null, true);
        echo "</h4>
                                        
                                        <h3></h3>
                                        <h3><i class=\"fa fa-medkit\"></i><b>&emsp;MEDICATIONS</b></h3>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"> <i class =\"fa fa-chevron-circle-right\"></i>&emsp; <b>Home Remedies:</b> ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "homeRemedies"), "html", null, true);
        echo "</h4>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"> <i class =\"fa fa-chevron-circle-right\"></i>&emsp; <b>Commonly Prescribed Drugs:</b> ";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "commonlyPrescribedDrugs"), "html", null, true);
        echo "</h4>
                                        <br/>
                                    ";
        // line 32
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 33
            echo "                                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("illness_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"><button class=\"btn btn-primary\">Edit Information</button></a>
                                    ";
        }
        // line 35
        echo "                                    </center>
                                </div>
                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                        
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Illness:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 35,  84 => 33,  82 => 32,  77 => 30,  73 => 29,  66 => 25,  62 => 24,  58 => 23,  54 => 22,  50 => 21,  31 => 4,  28 => 3,);
    }
}
