<?php

/* ::basenosidebar.html.twig */
class __TwigTemplate_f9c1b3cff0fe5e15ff9ae8506e5cc3f9b150876461859082ceacbf57ba10d1b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'home' => array($this, 'block_home'),
            'profile' => array($this, 'block_profile'),
            'doctor' => array($this, 'block_doctor'),
            'clinic' => array($this, 'block_clinic'),
            'illness' => array($this, 'block_illness'),
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
<head>
\t<meta charset=\"utf-8\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\" />
\t<title>Find Me A Doctor Iloilo</title>
\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("retina/default/css/images/favicon.ico"), "html", null, true);
        echo "\" />
\t
\t<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>
\t<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("retina/default/js/jquery-1.8.0.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<!--[if lt IE 9]>
\t\t<script src=\"js/modernizr.custom.js\"></script>
\t<![endif]-->
\t<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("retina/default/js/jquery.carouFredSel-5.5.0-packed.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("retina/default/js/functions.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <!-- Bootstrap core CSS -->
 <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/jquery-ui/jquery-ui.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
     <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/jquery-ui/jquery-ui.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
     <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/jquery-ui/jquery-ui.structure.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/formerrornotify.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/fonts/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom styling plus plugins -->
    <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/maps/jquery-jvectormap-2.0.1.css"), "html", null, true);
        echo "\" />
    <link href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/icheck/flat/green.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/floatexamples.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

    <script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/nprogress.js"), "html", null, true);
        echo "\"></script>
    <link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("retina/default/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\" />
    <link href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
 <link href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
</head>
<body>
<!-- wrapper -->
<div id=\"wrapper\">
\t<!-- shell -->
\t<div class =\"shell\">
\t\t<!-- container -->
\t\t<div class=\"container\">
\t\t\t<!-- header -->
\t\t\t<header id=\"header\">
\t\t\t\t<a href=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("retina/default/logo.png"), "html", null, true);
        echo "\"></a>
                                   ";
        // line 48
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            echo " 
                                    <span class=\"right\"><a href=\"";
            // line 49
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\"><button class=\"btn btn-default\"> Log Out</button></a></span>
                                    ";
        } else {
            // line 51
            echo "                                    <span class = \"right\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("login");
            echo "\"><button class=\"btn btn-default\">Login</button></a></span>
                                    ";
        }
        // line 53
        echo "                                <!-- search -->
\t\t\t\t<div class=\"search\">
                                    
\t\t\t\t\t<form action=\"";
        // line 56
        echo $this->env->getExtension('routing')->getPath("search");
        echo "\" method=\"POST\" role=\"search\">
\t\t\t\t\t\t<div class=\"input-group\">
                                                  <input type=\"text\" id=\"searchfield\" name=\"searchfield\" class=\"field\" placeholder=\"Search something.\" value=\"\" />
\t\t\t\t\t\t<input type=\"submit\" class=\"search-btn\" value=\"\" />
\t\t\t\t\t\t<div class=\"cl\">&nbsp;</div>
                                                  </div>
                                        </form>
                                  
\t\t\t\t</div>
\t\t\t\t<!-- end of search --> 
\t\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t</header>
\t\t\t<!-- end of header -->
\t\t\t<!-- navigaation -->
\t\t\t<nav id=\"navigation\">
\t\t\t\t<a href=\"#\" class=\"nav-btn\">HOME<span></span></a>
\t\t\t\t<ul>
\t\t\t\t\t<li ";
        // line 73
        $this->displayBlock('home', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\">Home</a></li>
                                        ";
        // line 74
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 75
            echo "                                            <li ";
            $this->displayBlock('profile', $context, $blocks);
            echo "><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "id"))), "html", null, true);
            echo "\">Profile</a></li>
                                        ";
        }
        // line 77
        echo "\t\t\t\t\t<li ";
        $this->displayBlock('doctor', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("doctorhomepage");
        echo "\">Doctors</a></li>
\t\t\t\t\t<li ";
        // line 78
        $this->displayBlock('clinic', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("clinichomepage");
        echo "\">Clinics</a></li>
\t\t\t\t\t<li ";
        // line 79
        $this->displayBlock('illness', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("illnesshomepage");
        echo "\">Illnesses</a></li>
                                        ";
        // line 80
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 81
            echo "                                        <li ";
            $this->displayBlock('doctorgroup', $context, $blocks);
            echo "><a href=\"";
            echo $this->env->getExtension('routing')->getPath("doctorgrouphomepage");
            echo "\">Doctor Group</a></li>
\t\t\t\t\t";
        }
        // line 83
        echo "\t\t\t\t</ul>
\t\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t</nav>
\t\t\t<!-- end of navigation -->
\t\t\t
\t\t\t<!-- main -->
\t\t\t<div class=\"main\">

\t\t\t\t<section>
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t
                                                ";
        // line 94
        $this->displayBlock('body', $context, $blocks);
        // line 95
        echo "\t\t\t\t\t</div>

\t\t\t\t</section>

\t\t\t\t
\t\t\t</div>
\t\t\t<!-- end of main -->
\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t
\t\t\t<!-- footer -->
\t\t\t<div id=\"footer\">
\t\t\t\t<div class=\"footer-nav\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a href=\"#\">Home</a></li>
\t\t\t\t\t\t<li><a href=\"#\">About</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Services</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Projects</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Solutions</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Jobs</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Blog</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Contacts</a></li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t\t</div>
\t\t\t\t<p class=\"copy\">&copy; Copyright 2012<span>|</span>Sitename. Design by <a href=\"http://chocotemplates.com\" target=\"_blank\">ChocoTemplates.com</a></p>
\t\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t</div>
\t\t\t<!-- end of footer -->
\t\t</div>
\t\t<!-- end of container -->
\t</div>
\t<!-- end of shell -->
</div>
<!-- end of wrapper -->
<script src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("notify/notify.js"), "html", null, true);
        echo "\"></script>
    <!-- gauge js -->
    <script type=\"text/javascript\" src=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/gauge/gauge.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/gauge/gauge_demo.js"), "html", null, true);
        echo "\"></script>
    <!-- chart js -->
    <script src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/chartjs/chart.min.js"), "html", null, true);
        echo "\"></script>
    <!-- bootstrap progress js -->
    <script src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/progressbar/bootstrap-progressbar.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/nicescroll/jquery.nicescroll.min.js"), "html", null, true);
        echo "\"></script>
    <!-- icheck -->
    <script src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
    <!-- daterangepicker -->
    <script type=\"text/javascript\" src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/moment.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/datepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "\"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type=\"text/javascript\" src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/excanvas.min.js"), "html", null, true);
        echo "\"></script><![endif]-->
    <script type=\"text/javascript\" src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.pie.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.orderBars.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.time.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/date.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.spline.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.stack.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/curvedLines.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/notify/pnotify.core.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/notify/pnotify.buttons.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/notify/pnotify.nonblock.js"), "html", null, true);
        echo "\"></script>
</body>
</html>";
    }

    // line 73
    public function block_home($context, array $blocks = array())
    {
        echo " ";
    }

    // line 75
    public function block_profile($context, array $blocks = array())
    {
        echo " ";
    }

    // line 77
    public function block_doctor($context, array $blocks = array())
    {
        echo " ";
    }

    // line 78
    public function block_clinic($context, array $blocks = array())
    {
        echo " ";
    }

    // line 79
    public function block_illness($context, array $blocks = array())
    {
        echo " ";
    }

    // line 81
    public function block_doctorgroup($context, array $blocks = array())
    {
        echo " ";
    }

    // line 94
    public function block_body($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::basenosidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  419 => 94,  413 => 81,  407 => 79,  401 => 78,  395 => 77,  389 => 75,  383 => 73,  376 => 160,  372 => 159,  368 => 158,  364 => 157,  360 => 156,  356 => 155,  352 => 154,  348 => 153,  344 => 152,  340 => 151,  336 => 150,  332 => 149,  328 => 148,  322 => 145,  317 => 143,  313 => 142,  308 => 140,  303 => 138,  299 => 137,  294 => 135,  289 => 133,  285 => 132,  280 => 130,  276 => 129,  240 => 95,  238 => 94,  225 => 83,  217 => 81,  215 => 80,  209 => 79,  203 => 78,  196 => 77,  188 => 75,  186 => 74,  180 => 73,  160 => 56,  155 => 53,  149 => 51,  144 => 49,  140 => 48,  134 => 47,  120 => 36,  116 => 35,  112 => 34,  108 => 33,  104 => 32,  99 => 30,  95 => 29,  91 => 28,  87 => 27,  81 => 24,  77 => 23,  73 => 22,  69 => 21,  65 => 20,  61 => 19,  57 => 18,  52 => 16,  48 => 15,  41 => 11,  35 => 8,  26 => 1,);
    }
}
