<?php

/* ResearchProjectMyProjectBundle:Admin:deleteIllness.html.twig */
class __TwigTemplate_17950338306994381b4f232479198200e2965dc62b21569737313e32145f71c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:deleteIllness.html.twig", 1);
        $this->blocks = array(
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f980e4839052adfd6bec60326c8ee15004f2b6bc815c486adad173148752222b = $this->env->getExtension("native_profiler");
        $__internal_f980e4839052adfd6bec60326c8ee15004f2b6bc815c486adad173148752222b->enter($__internal_f980e4839052adfd6bec60326c8ee15004f2b6bc815c486adad173148752222b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:deleteIllness.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f980e4839052adfd6bec60326c8ee15004f2b6bc815c486adad173148752222b->leave($__internal_f980e4839052adfd6bec60326c8ee15004f2b6bc815c486adad173148752222b_prof);

    }

    // line 2
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_1c108b64da359a7436a82d3d1defc78042c12492a99443b4fb012800439333f9 = $this->env->getExtension("native_profiler");
        $__internal_1c108b64da359a7436a82d3d1defc78042c12492a99443b4fb012800439333f9->enter($__internal_1c108b64da359a7436a82d3d1defc78042c12492a99443b4fb012800439333f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "Delete Illness";
        
        $__internal_1c108b64da359a7436a82d3d1defc78042c12492a99443b4fb012800439333f9->leave($__internal_1c108b64da359a7436a82d3d1defc78042c12492a99443b4fb012800439333f9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9ed2f6ec021b1c1928ec930ccde039c8c15aa04d939300e81c256fb9183e4b7e = $this->env->getExtension("native_profiler");
        $__internal_9ed2f6ec021b1c1928ec930ccde039c8c15aa04d939300e81c256fb9183e4b7e->enter($__internal_9ed2f6ec021b1c1928ec930ccde039c8c15aa04d939300e81c256fb9183e4b7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    
                        <h3>Delete Illness</h3>
                       
                    <div class=\"x_content\">
                        <input class=\"form-control\" id='search' type=\"text\" name=\"query\" placeholder=\"Type a name\" />
                         
                                                    <center><img hidden id=\"loading\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("charisma/img/ajax-loaders/ajax-loader-1.gif"), "html", null, true);
        echo "\" ></center>
                                                    <table class=\"table table-striped\" id=\"resulttable\" class=\"hidden\">

                                                        <tbody id=\"resultbody\">

                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- form validation -->
<script>
                                    var deleteBtnId;
                                    \$(document).on(\"click\", \".btn-remove\", function(x){
                                         x.preventDefault();
                                        deleteBtnId = this.id;
                                        deleteBtnId = deleteBtnId.replace('remove-', '');
                                         \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 44
        echo $this->env->getExtension('routing')->getPath("admin_delete_illness_click");
        echo "',
                                            data: {id: deleteBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                               location.reload();
                                                console.log(response);
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    });
    
   </script>
    <script>
\$('#search').keyup(function() {
    
     searchText = \$(this).val();
     if (this.value != ''){
         \$(\"#resultbody\").empty();
     }
     
     
     if (this.value.length >= 3 && this.value != '')
  {
     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 72
        echo $this->env->getExtension('routing')->getPath("admin_delete_illness");
        echo "',
        data: {searchText : searchText},
        beforeSend: function(){
        \$('#loading').show();
        },
        complete: function(){
        \$('#loading').hide();
        },
        success : function(data) 
           
           {if(data){
                  
                \$(\"#resultbody\").empty();
                var len = data.length;
                var txt = \"\";
                  if(len > 0){
                    for(var i=0;i<len;i++){
                            //var button= \$('<button type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\"></i> Invite to Group </button>');
                            var entityId = data[i].id;
                            var showPath = '";
        // line 91
        echo $this->env->getExtension('routing')->getPath("doctor_show", array("id" => "entityId"));
        echo "';
                            showPath = showPath.replace(\"entityId\", data[i].id);
                            txt += \"<tr><td>\"+data[i].illnessName+\"</td>\";
                            txt+= '<td><a href=\"#\" ><button id=\"remove-'+ data[i].id+'\" type=\"button\" class=\"btn btn-danger btn-xs btn-remove\"> <i class=\"fa fa-trash\"></i> Remove Illness </button>';
                        
                    }
                    if(txt != \"\"){
                        \$(\"#resultbody\").append(txt).removeClass(\"hidden\");
                         
                    }
                }
            }
        },error: function() {
         alert('error');
          }
        });
    }
});
       
    </script>
    ";
        
        $__internal_9ed2f6ec021b1c1928ec930ccde039c8c15aa04d939300e81c256fb9183e4b7e->leave($__internal_9ed2f6ec021b1c1928ec930ccde039c8c15aa04d939300e81c256fb9183e4b7e_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:deleteIllness.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 91,  129 => 72,  98 => 44,  70 => 19,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% block pagetitle %}Delete Illness{% endblock %}*/
/* {% block body -%}*/
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                     */
/*                         <h3>Delete Illness</h3>*/
/*                        */
/*                     <div class="x_content">*/
/*                         <input class="form-control" id='search' type="text" name="query" placeholder="Type a name" />*/
/*                          */
/*                                                     <center><img hidden id="loading" src="{{asset('charisma/img/ajax-loaders/ajax-loader-1.gif')}}" ></center>*/
/*                                                     <table class="table table-striped" id="resulttable" class="hidden">*/
/* */
/*                                                         <tbody id="resultbody">*/
/* */
/*                                                             </tr>*/
/*                                                         </tbody>*/
/*                                                     </table>*/
/* */
/*                                                 </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/* <script>*/
/*                                     var deleteBtnId;*/
/*                                     $(document).on("click", ".btn-remove", function(x){*/
/*                                          x.preventDefault();*/
/*                                         deleteBtnId = this.id;*/
/*                                         deleteBtnId = deleteBtnId.replace('remove-', '');*/
/*                                          $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('admin_delete_illness_click') }}',*/
/*                                             data: {id: deleteBtnId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                location.reload();*/
/*                                                 console.log(response);*/
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*     */
/*    </script>*/
/*     <script>*/
/* $('#search').keyup(function() {*/
/*     */
/*      searchText = $(this).val();*/
/*      if (this.value != ''){*/
/*          $("#resultbody").empty();*/
/*      }*/
/*      */
/*      */
/*      if (this.value.length >= 3 && this.value != '')*/
/*   {*/
/*      $.ajax({*/
/*         type: "POST",*/
/*         dataType: "json",*/
/*         url: '{{ path('admin_delete_illness') }}',*/
/*         data: {searchText : searchText},*/
/*         beforeSend: function(){*/
/*         $('#loading').show();*/
/*         },*/
/*         complete: function(){*/
/*         $('#loading').hide();*/
/*         },*/
/*         success : function(data) */
/*            */
/*            {if(data){*/
/*                   */
/*                 $("#resultbody").empty();*/
/*                 var len = data.length;*/
/*                 var txt = "";*/
/*                   if(len > 0){*/
/*                     for(var i=0;i<len;i++){*/
/*                             //var button= $('<button type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user"></i> Invite to Group </button>');*/
/*                             var entityId = data[i].id;*/
/*                             var showPath = '{{ path("doctor_show", {'id' : 'entityId' })}}';*/
/*                             showPath = showPath.replace("entityId", data[i].id);*/
/*                             txt += "<tr><td>"+data[i].illnessName+"</td>";*/
/*                             txt+= '<td><a href="#" ><button id="remove-'+ data[i].id+'" type="button" class="btn btn-danger btn-xs btn-remove"> <i class="fa fa-trash"></i> Remove Illness </button>';*/
/*                         */
/*                     }*/
/*                     if(txt != ""){*/
/*                         $("#resultbody").append(txt).removeClass("hidden");*/
/*                          */
/*                     }*/
/*                 }*/
/*             }*/
/*         },error: function() {*/
/*          alert('error');*/
/*           }*/
/*         });*/
/*     }*/
/* });*/
/*        */
/*     </script>*/
/*     {% endblock %}*/
/* */
