<?php

/* ResearchProjectMyProjectBundle:Fellowship:show.html.twig */
class __TwigTemplate_2b96d7671876dbe3fe905e7d97256de7e4e5679299a3be8f5b319b3fa9a16777 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "ResearchProjectMyProjectBundle:Fellowship:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd4dea38e3e4f127c2eaf72b506b17714ed88b38b4ac227b3a20425d8ed34107 = $this->env->getExtension("native_profiler");
        $__internal_fd4dea38e3e4f127c2eaf72b506b17714ed88b38b4ac227b3a20425d8ed34107->enter($__internal_fd4dea38e3e4f127c2eaf72b506b17714ed88b38b4ac227b3a20425d8ed34107_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Fellowship:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fd4dea38e3e4f127c2eaf72b506b17714ed88b38b4ac227b3a20425d8ed34107->leave($__internal_fd4dea38e3e4f127c2eaf72b506b17714ed88b38b4ac227b3a20425d8ed34107_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_61160f6b6a05b9e3e1477c2f6f913b46d1484dae29724db34e2dcecc670f331e = $this->env->getExtension("native_profiler");
        $__internal_61160f6b6a05b9e3e1477c2f6f913b46d1484dae29724db34e2dcecc670f331e->enter($__internal_61160f6b6a05b9e3e1477c2f6f913b46d1484dae29724db34e2dcecc670f331e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>Fellowship</h1>

    <table class=\"record_properties\">
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Fellowshipname</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fellowshipName", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("fellowship");
        echo "\">
            Back to the list
        </a>
    </li>
    <li>
        <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fellowship_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">
            Edit
        </a>
    </li>
    <li>";
        // line 30
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</li>
</ul>
";
        
        $__internal_61160f6b6a05b9e3e1477c2f6f913b46d1484dae29724db34e2dcecc670f331e->leave($__internal_61160f6b6a05b9e3e1477c2f6f913b46d1484dae29724db34e2dcecc670f331e_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Fellowship:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 30,  73 => 26,  65 => 21,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Fellowship</h1>*/
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <td>{{ entity.id }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Fellowshipname</th>*/
/*                 <td>{{ entity.fellowshipName }}</td>*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('fellowship') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/*     <li>*/
/*         <a href="{{ path('fellowship_edit', { 'id': entity.id }) }}">*/
/*             Edit*/
/*         </a>*/
/*     </li>*/
/*     <li>{{ form(delete_form) }}</li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
