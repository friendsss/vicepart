<?php

/* ResearchProjectMyProjectBundle:Default:settings.html.twig */
class __TwigTemplate_dc8cd355c2fd08e33eb9f1fdbcdc122f8a0f0ec310779c097cff35553129f2a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Default:settings.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'settings' => array($this, 'block_settings'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d9e6490fa436a6518aa3eaa70b1b55dce8fd52d1fd929675b96319e08db0f395 = $this->env->getExtension("native_profiler");
        $__internal_d9e6490fa436a6518aa3eaa70b1b55dce8fd52d1fd929675b96319e08db0f395->enter($__internal_d9e6490fa436a6518aa3eaa70b1b55dce8fd52d1fd929675b96319e08db0f395_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Default:settings.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d9e6490fa436a6518aa3eaa70b1b55dce8fd52d1fd929675b96319e08db0f395->leave($__internal_d9e6490fa436a6518aa3eaa70b1b55dce8fd52d1fd929675b96319e08db0f395_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_a9d59c246ec0a0fbd34ec31c583097f0bebb8c5493304f4113b24d3daf48fe4c = $this->env->getExtension("native_profiler");
        $__internal_a9d59c246ec0a0fbd34ec31c583097f0bebb8c5493304f4113b24d3daf48fe4c->enter($__internal_a9d59c246ec0a0fbd34ec31c583097f0bebb8c5493304f4113b24d3daf48fe4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_a9d59c246ec0a0fbd34ec31c583097f0bebb8c5493304f4113b24d3daf48fe4c->leave($__internal_a9d59c246ec0a0fbd34ec31c583097f0bebb8c5493304f4113b24d3daf48fe4c_prof);

    }

    // line 3
    public function block_settings($context, array $blocks = array())
    {
        $__internal_0cbbc6892f0a6459dce8336220fe8742c1ad099804dd991695f8f182427b74ff = $this->env->getExtension("native_profiler");
        $__internal_0cbbc6892f0a6459dce8336220fe8742c1ad099804dd991695f8f182427b74ff->enter($__internal_0cbbc6892f0a6459dce8336220fe8742c1ad099804dd991695f8f182427b74ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "settings"));

        echo " class=\"active\" ";
        
        $__internal_0cbbc6892f0a6459dce8336220fe8742c1ad099804dd991695f8f182427b74ff->leave($__internal_0cbbc6892f0a6459dce8336220fe8742c1ad099804dd991695f8f182427b74ff_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_11f99701232db3ec12705acff12e69a7c9e3949ff020fd444aa756364f017d42 = $this->env->getExtension("native_profiler");
        $__internal_11f99701232db3ec12705acff12e69a7c9e3949ff020fd444aa756364f017d42->enter($__internal_11f99701232db3ec12705acff12e69a7c9e3949ff020fd444aa756364f017d42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    <div>
    <div class=\"page-title\">
        <div class=\"title\">
            
        </div>
    </div>
    <div class=\"clearfix\"></div>
    
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\">
                                <div class=\"x_title\">
                                    <h2> Settings</h2>
                                    
                                       
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div class=\"x_content\">


                                    <div class=\"\" role=\"tabpanel\" data-example-id=\"togglable-tabs\">
                                        <ul id=\"myTab\" class=\"nav nav-tabs bar_tabs\" role=\"tablist\">
                                            <li role=\"presentation\" class=\"active\"><a href=\"#tab_content3\" role=\"tab\" id=\"profile-tab2\" data-toggle=\"tab\" aria-expanded=\"true\">Change Pasword</a>
                                            </li>
                                            <li role=\"presentation\" class=\"\"><a href=\"#tab_content1\" id=\"home-tab\" role=\"tab\" data-toggle=\"tab\" aria-expanded=\"false\">Remove Account</a>
                                            </li>
                                            <li role=\"presentation\" class=\"\"><a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("doctor_edit");
        echo "\" role=\"tab\">Edit Profile</a>
                                            </li>
                                            
                                        </ul>
                                        <div id=\"myTabContent\" class=\"tab-content\">
                                            <div role=\"tabpanel\" class=\"tab-pane fade \" id=\"tab_content1\" aria-labelledby=\"home-tab\">
                                                
                                               <button id =\"removeaccountmodal\" type=\"button\" class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#deletedocmodal\" data-backdrop=\"static\" data-keyboard=\"false\">
                                                    <i class=\"glyphicon glyphicon-close\"></i> Remove Account</button>
                                            </div>
                                            <div role=\"tabpanel\" class=\"tab-pane fade active in\" id=\"tab_content3\" aria-labelledby=\"profile-tab\">
                                                <h3>Change Password</h3>
                                                
                                                    ";
        // line 44
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "registerForm", "class" => "form-horizontal form-label-left")));
        echo "
                                                    <div id='fn' class=\"item form-group\">
                                                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Old Password <span class=\"required\">*</span>
                                                        </label>
                                                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                            ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "oldPassword", array()), 'widget', array("attr" => array("id" => "firstName", "name" => "firstName", "placeholder" => "Old Password", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 50
        echo "
                                                        </div>
                                                    </div>
                                                        <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "oldPassword", array()), 'errors');
        echo "</div></i>
                                                   
                                                      <div id='pass1' class=\"item form-group\">
                                                        <label for=\"first\" class=\"control-label col-md-3\" >Password <span class=\"required\">*</span>
                                                        </label>
                                                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                            ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "newPassword", array()), "first", array()), 'widget', array("attr" => array("type" => "password", "placeholder" => "Password", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "8,15", "required" => "required")));
        // line 60
        echo "
                                                        </div>
                                                    </div>
                                                        <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "newPassword", array()), "first", array()), 'errors');
        echo "</div></i>
                                                     <div id='pass2' class=\"item form-group\">
                                                        <label for=\"second\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Repeat Password <span class=\"required\">*</span>
                                                        </label>
                                                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                            ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "newPassword", array()), "second", array()), 'widget', array("attr" => array("id" => "second", "type" => "password", "name" => "second", "placeholder" => "Repeat Password", "class" => "form-control col-md-7 col-xs-12", "data-validate-linked" => "researchproject_myprojectbundle_doctor_password_first", "required" => "required")));
        // line 69
        echo "
                                                        </div>
                                                    </div>
                                                        <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "newPassword", array()), "second", array()), 'errors');
        echo "</div></i>
                                                        <div class=\"ln_solid\"></div>
                                                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                                            <a href=\"\">";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                                                            <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
                                                        </div>
                                                    </div>
                                                     ";
        // line 79
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                                               
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
    <div id=\"deletedocmodal\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" >
                                        <div class=\"modal-content\">
                                            
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 style=\"font-family: Century Gothic; font-size: 16px;\"class=\"modal-title\" id=\"deletegroupmodal\">Delete Group</h4>
                                            </div>

                                            <div class=\"modal-body\">
                                                <center><h4 style=\"font-family: Century Gothic; font-size: 16px;\">
                                                        <i class=\"fa fa-exclamation-triangle\" style=\"color:#ff4d4d; font-size: 30px;\"></i>&emsp;Are you sure you want to remove your account?</h4></center>
                                            </div>
                                            
                                            <div class=\"modal-footer\">
                                                    <button type=\"button\" id='con_delete_doc' class=\"btn btn-primary\" >Yes</button>
                                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
                                            </div>
                                        </div>
                                    </div>
                         </div>
    </div>
            
    
<script>
                                     var userId = ";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()), "html", null, true);
        echo "
                                    \$(\"#con_delete_doc\").click(function(x) {
                                        x.preventDefault();
                                        \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 118
        echo $this->env->getExtension('routing')->getPath("remove_doctor_click");
        echo "',
                                            data: {id: userId},
                                            dataType: 'json',
                                            success: function(response) {
                                                window.location.href = response;
                                                 
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    });
    
   </script>
";
        
        $__internal_11f99701232db3ec12705acff12e69a7c9e3949ff020fd444aa756364f017d42->leave($__internal_11f99701232db3ec12705acff12e69a7c9e3949ff020fd444aa756364f017d42_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 118,  206 => 113,  169 => 79,  162 => 75,  156 => 72,  151 => 69,  149 => 68,  141 => 63,  136 => 60,  134 => 59,  125 => 53,  120 => 50,  118 => 49,  110 => 44,  94 => 31,  66 => 5,  60 => 4,  48 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block title %} {% endblock %}*/
/* {% block settings %} class="active" {% endblock %}*/
/* {% block body %}*/
/*     <div>*/
/*     <div class="page-title">*/
/*         <div class="title">*/
/*             */
/*         </div>*/
/*     </div>*/
/*     <div class="clearfix"></div>*/
/*     */
/*     <div class="row">*/
/*         <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                             <div class="x_panel">*/
/*                                 <div class="x_title">*/
/*                                     <h2> Settings</h2>*/
/*                                     */
/*                                        */
/*                                     <div class="clearfix"></div>*/
/*                                 </div>*/
/*                                 <div class="x_content">*/
/* */
/* */
/*                                     <div class="" role="tabpanel" data-example-id="togglable-tabs">*/
/*                                         <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">*/
/*                                             <li role="presentation" class="active"><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="true">Change Pasword</a>*/
/*                                             </li>*/
/*                                             <li role="presentation" class=""><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="false">Remove Account</a>*/
/*                                             </li>*/
/*                                             <li role="presentation" class=""><a href="{{path("doctor_edit")}}" role="tab">Edit Profile</a>*/
/*                                             </li>*/
/*                                             */
/*                                         </ul>*/
/*                                         <div id="myTabContent" class="tab-content">*/
/*                                             <div role="tabpanel" class="tab-pane fade " id="tab_content1" aria-labelledby="home-tab">*/
/*                                                 */
/*                                                <button id ="removeaccountmodal" type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletedocmodal" data-backdrop="static" data-keyboard="false">*/
/*                                                     <i class="glyphicon glyphicon-close"></i> Remove Account</button>*/
/*                                             </div>*/
/*                                             <div role="tabpanel" class="tab-pane fade active in" id="tab_content3" aria-labelledby="profile-tab">*/
/*                                                 <h3>Change Password</h3>*/
/*                                                 */
/*                                                     {{ form_start(form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'registerForm', 'class':'form-horizontal form-label-left' } }) }}*/
/*                                                     <div id='fn' class="item form-group">*/
/*                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" >Old Password <span class="required">*</span>*/
/*                                                         </label>*/
/*                                                         <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                                                             {{ form_widget(form.oldPassword, { 'attr' : { 'id': 'firstName', 'name': 'firstName', 'placeholder' : 'Old Password', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                                                             'data-validate-length-range' : '1',  'required': 'required', 'type' : 'text' } }) }}*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                         <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.oldPassword)}}</div></i>*/
/*                                                    */
/*                                                       <div id='pass1' class="item form-group">*/
/*                                                         <label for="first" class="control-label col-md-3" >Password <span class="required">*</span>*/
/*                                                         </label>*/
/*                                                         <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                                                             {{  form_widget(form.newPassword.first, { 'attr' : { 'type' : 'password', 'placeholder' : 'Password', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                                                                     'data-validate-length-range' : '8,15', 'required': 'required'  } }) }}*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                         <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.newPassword.first)}}</div></i>*/
/*                                                      <div id='pass2' class="item form-group">*/
/*                                                         <label for="second" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat Password <span class="required">*</span>*/
/*                                                         </label>*/
/*                                                         <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                                                             {{  form_widget(form.newPassword.second, { 'attr' : {  'id' : 'second' ,'type' : 'password', 'name': 'second', 'placeholder' : 'Repeat Password', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                                                                     'data-validate-linked' : 'researchproject_myprojectbundle_doctor_password_first', 'required': 'required' } }) }}*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                         <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.newPassword.second)}}</div></i>*/
/*                                                         <div class="ln_solid"></div>*/
/*                                                         <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                                                             <a href="">{{ form_widget(form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn'} }) }}</a>*/
/*                                                             <button type="reset" class="btn btn-dark" onclick="goBack()" >Cancel</button>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                      {{form_end(form)}}*/
/*                                                */
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/* */
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*     <div id="deletedocmodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">*/
/*                                     <div class="modal-dialog" >*/
/*                                         <div class="modal-content">*/
/*                                             */
/*                                             <div class="modal-header">*/
/*                                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 16px;"class="modal-title" id="deletegroupmodal">Delete Group</h4>*/
/*                                             </div>*/
/* */
/*                                             <div class="modal-body">*/
/*                                                 <center><h4 style="font-family: Century Gothic; font-size: 16px;">*/
/*                                                         <i class="fa fa-exclamation-triangle" style="color:#ff4d4d; font-size: 30px;"></i>&emsp;Are you sure you want to remove your account?</h4></center>*/
/*                                             </div>*/
/*                                             */
/*                                             <div class="modal-footer">*/
/*                                                     <button type="button" id='con_delete_doc' class="btn btn-primary" >Yes</button>*/
/*                                                     <button type="button" class="btn btn-default" data-dismiss="modal">No</button>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                          </div>*/
/*     </div>*/
/*             */
/*     */
/* <script>*/
/*                                      var userId = {{app.user.id}}*/
/*                                     $("#con_delete_doc").click(function(x) {*/
/*                                         x.preventDefault();*/
/*                                         $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('remove_doctor_click') }}',*/
/*                                             data: {id: userId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                 window.location.href = response;*/
/*                                                  */
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*     */
/*    </script>*/
/* {% endblock %}*/
