<?php

/* ResearchProjectMyProjectBundle:Default:illness.html.twig */
class __TwigTemplate_744bdc5c41cfc1f365363f6aa05f1435699d40661edaf924f0625e4635a1c03a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Default:illness.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'illness' => array($this, 'block_illness'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_14d61c778ba1bddcc7767ea172f48f588ab243a47e6fdab295c0881168288377 = $this->env->getExtension("native_profiler");
        $__internal_14d61c778ba1bddcc7767ea172f48f588ab243a47e6fdab295c0881168288377->enter($__internal_14d61c778ba1bddcc7767ea172f48f588ab243a47e6fdab295c0881168288377_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Default:illness.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_14d61c778ba1bddcc7767ea172f48f588ab243a47e6fdab295c0881168288377->leave($__internal_14d61c778ba1bddcc7767ea172f48f588ab243a47e6fdab295c0881168288377_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_62788ec2f8988814304ce6872cf554311cab9ce8c1f70804d7ab6e62d9d3e250 = $this->env->getExtension("native_profiler");
        $__internal_62788ec2f8988814304ce6872cf554311cab9ce8c1f70804d7ab6e62d9d3e250->enter($__internal_62788ec2f8988814304ce6872cf554311cab9ce8c1f70804d7ab6e62d9d3e250_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_62788ec2f8988814304ce6872cf554311cab9ce8c1f70804d7ab6e62d9d3e250->leave($__internal_62788ec2f8988814304ce6872cf554311cab9ce8c1f70804d7ab6e62d9d3e250_prof);

    }

    // line 3
    public function block_illness($context, array $blocks = array())
    {
        $__internal_1b59716b3e6e5f28837a0b575381a3eb4b848fa6020c86cf389b543f4845d279 = $this->env->getExtension("native_profiler");
        $__internal_1b59716b3e6e5f28837a0b575381a3eb4b848fa6020c86cf389b543f4845d279->enter($__internal_1b59716b3e6e5f28837a0b575381a3eb4b848fa6020c86cf389b543f4845d279_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "illness"));

        echo " class=\"active\" ";
        
        $__internal_1b59716b3e6e5f28837a0b575381a3eb4b848fa6020c86cf389b543f4845d279->leave($__internal_1b59716b3e6e5f28837a0b575381a3eb4b848fa6020c86cf389b543f4845d279_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_359f02262e4901ff77af5b2032db8bf68dcc012cc3504702a205c4bee869c098 = $this->env->getExtension("native_profiler");
        $__internal_359f02262e4901ff77af5b2032db8bf68dcc012cc3504702a205c4bee869c098->enter($__internal_359f02262e4901ff77af5b2032db8bf68dcc012cc3504702a205c4bee869c098_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    <div>
    <div class=\"page-title\">
        <div class=\"title\">
            <h3>&nbsp;ILLNESS</h3>
        </div>
    </div>
    <div class=\"clearfix\"></div>
    
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            ";
        // line 15
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 16
            echo "                    <a href=\"";
            echo $this->env->getExtension('routing')->getPath("illness_new");
            echo "\">
                        <button class=\"btn btn-success right\" style=\"margin-bottom: 5px;\"><i class=\"fa fa-plus\"></i> Define New Illness</button>
                    </a>
                    ";
        }
        // line 20
        echo "            <div class=\"x_panel\">
                <h3>&nbsp;Health Topics</h3>
                <br/>
                <div style=\"overflow-y: scroll; height:500px;\">
                    ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 25
            echo "                        <a href='";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("illness_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "'>
                        <div class='col-md-5 col-sm-5 col-xs-5' style='box-shadow: 4px 4px 2px #888888; border-radius: 5px 25px 10px; background-color: #404040; margin-right: 30px; margin-left: 30px;
                             margin-bottom: 20px;' >
                            <center>
                                <h4 style ='color:#379DFF; font-family: Century Gothic; font-size:20px;'>
                                    ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "illnessName", array()), "html", null, true);
            echo "</h4> 
                                <h4 style ='color: white; font-family:Century Gothic; font-size:15px'>
                                    <i class='fa fa-quote-left'> </i> ";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "illnessCommonName", array()), "html", null, true);
            echo " <i class='fa fa-quote-right'> </i></h4>
                            </center>
                        </div>
                        </a>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "                </div>
            </div>
        </div>
    </div>
            
    </div>

";
        
        $__internal_359f02262e4901ff77af5b2032db8bf68dcc012cc3504702a205c4bee869c098->leave($__internal_359f02262e4901ff77af5b2032db8bf68dcc012cc3504702a205c4bee869c098_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:illness.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 37,  112 => 32,  107 => 30,  98 => 25,  94 => 24,  88 => 20,  80 => 16,  78 => 15,  66 => 5,  60 => 4,  48 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block title %} {% endblock %}*/
/* {% block illness %} class="active" {% endblock %}*/
/* {% block body %}*/
/*     <div>*/
/*     <div class="page-title">*/
/*         <div class="title">*/
/*             <h3>&nbsp;ILLNESS</h3>*/
/*         </div>*/
/*     </div>*/
/*     <div class="clearfix"></div>*/
/*     */
/*     <div class="row">*/
/*         <div class="col-md-12 col-sm-12 col-xs-12">*/
/*             {% if app.user%}*/
/*                     <a href="{{path('illness_new')}}">*/
/*                         <button class="btn btn-success right" style="margin-bottom: 5px;"><i class="fa fa-plus"></i> Define New Illness</button>*/
/*                     </a>*/
/*                     {% endif %}*/
/*             <div class="x_panel">*/
/*                 <h3>&nbsp;Health Topics</h3>*/
/*                 <br/>*/
/*                 <div style="overflow-y: scroll; height:500px;">*/
/*                     {% for entity in entities%}*/
/*                         <a href='{{path('illness_show', {'id' :entity.id})}}'>*/
/*                         <div class='col-md-5 col-sm-5 col-xs-5' style='box-shadow: 4px 4px 2px #888888; border-radius: 5px 25px 10px; background-color: #404040; margin-right: 30px; margin-left: 30px;*/
/*                              margin-bottom: 20px;' >*/
/*                             <center>*/
/*                                 <h4 style ='color:#379DFF; font-family: Century Gothic; font-size:20px;'>*/
/*                                     {{entity.illnessName}}</h4> */
/*                                 <h4 style ='color: white; font-family:Century Gothic; font-size:15px'>*/
/*                                     <i class='fa fa-quote-left'> </i> {{entity.illnessCommonName}} <i class='fa fa-quote-right'> </i></h4>*/
/*                             </center>*/
/*                         </div>*/
/*                         </a>*/
/*                     {% endfor %}*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*             */
/*     </div>*/
/* */
/* {% endblock %}*/
