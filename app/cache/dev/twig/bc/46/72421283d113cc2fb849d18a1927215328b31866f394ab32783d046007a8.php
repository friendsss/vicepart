<?php

/* ResearchProjectMyProjectBundle:Default:doctor.html.twig */
class __TwigTemplate_bc4672421283d113cc2fb849d18a1927215328b31866f394ab32783d046007a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'doctor' => array($this, 'block_doctor'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " ";
    }

    // line 3
    public function block_doctor($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "        <div class=\"\">
            <div class=\"page-title\">
                <div class=\"title\">
                    <h3>&nbsp;DOCTOR</h3>
                </div>
            </div>
            <div class=\"clearfix\"></div>

            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12\">
                    <div class=\"x_panel\" >
                        <br/>
                 
                    ";
        // line 19
        $context["firstname"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "firstName");
        // line 20
        echo "                    ";
        $context["lastname"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "lastName");
        // line 21
        echo "                    ";
        $context["fop"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "fieldOfPractice");
        // line 22
        echo "                        
                    ";
        // line 23
        $context["firstname2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "firstName");
        // line 24
        echo "                    ";
        $context["lastname2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "lastName");
        // line 25
        echo "                    ";
        $context["fop2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "fieldOfPractice");
        // line 26
        echo "                        
                    ";
        // line 27
        $context["firstname3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "firstName");
        // line 28
        echo "                    ";
        $context["lastname3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "lastName");
        // line 29
        echo "                    ";
        $context["fop3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "fieldOfPractice");
        // line 30
        echo "                        
                        
                    <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["firstname"]) ? $context["firstname"] : $this->getContext($context, "firstname")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lastname"]) ? $context["lastname"] : $this->getContext($context, "lastname")), "html", null, true);
        echo "</b></h3></i>
                                        <h4>";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["fop"]) ? $context["fop"] : $this->getContext($context, "fop")), "html", null, true);
        echo "</h4>
                                        ";
        // line 38
        if (((isset($context["fop"]) ? $context["fop"] : $this->getContext($context, "fop")) == "General")) {
            // line 39
            echo "                                             <br/>
                                        ";
        } elseif (((isset($context["fop"]) ? $context["fop"] : $this->getContext($context, "fop")) == "Specialized")) {
            // line 41
            echo "                                            <div id=\"spec\">
                                                    ";
            // line 42
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "specialties"));
            foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                // line 43
                echo "                                                        <h5>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["specialty"]) ? $context["specialty"] : $this->getContext($context, "specialty")), "specialtyName"), "html", null, true);
                echo "</h5>
                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "                                            </div>
                                        ";
        }
        // line 47
        echo "                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        <img src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
        echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        <br/>
                                        <a href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "id"))), "html", null, true);
        echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
        // line 69
        echo twig_escape_filter($this->env, (isset($context["firstname2"]) ? $context["firstname2"] : $this->getContext($context, "firstname2")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lastname2"]) ? $context["lastname2"] : $this->getContext($context, "lastname2")), "html", null, true);
        echo "</b></h3></i>
                                        <h4>";
        // line 70
        echo twig_escape_filter($this->env, (isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")), "html", null, true);
        echo "</h4>
                                        ";
        // line 71
        if (((isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")) == "General")) {
            // line 72
            echo "                                                <br/>
                                        ";
        } elseif (((isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")) == "Specialized")) {
            // line 74
            echo "                                            <div id=\"spec\">
                                                    ";
            // line 75
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "specialties"));
            foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                // line 76
                echo "                                                        <h5>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["specialty"]) ? $context["specialty"] : $this->getContext($context, "specialty")), "specialtyName"), "html", null, true);
                echo "</h5>
                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 78
            echo "                                            </div>
                                        ";
        }
        // line 80
        echo "                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        <img src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
        echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        <br/>
                                        <a href=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "id"))), "html", null, true);
        echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
                                            
                    <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
        // line 101
        echo twig_escape_filter($this->env, (isset($context["firstname3"]) ? $context["firstname3"] : $this->getContext($context, "firstname3")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lastname3"]) ? $context["lastname3"] : $this->getContext($context, "lastname3")), "html", null, true);
        echo "</b></h3></i>
                                        <h4>";
        // line 102
        echo twig_escape_filter($this->env, (isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")), "html", null, true);
        echo "</h4>
                                       ";
        // line 103
        if (((isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")) == "General")) {
            // line 104
            echo "                                                <br/>
                                        ";
        } elseif (((isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")) == "Specialized")) {
            // line 106
            echo "                                            <div id=\"spec\">
                                                    ";
            // line 107
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "specialties"));
            foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                // line 108
                echo "                                                        <h5>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["specialty"]) ? $context["specialty"] : $this->getContext($context, "specialty")), "specialtyName"), "html", null, true);
                echo "</h5>
                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 110
            echo "                                            </div>
                                        ";
        }
        // line 112
        echo "                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        <img src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
        echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        <br/>
                                        <a href=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "id"))), "html", null, true);
        echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div>
            </div>
        </div>
                
            <div class=\"row\">
                <div class=\"col-md-6 col-xs-12\">
                    <div class=\"x_panel\" >
                        <h3></h3>
                        <center><h3 style =\"font-size: 25px;\">SPECIALTIES</h3></center>
                        ";
        // line 137
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["specialties"]) ? $context["specialties"] : $this->getContext($context, "specialties")));
        foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
            echo "                
                            <a href=";
            // line 138
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("searchBySpecialty", array("specialty" => $this->getAttribute((isset($context["specialty"]) ? $context["specialty"] : $this->getContext($context, "specialty")), "specialtyName"))), "html", null, true);
            echo "><center><h4 style = \"font-family: Century Gothic; font-size: 15px;\"> <i class=\"fa fa-dot-circle-o\"></i> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["specialty"]) ? $context["specialty"] : $this->getContext($context, "specialty")), "specialtyName"), "html", null, true);
            echo " </h4></center></a>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 140
        echo "                    </div>
                </div>
            
            
                <div class=\"col-md-6 col-xs-12\">
                    <div class=\"x_panel\" >
                        <h3></h3>
                        <center><h3 style=\"font-size: 25px;\">GENERAL DOCTORS</h3></center>
                        ";
        // line 148
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(twig_escape_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            echo "                
                            ";
            // line 149
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fieldOfPractice") == "General")) {
                // line 150
                echo "                                <center><i class=\"fa fa-user-md\" style=\"font-size: 20px;\"></i></center>  
                                <a href=\"";
                // line 151
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\"><center><h4 style=\"font-family: Century Gothic; font-size: 16px;\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "firstName"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "lastName"), "html", null, true);
                echo "</h4></center></a> 
                                <div class=\"ln_solid\"></div>
                            ";
            }
            // line 154
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 155
        echo "                    </div>
                </div>
            </div>    
                
        </div>
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:doctor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  351 => 155,  345 => 154,  335 => 151,  332 => 150,  330 => 149,  324 => 148,  314 => 140,  304 => 138,  298 => 137,  276 => 118,  271 => 116,  265 => 112,  261 => 110,  252 => 108,  248 => 107,  245 => 106,  241 => 104,  239 => 103,  235 => 102,  229 => 101,  211 => 86,  206 => 84,  200 => 80,  196 => 78,  187 => 76,  183 => 75,  180 => 74,  176 => 72,  174 => 71,  170 => 70,  164 => 69,  146 => 54,  141 => 52,  134 => 47,  130 => 45,  121 => 43,  117 => 42,  114 => 41,  110 => 39,  108 => 38,  104 => 37,  98 => 36,  90 => 30,  87 => 29,  84 => 28,  82 => 27,  79 => 26,  76 => 25,  73 => 24,  71 => 23,  68 => 22,  65 => 21,  62 => 20,  60 => 19,  45 => 6,  42 => 5,  36 => 3,  30 => 2,);
    }
}
