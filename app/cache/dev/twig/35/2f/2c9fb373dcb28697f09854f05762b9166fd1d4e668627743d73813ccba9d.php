<?php

/* ResearchProjectMyProjectBundle:Doctor:show.html.twig */
class __TwigTemplate_352f2c9fb373dcb28697f09854f05762b9166fd1d4e668627743d73813ccba9d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'doctor' => array($this, 'block_doctor'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_doctor($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"\">
            <div class=\"page-title\">
                <div class=\"title\">
                    <h3>&nbsp;DOCTOR</h3>
                </div>
            </div>
                    <div class=\"clearfix\"></div>

                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\" >
                                <div class=\"x_title\">
                                    <h3>User Profile</h3>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div class=\"x_content\">
                                    <div class=\"col-md-12 col-sm-12 col-xs-12\">
                                        <center>
                                        <div class=\"profile_img\">
                                            <!-- end of image cropping -->
                                            <div id=\"crop-avatar\">
                                                <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    <img src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
        echo "\" alt=\"Avatar\">
                                                </div>

                                                <!-- Cropping modal -->
                                                <div class=\"modal fade\" id=\"avatar-modal\" aria-hidden=\"true\" aria-labelledby=\"avatar-modal-label\" role=\"dialog\" tabindex=\"-1\">
                                                    <div class=\"modal-dialog modal-lg\">
                                                        <div class=\"modal-content\">
                                                            <form class=\"avatar-form\" action=\"crop.php\" enctype=\"multipart/form-data\" method=\"post\">
                                                                <div class=\"modal-header\">
                                                                    <button class=\"close\" data-dismiss=\"modal\" type=\"button\">&times;</button>
                                                                    <h4 class=\"modal-title\" id=\"avatar-modal-label\">Change Avatar</h4>
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"avatar-body\">

                                                                        <!-- Upload image and data -->
                                                                        <div class=\"avatar-upload\">
                                                                            <input class=\"avatar-src\" name=\"avatar_src\" type=\"hidden\">
                                                                            <input class=\"avatar-data\" name=\"avatar_data\" type=\"hidden\">
                                                                            <label for=\"avatarInput\">Local upload</label>
                                                                            <input class=\"avatar-input\" id=\"avatarInput\" name=\"avatar_file\" type=\"file\">
                                                                        </div>

                                                                        <!-- Crop and preview -->
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-9\">
                                                                                <div class=\"avatar-wrapper\"></div>
                                                                            </div>
                                                                            <div class=\"col-md-3\">
                                                                                <div class=\"avatar-preview preview-lg\"></div>
                                                                                <div class=\"avatar-preview preview-md\"></div>
                                                                                <div class=\"avatar-preview preview-sm\"></div>
                                                                            </div>
                                                                        </div>

                                                                        <div class=\"row avatar-btns\">
                                                                            <div class=\"col-md-9\">
                                                                                <div class=\"btn-group\">
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-90\" type=\"button\" title=\"Rotate -90 degrees\">Rotate Left</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-15\" type=\"button\">-15deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-30\" type=\"button\">-30deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-45\" type=\"button\">-45deg</button>
                                                                                </div>
                                                                                <div class=\"btn-group\">
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"90\" type=\"button\" title=\"Rotate 90 degrees\">Rotate Right</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"15\" type=\"button\">15deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"30\" type=\"button\">30deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"45\" type=\"button\">45deg</button>
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"col-md-3\">
                                                                                <button class=\"btn btn-primary btn-block avatar-save\" type=\"submit\">Done</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class=\"modal-footer\">
                                                  <button class=\"btn btn-default\" data-dismiss=\"modal\" type=\"button\">Close</button>
                                                </div> -->
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.modal -->

                                                <!-- Loading state -->
                                                <div class=\"loading\" aria-label=\"Loading\" role=\"img\" tabindex=\"-1\"></div>
                                            </div>
                                            <!-- end of image cropping -->

                                        </div>
                                        
                                        <h3 style=\"font-size: 30px;\">";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "firstName"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "middleName"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "lastName"), "html", null, true);
        echo "</h3>       
                                         
                                        <div class=\"accordion\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
                                        <div class=\"panel\">
                                            <a class=\"panel-heading\" role=\"tab\" id=\"headingOne\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                                                 <h4 class=\"panel-title\"style=\"font-family: Century Gothic; font-size: 16px; color:#379DFF;\"><b><i class=\"fa fa-user-md\"></i>&emsp;MEDICAL PRACTICE and CONTACT INFORMATION</b></h4>
                                            </a>
                                            <div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                                                <div class=\"panel-body\">
                                                    
                                                    <h3 style=\" font-size: 16px; \"><b><i class=\"fa fa-medkit\"></i>&emsp;MEDICAL PRACTICE</b></h3>
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i><p>";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "aboutMyPractice"), "html", null, true);
        echo "</p></i></h4></br>
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-stethoscope\"></i> Field of Practice: <b>";
        // line 111
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fieldOfPractice"), "html", null, true);
        echo "</b></h4>
                                                    
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-tag\"></i> Specialty:</h4>
                                                    ";
        // line 114
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "specialties"));
        foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
            // line 115
            echo "                                                     <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-circle\"></i><b> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["specialty"]) ? $context["specialty"] : $this->getContext($context, "specialty")), "specialtyName"), "html", null, true);
            echo "</b></h4>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-group\"></i> Fellows: </h4>
                                                    ";
        // line 118
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fellowships"));
        foreach ($context['_seq'] as $context["_key"] => $context["fellowship"]) {
            // line 119
            echo "                                                     <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-circle\"></i><b> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["fellowship"]) ? $context["fellowship"] : $this->getContext($context, "fellowship")), "fellowshipName"), "html", null, true);
            echo "</b></h4>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fellowship'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 121
        echo "                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-credit-card\"></i> PRC Number: <b>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prcNumber"), "html", null, true);
        echo " acquired on
                                                                ";
        // line 122
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "origIssueDate"), "m/d/Y"), "html", null, true);
        echo "</b></h4>
                                                    
                                                    <h3></h3>
                                        
                                                    <h3 style=\" font-size: 16px;\"><b><i class=\"fa fa-phone-square\"></i>&emsp;CONTACT INFORMATION</b></h3>
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"> <i class=\"fa fa-at user-profile-icon\"></i> Username: <b>";
        // line 127
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "username"), "html", null, true);
        echo "</b></h4>
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"> <i class=\"fa fa-envelope user-profile-icon\"></i> Email:  <b>";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "emailAddress"), "html", null, true);
        echo "</b></h4>
                                                    <br/>
                                                 
                                                    ";
        // line 131
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 132
            echo "                                                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"><button class=\"btn btn-primary\">Edit Information</button></a>
                                                    ";
        }
        // line 134
        echo "                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"panel\">
                                            <a class=\"panel-heading collapsed\" role=\"tab\" id=\"headingTwo\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
                                                <h4 class=\"panel-title\"style=\"font-family: Century Gothic; font-size: 16px; color:#379DFF;\"><b><i class=\"fa fa-hospital-o\"></i>&emsp;CLINIC</b></h4>
                                            </a>
                                            <div id=\"collapseTwo\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">
                                                <div class=\"panel-body\">
                                                    <div class=\"x_content\">
                                                        ";
        // line 144
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctorClinics"));
        foreach ($context['_seq'] as $context["_key"] => $context["clinic"]) {
            // line 145
            echo "                                                            <ul class=\"list-unstyled timeline\">
                                                                <li>
                                                                    <div class=\"block\">
                                                                        <div class=\"tags\">
                                                                            <a href=\"\" class=\"tag\">
                                                                                <span><i class=\"fa fa-location-arrow\"></i> Clinic</span>
                                                                            </a>
                                                                        </div>
                                                                        <div class=\"block_content\">
                                                                            <h2 class=\"title\">
                                                                                <a href=\"";
            // line 155
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["clinic"]) ? $context["clinic"] : $this->getContext($context, "clinic")), "id"))), "html", null, true);
            echo "\"><h3 style=\"font-family: Century Gothic; font-size: 16px\"><b>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["clinic"]) ? $context["clinic"] : $this->getContext($context, "clinic")), "clinicName"), "html", null, true);
            echo "</b></h3></a> 
                                                                            </h2>
                                                                            <div class=\"byline\">
                                                                                ";
            // line 158
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["clinic"]) ? $context["clinic"] : $this->getContext($context, "clinic")), "clinicAddress"), "html", null, true);
            echo "
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clinic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 165
        echo "                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"panel\">
                                            <a class=\"panel-heading collapsed\" role=\"tab\" id=\"headingThree\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
                                                <h4 class=\"panel-title\"style=\"font-family: Century Gothic; font-size: 16px; color:#379DFF;\"><b><i class=\"fa fa-users\"></i>&emsp;GROUPS</b></h4>
                                            </a>
                                            <div id=\"collapseThree\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\">
                                                <div class=\"panel-body\">
                                                    ";
        // line 175
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctorGroups"));
        foreach ($context['_seq'] as $context["_key"] => $context["groups"]) {
            // line 176
            echo "                                                       <center>
                                                        <a href=\"";
            // line 177
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")), "id"))), "html", null, true);
            echo "\">
                                                            <div align=\"center\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Go to ";
            // line 178
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")), "groupName"), "html", null, true);
            echo "\" style=\"border-radius: 15px;  margin-bottom: 15px; margin-left: 35px; margin-right: 30px; background:#404040 ; height: 100px;
                                                                 \"class=\"col-md-3 col-xs-3 col-sm-3\">
                                                                <h3 style=\"font-family: Century Gothic; font-size: 16px; color:#379DFF;\"><b> ";
            // line 180
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")), "groupName"), "html", null, true);
            echo " </b></h3>
                                                                <h4 style=\"font-family: Century Gothic; font-size: 12px; color: white; overflow:hidden; white-space: nowrap; text-overflow:ellipsis; \"><i>";
            // line 181
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")), "description"), "html", null, true);
            echo "</i></h4>
                                                            </div>
                                                        </a>
                                                        </center>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['groups'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 186
        echo "                                                </div>
                                            </div>
                                        </div>
                                    </div>        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                                
    </div>
    <script src=\"js/bootstrap.min.js\"></script>

    <!-- chart js -->
    <script src=\"js/chartjs/chart.min.js\"></script>
    <!-- bootstrap progress js -->
    <script src=\"js/progressbar/bootstrap-progressbar.min.js\"></script>
    <script src=\"js/nicescroll/jquery.nicescroll.min.js\"></script>
    <!-- icheck -->
    <script src=\"js/icheck/icheck.min.js\"></script>

    <script src=\"js/custom.js\"></script>
    <!-- form validation -->
    <script src=\"js/validator/validator.js\"></script>
    <script type=\"text/javascript\" src=\"js/parsley/parsley.min.js\"></script>
    <!-- /form validation -->
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Doctor:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 186,  307 => 181,  303 => 180,  298 => 178,  294 => 177,  291 => 176,  287 => 175,  275 => 165,  262 => 158,  254 => 155,  242 => 145,  238 => 144,  226 => 134,  220 => 132,  218 => 131,  212 => 128,  208 => 127,  200 => 122,  195 => 121,  186 => 119,  182 => 118,  179 => 117,  170 => 115,  166 => 114,  160 => 111,  156 => 110,  138 => 99,  63 => 27,  38 => 4,  35 => 3,  29 => 2,);
    }
}
