<?php

/* ResearchProjectMyProjectBundle:Doctor:upload.html.twig */
class __TwigTemplate_872a2f173aaec62aa14331ea2cd91566d8eebd4e1b95fbe0b0cc9440dda77868 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Doctor:upload.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c68b345385e1b313ee2bd2e2e7304ff73fee44ac0585e946e9b6e250505dfff = $this->env->getExtension("native_profiler");
        $__internal_9c68b345385e1b313ee2bd2e2e7304ff73fee44ac0585e946e9b6e250505dfff->enter($__internal_9c68b345385e1b313ee2bd2e2e7304ff73fee44ac0585e946e9b6e250505dfff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Doctor:upload.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9c68b345385e1b313ee2bd2e2e7304ff73fee44ac0585e946e9b6e250505dfff->leave($__internal_9c68b345385e1b313ee2bd2e2e7304ff73fee44ac0585e946e9b6e250505dfff_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6224a6367b071492d348653051c45bf700752c1e7bf465cbc01c1c4da791078c = $this->env->getExtension("native_profiler");
        $__internal_6224a6367b071492d348653051c45bf700752c1e7bf465cbc01c1c4da791078c->enter($__internal_6224a6367b071492d348653051c45bf700752c1e7bf465cbc01c1c4da791078c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
    ";
        
        $__internal_6224a6367b071492d348653051c45bf700752c1e7bf465cbc01c1c4da791078c->leave($__internal_6224a6367b071492d348653051c45bf700752c1e7bf465cbc01c1c4da791078c_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Doctor:upload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     {{form(form)}}*/
/*     {% endblock %}*/
/* */
