<?php

/* ResearchProjectMyProjectBundle:Doctor:show.html.twig */
class __TwigTemplate_1e22d3c645b1fd8c4241699029ab96726b0868544fd7c063bf72f9223cb6f134 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Doctor:show.html.twig", 1);
        $this->blocks = array(
            'profile' => array($this, 'block_profile'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c19f651ef1b6026ff3d6dae4e8658ce32ced34d418986ee09beca25d5bb6e32 = $this->env->getExtension("native_profiler");
        $__internal_2c19f651ef1b6026ff3d6dae4e8658ce32ced34d418986ee09beca25d5bb6e32->enter($__internal_2c19f651ef1b6026ff3d6dae4e8658ce32ced34d418986ee09beca25d5bb6e32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Doctor:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2c19f651ef1b6026ff3d6dae4e8658ce32ced34d418986ee09beca25d5bb6e32->leave($__internal_2c19f651ef1b6026ff3d6dae4e8658ce32ced34d418986ee09beca25d5bb6e32_prof);

    }

    // line 2
    public function block_profile($context, array $blocks = array())
    {
        $__internal_2d40cda58e358e1b082206b119015dd45be8d1e08312ee5c9c28c51762a6ae3e = $this->env->getExtension("native_profiler");
        $__internal_2d40cda58e358e1b082206b119015dd45be8d1e08312ee5c9c28c51762a6ae3e->enter($__internal_2d40cda58e358e1b082206b119015dd45be8d1e08312ee5c9c28c51762a6ae3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "profile"));

        echo " class=\"active\" ";
        
        $__internal_2d40cda58e358e1b082206b119015dd45be8d1e08312ee5c9c28c51762a6ae3e->leave($__internal_2d40cda58e358e1b082206b119015dd45be8d1e08312ee5c9c28c51762a6ae3e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_dee1921ccff9cfec9b4316fe63a0e4b1f103847d15b6c0d88bd5a7d0487306ec = $this->env->getExtension("native_profiler");
        $__internal_dee1921ccff9cfec9b4316fe63a0e4b1f103847d15b6c0d88bd5a7d0487306ec->enter($__internal_dee1921ccff9cfec9b4316fe63a0e4b1f103847d15b6c0d88bd5a7d0487306ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<div class=\"\">
            <div class=\"page-title\">
               
                </div>
            </div>
                    <div class=\"clearfix\"></div>

                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\" >
                                    <h3>User Profile</h3>
                                <div class=\"x_content\">
                                    <div class=\"col-md-12 col-sm-12 col-xs-12\">
                                        <center>
                                        <div class=\"profile_img\">
                                            <!-- end of image cropping -->
                                            <div id=\"crop-avatar\">
                                                <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    ";
        // line 24
        if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getWebPath", array()) == null)) {
            // line 25
            echo "                                                        <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
            echo "\" alt=\"\">
                                                    ";
        } else {
            // line 27
            echo "                                                    <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getWebPath", array())), "html", null, true);
            echo "\" alt=\"Avatar\">
                                                    ";
        }
        // line 29
        echo "                                                </div>

                                                <!-- Cropping modal -->
                                                <div class=\"modal fade\" id=\"avatar-modal\" aria-hidden=\"true\" aria-labelledby=\"avatar-modal-label\" role=\"dialog\" tabindex=\"-1\">
                                                    <div class=\"modal-dialog modal-lg\">
                                                        <div class=\"modal-content\">
                                                            <form class=\"avatar-form\" action=\"crop.php\" enctype=\"multipart/form-data\" method=\"post\">
                                                                <div class=\"modal-header\">
                                                                    <button class=\"close\" data-dismiss=\"modal\" type=\"button\">&times;</button>
                                                                    <h4 class=\"modal-title\" id=\"avatar-modal-label\">Change Avatar</h4>
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"avatar-body\">

                                                                        <!-- Upload image and data -->
                                                                        <div class=\"avatar-upload\">
                                                                            <input class=\"avatar-src\" name=\"avatar_src\" type=\"hidden\">
                                                                            <input class=\"avatar-data\" name=\"avatar_data\" type=\"hidden\">
                                                                            <label for=\"avatarInput\">Local upload</label>
                                                                            <input class=\"avatar-input\" id=\"avatarInput\" name=\"avatar_file\" type=\"file\">
                                                                        </div>

                                                                        <!-- Crop and preview -->
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-9\">
                                                                                <div class=\"avatar-wrapper\"></div>
                                                                            </div>
                                                                            <div class=\"col-md-3\">
                                                                                <div class=\"avatar-preview preview-lg\"></div>
                                                                                <div class=\"avatar-preview preview-md\"></div>
                                                                                <div class=\"avatar-preview preview-sm\"></div>
                                                                            </div>
                                                                        </div>

                                                                        <div class=\"row avatar-btns\">
                                                                            <div class=\"col-md-9\">
                                                                                <div class=\"btn-group\">
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-90\" type=\"button\" title=\"Rotate -90 degrees\">Rotate Left</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-15\" type=\"button\">-15deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-30\" type=\"button\">-30deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-45\" type=\"button\">-45deg</button>
                                                                                </div>
                                                                                <div class=\"btn-group\">
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"90\" type=\"button\" title=\"Rotate 90 degrees\">Rotate Right</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"15\" type=\"button\">15deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"30\" type=\"button\">30deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"45\" type=\"button\">45deg</button>
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"col-md-3\">
                                                                                <button class=\"btn btn-primary btn-block avatar-save\" type=\"submit\">Done</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class=\"modal-footer\">
                                                  <button class=\"btn btn-default\" data-dismiss=\"modal\" type=\"button\">Close</button>
                                                </div> -->
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.modal -->

                                                <!-- Loading state -->
                                                <div class=\"loading\" aria-label=\"Loading\" role=\"img\" tabindex=\"-1\"></div>
                                            </div>
                                            <!-- end of image cropping -->

                                        </div>
                                        
                                        <h3 style=\"font-size: 30px;\">";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "middleName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "lastName", array()), "html", null, true);
        echo "</h3>       
                                         
                                        <div class=\"accordion\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
                                        <div class=\"panel\">
                                            <a class=\"panel-heading\" role=\"tab\" id=\"headingOne\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                                                 <h4 class=\"panel-title\"style=\"font-family: Century Gothic; font-size: 16px; color:#379DFF;\"><b><i class=\"fa fa-user-md\"></i>&emsp;MEDICAL PRACTICE and CONTACT INFORMATION</b></h4>
                                            </a>
                                            <div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                                                <div class=\"panel-body\">
                                                    <h3 style=\" font-size: 16px; \"><b><i class=\"fa fa-medkit\"></i>&emsp;MEDICAL PRACTICE</b></h3>
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i><p>";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "aboutMyPractice", array()), "html", null, true);
        echo "</p></i></h4></br>
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-stethoscope\"></i> <b>Field of Practice:</b> ";
        // line 111
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fieldOfPractice", array()), "html", null, true);
        echo "</h4>
                                                    
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-tag\"></i> <b>Specialty:</b></h4>
                                                    ";
        // line 114
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "specialties", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
            // line 115
            echo "                                                     <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-circle\"></i> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["specialty"], "specialtyName", array()), "html", null, true);
            echo "</h4>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-group\"></i><b> Fellows:</b> </h4>
                                                    ";
        // line 118
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "fellowships", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["fellowship"]) {
            // line 119
            echo "                                                     <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-circle\"></i> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["fellowship"], "fellowshipName", array()), "html", null, true);
            echo "</h4>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fellowship'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 121
        echo "                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class=\"fa fa-credit-card\"></i> <b>PRC Number:</b> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "prcNumber", array()), "html", null, true);
        echo " acquired on
                                                                ";
        // line 122
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "origIssueDate", array()), "m/d/Y"), "html", null, true);
        echo "</h4>
                                                    
                                                    <h3></h3>
                                        
                                                    <h3 style=\" font-size: 16px;\"><b><i class=\"fa fa-phone-square\"></i>&emsp;CONTACT INFORMATION</b></h3>
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"> <i class=\"fa fa-at user-profile-icon\"></i> <b>Username:</b> ";
        // line 127
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "username", array()), "html", null, true);
        echo "</h4>
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"> <i class=\"fa fa-envelope user-profile-icon\"></i> <b>Email:</b>  ";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "emailAddress", array()), "html", null, true);
        echo "</h4>
                                                    <h4 style=\"font-family: Century Gothic; font-size: 16px\"> <i class=\"fa fa-phone user-profile-icon\"></i> <b>Contact Number:</b>  ";
        // line 129
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "contactNumber", array()), "html", null, true);
        echo "</h4>
                                                    
                                                    <br/>
                                                 
                                                     <br/>
                                                 ";
        // line 134
        if (((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")) == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()))) {
            // line 135
            echo "                                                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
            echo "\"><button class=\"btn btn-primary\"><i class=\"glyphicon glyphicon-edit icon-white\"></i> Edit Information</button></a>
                                                    ";
        }
        // line 137
        echo "                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"panel\">
                                            <a class=\"panel-heading collapsed\" role=\"tab\" id=\"headingTwo\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
                                                <h4 class=\"panel-title\"style=\"font-family: Century Gothic; font-size: 16px; color:#379DFF;\"><b><i class=\"fa fa-hospital-o\"></i>&emsp;CLINIC</b></h4>
                                            </a>
                                            <div id=\"collapseTwo\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">
                                                <div class=\"panel-body\">
                                                    <div class=\"x_content\">
                                                        ";
        // line 147
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctorClinics", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["clinic"]) {
            // line 148
            echo "                                                            <ul class=\"list-unstyled timeline\">
                                                                <li>
                                                                    <div class=\"block\">
                                                                        <div class=\"tags\">
                                                                            <a href=\"";
            // line 152
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute($context["clinic"], "id", array()))), "html", null, true);
            echo "\" class=\"tag\">
                                                                                <span><i class=\"fa fa-location-arrow\"></i> Clinic</span>
                                                                            </a>
                                                                        </div>
                                                                        <div class=\"block_content\">
                                                                            <h2 class=\"title\">
                                                                                <a href=\"";
            // line 158
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute($context["clinic"], "id", array()))), "html", null, true);
            echo "\"><h3 style=\"font-family: Century Gothic; font-size: 16px\"><b>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["clinic"], "clinicName", array()), "html", null, true);
            echo "</b></h3></a> 
                                                                            </h2>
                                                                            <div class=\"byline\">
                                                                                ";
            // line 161
            echo twig_escape_filter($this->env, $this->getAttribute($context["clinic"], "clinicAddress", array()), "html", null, true);
            echo "
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clinic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 168
        echo "                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"panel\">
                                            <a class=\"panel-heading collapsed\" role=\"tab\" id=\"headingThree\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
                                                <h4 class=\"panel-title\"style=\"font-family: Century Gothic; font-size: 16px; color:#379DFF;\"><b><i class=\"fa fa-users\"></i>&emsp;GROUPS</b></h4>
                                            </a>
                                            <div id=\"collapseThree\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\">
                                                <div class=\"panel-body\">
                                                    ";
        // line 178
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctorGroups", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["groups"]) {
            // line 179
            echo "                                                       <center>
                                                        <a href=\"";
            // line 180
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute($context["groups"], "id", array()))), "html", null, true);
            echo "\">
                                                            <div align=\"center\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Go to ";
            // line 181
            echo twig_escape_filter($this->env, $this->getAttribute($context["groups"], "groupName", array()), "html", null, true);
            echo "\" style=\"border-radius: 15px;  margin-bottom: 15px; margin-left: 35px; margin-right: 30px; background:#404040 ; height: 100px;
                                                                 \"class=\"col-md-3 col-xs-3 col-sm-3\">
                                                                <h3 style=\"font-family: Century Gothic; font-size: 16px; color:#379DFF;\"><b> ";
            // line 183
            echo twig_escape_filter($this->env, $this->getAttribute($context["groups"], "groupName", array()), "html", null, true);
            echo " </b></h3>
                                                                <h4 style=\"font-family: Century Gothic; font-size: 12px; color: white; overflow:hidden; white-space: nowrap; text-overflow:ellipsis; \"><i>";
            // line 184
            echo twig_escape_filter($this->env, $this->getAttribute($context["groups"], "description", array()), "html", null, true);
            echo "</i></h4>
                                                            </div>
                                                        </a>
                                                        </center>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['groups'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 189
        echo "                                                </div>
                                            </div>
                                        </div>
                                    </div>        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                                
    </div>
    <script src=\"js/bootstrap.min.js\"></script>

    <!-- chart js -->
    <script src=\"js/chartjs/chart.min.js\"></script>
    <!-- bootstrap progress js -->
    <script src=\"js/progressbar/bootstrap-progressbar.min.js\"></script>
    <script src=\"js/nicescroll/jquery.nicescroll.min.js\"></script>
    <!-- icheck -->
    <script src=\"js/icheck/icheck.min.js\"></script>

    <script src=\"js/custom.js\"></script>
    <!-- form validation -->
    <script src=\"js/validator/validator.js\"></script>
    <script type=\"text/javascript\" src=\"js/parsley/parsley.min.js\"></script>
    <!-- /form validation -->
";
        
        $__internal_dee1921ccff9cfec9b4316fe63a0e4b1f103847d15b6c0d88bd5a7d0487306ec->leave($__internal_dee1921ccff9cfec9b4316fe63a0e4b1f103847d15b6c0d88bd5a7d0487306ec_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Doctor:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  349 => 189,  338 => 184,  334 => 183,  329 => 181,  325 => 180,  322 => 179,  318 => 178,  306 => 168,  293 => 161,  285 => 158,  276 => 152,  270 => 148,  266 => 147,  254 => 137,  248 => 135,  246 => 134,  238 => 129,  234 => 128,  230 => 127,  222 => 122,  217 => 121,  208 => 119,  204 => 118,  201 => 117,  192 => 115,  188 => 114,  182 => 111,  178 => 110,  161 => 100,  88 => 29,  82 => 27,  76 => 25,  74 => 24,  53 => 5,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block profile %} class="active" {% endblock %}*/
/* {% block body -%}*/
/* */
/*         <div class="">*/
/*             <div class="page-title">*/
/*                */
/*                 </div>*/
/*             </div>*/
/*                     <div class="clearfix"></div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                             <div class="x_panel" >*/
/*                                     <h3>User Profile</h3>*/
/*                                 <div class="x_content">*/
/*                                     <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                                         <center>*/
/*                                         <div class="profile_img">*/
/*                                             <!-- end of image cropping -->*/
/*                                             <div id="crop-avatar">*/
/*                                                 <!-- Current avatar -->*/
/*                                                 <div class="avatar-view" title="Change the avatar">*/
/*                                                     {% if entity.getWebPath == null%}*/
/*                                                         <img src="{{asset('gentelella/images/picture.jpg')}}" alt="">*/
/*                                                     {% else %}*/
/*                                                     <img src="{{asset(entity.getWebPath)}}" alt="Avatar">*/
/*                                                     {% endif %}*/
/*                                                 </div>*/
/* */
/*                                                 <!-- Cropping modal -->*/
/*                                                 <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">*/
/*                                                     <div class="modal-dialog modal-lg">*/
/*                                                         <div class="modal-content">*/
/*                                                             <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">*/
/*                                                                 <div class="modal-header">*/
/*                                                                     <button class="close" data-dismiss="modal" type="button">&times;</button>*/
/*                                                                     <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>*/
/*                                                                 </div>*/
/*                                                                 <div class="modal-body">*/
/*                                                                     <div class="avatar-body">*/
/* */
/*                                                                         <!-- Upload image and data -->*/
/*                                                                         <div class="avatar-upload">*/
/*                                                                             <input class="avatar-src" name="avatar_src" type="hidden">*/
/*                                                                             <input class="avatar-data" name="avatar_data" type="hidden">*/
/*                                                                             <label for="avatarInput">Local upload</label>*/
/*                                                                             <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">*/
/*                                                                         </div>*/
/* */
/*                                                                         <!-- Crop and preview -->*/
/*                                                                         <div class="row">*/
/*                                                                             <div class="col-md-9">*/
/*                                                                                 <div class="avatar-wrapper"></div>*/
/*                                                                             </div>*/
/*                                                                             <div class="col-md-3">*/
/*                                                                                 <div class="avatar-preview preview-lg"></div>*/
/*                                                                                 <div class="avatar-preview preview-md"></div>*/
/*                                                                                 <div class="avatar-preview preview-sm"></div>*/
/*                                                                             </div>*/
/*                                                                         </div>*/
/* */
/*                                                                         <div class="row avatar-btns">*/
/*                                                                             <div class="col-md-9">*/
/*                                                                                 <div class="btn-group">*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>*/
/*                                                                                 </div>*/
/*                                                                                 <div class="btn-group">*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>*/
/*                                                                                 </div>*/
/*                                                                             </div>*/
/*                                                                             <div class="col-md-3">*/
/*                                                                                 <button class="btn btn-primary btn-block avatar-save" type="submit">Done</button>*/
/*                                                                             </div>*/
/*                                                                         </div>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                                 <!-- <div class="modal-footer">*/
/*                                                   <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>*/
/*                                                 </div> -->*/
/*                                                             </form>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <!-- /.modal -->*/
/* */
/*                                                 <!-- Loading state -->*/
/*                                                 <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>*/
/*                                             </div>*/
/*                                             <!-- end of image cropping -->*/
/* */
/*                                         </div>*/
/*                                         */
/*                                         <h3 style="font-size: 30px;">{{ entity.firstName }} {{ entity.middleName }} {{ entity.lastName }}</h3>       */
/*                                          */
/*                                         <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">*/
/*                                         <div class="panel">*/
/*                                             <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">*/
/*                                                  <h4 class="panel-title"style="font-family: Century Gothic; font-size: 16px; color:#379DFF;"><b><i class="fa fa-user-md"></i>&emsp;MEDICAL PRACTICE and CONTACT INFORMATION</b></h4>*/
/*                                             </a>*/
/*                                             <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">*/
/*                                                 <div class="panel-body">*/
/*                                                     <h3 style=" font-size: 16px; "><b><i class="fa fa-medkit"></i>&emsp;MEDICAL PRACTICE</b></h3>*/
/*                                                     <h4 style="font-family: Century Gothic; font-size: 16px"><i><p>{{ entity.aboutMyPractice }}</p></i></h4></br>*/
/*                                                     <h4 style="font-family: Century Gothic; font-size: 16px"><i class="fa fa-stethoscope"></i> <b>Field of Practice:</b> {{ entity.fieldOfPractice }}</h4>*/
/*                                                     */
/*                                                     <h4 style="font-family: Century Gothic; font-size: 16px"><i class="fa fa-tag"></i> <b>Specialty:</b></h4>*/
/*                                                     {% for specialty in entity.specialties %}*/
/*                                                      <h4 style="font-family: Century Gothic; font-size: 16px"><i class="fa fa-circle"></i> {{ specialty.specialtyName }}</h4>*/
/*                                                     {% endfor %}*/
/*                                                     <h4 style="font-family: Century Gothic; font-size: 16px"><i class="fa fa-group"></i><b> Fellows:</b> </h4>*/
/*                                                     {% for fellowship in entity.fellowships %}*/
/*                                                      <h4 style="font-family: Century Gothic; font-size: 16px"><i class="fa fa-circle"></i> {{ fellowship.fellowshipName }}</h4>*/
/*                                                     {% endfor %}*/
/*                                                     <h4 style="font-family: Century Gothic; font-size: 16px"><i class="fa fa-credit-card"></i> <b>PRC Number:</b> {{ entity.prcNumber }} acquired on*/
/*                                                                 {{ entity.origIssueDate | date("m/d/Y") }}</h4>*/
/*                                                     */
/*                                                     <h3></h3>*/
/*                                         */
/*                                                     <h3 style=" font-size: 16px;"><b><i class="fa fa-phone-square"></i>&emsp;CONTACT INFORMATION</b></h3>*/
/*                                                     <h4 style="font-family: Century Gothic; font-size: 16px"> <i class="fa fa-at user-profile-icon"></i> <b>Username:</b> {{ entity.username }}</h4>*/
/*                                                     <h4 style="font-family: Century Gothic; font-size: 16px"> <i class="fa fa-envelope user-profile-icon"></i> <b>Email:</b>  {{ entity.emailAddress }}</h4>*/
/*                                                     <h4 style="font-family: Century Gothic; font-size: 16px"> <i class="fa fa-phone user-profile-icon"></i> <b>Contact Number:</b>  {{ entity.contactNumber }}</h4>*/
/*                                                     */
/*                                                     <br/>*/
/*                                                  */
/*                                                      <br/>*/
/*                                                  {% if entity == app.user %}*/
/*                                                         <a href="{{path('doctor_edit',{'id':entity.id})}}"><button class="btn btn-primary"><i class="glyphicon glyphicon-edit icon-white"></i> Edit Information</button></a>*/
/*                                                     {% endif %}*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="panel">*/
/*                                             <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">*/
/*                                                 <h4 class="panel-title"style="font-family: Century Gothic; font-size: 16px; color:#379DFF;"><b><i class="fa fa-hospital-o"></i>&emsp;CLINIC</b></h4>*/
/*                                             </a>*/
/*                                             <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">*/
/*                                                 <div class="panel-body">*/
/*                                                     <div class="x_content">*/
/*                                                         {% for clinic in entity.getDoctorClinics %}*/
/*                                                             <ul class="list-unstyled timeline">*/
/*                                                                 <li>*/
/*                                                                     <div class="block">*/
/*                                                                         <div class="tags">*/
/*                                                                             <a href="{{path('clinic_show', {'id' : clinic.id})}}" class="tag">*/
/*                                                                                 <span><i class="fa fa-location-arrow"></i> Clinic</span>*/
/*                                                                             </a>*/
/*                                                                         </div>*/
/*                                                                         <div class="block_content">*/
/*                                                                             <h2 class="title">*/
/*                                                                                 <a href="{{path('clinic_show', {'id' : clinic.id})}}"><h3 style="font-family: Century Gothic; font-size: 16px"><b>{{clinic.clinicName}}</b></h3></a> */
/*                                                                             </h2>*/
/*                                                                             <div class="byline">*/
/*                                                                                 {{clinic.clinicAddress}}*/
/*                                                                             </div>*/
/*                                                                         </div>*/
/*                                                                     </div>*/
/*                                                                 </li>*/
/*                                                             </ul>*/
/*                                                         {% endfor %}*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <div class="panel">*/
/*                                             <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">*/
/*                                                 <h4 class="panel-title"style="font-family: Century Gothic; font-size: 16px; color:#379DFF;"><b><i class="fa fa-users"></i>&emsp;GROUPS</b></h4>*/
/*                                             </a>*/
/*                                             <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">*/
/*                                                 <div class="panel-body">*/
/*                                                     {% for groups in entity.getDoctorGroups %}*/
/*                                                        <center>*/
/*                                                         <a href="{{path('doctorgroup_show', {'id' : groups.id})}}">*/
/*                                                             <div align="center" data-toggle="tooltip" data-placement="top" title="Go to {{groups.groupName}}" style="border-radius: 15px;  margin-bottom: 15px; margin-left: 35px; margin-right: 30px; background:#404040 ; height: 100px;*/
/*                                                                  "class="col-md-3 col-xs-3 col-sm-3">*/
/*                                                                 <h3 style="font-family: Century Gothic; font-size: 16px; color:#379DFF;"><b> {{groups.groupName}} </b></h3>*/
/*                                                                 <h4 style="font-family: Century Gothic; font-size: 12px; color: white; overflow:hidden; white-space: nowrap; text-overflow:ellipsis; "><i>{{groups.description}}</i></h4>*/
/*                                                             </div>*/
/*                                                         </a>*/
/*                                                         </center>*/
/*                                                     {% endfor %}*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>        */
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                                                                 */
/*     </div>*/
/*     <script src="js/bootstrap.min.js"></script>*/
/* */
/*     <!-- chart js -->*/
/*     <script src="js/chartjs/chart.min.js"></script>*/
/*     <!-- bootstrap progress js -->*/
/*     <script src="js/progressbar/bootstrap-progressbar.min.js"></script>*/
/*     <script src="js/nicescroll/jquery.nicescroll.min.js"></script>*/
/*     <!-- icheck -->*/
/*     <script src="js/icheck/icheck.min.js"></script>*/
/* */
/*     <script src="js/custom.js"></script>*/
/*     <!-- form validation -->*/
/*     <script src="js/validator/validator.js"></script>*/
/*     <script type="text/javascript" src="js/parsley/parsley.min.js"></script>*/
/*     <!-- /form validation -->*/
/* {% endblock %}*/
/* */
