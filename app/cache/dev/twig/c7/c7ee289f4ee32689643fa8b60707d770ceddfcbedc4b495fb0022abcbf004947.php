<?php

/* ResearchProjectMyProjectBundle:Form:field.html.twig */
class __TwigTemplate_26d27551aed0945195d150a64b8ae56137fb488a3b1e879ca806c5bed3b2971c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'choice_widget_options' => array($this, 'block_choice_widget_options'),
            'form_errors' => array($this, 'block_form_errors'),
            'widget_attributes' => array($this, 'block_widget_attributes'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe54e717cc65600b1d7d7142acdadca84c7692871c22f9bd062ddc2b8d1cff5b = $this->env->getExtension("native_profiler");
        $__internal_fe54e717cc65600b1d7d7142acdadca84c7692871c22f9bd062ddc2b8d1cff5b->enter($__internal_fe54e717cc65600b1d7d7142acdadca84c7692871c22f9bd062ddc2b8d1cff5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Form:field.html.twig"));

        // line 1
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 23
        echo "
";
        // line 24
        $this->displayBlock('choice_widget_options', $context, $blocks);
        // line 40
        $this->displayBlock('form_errors', $context, $blocks);
        // line 50
        $this->displayBlock('widget_attributes', $context, $blocks);
        
        $__internal_fe54e717cc65600b1d7d7142acdadca84c7692871c22f9bd062ddc2b8d1cff5b->leave($__internal_fe54e717cc65600b1d7d7142acdadca84c7692871c22f9bd062ddc2b8d1cff5b_prof);

    }

    // line 1
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_f9dc0454fb2ee60deaea78fb96e4696513a46d5ecc02d63b5c8a33bdf6b11127 = $this->env->getExtension("native_profiler");
        $__internal_f9dc0454fb2ee60deaea78fb96e4696513a46d5ecc02d63b5c8a33bdf6b11127->enter($__internal_f9dc0454fb2ee60deaea78fb96e4696513a46d5ecc02d63b5c8a33bdf6b11127_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 2
        ob_start();
        // line 3
        echo "    
        <select ";
        // line 4
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ((isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple"))) {
            echo " multiple=\"multiple\"";
        }
        echo ">
    
     ";
        // line 6
        if ( !(null === (isset($context["empty_value"]) ? $context["empty_value"] : $this->getContext($context, "empty_value")))) {
            // line 7
            echo "            <option value=\"\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["empty_value"]) ? $context["empty_value"] : $this->getContext($context, "empty_value")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
            echo "</option>
        ";
        }
        // line 9
        echo "        ";
        if ((twig_length_filter($this->env, (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"))) > 0)) {
            // line 10
            echo "            ";
            $context["options"] = (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"));
            // line 11
            echo "            ";
            $this->displayBlock("choice_widget_options", $context, $blocks);
            echo "
            ";
            // line 12
            if (((twig_length_filter($this->env, (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"))) > 0) &&  !(null === (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator"))))) {
                // line 13
                echo "                <option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator")), "html", null, true);
                echo "</option>
            ";
            }
            // line 15
            echo "        ";
        }
        // line 16
        echo "        ";
        $context["options"] = (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"));
        // line 17
        echo "        ";
        $this->displayBlock("choice_widget_options", $context, $blocks);
        echo "
    
    </select>
    
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_f9dc0454fb2ee60deaea78fb96e4696513a46d5ecc02d63b5c8a33bdf6b11127->leave($__internal_f9dc0454fb2ee60deaea78fb96e4696513a46d5ecc02d63b5c8a33bdf6b11127_prof);

    }

    // line 24
    public function block_choice_widget_options($context, array $blocks = array())
    {
        $__internal_568a952cb85f8fbb95cb5b76014b0fff64f7b7819f7946927bfbf55bbecb30a0 = $this->env->getExtension("native_profiler");
        $__internal_568a952cb85f8fbb95cb5b76014b0fff64f7b7819f7946927bfbf55bbecb30a0->enter($__internal_568a952cb85f8fbb95cb5b76014b0fff64f7b7819f7946927bfbf55bbecb30a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_options"));

        // line 25
        ob_start();
        // line 26
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["group_label"] => $context["choice"]) {
            // line 27
            echo "        ";
            if (twig_test_iterable($context["choice"])) {
                // line 28
                echo "            <optgroup label=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["group_label"], array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
                echo "\">
                ";
                // line 29
                $context["options"] = $context["choice"];
                // line 30
                echo "                ";
                $this->displayBlock("choice_widget_options", $context, $blocks);
                echo "
            </optgroup>
        ";
            } else {
                // line 33
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["choice"], "value", array()), "html", null, true);
                echo "\"";
                if ($this->env->getExtension('form')->isSelectedChoice($context["choice"], (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
                    echo " selected=\"selected\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($context["choice"], "label", array()), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
                echo "</option>
        ";
            }
            // line 35
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['group_label'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_568a952cb85f8fbb95cb5b76014b0fff64f7b7819f7946927bfbf55bbecb30a0->leave($__internal_568a952cb85f8fbb95cb5b76014b0fff64f7b7819f7946927bfbf55bbecb30a0_prof);

    }

    // line 40
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_0dbc2d4008c39b7b77828b54d5a529870d8aeefca76200c0367db988952e213e = $this->env->getExtension("native_profiler");
        $__internal_0dbc2d4008c39b7b77828b54d5a529870d8aeefca76200c0367db988952e213e->enter($__internal_0dbc2d4008c39b7b77828b54d5a529870d8aeefca76200c0367db988952e213e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 41
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 43
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_0dbc2d4008c39b7b77828b54d5a529870d8aeefca76200c0367db988952e213e->leave($__internal_0dbc2d4008c39b7b77828b54d5a529870d8aeefca76200c0367db988952e213e_prof);

    }

    // line 50
    public function block_widget_attributes($context, array $blocks = array())
    {
        $__internal_8a12493de37cff847e8719fe251a37c1daaf01c6e772e2a37ac2604f07617ac0 = $this->env->getExtension("native_profiler");
        $__internal_8a12493de37cff847e8719fe251a37c1daaf01c6e772e2a37ac2604f07617ac0->enter($__internal_8a12493de37cff847e8719fe251a37c1daaf01c6e772e2a37ac2604f07617ac0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_attributes"));

        // line 51
        ob_start();
        // line 52
        echo "        ";
        // line 53
        echo "        ";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 54
            echo "            ";
            $context["errorClass"] = "parsley-error";
            // line 55
            echo "            ";
            if ($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) {
                // line 56
                echo "                ";
                $context["errorClass"] = (((isset($context["errorClass"]) ? $context["errorClass"] : $this->getContext($context, "errorClass")) . " ") . $this->getAttribute((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), "class", array()));
                // line 57
                echo "            ";
            }
            // line 58
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => (isset($context["errorClass"]) ? $context["errorClass"] : $this->getContext($context, "errorClass"))));
            // line 59
            echo "        ";
        }
        // line 60
        echo "        ";
        echo "    
            
    id=\"";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, (isset($context["full_name"]) ? $context["full_name"] : $this->getContext($context, "full_name")), "html", null, true);
        echo "\"";
        if ((isset($context["read_only"]) ? $context["read_only"] : $this->getContext($context, "read_only"))) {
            echo " readonly=\"readonly\"";
        }
        if ((isset($context["disabled"]) ? $context["disabled"] : $this->getContext($context, "disabled"))) {
            echo " disabled=\"disabled\"";
        }
        if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
            echo " required=\"required\"";
        }
        if ((isset($context["max_length"]) ? $context["max_length"] : $this->getContext($context, "max_length"))) {
            echo " maxlength=\"";
            echo twig_escape_filter($this->env, (isset($context["max_length"]) ? $context["max_length"] : $this->getContext($context, "max_length")), "html", null, true);
            echo "\"";
        }
        if ((isset($context["pattern"]) ? $context["pattern"] : $this->getContext($context, "pattern"))) {
            echo " pattern=\"";
            echo twig_escape_filter($this->env, (isset($context["pattern"]) ? $context["pattern"] : $this->getContext($context, "pattern")), "html", null, true);
            echo "\"";
        }
        // line 63
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            if (twig_in_filter($context["attrname"], array(0 => "placeholder", 1 => "title"))) {
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($context["attrvalue"], array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))), "html", null, true);
                echo "\" ";
            } else {
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\" ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_8a12493de37cff847e8719fe251a37c1daaf01c6e772e2a37ac2604f07617ac0->leave($__internal_8a12493de37cff847e8719fe251a37c1daaf01c6e772e2a37ac2604f07617ac0_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Form:field.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  267 => 63,  243 => 62,  238 => 60,  235 => 59,  232 => 58,  229 => 57,  226 => 56,  223 => 55,  220 => 54,  217 => 53,  215 => 52,  213 => 51,  207 => 50,  195 => 44,  191 => 43,  189 => 41,  183 => 40,  163 => 35,  151 => 33,  144 => 30,  142 => 29,  137 => 28,  134 => 27,  116 => 26,  114 => 25,  108 => 24,  94 => 17,  91 => 16,  88 => 15,  82 => 13,  80 => 12,  75 => 11,  72 => 10,  69 => 9,  63 => 7,  61 => 6,  53 => 4,  50 => 3,  48 => 2,  42 => 1,  35 => 50,  33 => 40,  31 => 24,  28 => 23,  26 => 1,);
    }
}
/* {% block choice_widget_collapsed %}*/
/* {% spaceless %}*/
/*     */
/*         <select {{ block('widget_attributes') }}{% if multiple %} multiple="multiple"{% endif %}>*/
/*     */
/*      {% if empty_value is not none %}*/
/*             <option value="">{{ empty_value|trans({}, translation_domain) }}</option>*/
/*         {% endif %}*/
/*         {% if preferred_choices|length > 0 %}*/
/*             {% set options = preferred_choices %}*/
/*             {{ block('choice_widget_options') }}*/
/*             {% if choices|length > 0 and separator is not none %}*/
/*                 <option disabled="disabled">{{ separator }}</option>*/
/*             {% endif %}*/
/*         {% endif %}*/
/*         {% set options = choices %}*/
/*         {{ block('choice_widget_options') }}*/
/*     */
/*     </select>*/
/*     */
/* {% endspaceless %}*/
/* {% endblock choice_widget_collapsed %}*/
/* */
/* {% block choice_widget_options %}*/
/* {% spaceless %}*/
/*     {% for group_label, choice in options %}*/
/*         {% if choice is iterable %}*/
/*             <optgroup label="{{ group_label|trans({}, translation_domain) }}">*/
/*                 {% set options = choice %}*/
/*                 {{ block('choice_widget_options') }}*/
/*             </optgroup>*/
/*         {% else %}*/
/*             <option value="{{ choice.value }}"{% if choice is selectedchoice(value) %} selected="selected"{% endif %}>{{ choice.label|trans({}, translation_domain) }}</option>*/
/*         {% endif %}*/
/*     {% endfor %}*/
/* {% endspaceless %}*/
/* {% endblock choice_widget_options %}*/
/* */
/*             */
/* {%- block form_errors -%}*/
/*     {%- if errors|length > 0 -%}*/
/*     */
/*         {%- for error in errors -%}*/
/*             {{ error.message }}*/
/*         {%- endfor -%}*/
/*    */
/*     {%- endif -%}*/
/* {%- endblock form_errors -%}*/
/*             */
/* {% block widget_attributes %}*/
/* {% spaceless %}*/
/*         {# ADD ERROR START #}*/
/*         {% if errors|length > 0 %}*/
/*             {% set errorClass = 'parsley-error' %}*/
/*             {% if attr.class is defined %}*/
/*                 {% set errorClass = errorClass ~ ' ' ~ attr.class %}*/
/*             {% endif %}*/
/*             {% set attr = attr|merge({'class': errorClass}) %}*/
/*         {% endif %}*/
/*         {# ADD ERROR END #}    */
/*             */
/*     id="{{ id }}" name="{{ full_name }}"{% if read_only %} readonly="readonly"{% endif %}{% if disabled %} disabled="disabled"{% endif %}{% if required %} required="required"{% endif %}{% if max_length %} maxlength="{{ max_length }}"{% endif %}{% if pattern %} pattern="{{ pattern }}"{% endif %}*/
/*     {% for attrname, attrvalue in attr %}{% if attrname in ['placeholder', 'title'] %}{{ attrname }}="{{ attrvalue|trans({}, translation_domain) }}" {% else %}{{ attrname }}="{{ attrvalue }}" {% endif %}{% endfor %}*/
/* {% endspaceless %}*/
/* {% endblock widget_attributes %}*/
