<?php

/* ResearchProjectMyProjectBundle:GroupPost:show.html.twig */
class __TwigTemplate_cd1baea29a617ffe86e7a2787a8c0a0048ef229ecc9897411fefda95aea718b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_doctorgroup($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        echo "DOCTOR GROUP";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                <h3>&nbsp;DOCTOR GROUP
                <a class=\"right\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getGroup"), "getId"))), "html", null, true);
        echo "\"><button class=\"btn btn-primary\">Back to Group</button></a></h3>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                        <h3>Group Post</h3>
                    <div class=\"x_content\">
                    <h3 style=\"font-size: 20px;\">
                        <b>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "postTitle"), "html", null, true);
        echo "</b>
                        <br/>
                        <i style=\"font-size: 11px;\">";
        // line 23
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "datePosted"), "Y-m-d H:i:s"), "html", null, true);
        echo "</i>
                    </h3>
                     <div style=\"word-wrap: break-word;\">
                         <h3 style=\"font-size: 14px;\"><i class='fa fa-circle'></i> &emsp; ";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "postContent"), "html", null, true);
        echo " </h3>
                     </div>
                    ";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getPostComments"), 0, 1));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 29
            echo "                        ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user") == $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "getCommentAuthor"))) {
                // line 30
                echo "                            <a href='";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grouppost_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "'><button class='btn btn-primary btn-xs' >Edit Group Post</button></a>
                        ";
            } else {
                // line 32
                echo "                            <div></div>
                        ";
            }
            // line 34
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                    <h3 style='margin-bottom: 3px;'>Comments:</h3><br/>
                    ";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getPostComments"));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 37
            echo "                                <div class=\"alert alert-info\" style=\"margin-bottom: 3px;\">
                                    <div style=\"word-wrap: break-word;\"><h4 style=\"font-family: Century Gothic; font-size: 15px;\"><i class=\"fa fa-circle-o\"></i> &emsp;";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "getCommentContent"), "html", null, true);
            echo "<br/></div>
                                     &emsp;&emsp;<i style=\"font-size: 11px;\"> <b>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "getCommentAuthor"), "firstName"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "getCommentAuthor"), "lastName"), "html", null, true);
            echo "</b>
                                         (";
            // line 40
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "dateCommented"), "Y-m-d H:i:s"), "html", null, true);
            echo ")
                                     
                                     </i>
                                     </h4>
                                 ";
            // line 44
            if (($this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "commentAuthor") == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"))) {
                // line 45
                echo "                                        <div>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "id"), "html", null, true);
                echo "  <a  id=\"dlt_comment-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["comment"]) ? $context["comment"] : $this->getContext($context, "comment")), "id"), "html", null, true);
                echo "\" class=\"btn btn-danger btn-xs btn-delete-comment\" href=\"#\">
                                                            <i class=\"glyphicon glyphicon-trash icon-white\"></i>
                                                            Delete
                                            </a></div>
                                 ";
            }
            // line 50
            echo "                                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                    
                    ";
        // line 53
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity1"]) ? $context["entity1"] : $this->getContext($context, "entity1")), "getDoctors"));
        foreach ($context['_seq'] as $context["_key"] => $context["doc"]) {
            // line 54
            echo "                        ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user") == (isset($context["doc"]) ? $context["doc"] : $this->getContext($context, "doc")))) {
                // line 55
                echo "                            <button type=\"button\" class=\"btn btn-info right\" data-toggle=\"modal\" data-target=\"#commentmodal\">Add a Comment</button>
                        ";
            } else {
                // line 57
                echo "                            <div></div>
                        ";
            }
            // line 59
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doc'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "                        
                                <div id=\"commentmodal\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog modal-lg\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                                </button>
                                                <h3 class=\"modal-title\" id=\"myModalLabel\">New Comment</h3>
                                            </div>
                                            <div class=\"modal-body\">
                                                ";
        // line 71
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "newComment", "class" => "form-horizontal form-label-left")));
        echo "
                                                <label class=\"control-label col-md-3 col-sm-3 col-xs-6 left-align\" >Comment Content: </label>
                                                    <div class=\"col-md-7\">
                                                        ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commentContent"), 'widget', array("attr" => array("name" => "commentContent", "placeholder" => "Post your comment here", "class" => "form-control col-md-7 col-xs-7", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 75
        echo "
                                                    </div> 
                                            </div>
                                            <div class=\"modal-footer\">
                                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                <a href=\"\">";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Comment"), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn", "onClick" => "submitClicked()")));
        echo "</a> 
                                                
                                            </div>
                                            ";
        // line 83
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

 <div id=\"delete\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" >
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h2 class=\"modal-title\" id=\"deleteModal\">Delete Comment</h2>
                                            </div>

                                            <div class=\"modal-body confirm\">
                                                Are you sure you want to remove this comment?
                                                <div class=\"modal-footer\">
                                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                    <button type=\"button\" id='con_delete_comment' class=\"marginTop btn btn-primary col-sm-2 col-sm-offset-5\" >Yes</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
<script>
        function goBack() {
        window.history.back();
        }
    </script>
    
    <script>
                                    var dltCommentBtnId;
                                    \$(\".btn-delete-comment\").click(function(x) {
                                        x.preventDefault();
                                        dltCommentBtnId = this.id;
                                        dltCommentBtnId = dltCommentBtnId.replace('dlt_comment-', '');
                                        \$('#delete').modal();
                                    });
                                    \$(\"#con_delete_comment\").click(function(d) {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 129
        echo $this->env->getExtension('routing')->getPath("comment_delete");
        echo "',
                                            data: {id: dltCommentBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                                
                                                console.log(response);
                                                location.reload();
                                            },
                                            error: function() {
                                                alert('error');
                                                
                                            }
                                        });
                                    });
                                   
                                   
</script>
    
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:GroupPost:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 129,  209 => 83,  203 => 80,  196 => 75,  194 => 74,  188 => 71,  175 => 60,  169 => 59,  165 => 57,  161 => 55,  158 => 54,  154 => 53,  151 => 52,  144 => 50,  133 => 45,  131 => 44,  124 => 40,  118 => 39,  114 => 38,  111 => 37,  107 => 36,  104 => 35,  98 => 34,  94 => 32,  88 => 30,  85 => 29,  81 => 28,  76 => 26,  70 => 23,  65 => 21,  53 => 12,  47 => 8,  44 => 6,  38 => 4,  32 => 3,  27 => 2,);
    }
}
