<?php

/* ResearchProjectMyProjectBundle:Admin:login.html.twig */
class __TwigTemplate_6f2ae2c264402ffc5a065a3faa9ba0748891ce43f0afde2439a692f54b89eb22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ea143e5d9ce560847b82822bb89510019ee2d1fd378b83562d638ce18fe36e84 = $this->env->getExtension("native_profiler");
        $__internal_ea143e5d9ce560847b82822bb89510019ee2d1fd378b83562d638ce18fe36e84->enter($__internal_ea143e5d9ce560847b82822bb89510019ee2d1fd378b83562d638ce18fe36e84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Gentallela Alela! | </title>

    <!-- Bootstrap core CSS -->

   <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom styling plus plugins -->
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/css/icheck/flat/green.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">


    <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelellaoriginal/js/jquery.min.js"), "html", null, true);
        echo "\"></script>

    <!--[if lt IE 9]>
        <script src=\"../assets/js/ie8-responsive-file-warning.js\"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
          <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->

</head>

<body style=\"background:#F7F7F7;\">
    
    <div class=\"\">
        <a class=\"hiddenanchor\" id=\"toregister\"></a>
        <a class=\"hiddenanchor\" id=\"tologin\"></a>

        <div id=\"wrapper\">
            <div id=\"login\" class=\"animate form\">
                <section class=\"login_content\">
                    <form id=\"loginform\" class=\"form-signin\" role=\"form\" action=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("admin_login_check");
        echo "\" method=\"post\">
                        <img style =\"margin-bottom: 30px; \"src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("retina/default/logo.png"), "html", null, true);
        echo "\">
                        <div><h4>ADMIN LOGIN</h4></div>
                        <div>
                            <input type=\"text\" class=\"form-control\" placeholder=\"Username\" required autofocus id=\"username\" name=\"_username\" value=\"";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" />
                        </div>
                        <div>
                            <input type=\"password\" class=\"form-control\" placeholder=\"Password\" id=\"password\" name=\"_password\" required />
                        </div>
                        <div>
                            
                            <button type=\"submit\" id=\"submit\" class=\"btn btn-default submit\" >Log in</button>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id=\"register\" class=\"animate form\">
                <section class=\"login_content\">
                    <form>
                        <h1>Create Account</h1>
                        <div>
                            <input type=\"text\" class=\"form-control\" placeholder=\"Username\" required=\"\" />
                        </div>
                        <div>
                            <input type=\"email\" class=\"form-control\" placeholder=\"Email\" required=\"\" />
                        </div>
                        <div>
                            <input type=\"password\" class=\"form-control\" placeholder=\"Password\" required=\"\" />
                        </div>
                        <div>
                            <a class=\"btn btn-default submit\" href=\"index.html\">Submit</a>
                        </div>
                        <div class=\"clearfix\"></div>
                        <div class=\"separator\">

                            <p class=\"change_link\">Already a member ?
                                <a href=\"#tologin\" class=\"to_register\"> Log in </a>
                            </p>
                            <div class=\"clearfix\"></div>
                            <br />
                            <div>
                                <h1><i class=\"fa fa-paw\" style=\"font-size: 26px;\"></i> Gentelella Alela!</h1>

                                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

</body>

</html>";
        
        $__internal_ea143e5d9ce560847b82822bb89510019ee2d1fd378b83562d638ce18fe36e84->leave($__internal_ea143e5d9ce560847b82822bb89510019ee2d1fd378b83562d638ce18fe36e84_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 52,  93 => 49,  89 => 48,  63 => 25,  57 => 22,  53 => 21,  47 => 18,  43 => 17,  38 => 15,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* */
/* <head>*/
/*     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">*/
/*     <!-- Meta, title, CSS, favicons, etc. -->*/
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* */
/*     <title>Gentallela Alela! | </title>*/
/* */
/*     <!-- Bootstrap core CSS -->*/
/* */
/*    <link href="{{asset('gentelellaoriginal/css/bootstrap.min.css')}}" rel="stylesheet">*/
/* */
/*     <link href="{{asset('gentelellaoriginal/css/font-awesome.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('gentelellaoriginal/css/animate.min.css')}}" rel="stylesheet">*/
/* */
/*     <!-- Custom styling plus plugins -->*/
/*     <link href="{{asset('gentelellaoriginal/css/custom.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('gentelellaoriginal/css/icheck/flat/green.css')}}" rel="stylesheet">*/
/* */
/* */
/*     <script src="{{asset('gentelellaoriginal/js/jquery.min.js')}}"></script>*/
/* */
/*     <!--[if lt IE 9]>*/
/*         <script src="../assets/js/ie8-responsive-file-warning.js"></script>*/
/*         <![endif]-->*/
/* */
/*     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->*/
/*     <!--[if lt IE 9]>*/
/*           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>*/
/*           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>*/
/*         <![endif]-->*/
/* */
/* </head>*/
/* */
/* <body style="background:#F7F7F7;">*/
/*     */
/*     <div class="">*/
/*         <a class="hiddenanchor" id="toregister"></a>*/
/*         <a class="hiddenanchor" id="tologin"></a>*/
/* */
/*         <div id="wrapper">*/
/*             <div id="login" class="animate form">*/
/*                 <section class="login_content">*/
/*                     <form id="loginform" class="form-signin" role="form" action="{{ path('admin_login_check') }}" method="post">*/
/*                         <img style ="margin-bottom: 30px; "src="{{asset('retina/default/logo.png')}}">*/
/*                         <div><h4>ADMIN LOGIN</h4></div>*/
/*                         <div>*/
/*                             <input type="text" class="form-control" placeholder="Username" required autofocus id="username" name="_username" value="{{ last_username }}" />*/
/*                         </div>*/
/*                         <div>*/
/*                             <input type="password" class="form-control" placeholder="Password" id="password" name="_password" required />*/
/*                         </div>*/
/*                         <div>*/
/*                             */
/*                             <button type="submit" id="submit" class="btn btn-default submit" >Log in</button>*/
/*                         </div>*/
/*                     </form>*/
/*                     <!-- form -->*/
/*                 </section>*/
/*                 <!-- content -->*/
/*             </div>*/
/*             <div id="register" class="animate form">*/
/*                 <section class="login_content">*/
/*                     <form>*/
/*                         <h1>Create Account</h1>*/
/*                         <div>*/
/*                             <input type="text" class="form-control" placeholder="Username" required="" />*/
/*                         </div>*/
/*                         <div>*/
/*                             <input type="email" class="form-control" placeholder="Email" required="" />*/
/*                         </div>*/
/*                         <div>*/
/*                             <input type="password" class="form-control" placeholder="Password" required="" />*/
/*                         </div>*/
/*                         <div>*/
/*                             <a class="btn btn-default submit" href="index.html">Submit</a>*/
/*                         </div>*/
/*                         <div class="clearfix"></div>*/
/*                         <div class="separator">*/
/* */
/*                             <p class="change_link">Already a member ?*/
/*                                 <a href="#tologin" class="to_register"> Log in </a>*/
/*                             </p>*/
/*                             <div class="clearfix"></div>*/
/*                             <br />*/
/*                             <div>*/
/*                                 <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>*/
/* */
/*                                 <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </form>*/
/*                     <!-- form -->*/
/*                 </section>*/
/*                 <!-- content -->*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* </body>*/
/* */
/* </html>*/
