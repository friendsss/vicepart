<?php

/* ResearchProjectMyProjectBundle:Doctor:home.html.twig */
class __TwigTemplate_061d226c450d39ea1c3aa7525c4dfdcb7a2bba0593edceacc94b5e68041aee2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basesidebar.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basesidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " FIMADILO - Welcome! ";
    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        echo " Home ";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        echo " This is home! ";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Doctor:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 5,  36 => 4,  30 => 2,);
    }
}
