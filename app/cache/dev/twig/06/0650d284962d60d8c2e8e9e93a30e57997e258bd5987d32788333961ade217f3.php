<?php

/* ResearchProjectMyProjectBundle:DoctorGroup:new.html.twig */
class __TwigTemplate_a30a24b93c40e5c72520512addccce40d6855dc124458af3e219d08a5c541428 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:DoctorGroup:new.html.twig", 1);
        $this->blocks = array(
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9f8c1a0c7f351755778f6e155915e8b4c1d3b5badf04aed5ba923b7da5443877 = $this->env->getExtension("native_profiler");
        $__internal_9f8c1a0c7f351755778f6e155915e8b4c1d3b5badf04aed5ba923b7da5443877->enter($__internal_9f8c1a0c7f351755778f6e155915e8b4c1d3b5badf04aed5ba923b7da5443877_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:DoctorGroup:new.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9f8c1a0c7f351755778f6e155915e8b4c1d3b5badf04aed5ba923b7da5443877->leave($__internal_9f8c1a0c7f351755778f6e155915e8b4c1d3b5badf04aed5ba923b7da5443877_prof);

    }

    // line 3
    public function block_doctorgroup($context, array $blocks = array())
    {
        $__internal_2c77a24ab54734a3a887dae64163aeb226cc31263816bb04c67543524e8b6c3a = $this->env->getExtension("native_profiler");
        $__internal_2c77a24ab54734a3a887dae64163aeb226cc31263816bb04c67543524e8b6c3a->enter($__internal_2c77a24ab54734a3a887dae64163aeb226cc31263816bb04c67543524e8b6c3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctorgroup"));

        echo " class=\"active\" ";
        
        $__internal_2c77a24ab54734a3a887dae64163aeb226cc31263816bb04c67543524e8b6c3a->leave($__internal_2c77a24ab54734a3a887dae64163aeb226cc31263816bb04c67543524e8b6c3a_prof);

    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_cdaeedf96e936bf794a3c9e44feede7316a5729a40b0fbfaf3a36a8971ad5be9 = $this->env->getExtension("native_profiler");
        $__internal_cdaeedf96e936bf794a3c9e44feede7316a5729a40b0fbfaf3a36a8971ad5be9->enter($__internal_cdaeedf96e936bf794a3c9e44feede7316a5729a40b0fbfaf3a36a8971ad5be9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "DOCTOR GROUP";
        
        $__internal_cdaeedf96e936bf794a3c9e44feede7316a5729a40b0fbfaf3a36a8971ad5be9->leave($__internal_cdaeedf96e936bf794a3c9e44feede7316a5729a40b0fbfaf3a36a8971ad5be9_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_d8e3baa817a15f4a9ee828791abbe7906381b2f69b36da2af3c3e510544d7cf3 = $this->env->getExtension("native_profiler");
        $__internal_d8e3baa817a15f4a9ee828791abbe7906381b2f69b36da2af3c3e510544d7cf3->enter($__internal_d8e3baa817a15f4a9ee828791abbe7906381b2f69b36da2af3c3e510544d7cf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<center><h1><b>Create New Doctor Group</b></h1>
    <div class=\"x_content\">
        <br/>
        <div data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 10
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "createNewGroup")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Group Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "groupName", array()), 'widget', array("attr" => array("name" => "groupName", "placeholder" => "Group Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 16
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "groupName", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Description <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description", array()), 'widget', array("attr" => array("name" => "description", "placeholder" => "Description", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 25
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "description", array()), 'errors');
        echo "</div></i>
            
            <div class=\"ln_solid\"></div>
            <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                <a href=\"\">";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn", "onClick" => "submitClicked()")));
        echo "</a>
                <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
            </div>
        </div>
        
        
        ";
        // line 38
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
        
    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
<!-- form validation -->
    <script src=";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#createNewGroup')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#createNewGroup').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
";
        
        $__internal_d8e3baa817a15f4a9ee828791abbe7906381b2f69b36da2af3c3e510544d7cf3->leave($__internal_d8e3baa817a15f4a9ee828791abbe7906381b2f69b36da2af3c3e510544d7cf3_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:DoctorGroup:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 49,  121 => 38,  112 => 32,  105 => 28,  100 => 25,  98 => 24,  90 => 19,  85 => 16,  83 => 15,  75 => 10,  69 => 6,  63 => 5,  51 => 4,  39 => 3,  32 => 1,  30 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% form_theme form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {% block doctorgroup %} class="active" {% endblock %}*/
/* {% block pagetitle %}DOCTOR GROUP{% endblock %}*/
/* {% block body -%}*/
/*     <center><h1><b>Create New Doctor Group</b></h1>*/
/*     <div class="x_content">*/
/*         <br/>*/
/*         <div data-parsley-validate class="form-horizontal form-label-left">*/
/*             {{ form_start(form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'createNewGroup' } }) }}*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Group Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.groupName, { 'attr' : { 'name': 'groupName', 'placeholder' : 'Group Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.groupName)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Description <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.description, { 'attr' : { 'name': 'description', 'placeholder' : 'Description', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.description)}}</div></i>*/
/*             */
/*             <div class="ln_solid"></div>*/
/*             <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                 <a href="">{{ form_widget(form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn', 'onClick' : 'submitClicked()'} }) }}</a>*/
/*                 <button type="reset" class="btn btn-dark" onclick="goBack()" >Cancel</button>*/
/*             </div>*/
/*         </div>*/
/*         */
/*         */
/*         {{ form_end(form)}}*/
/*     </div>*/
/*         */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#createNewGroup')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#createNewGroup').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*     */
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/* {% endblock %}*/
/* */
