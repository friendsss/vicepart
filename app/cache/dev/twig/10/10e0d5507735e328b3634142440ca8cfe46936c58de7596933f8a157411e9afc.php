<?php

/* ResearchProjectMyProjectBundle:Admin:new.html.twig */
class __TwigTemplate_ab558dd5de886ba20688d9eda0757a7e099b3fce137d0a70c76a3773cbb56627 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "ResearchProjectMyProjectBundle:Admin:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_97814d28e2588cfb5ec6d9cf32be47566120d579df6b63278ad719e466635e0b = $this->env->getExtension("native_profiler");
        $__internal_97814d28e2588cfb5ec6d9cf32be47566120d579df6b63278ad719e466635e0b->enter($__internal_97814d28e2588cfb5ec6d9cf32be47566120d579df6b63278ad719e466635e0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_97814d28e2588cfb5ec6d9cf32be47566120d579df6b63278ad719e466635e0b->leave($__internal_97814d28e2588cfb5ec6d9cf32be47566120d579df6b63278ad719e466635e0b_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3aa0d2df7a774c40021d5cb72deb3094a3293cf429c976d4c163db4d851032fe = $this->env->getExtension("native_profiler");
        $__internal_3aa0d2df7a774c40021d5cb72deb3094a3293cf429c976d4c163db4d851032fe->enter($__internal_3aa0d2df7a774c40021d5cb72deb3094a3293cf429c976d4c163db4d851032fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>Admin creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("admin");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>
";
        
        $__internal_3aa0d2df7a774c40021d5cb72deb3094a3293cf429c976d4c163db4d851032fe->leave($__internal_3aa0d2df7a774c40021d5cb72deb3094a3293cf429c976d4c163db4d851032fe_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 10,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Admin creation</h1>*/
/* */
/*     {{ form(form) }}*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('admin') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
