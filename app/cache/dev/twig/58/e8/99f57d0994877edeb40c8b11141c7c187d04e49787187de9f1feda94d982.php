<?php

/* ResearchProjectMyProjectBundle:Illness:new.html.twig */
class __TwigTemplate_58e899f57d0994877edeb40c8b11141c7c187d04e49787187de9f1feda94d982 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'illness' => array($this, 'block_illness'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_illness($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<center><h1><b>Define New Illness</b></h1></center>
    <div class=\"x_content\">
        <br />
        <div data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "registerIllnessForm")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Illness Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessName"), 'widget', array("attr" => array("id" => "illnessName", "name" => "illnessName", "placeholder" => "Illness Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 15
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessName"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Illness Common Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessCommonName"), 'widget', array("attr" => array("id" => "illnessCommonName", "name" => "illnessCommonName", "placeholder" => "Illness Common Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 24
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessCommonName"), 'errors');
        echo "</div></i>
             <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Define Illness <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessDefinition"), 'widget', array("attr" => array("id" => "illnessDefinition", "name" => "illnessDefinition", "placeholder" => "Define Illness", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 33
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessDefinition"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Describe Illness <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessDescription"), 'widget', array("attr" => array("id" => "illnessDescription", "name" => "illnessDescription", "placeholder" => "Describe illness", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 42
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessDescription"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Symptoms <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "symptoms"), 'widget', array("attr" => array("id" => "symptoms", "name" => "symptoms", "placeholder" => "Describe symptoms", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 51
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "symptoms"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Home Remedies <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homeRemedies"), 'widget', array("attr" => array("id" => "homeRemedies", "name" => "homeRemedies", "placeholder" => "Alternatives found at home / First Aid", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 60
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homeRemedies"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Commonly Prescribed Drugs <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commonlyPrescribedDrugs"), 'widget', array("attr" => array("id" => "commonlyPrescribedDrugs", "name" => "commonlyPrescribedDrugs", "placeholder" => "Commonly Prescribed Drugs", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 69
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commonlyPrescribedDrugs"), 'errors');
        echo "</div></i>
            <div class=\"ln_solid\"></div>
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"\">";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit"), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
                </div>
            </div>
            ";
        // line 79
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
           
    
    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>

    
    <!-- icheck -->
    <script src=";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "></script>

    <script src=";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "></script>
    <!-- form validation -->
    <script src=";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#registerIllnessForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#registerIllnessForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Illness:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 96,  185 => 94,  180 => 92,  164 => 79,  157 => 75,  151 => 72,  146 => 69,  144 => 68,  136 => 63,  131 => 60,  129 => 59,  121 => 54,  116 => 51,  114 => 50,  106 => 45,  101 => 42,  99 => 41,  91 => 36,  86 => 33,  84 => 32,  76 => 27,  71 => 24,  69 => 23,  61 => 18,  56 => 15,  54 => 14,  46 => 9,  40 => 5,  37 => 4,  31 => 3,  26 => 2,);
    }
}
