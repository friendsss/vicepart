<?php

/* ResearchProjectMyProjectBundle:DoctorGroup:invite.html.twig */
class __TwigTemplate_f5e88c3267ffb530f5744ea188e4b985558367b804aaa438694e2d249490c312 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_doctorgroup($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 3
    public function block_pagetitle($context, array $blocks = array())
    {
        echo "DOCTOR GROUP";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                <h3>&nbsp;Invite Members to Group</h3>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    
                    <div class=\"x_content\">
                        <center>
                                 <div class=\"search\">
                                    
                                        <input id='search' type=\"text\" name=\"query\" />
                                        
                                </div>
                                
                        </center>
                    </div>
                </div>
            </div>
        </div>
        


<div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
<!-- form validation -->
    <script src=";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script  
  src=\"//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\">
</script>
    <script>
\$('#search').keyup(function() {

     searchText = \$(this).val();

     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 51
        echo $this->env->getExtension('routing')->getPath("invite_doctor");
        echo "',
        data: {searchText : searchText},
        success : function(response) 
          {
                console.log(response);
                alert(response);
          },error: function() {
                                                alert('error');
                                            }
    });
});
    </script>
    ";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:DoctorGroup:invite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 51,  81 => 39,  45 => 5,  42 => 4,  36 => 3,  30 => 2,);
    }
}
