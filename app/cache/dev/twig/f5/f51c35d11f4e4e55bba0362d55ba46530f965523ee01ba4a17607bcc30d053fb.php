<?php

/* ResearchProjectMyProjectBundle:Default:clinic.html.twig */
class __TwigTemplate_e62bf574ce3fa342babf942be827bc83d284a54061ce356858a24f1d220cced1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Default:clinic.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'clinic' => array($this, 'block_clinic'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25a764a2d4787e7cc6a2af72cd4e17b0f2d888b3af303be8a7c92b1efb7e985a = $this->env->getExtension("native_profiler");
        $__internal_25a764a2d4787e7cc6a2af72cd4e17b0f2d888b3af303be8a7c92b1efb7e985a->enter($__internal_25a764a2d4787e7cc6a2af72cd4e17b0f2d888b3af303be8a7c92b1efb7e985a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Default:clinic.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_25a764a2d4787e7cc6a2af72cd4e17b0f2d888b3af303be8a7c92b1efb7e985a->leave($__internal_25a764a2d4787e7cc6a2af72cd4e17b0f2d888b3af303be8a7c92b1efb7e985a_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_528fb894fc190b1d066a96257f3f64818ae89dc8fb3313c44778a8b37caf690b = $this->env->getExtension("native_profiler");
        $__internal_528fb894fc190b1d066a96257f3f64818ae89dc8fb3313c44778a8b37caf690b->enter($__internal_528fb894fc190b1d066a96257f3f64818ae89dc8fb3313c44778a8b37caf690b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_528fb894fc190b1d066a96257f3f64818ae89dc8fb3313c44778a8b37caf690b->leave($__internal_528fb894fc190b1d066a96257f3f64818ae89dc8fb3313c44778a8b37caf690b_prof);

    }

    // line 3
    public function block_clinic($context, array $blocks = array())
    {
        $__internal_6d66ad660425f1ec841b87a5e1e6e85e02cea36bbdc0f5d55266a70b92564c2d = $this->env->getExtension("native_profiler");
        $__internal_6d66ad660425f1ec841b87a5e1e6e85e02cea36bbdc0f5d55266a70b92564c2d->enter($__internal_6d66ad660425f1ec841b87a5e1e6e85e02cea36bbdc0f5d55266a70b92564c2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "clinic"));

        echo " class=\"active\" ";
        
        $__internal_6d66ad660425f1ec841b87a5e1e6e85e02cea36bbdc0f5d55266a70b92564c2d->leave($__internal_6d66ad660425f1ec841b87a5e1e6e85e02cea36bbdc0f5d55266a70b92564c2d_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_5acea2a435a7ba244fc479fd31082736e5f1770b1e1194c867a7b3549e0ba6b7 = $this->env->getExtension("native_profiler");
        $__internal_5acea2a435a7ba244fc479fd31082736e5f1770b1e1194c867a7b3549e0ba6b7->enter($__internal_5acea2a435a7ba244fc479fd31082736e5f1770b1e1194c867a7b3549e0ba6b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    <div class=\"\">
            <div class=\"page-title\">
                <div class=\"title\">
                    <h3>&nbsp;CLINIC</h3>
                </div>
            </div>
            <div class=\"clearfix\"></div>

            <center>
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12\">
                    ";
        // line 16
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 17
            echo "                    <a href=\"";
            echo $this->env->getExtension('routing')->getPath("clinic_new");
            echo "\">
                        <button class=\"btn btn-success right\" style=\"margin-bottom: 5px;\"><i class=\"fa fa-plus\"></i> Add New Clinic</button>
                    </a>
                    ";
        }
        // line 21
        echo "                    <div class=\"x_panel\">
                        <br/>
                        ";
        // line 23
        $context["clinicname"] = $this->getAttribute((isset($context["randomclinic1"]) ? $context["randomclinic1"] : $this->getContext($context, "randomclinic1")), "clinicName", array());
        // line 24
        echo "                        ";
        $context["clinicadd"] = $this->getAttribute((isset($context["randomclinic1"]) ? $context["randomclinic1"] : $this->getContext($context, "randomclinic1")), "clinicAddress", array());
        // line 25
        echo "                        
                        ";
        // line 26
        $context["clinicname1"] = $this->getAttribute((isset($context["randomclinic2"]) ? $context["randomclinic2"] : $this->getContext($context, "randomclinic2")), "clinicName", array());
        // line 27
        echo "                        ";
        $context["clinicadd1"] = $this->getAttribute((isset($context["randomclinic2"]) ? $context["randomclinic2"] : $this->getContext($context, "randomclinic2")), "clinicAddress", array());
        // line 28
        echo "                        
                        ";
        // line 29
        $context["clinicname2"] = $this->getAttribute((isset($context["randomclinic3"]) ? $context["randomclinic3"] : $this->getContext($context, "randomclinic3")), "clinicName", array());
        // line 30
        echo "                        ";
        $context["clinicadd2"] = $this->getAttribute((isset($context["randomclinic3"]) ? $context["randomclinic3"] : $this->getContext($context, "randomclinic3")), "clinicAddress", array());
        // line 31
        echo "                            
                            
                            
                                <div class=\"col-md-4 col-sm-4 col-xs-12 profile\">
                                        <div class=\"profile_img\">
                                            <!-- end of image cropping -->
                                            <div id=\"crop-avatar\">
                                                <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    <img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
        echo "\" alt=\"Avatar\">
                                                </div>
                                            </div>
                                            <!-- end of image cropping -->
                                        </div>
                                        <h3><a href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomclinic1"]) ? $context["randomclinic1"] : $this->getContext($context, "randomclinic1")), "id", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["clinicname"]) ? $context["clinicname"] : $this->getContext($context, "clinicname")), "html", null, true);
        echo "</a></h3>

                                        <ul class=\"list-unstyled user_data\">
                                            <li><i class=\"fa fa-map-marker user-profile-icon\"></i> ";
        // line 48
        echo twig_escape_filter($this->env, (isset($context["clinicadd"]) ? $context["clinicadd"] : $this->getContext($context, "clinicadd")), "html", null, true);
        echo "
                                            </li>

                                        </ul>
                                        
                                     </div>
                                
                                                
                                <div class=\"col-md-4 col-sm-4 col-xs-12 profile\">
                                    
                                        <div class=\"profile_img\">
                                            <!-- end of image cropping -->
                                            <div id=\"crop-avatar\">
                                                <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
        echo "\" alt=\"Avatar\">
                                                </div>
                                            </div>
                                            <!-- end of image cropping -->
                                        </div>
                                        <h3><a href=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomclinic2"]) ? $context["randomclinic2"] : $this->getContext($context, "randomclinic2")), "id", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["clinicname1"]) ? $context["clinicname1"] : $this->getContext($context, "clinicname1")), "html", null, true);
        echo "</a></h3>

                                        <ul class=\"list-unstyled user_data\">
                                            <li><i class=\"fa fa-map-marker user-profile-icon\"></i> ";
        // line 71
        echo twig_escape_filter($this->env, (isset($context["clinicadd1"]) ? $context["clinicadd1"] : $this->getContext($context, "clinicadd1")), "html", null, true);
        echo "
                                            </li>
                                        </ul>
                                </div>

                                <div class=\"col-md-4 col-sm-4 col-xs-12 profile\">
                                    
                                        <div class=\"profile_img\">
                                            <!-- end of image cropping -->
                                            <div id=\"crop-avatar\">
                                                <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    <img src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
        echo "\" alt=\"Avatar\">
                                                </div>
                                            </div>
                                            <!-- end of image cropping -->
                                        </div>
                                        <h3><a href=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomclinic3"]) ? $context["randomclinic3"] : $this->getContext($context, "randomclinic3")), "id", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["clinicname2"]) ? $context["clinicname2"] : $this->getContext($context, "clinicname2")), "html", null, true);
        echo "</a></h3>

                                        <ul class=\"list-unstyled user_data\">
                                            <li><i class=\"fa fa-map-marker user-profile-icon\"></i> ";
        // line 91
        echo twig_escape_filter($this->env, (isset($context["clinicadd2"]) ? $context["clinicadd2"] : $this->getContext($context, "clinicadd2")), "html", null, true);
        echo "
                                            </li>
                                        </ul>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                  </center>
            
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12\">  
                    <center>
                    <div class=\"x_panel\">
                        <h3>CLINICS</h3>
                        <div style=\"overflow-y: scroll; height:300px;\">
                        <table class=\"table table-striped\">
                                        <thead>
                                            <tr>
                                                <th>Clinic Name</th>
                                                <th>Address</th>
                                                <th>Doctors</th>
                                                <th>View Profile</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ";
        // line 117
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 118
            echo "                                                <tr>
                                                    <td>";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "clinicName", array()), "html", null, true);
            echo "</td>
                                                    <td>";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "clinicAddress", array()), "html", null, true);
            echo "</td>
                                                    <td>
                                                    ";
            // line 122
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["entity"], "getDoctors", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
                // line 123
                echo "                                                    
                                                        <a style=\"font-family: Century Gothic; color:#379DFF; font-size: 15px\" href=\"";
                // line 124
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
                echo "\">
                                                        ";
                // line 125
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "firstName", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "middleName", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "lastName", array()), "html", null, true);
                echo "</a>
                                                        <br/>
                                                    
                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo "                                                    </td>
                                                    <td><a href=\"";
            // line 130
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\"><button class=\"btn btn-primary btn-xs\">View Clinic Profile</button></a></td>
                                                </tr>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 133
        echo "                                            
                                        </tbody>
                        </table>
                    </div>
                    </div>
                   </center>
                </div>
            </div>
        </div>

";
        
        $__internal_5acea2a435a7ba244fc479fd31082736e5f1770b1e1194c867a7b3549e0ba6b7->leave($__internal_5acea2a435a7ba244fc479fd31082736e5f1770b1e1194c867a7b3549e0ba6b7_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:clinic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 133,  279 => 130,  276 => 129,  262 => 125,  258 => 124,  255 => 123,  251 => 122,  246 => 120,  242 => 119,  239 => 118,  235 => 117,  206 => 91,  198 => 88,  190 => 83,  175 => 71,  167 => 68,  159 => 63,  141 => 48,  133 => 45,  125 => 40,  114 => 31,  111 => 30,  109 => 29,  106 => 28,  103 => 27,  101 => 26,  98 => 25,  95 => 24,  93 => 23,  89 => 21,  81 => 17,  79 => 16,  66 => 5,  60 => 4,  48 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block title %} {% endblock %}*/
/* {% block clinic %} class="active" {% endblock %}*/
/* {% block body %}*/
/*     <div class="">*/
/*             <div class="page-title">*/
/*                 <div class="title">*/
/*                     <h3>&nbsp;CLINIC</h3>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="clearfix"></div>*/
/* */
/*             <center>*/
/*             <div class="row">*/
/*                 <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                     {% if app.user%}*/
/*                     <a href="{{path('clinic_new')}}">*/
/*                         <button class="btn btn-success right" style="margin-bottom: 5px;"><i class="fa fa-plus"></i> Add New Clinic</button>*/
/*                     </a>*/
/*                     {% endif %}*/
/*                     <div class="x_panel">*/
/*                         <br/>*/
/*                         {% set clinicname = randomclinic1.clinicName %}*/
/*                         {% set clinicadd = randomclinic1.clinicAddress %}*/
/*                         */
/*                         {% set clinicname1 = randomclinic2.clinicName %}*/
/*                         {% set clinicadd1 = randomclinic2.clinicAddress %}*/
/*                         */
/*                         {% set clinicname2 = randomclinic3.clinicName %}*/
/*                         {% set clinicadd2 = randomclinic3.clinicAddress %}*/
/*                             */
/*                             */
/*                             */
/*                                 <div class="col-md-4 col-sm-4 col-xs-12 profile">*/
/*                                         <div class="profile_img">*/
/*                                             <!-- end of image cropping -->*/
/*                                             <div id="crop-avatar">*/
/*                                                 <!-- Current avatar -->*/
/*                                                 <div class="avatar-view" title="Change the avatar">*/
/*                                                     <img src="{{asset('gentelella/images/picture.jpg')}}" alt="Avatar">*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <!-- end of image cropping -->*/
/*                                         </div>*/
/*                                         <h3><a href="{{path('clinic_show', {'id': randomclinic1.id})}}">{{clinicname}}</a></h3>*/
/* */
/*                                         <ul class="list-unstyled user_data">*/
/*                                             <li><i class="fa fa-map-marker user-profile-icon"></i> {{clinicadd}}*/
/*                                             </li>*/
/* */
/*                                         </ul>*/
/*                                         */
/*                                      </div>*/
/*                                 */
/*                                                 */
/*                                 <div class="col-md-4 col-sm-4 col-xs-12 profile">*/
/*                                     */
/*                                         <div class="profile_img">*/
/*                                             <!-- end of image cropping -->*/
/*                                             <div id="crop-avatar">*/
/*                                                 <!-- Current avatar -->*/
/*                                                 <div class="avatar-view" title="Change the avatar">*/
/*                                                     <img src="{{asset('gentelella/images/picture.jpg')}}" alt="Avatar">*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <!-- end of image cropping -->*/
/*                                         </div>*/
/*                                         <h3><a href="{{path('clinic_show', {'id': randomclinic2.id})}}">{{clinicname1}}</a></h3>*/
/* */
/*                                         <ul class="list-unstyled user_data">*/
/*                                             <li><i class="fa fa-map-marker user-profile-icon"></i> {{clinicadd1}}*/
/*                                             </li>*/
/*                                         </ul>*/
/*                                 </div>*/
/* */
/*                                 <div class="col-md-4 col-sm-4 col-xs-12 profile">*/
/*                                     */
/*                                         <div class="profile_img">*/
/*                                             <!-- end of image cropping -->*/
/*                                             <div id="crop-avatar">*/
/*                                                 <!-- Current avatar -->*/
/*                                                 <div class="avatar-view" title="Change the avatar">*/
/*                                                     <img src="{{asset('gentelella/images/picture.jpg')}}" alt="Avatar">*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <!-- end of image cropping -->*/
/*                                         </div>*/
/*                                         <h3><a href="{{path('clinic_show', {'id': randomclinic3.id})}}">{{clinicname2}}</a></h3>*/
/* */
/*                                         <ul class="list-unstyled user_data">*/
/*                                             <li><i class="fa fa-map-marker user-profile-icon"></i> {{clinicadd2}}*/
/*                                             </li>*/
/*                                         </ul>*/
/*                                         */
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                   </center>*/
/*             */
/*             <div class="row">*/
/*                 <div class="col-md-12 col-sm-12 col-xs-12">  */
/*                     <center>*/
/*                     <div class="x_panel">*/
/*                         <h3>CLINICS</h3>*/
/*                         <div style="overflow-y: scroll; height:300px;">*/
/*                         <table class="table table-striped">*/
/*                                         <thead>*/
/*                                             <tr>*/
/*                                                 <th>Clinic Name</th>*/
/*                                                 <th>Address</th>*/
/*                                                 <th>Doctors</th>*/
/*                                                 <th>View Profile</th>*/
/*                                             </tr>*/
/*                                         </thead>*/
/*                                         <tbody>*/
/*                                             {% for entity in entities %}*/
/*                                                 <tr>*/
/*                                                     <td>{{entity.clinicName}}</td>*/
/*                                                     <td>{{entity.clinicAddress}}</td>*/
/*                                                     <td>*/
/*                                                     {% for doctor in entity.getDoctors %}*/
/*                                                     */
/*                                                         <a style="font-family: Century Gothic; color:#379DFF; font-size: 15px" href="{{path('doctor_show', {'id': doctor.id})}}">*/
/*                                                         {{doctor.firstName}} {{doctor.middleName}} {{doctor.lastName}}</a>*/
/*                                                         <br/>*/
/*                                                     */
/*                                                     {% endfor %}*/
/*                                                     </td>*/
/*                                                     <td><a href="{{path('clinic_show', {'id': entity.id})}}"><button class="btn btn-primary btn-xs">View Clinic Profile</button></a></td>*/
/*                                                 </tr>*/
/*                                             {% endfor %}*/
/*                                             */
/*                                         </tbody>*/
/*                         </table>*/
/*                     </div>*/
/*                     </div>*/
/*                    </center>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/* {% endblock %}*/
