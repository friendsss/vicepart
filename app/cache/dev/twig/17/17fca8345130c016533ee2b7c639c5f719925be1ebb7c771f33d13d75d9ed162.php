<?php

/* ResearchProjectMyProjectBundle:Admin:editClinic.html.twig */
class __TwigTemplate_f44f38216d1f5aab1a4757e0863b33c6ae80dd332c69537621a44d96fc44f82c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:editClinic.html.twig", 1);
        $this->blocks = array(
            'pagetitle' => array($this, 'block_pagetitle'),
            'doctor' => array($this, 'block_doctor'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d27bab1060ef1901855e205bfd9e8f9ea1d85869e41538cfb64c29d4c0288cff = $this->env->getExtension("native_profiler");
        $__internal_d27bab1060ef1901855e205bfd9e8f9ea1d85869e41538cfb64c29d4c0288cff->enter($__internal_d27bab1060ef1901855e205bfd9e8f9ea1d85869e41538cfb64c29d4c0288cff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:editClinic.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d27bab1060ef1901855e205bfd9e8f9ea1d85869e41538cfb64c29d4c0288cff->leave($__internal_d27bab1060ef1901855e205bfd9e8f9ea1d85869e41538cfb64c29d4c0288cff_prof);

    }

    // line 3
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_ac4f8d51758501db577da3d04bfa71d299b830858ab11834105bec771a845290 = $this->env->getExtension("native_profiler");
        $__internal_ac4f8d51758501db577da3d04bfa71d299b830858ab11834105bec771a845290->enter($__internal_ac4f8d51758501db577da3d04bfa71d299b830858ab11834105bec771a845290_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "DOCTOR";
        
        $__internal_ac4f8d51758501db577da3d04bfa71d299b830858ab11834105bec771a845290->leave($__internal_ac4f8d51758501db577da3d04bfa71d299b830858ab11834105bec771a845290_prof);

    }

    // line 4
    public function block_doctor($context, array $blocks = array())
    {
        $__internal_c2a8ee2c9573b91507eedbe22ec960cd6fd719ccc12d977513cc876701533eb9 = $this->env->getExtension("native_profiler");
        $__internal_c2a8ee2c9573b91507eedbe22ec960cd6fd719ccc12d977513cc876701533eb9->enter($__internal_c2a8ee2c9573b91507eedbe22ec960cd6fd719ccc12d977513cc876701533eb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctor"));

        echo " class=\"active\" ";
        
        $__internal_c2a8ee2c9573b91507eedbe22ec960cd6fd719ccc12d977513cc876701533eb9->leave($__internal_c2a8ee2c9573b91507eedbe22ec960cd6fd719ccc12d977513cc876701533eb9_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_f15d78bf0c6538106686ec881c009f72d87ae84f04d076007f6054fb6bd922d2 = $this->env->getExtension("native_profiler");
        $__internal_f15d78bf0c6538106686ec881c009f72d87ae84f04d076007f6054fb6bd922d2->enter($__internal_f15d78bf0c6538106686ec881c009f72d87ae84f04d076007f6054fb6bd922d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<style>
#clinicmap{
    width: 500px;
    height: 400px;
}
.x_content{
    height: fit-content;
}
</style>
<center><h1><b>Edit Clinic Information</b></h1>
  <div id=\"overlay\">
    <div id=\"progstat\"></div>
    <div id=\"progress\"></div>
  </div>
<div class=\"x_panel\">
    <div class=\"x_content\">
        <br />
        <div data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 24
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "editClinicForm")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Clinic Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicName", array()), 'widget', array("attr" => array("name" => "clinicName", "placeholder" => "Clinic Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 30
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicName", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Clinic Description <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicDescription", array()), 'widget', array("attr" => array("name" => "clinicName", "placeholder" => "Clinic Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "10", "required" => "required", "type" => "text")));
        // line 39
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicDescription", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Clinic Address <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicAddress", array()), 'widget', array("attr" => array("name" => "clinicAddress", "placeholder" => "Clinic Address", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 48
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicAddress", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Contact Number<span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "contactNumber", array()), 'widget', array("attr" => array("name" => "clinicName", "placeholder" => "Contact Number", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 57
        echo "
                </div>
            </div>
                <div hidden class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicAddress", array()), 'widget', array("attr" => array("placeholder" => "Clinic Address", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 62
        echo "
                </div>
               <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "contactNumber", array()), 'errors');
        echo "</div></i>
               <div hidden class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicLong", array()), 'widget');
        echo "
                </div>
               <div hidden class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "clinicLat", array()), 'widget');
        echo "
                </div>
                <div id=\"clinicmap\" class =\"form-group\">  </div> 
                <div hidden id=\"current\">Nothing yet...</div>
                <div class=\"item form-group\">
                <label class=\"control-label col-md-12 col-sm-12 col-xs-12\"><h4><b><center>Clinic Schedule</center></b></h4></label>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Sunday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "sunOpenTime", array()), 'widget');
        echo " 
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                    ";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "sunCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"sunday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Monday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "monOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "monCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"monday\" type=\"checkbox\" value=\"\"> Closed
                   </label>                             
                </div>
            </div>
               <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Tuesday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 112
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "tueOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 116
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "tueCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"tuesday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Wednesday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "wedOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 132
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "wedCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"wednesday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Thursday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 144
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "thuOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 148
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "thuCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"thursday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Friday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 160
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "friOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 164
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "friCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"friday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Saturday: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 176
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "satOpenTime", array()), 'widget');
        echo "
                 </div>
                 <span class=\"col-md-2 col-sm-2\">up to</span>
                 <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   ";
        // line 180
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "satCloseTime", array()), 'widget');
        echo "
                 </div>
                <div class=\"col-md-2 col-sm-12 col-xs-12 form-group\">
                   <label>
                       <input id=\"saturday\" type=\"checkbox\" value=\"\"> Closed
                   </label>
                </div>
            </div>
             <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Clinic Display Picture <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 192
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "file", array()), 'widget');
        echo "
                </div>
            </div>
            <div class=\"form-group\">
            <div class=\"ln_solid\"></div>
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
                </div>
             </div> 
            ";
        // line 202
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
                </div>
    </div>
</div>
    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
<!-- form validation -->
    <script src=";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#editClinicForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#editClinicForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
    
    
    <script type=\"text/javascript\" src=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/jquery.geocomplete.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 251
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "\"></script>
    <script async defer src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDRJhfHphrYhN10mIoK_5CtgJQ0M9qbO14&callback=initMap&libraries=places&sensor=false\"></script> 
    
    <script>                
function initMap() {
 
  var thislat = \$('#researchproject_myprojectbundle_clinic_clinicLat').val();
  var thislong =\$('#researchproject_myprojectbundle_clinic_clinicLong').val();
  var center = new google.maps.LatLng(thislat, thislong);
  var myLatLng = {lat: thislat, lng: thislong};
  var map = new google.maps.Map(document.getElementById('clinicmap'), {
    zoom: 16,
    center: center
  });
  
   var input = /** @type {!HTMLInputElement} */(
      document.getElementById('researchproject_myprojectbundle_clinic_clinicAddress'));
    var options = {
  
   componentRestrictions: {country: 'ph'}
};
 
  var autocomplete = new google.maps.places.Autocomplete(input, options);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    position: center,
    map: map,
    title: 'Hello World!',
    draggable: true
  });

  google.maps.event.addListener(marker, 'dragend', function(evt){
    document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';
    \$('#researchproject_myprojectbundle_clinic_clinicLat').val(evt.latLng.lat().toFixed(3));
    \$('#researchproject_myprojectbundle_clinic_clinicLong').val(evt.latLng.lng().toFixed(3));
    
});

google.maps.event.addListener(marker, 'dragstart', function(evt){
    document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
}); 


  autocomplete.addListener('place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      window.alert(\"Autocomplete's returned place contains no geometry\");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
     // map.setZoom(17);  // Why 17? Because it looks good.
    }
    
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }
    \$('#researchproject_myprojectbundle_clinic_clinicLat').val(place.geometry.location.lat());
    \$('#researchproject_myprojectbundle_clinic_clinicLong').val(place.geometry.location.lng());
    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
    radioButton.addEventListener('click', function() {
      autocomplete.setTypes(types);
    });
  }
  
  

  setupClickListener('changetype-all', []);
  setupClickListener('changetype-address', ['address']);
  setupClickListener('changetype-establishment', ['establishment']);
  setupClickListener('changetype-geocode', ['geocode']);
}
    </script>
    <script>
    
    document.getElementById('sunday').onclick = function() {
    // access properties using this keyword
     sundayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_hour');
     sundayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_minute');
     sundayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_hour');
     sundayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_minute');
     
     if ( this.checked ) {
        sundayOpenH.disabled = true;
        sundayOpenM.disabled = true;
        sundayOpenH.value = 00;
        sundayOpenM.value = 00;
        sundayCloseH.disabled = true;
        sundayCloseM.disabled = true;
        sundayCloseH.value = 00;
        sundayCloseM.value = 00;
        
         } else {
        sundayOpenH.disabled = false;
        sundayOpenM.disabled = false;
        sundayCloseH.disabled = false;
        sundayCloseM.disabled = false;
         }
         
        
};

    document.getElementById('monday').onclick = function() {
    // access properties using this keyword
     mondayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_hour');
     mondayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_minute');
     mondayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_hour');
     mondayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_minute');
     
     if ( this.checked ) {
         mondayOpenH.disabled = true;
        mondayOpenM.disabled = true;
        mondayOpenH.value = 00;
        mondayOpenM.value = 00;
        mondayCloseH.disabled = true;
        mondayCloseM.disabled = true;
        mondayCloseH.value = 00;
        mondayCloseM.value = 00;
        
         } else {
        mondayOpenH.disabled = false;
        mondayOpenM.disabled = false;
        mondayCloseH.disabled = false;
        mondayCloseM.disabled = false;
         }
         
        
};
      document.getElementById('tuesday').onclick = function() {
    // access properties using this keyword
    tuesdayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_hour');
     tuesdayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_minute');
     tuesdayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_hour');
     tuesdayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_minute');
     
     if ( this.checked ) {
        
        tuesdayOpenH.disabled = true;
        tuesdayOpenM.disabled = true;
        tuesdayOpenH.value = 00;
        tuesdayOpenM.value = 00;
        tuesdayCloseH.disabled = true;
        tuesdayCloseM.disabled = true;
        tuesdayCloseH.value = 00;
        tuesdayCloseM.value = 00;
        
         } else {
         
        tuesdayOpenH.disabled = false;
        tuesdayOpenM.disabled = false;
        tuesdayCloseH.disabled = false;
        tuesdayCloseM.disabled = false;
       
         }
         
        
};

 document.getElementById('wednesday').onclick = function() {
    // access properties using this keyword
     wedOpenH = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_hour');
     wedOpenM = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_minute');
     wedCloseH = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_hour');
     wedCloseM = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_minute');
     
     if ( this.checked ) {
         wedOpenH.disabled = true;
        wedOpenM.disabled = true;
        wedOpenH.value = 00;
        wedOpenM.value = 00;
        wedCloseH.disabled = true;
        wedCloseM.disabled = true;
        wedCloseH.value = 00;
        wedCloseM.value = 00;
        
       
        
         } else {
         
        wedOpenH.disabled = false;
        wedOpenM.disabled = false;
        wedCloseH.disabled = false;
        wedCloseM.disabled = false;
        
        
         }
         
        
};

 document.getElementById('thursday').onclick = function() {
    // access properties using this keyword
    thOpenH = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_hour');
     thOpenM = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_minute');
     thCloseH = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_hour');
     thCloseM = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_minute');
     
     if ( this.checked ) {
         thOpenH.disabled = true;
        thOpenM.disabled = true;
        thOpenH.value = 00;
        thOpenM.value = 00;
        thCloseH.disabled = true;
        thCloseM.disabled = true;
        thCloseH.value = 00;
        thCloseM.value = 00;
        
         } else {
        thOpenH.disabled = false;
        thOpenM.disabled = false;
        thCloseH.disabled = false;
        thCloseM.disabled = false;
       
         }
         
        
};

 document.getElementById('friday').onclick = function() {
    // access properties using this keyword
     
     friOpenH = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_hour');
     friOpenM = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_minute');
     friCloseH = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_hour');
     friCloseM = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_minute');
     
     if ( this.checked ) {
          friOpenH.disabled = true;
        friOpenM.disabled = true;
        friOpenH.value = 00;
        friOpenM.value = 00;
        friCloseH.disabled = true;
        friCloseM.disabled = true;
        friCloseH.value = 00;
        friCloseM.value = 00;
        
        
        
         } else {
         
        friOpenH.disabled = false;
        friOpenM.disabled = false;
        friCloseH.disabled = false;
        friCloseM.disabled = false;
       
         }
         
        
};

 document.getElementById('saturday').onclick = function() {
    // access properties using this keyword
     
     satOpenH = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_hour');
     satOpenM = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_minute');
     satCloseH = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_hour');
     satCloseM = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_minute');
     
     
     if ( this.checked ) {
       satOpenH.disabled = true;
        satOpenM.disabled = true;
        satOpenH.value = 00;
        satOpenM.value = 00;
        satCloseH.disabled = true;
        satCloseM.disabled = true;
        satCloseH.value = 00;
        satCloseM.value = 00;
        
         } else {
        
        satOpenH.disabled = false;
        satOpenM.disabled = false;
        satCloseH.disabled = false;
        satCloseM.disabled = false;
         }
         
        
};
     
     
    
</script>




";
        
        $__internal_f15d78bf0c6538106686ec881c009f72d87ae84f04d076007f6054fb6bd922d2->leave($__internal_f15d78bf0c6538106686ec881c009f72d87ae84f04d076007f6054fb6bd922d2_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:editClinic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  414 => 251,  410 => 250,  406 => 249,  368 => 214,  353 => 202,  344 => 198,  335 => 192,  320 => 180,  313 => 176,  298 => 164,  291 => 160,  276 => 148,  269 => 144,  254 => 132,  247 => 128,  232 => 116,  225 => 112,  210 => 100,  203 => 96,  188 => 84,  181 => 80,  167 => 69,  161 => 66,  156 => 64,  152 => 62,  150 => 61,  144 => 57,  142 => 56,  134 => 51,  129 => 48,  127 => 47,  119 => 42,  114 => 39,  112 => 38,  104 => 33,  99 => 30,  97 => 29,  89 => 24,  69 => 6,  63 => 5,  51 => 4,  39 => 3,  32 => 1,  30 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% form_theme edit_form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {% block pagetitle %}DOCTOR{% endblock %}*/
/* {%block doctor%} class="active" {% endblock %}*/
/* {% block body -%}*/
/* <style>*/
/* #clinicmap{*/
/*     width: 500px;*/
/*     height: 400px;*/
/* }*/
/* .x_content{*/
/*     height: fit-content;*/
/* }*/
/* </style>*/
/* <center><h1><b>Edit Clinic Information</b></h1>*/
/*   <div id="overlay">*/
/*     <div id="progstat"></div>*/
/*     <div id="progress"></div>*/
/*   </div>*/
/* <div class="x_panel">*/
/*     <div class="x_content">*/
/*         <br />*/
/*         <div data-parsley-validate class="form-horizontal form-label-left">*/
/*             {{ form_start(edit_form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'editClinicForm' } }) }}*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Clinic Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.clinicName, { 'attr' : { 'name': 'clinicName', 'placeholder' : 'Clinic Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.clinicName)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Clinic Description <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.clinicDescription, { 'attr' : { 'name': 'clinicName', 'placeholder' : 'Clinic Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                          'data-validate-length-range' : '10', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.clinicDescription)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Clinic Address <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.clinicAddress, { 'attr' : { 'name': 'clinicAddress', 'placeholder' : 'Clinic Address', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                          'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.clinicAddress)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Contact Number<span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.contactNumber, { 'attr' : { 'name': 'clinicName', 'placeholder' : 'Contact Number', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <div hidden class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.clinicAddress, { 'attr' : {'placeholder' : 'Clinic Address', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                          'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*                <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.contactNumber)}}</div></i>*/
/*                <div hidden class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.clinicLong) }}*/
/*                 </div>*/
/*                <div hidden class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.clinicLat) }}*/
/*                 </div>*/
/*                 <div id="clinicmap" class ="form-group">  </div> */
/*                 <div hidden id="current">Nothing yet...</div>*/
/*                 <div class="item form-group">*/
/*                 <label class="control-label col-md-12 col-sm-12 col-xs-12"><h4><b><center>Clinic Schedule</center></b></h4></label>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Sunday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.sunOpenTime)}} */
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                     {{ form_widget(edit_form.sunCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="sunday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Monday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.monOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.monCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="monday" type="checkbox" value=""> Closed*/
/*                    </label>                             */
/*                 </div>*/
/*             </div>*/
/*                <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Tuesday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.tueOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.tueCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="tuesday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Wednesday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.wedOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.wedCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="wednesday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Thursday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.thuOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.thuCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="thursday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Friday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.friOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.friCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="friday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Saturday: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.satOpenTime) }}*/
/*                  </div>*/
/*                  <span class="col-md-2 col-sm-2">up to</span>*/
/*                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    {{ form_widget(edit_form.satCloseTime) }}*/
/*                  </div>*/
/*                 <div class="col-md-2 col-sm-12 col-xs-12 form-group">*/
/*                    <label>*/
/*                        <input id="saturday" type="checkbox" value=""> Closed*/
/*                    </label>*/
/*                 </div>*/
/*             </div>*/
/*              <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Clinic Display Picture <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.file) }}*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*             <div class="ln_solid"></div>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                     <a href="{{ path('clinic_show', {'id': entity.id})}}"> {{ form_widget(edit_form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn'} }) }}</a>*/
/*                     <button type="reset" class="btn btn-dark" onclick="goBack()" >Cancel</button>*/
/*                 </div>*/
/*              </div> */
/*             {{ form_end(edit_form)}}*/
/*                 </div>*/
/*     </div>*/
/* </div>*/
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#editClinicForm')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#editClinicForm').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*     */
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/*     */
/*     */
/*     <script type="text/javascript" src="{{asset('gentelella/js/jquery.geocomplete.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/icheck/icheck.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/custom.js')}}"></script>*/
/*     <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRJhfHphrYhN10mIoK_5CtgJQ0M9qbO14&callback=initMap&libraries=places&sensor=false"></script> */
/*     */
/*     <script>                */
/* function initMap() {*/
/*  */
/*   var thislat = $('#researchproject_myprojectbundle_clinic_clinicLat').val();*/
/*   var thislong =$('#researchproject_myprojectbundle_clinic_clinicLong').val();*/
/*   var center = new google.maps.LatLng(thislat, thislong);*/
/*   var myLatLng = {lat: thislat, lng: thislong};*/
/*   var map = new google.maps.Map(document.getElementById('clinicmap'), {*/
/*     zoom: 16,*/
/*     center: center*/
/*   });*/
/*   */
/*    var input = /** @type {!HTMLInputElement} *//* (*/
/*       document.getElementById('researchproject_myprojectbundle_clinic_clinicAddress'));*/
/*     var options = {*/
/*   */
/*    componentRestrictions: {country: 'ph'}*/
/* };*/
/*  */
/*   var autocomplete = new google.maps.places.Autocomplete(input, options);*/
/*   autocomplete.bindTo('bounds', map);*/
/* */
/*   var infowindow = new google.maps.InfoWindow();*/
/*   var marker = new google.maps.Marker({*/
/*     position: center,*/
/*     map: map,*/
/*     title: 'Hello World!',*/
/*     draggable: true*/
/*   });*/
/* */
/*   google.maps.event.addListener(marker, 'dragend', function(evt){*/
/*     document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';*/
/*     $('#researchproject_myprojectbundle_clinic_clinicLat').val(evt.latLng.lat().toFixed(3));*/
/*     $('#researchproject_myprojectbundle_clinic_clinicLong').val(evt.latLng.lng().toFixed(3));*/
/*     */
/* });*/
/* */
/* google.maps.event.addListener(marker, 'dragstart', function(evt){*/
/*     document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';*/
/* }); */
/* */
/* */
/*   autocomplete.addListener('place_changed', function() {*/
/*     infowindow.close();*/
/*     marker.setVisible(false);*/
/*     var place = autocomplete.getPlace();*/
/*     if (!place.geometry) {*/
/*       window.alert("Autocomplete's returned place contains no geometry");*/
/*       return;*/
/*     }*/
/* */
/*     // If the place has a geometry, then present it on a map.*/
/*     if (place.geometry.viewport) {*/
/*       map.fitBounds(place.geometry.viewport);*/
/*     } else {*/
/*       map.setCenter(place.geometry.location);*/
/*      // map.setZoom(17);  // Why 17? Because it looks good.*/
/*     }*/
/*     */
/*     marker.setPosition(place.geometry.location);*/
/*     marker.setVisible(true);*/
/* */
/*     var address = '';*/
/*     if (place.address_components) {*/
/*       address = [*/
/*         (place.address_components[0] && place.address_components[0].short_name || ''),*/
/*         (place.address_components[1] && place.address_components[1].short_name || ''),*/
/*         (place.address_components[2] && place.address_components[2].short_name || '')*/
/*       ].join(' ');*/
/*     }*/
/*     $('#researchproject_myprojectbundle_clinic_clinicLat').val(place.geometry.location.lat());*/
/*     $('#researchproject_myprojectbundle_clinic_clinicLong').val(place.geometry.location.lng());*/
/*     infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);*/
/*     infowindow.open(map, marker);*/
/*   });*/
/* */
/*   // Sets a listener on a radio button to change the filter type on Places*/
/*   // Autocomplete.*/
/*   function setupClickListener(id, types) {*/
/*     var radioButton = document.getElementById(id);*/
/*     radioButton.addEventListener('click', function() {*/
/*       autocomplete.setTypes(types);*/
/*     });*/
/*   }*/
/*   */
/*   */
/* */
/*   setupClickListener('changetype-all', []);*/
/*   setupClickListener('changetype-address', ['address']);*/
/*   setupClickListener('changetype-establishment', ['establishment']);*/
/*   setupClickListener('changetype-geocode', ['geocode']);*/
/* }*/
/*     </script>*/
/*     <script>*/
/*     */
/*     document.getElementById('sunday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      sundayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_hour');*/
/*      sundayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_sunOpenTime_minute');*/
/*      sundayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_hour');*/
/*      sundayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_sunCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*         sundayOpenH.disabled = true;*/
/*         sundayOpenM.disabled = true;*/
/*         sundayOpenH.value = 00;*/
/*         sundayOpenM.value = 00;*/
/*         sundayCloseH.disabled = true;*/
/*         sundayCloseM.disabled = true;*/
/*         sundayCloseH.value = 00;*/
/*         sundayCloseM.value = 00;*/
/*         */
/*          } else {*/
/*         sundayOpenH.disabled = false;*/
/*         sundayOpenM.disabled = false;*/
/*         sundayCloseH.disabled = false;*/
/*         sundayCloseM.disabled = false;*/
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*     document.getElementById('monday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      mondayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_hour');*/
/*      mondayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_monOpenTime_minute');*/
/*      mondayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_hour');*/
/*      mondayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_monCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*          mondayOpenH.disabled = true;*/
/*         mondayOpenM.disabled = true;*/
/*         mondayOpenH.value = 00;*/
/*         mondayOpenM.value = 00;*/
/*         mondayCloseH.disabled = true;*/
/*         mondayCloseM.disabled = true;*/
/*         mondayCloseH.value = 00;*/
/*         mondayCloseM.value = 00;*/
/*         */
/*          } else {*/
/*         mondayOpenH.disabled = false;*/
/*         mondayOpenM.disabled = false;*/
/*         mondayCloseH.disabled = false;*/
/*         mondayCloseM.disabled = false;*/
/*          }*/
/*          */
/*         */
/* };*/
/*       document.getElementById('tuesday').onclick = function() {*/
/*     // access properties using this keyword*/
/*     tuesdayOpenH = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_hour');*/
/*      tuesdayOpenM = document.getElementById('researchproject_myprojectbundle_clinic_tueOpenTime_minute');*/
/*      tuesdayCloseH = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_hour');*/
/*      tuesdayCloseM = document.getElementById('researchproject_myprojectbundle_clinic_tueCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*         */
/*         tuesdayOpenH.disabled = true;*/
/*         tuesdayOpenM.disabled = true;*/
/*         tuesdayOpenH.value = 00;*/
/*         tuesdayOpenM.value = 00;*/
/*         tuesdayCloseH.disabled = true;*/
/*         tuesdayCloseM.disabled = true;*/
/*         tuesdayCloseH.value = 00;*/
/*         tuesdayCloseM.value = 00;*/
/*         */
/*          } else {*/
/*          */
/*         tuesdayOpenH.disabled = false;*/
/*         tuesdayOpenM.disabled = false;*/
/*         tuesdayCloseH.disabled = false;*/
/*         tuesdayCloseM.disabled = false;*/
/*        */
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*  document.getElementById('wednesday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      wedOpenH = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_hour');*/
/*      wedOpenM = document.getElementById('researchproject_myprojectbundle_clinic_wedOpenTime_minute');*/
/*      wedCloseH = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_hour');*/
/*      wedCloseM = document.getElementById('researchproject_myprojectbundle_clinic_wedCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*          wedOpenH.disabled = true;*/
/*         wedOpenM.disabled = true;*/
/*         wedOpenH.value = 00;*/
/*         wedOpenM.value = 00;*/
/*         wedCloseH.disabled = true;*/
/*         wedCloseM.disabled = true;*/
/*         wedCloseH.value = 00;*/
/*         wedCloseM.value = 00;*/
/*         */
/*        */
/*         */
/*          } else {*/
/*          */
/*         wedOpenH.disabled = false;*/
/*         wedOpenM.disabled = false;*/
/*         wedCloseH.disabled = false;*/
/*         wedCloseM.disabled = false;*/
/*         */
/*         */
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*  document.getElementById('thursday').onclick = function() {*/
/*     // access properties using this keyword*/
/*     thOpenH = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_hour');*/
/*      thOpenM = document.getElementById('researchproject_myprojectbundle_clinic_thuOpenTime_minute');*/
/*      thCloseH = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_hour');*/
/*      thCloseM = document.getElementById('researchproject_myprojectbundle_clinic_thuCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*          thOpenH.disabled = true;*/
/*         thOpenM.disabled = true;*/
/*         thOpenH.value = 00;*/
/*         thOpenM.value = 00;*/
/*         thCloseH.disabled = true;*/
/*         thCloseM.disabled = true;*/
/*         thCloseH.value = 00;*/
/*         thCloseM.value = 00;*/
/*         */
/*          } else {*/
/*         thOpenH.disabled = false;*/
/*         thOpenM.disabled = false;*/
/*         thCloseH.disabled = false;*/
/*         thCloseM.disabled = false;*/
/*        */
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*  document.getElementById('friday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      */
/*      friOpenH = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_hour');*/
/*      friOpenM = document.getElementById('researchproject_myprojectbundle_clinic_friOpenTime_minute');*/
/*      friCloseH = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_hour');*/
/*      friCloseM = document.getElementById('researchproject_myprojectbundle_clinic_friCloseTime_minute');*/
/*      */
/*      if ( this.checked ) {*/
/*           friOpenH.disabled = true;*/
/*         friOpenM.disabled = true;*/
/*         friOpenH.value = 00;*/
/*         friOpenM.value = 00;*/
/*         friCloseH.disabled = true;*/
/*         friCloseM.disabled = true;*/
/*         friCloseH.value = 00;*/
/*         friCloseM.value = 00;*/
/*         */
/*         */
/*         */
/*          } else {*/
/*          */
/*         friOpenH.disabled = false;*/
/*         friOpenM.disabled = false;*/
/*         friCloseH.disabled = false;*/
/*         friCloseM.disabled = false;*/
/*        */
/*          }*/
/*          */
/*         */
/* };*/
/* */
/*  document.getElementById('saturday').onclick = function() {*/
/*     // access properties using this keyword*/
/*      */
/*      satOpenH = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_hour');*/
/*      satOpenM = document.getElementById('researchproject_myprojectbundle_clinic_satOpenTime_minute');*/
/*      satCloseH = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_hour');*/
/*      satCloseM = document.getElementById('researchproject_myprojectbundle_clinic_satCloseTime_minute');*/
/*      */
/*      */
/*      if ( this.checked ) {*/
/*        satOpenH.disabled = true;*/
/*         satOpenM.disabled = true;*/
/*         satOpenH.value = 00;*/
/*         satOpenM.value = 00;*/
/*         satCloseH.disabled = true;*/
/*         satCloseM.disabled = true;*/
/*         satCloseH.value = 00;*/
/*         satCloseM.value = 00;*/
/*         */
/*          } else {*/
/*         */
/*         satOpenH.disabled = false;*/
/*         satOpenM.disabled = false;*/
/*         satCloseH.disabled = false;*/
/*         satCloseM.disabled = false;*/
/*          }*/
/*          */
/*         */
/* };*/
/*      */
/*      */
/*     */
/* </script>*/
/* */
/* */
/* */
/* */
/* {% endblock %}*/
