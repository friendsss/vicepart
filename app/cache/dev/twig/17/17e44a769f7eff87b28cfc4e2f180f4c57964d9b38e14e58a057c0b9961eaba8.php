<?php

/* ResearchProjectMyProjectBundle:Illness:new.html.twig */
class __TwigTemplate_d9bd577a89f1cad40834eb28e61233a7a1668f0e3167bff2198b754d45510ce2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Illness:new.html.twig", 1);
        $this->blocks = array(
            'illness' => array($this, 'block_illness'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_73ee128077237fd284d96c185719563e890a675e9df89da0c52b5c3018bd73a5 = $this->env->getExtension("native_profiler");
        $__internal_73ee128077237fd284d96c185719563e890a675e9df89da0c52b5c3018bd73a5->enter($__internal_73ee128077237fd284d96c185719563e890a675e9df89da0c52b5c3018bd73a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Illness:new.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_73ee128077237fd284d96c185719563e890a675e9df89da0c52b5c3018bd73a5->leave($__internal_73ee128077237fd284d96c185719563e890a675e9df89da0c52b5c3018bd73a5_prof);

    }

    // line 3
    public function block_illness($context, array $blocks = array())
    {
        $__internal_0123aa50ef63127989266eb6ea5aaaad70dfc070472bc18a8a3b3f91831b57af = $this->env->getExtension("native_profiler");
        $__internal_0123aa50ef63127989266eb6ea5aaaad70dfc070472bc18a8a3b3f91831b57af->enter($__internal_0123aa50ef63127989266eb6ea5aaaad70dfc070472bc18a8a3b3f91831b57af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "illness"));

        echo " class=\"active\" ";
        
        $__internal_0123aa50ef63127989266eb6ea5aaaad70dfc070472bc18a8a3b3f91831b57af->leave($__internal_0123aa50ef63127989266eb6ea5aaaad70dfc070472bc18a8a3b3f91831b57af_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_d5568c407261053f10a0126c8394dd92a650119ca3c7065ba52a157e18120be9 = $this->env->getExtension("native_profiler");
        $__internal_d5568c407261053f10a0126c8394dd92a650119ca3c7065ba52a157e18120be9->enter($__internal_d5568c407261053f10a0126c8394dd92a650119ca3c7065ba52a157e18120be9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<center><h1><b>Define New Illness</b></h1></center>
    <div class=\"x_content\">
        <br />
        <div data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "registerIllnessForm")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Illness Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessName", array()), 'widget', array("attr" => array("id" => "illnessName", "name" => "illnessName", "placeholder" => "Illness Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 15
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessName", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Illness Common Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessCommonName", array()), 'widget', array("attr" => array("id" => "illnessCommonName", "name" => "illnessCommonName", "placeholder" => "Illness Common Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 24
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessCommonName", array()), 'errors');
        echo "</div></i>
             <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Define Illness <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessDefinition", array()), 'widget', array("attr" => array("id" => "illnessDefinition", "name" => "illnessDefinition", "placeholder" => "Define Illness", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 33
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessDefinition", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Describe Illness <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessDescription", array()), 'widget', array("attr" => array("id" => "illnessDescription", "name" => "illnessDescription", "placeholder" => "Describe illness", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 42
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "illnessDescription", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Symptoms <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "symptoms", array()), 'widget', array("attr" => array("id" => "symptoms", "name" => "symptoms", "placeholder" => "Describe symptoms", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 51
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "symptoms", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Home Remedies <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homeRemedies", array()), 'widget', array("attr" => array("id" => "homeRemedies", "name" => "homeRemedies", "placeholder" => "Alternatives found at home / First Aid", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 60
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "homeRemedies", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Commonly Prescribed Drugs <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commonlyPrescribedDrugs", array()), 'widget', array("attr" => array("id" => "commonlyPrescribedDrugs", "name" => "commonlyPrescribedDrugs", "placeholder" => "Commonly Prescribed Drugs", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 69
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "commonlyPrescribedDrugs", array()), 'errors');
        echo "</div></i>
            <div class=\"ln_solid\"></div>
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"\">";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
                </div>
            </div>
            ";
        // line 79
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
           
    
    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>

    
    <!-- icheck -->
    <script src=";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "></script>

    <script src=";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "></script>
    <!-- form validation -->
    <script src=";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#registerIllnessForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#registerIllnessForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
";
        
        $__internal_d5568c407261053f10a0126c8394dd92a650119ca3c7065ba52a157e18120be9->leave($__internal_d5568c407261053f10a0126c8394dd92a650119ca3c7065ba52a157e18120be9_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Illness:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 96,  201 => 94,  196 => 92,  180 => 79,  173 => 75,  167 => 72,  162 => 69,  160 => 68,  152 => 63,  147 => 60,  145 => 59,  137 => 54,  132 => 51,  130 => 50,  122 => 45,  117 => 42,  115 => 41,  107 => 36,  102 => 33,  100 => 32,  92 => 27,  87 => 24,  85 => 23,  77 => 18,  72 => 15,  70 => 14,  62 => 9,  56 => 5,  50 => 4,  38 => 3,  31 => 1,  29 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% form_theme form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {%block illness%} class="active" {% endblock %}*/
/* {% block body -%}*/
/* <center><h1><b>Define New Illness</b></h1></center>*/
/*     <div class="x_content">*/
/*         <br />*/
/*         <div data-parsley-validate class="form-horizontal form-label-left">*/
/*             {{ form_start(form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'registerIllnessForm' } }) }}*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Illness Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.illnessName, { 'attr' : { 'id': 'illnessName', 'name': 'illnessName', 'placeholder' : 'Illness Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.illnessName)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Illness Common Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.illnessCommonName, { 'attr' : { 'id': 'illnessCommonName', 'name': 'illnessCommonName', 'placeholder' : 'Illness Common Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.illnessCommonName)}}</div></i>*/
/*              <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Define Illness <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.illnessDefinition, { 'attr' : { 'id': 'illnessDefinition', 'name': 'illnessDefinition', 'placeholder' : 'Define Illness', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.illnessDefinition)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Describe Illness <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.illnessDescription, { 'attr' : { 'id': 'illnessDescription', 'name': 'illnessDescription', 'placeholder' : 'Describe illness', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.illnessDescription)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Symptoms <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.symptoms, { 'attr' : { 'id': 'symptoms', 'name': 'symptoms', 'placeholder' : 'Describe symptoms', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.symptoms)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Home Remedies <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.homeRemedies, { 'attr' : { 'id': 'homeRemedies', 'name': 'homeRemedies', 'placeholder' : 'Alternatives found at home / First Aid', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.homeRemedies)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Commonly Prescribed Drugs <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.commonlyPrescribedDrugs, { 'attr' : { 'id': 'commonlyPrescribedDrugs', 'name': 'commonlyPrescribedDrugs', 'placeholder' : 'Commonly Prescribed Drugs', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.commonlyPrescribedDrugs)}}</div></i>*/
/*             <div class="ln_solid"></div>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                     <a href="">{{ form_widget(form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn'} }) }}</a>*/
/*                     <button type="reset" class="btn btn-dark" onclick="goBack()" >Cancel</button>*/
/*                 </div>*/
/*             </div>*/
/*             {{ form_end(form)}}*/
/*     </div>*/
/*            */
/*     */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/* */
/*     */
/*     <!-- icheck -->*/
/*     <script src={{asset('gentelella/js/icheck/icheck.min.js')}}></script>*/
/* */
/*     <script src={{asset('gentelella/js/custom.js')}}></script>*/
/*     <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#registerIllnessForm')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#registerIllnessForm').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
