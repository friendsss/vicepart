<?php

/* ResearchProjectMyProjectBundle:DoctorGroup:edit.html.twig */
class __TwigTemplate_d8947652bfcd331216f839d7a28d978bb2b85039c5f7a1092cec9b819eacd8fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:DoctorGroup:edit.html.twig", 1);
        $this->blocks = array(
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf57a8e11aeaff672a3b9e6fb1991da8d101e7a94cf63143791d7dfc96558bb2 = $this->env->getExtension("native_profiler");
        $__internal_cf57a8e11aeaff672a3b9e6fb1991da8d101e7a94cf63143791d7dfc96558bb2->enter($__internal_cf57a8e11aeaff672a3b9e6fb1991da8d101e7a94cf63143791d7dfc96558bb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:DoctorGroup:edit.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cf57a8e11aeaff672a3b9e6fb1991da8d101e7a94cf63143791d7dfc96558bb2->leave($__internal_cf57a8e11aeaff672a3b9e6fb1991da8d101e7a94cf63143791d7dfc96558bb2_prof);

    }

    // line 3
    public function block_doctorgroup($context, array $blocks = array())
    {
        $__internal_41fe62dbbabd77a665509baed65128c0c8ca7c5a212f937f010fe29a9e84e91d = $this->env->getExtension("native_profiler");
        $__internal_41fe62dbbabd77a665509baed65128c0c8ca7c5a212f937f010fe29a9e84e91d->enter($__internal_41fe62dbbabd77a665509baed65128c0c8ca7c5a212f937f010fe29a9e84e91d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctorgroup"));

        echo " class=\"active\" ";
        
        $__internal_41fe62dbbabd77a665509baed65128c0c8ca7c5a212f937f010fe29a9e84e91d->leave($__internal_41fe62dbbabd77a665509baed65128c0c8ca7c5a212f937f010fe29a9e84e91d_prof);

    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_3c650e55467ecd01322228952b6af415926e756636adf146ae6b31756bc3adc0 = $this->env->getExtension("native_profiler");
        $__internal_3c650e55467ecd01322228952b6af415926e756636adf146ae6b31756bc3adc0->enter($__internal_3c650e55467ecd01322228952b6af415926e756636adf146ae6b31756bc3adc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "DOCTOR GROUP";
        
        $__internal_3c650e55467ecd01322228952b6af415926e756636adf146ae6b31756bc3adc0->leave($__internal_3c650e55467ecd01322228952b6af415926e756636adf146ae6b31756bc3adc0_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_de86749035fec343550f3bba7ca81ae9b137fafedb8a4616674a9952c11bd987 = $this->env->getExtension("native_profiler");
        $__internal_de86749035fec343550f3bba7ca81ae9b137fafedb8a4616674a9952c11bd987->enter($__internal_de86749035fec343550f3bba7ca81ae9b137fafedb8a4616674a9952c11bd987_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<center><h1><b>Edit Group Information</b></h1>

    <div class=\"x_content\">
        <br/>
        <div data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "editGroup")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Group Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "groupName", array()), 'widget', array("attr" => array("name" => "groupName", "placeholder" => "Group Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 17
        echo "
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Description <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "description", array()), 'widget', array("attr" => array("name" => "description", "placeholder" => "Description", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 25
        echo "
                </div>
            </div>
            ";
        // line 36
        echo "            <div class=\"ln_solid\"></div>
            <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                <a href=\"\">";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn", "onClick" => "submitClicked()")));
        echo "</a>
                <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
            </div>
        </div>
        
        
        ";
        // line 44
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
    </div>
    
    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
    <!-- form validation -->
    <script src=";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#editGroup')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#editGroup').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    
    
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
";
        
        $__internal_de86749035fec343550f3bba7ca81ae9b137fafedb8a4616674a9952c11bd987->leave($__internal_de86749035fec343550f3bba7ca81ae9b137fafedb8a4616674a9952c11bd987_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:DoctorGroup:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 55,  115 => 44,  106 => 38,  102 => 36,  97 => 25,  95 => 24,  86 => 17,  84 => 16,  76 => 11,  69 => 6,  63 => 5,  51 => 4,  39 => 3,  32 => 1,  30 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% form_theme edit_form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {% block doctorgroup %} class="active" {% endblock %}*/
/* {% block pagetitle %}DOCTOR GROUP{% endblock %}*/
/* {% block body -%}*/
/*     <center><h1><b>Edit Group Information</b></h1>*/
/* */
/*     <div class="x_content">*/
/*         <br/>*/
/*         <div data-parsley-validate class="form-horizontal form-label-left">*/
/*             {{ form_start(edit_form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'editGroup' } }) }}*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Group Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.groupName, { 'attr' : { 'name': 'groupName', 'placeholder' : 'Group Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Description <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.description, { 'attr' : { 'name': 'description', 'placeholder' : 'Description', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*             {#<div class="item form-group" hidden>*/
/*                  <label class="control-label col-md-3 col-sm-3 col-xs-12" >Date Created: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.dateCreated, { 'attr' : { 'name': 'dateCreated', 'placeholder' : 'Date Created', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>#}*/
/*             <div class="ln_solid"></div>*/
/*             <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                 <a href="">{{ form_widget(edit_form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn', 'onClick' : 'submitClicked()'} }) }}</a>*/
/*                 <button type="reset" class="btn btn-dark" onclick="goBack()" >Cancel</button>*/
/*             </div>*/
/*         </div>*/
/*         */
/*         */
/*         {{ form_end(edit_form)}}*/
/*     </div>*/
/*     */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/*     */
/*     <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#editGroup')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#editGroup').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*     */
/*     */
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/* {% endblock %}*/
/* */
