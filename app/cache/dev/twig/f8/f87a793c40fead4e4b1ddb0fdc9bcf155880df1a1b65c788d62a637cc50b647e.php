<?php

/* ResearchProjectMyProjectBundle:Admin:editIllnessSearch.html.twig */
class __TwigTemplate_1ce4f06aeafb846cad9cb337f19916e6b333c1ff613f139b07c8b5a286a95d10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:editIllnessSearch.html.twig", 1);
        $this->blocks = array(
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a38304b375b9c4b2e28b1d26fbc47fa11da42255811fdd765b4c0c0813f808fa = $this->env->getExtension("native_profiler");
        $__internal_a38304b375b9c4b2e28b1d26fbc47fa11da42255811fdd765b4c0c0813f808fa->enter($__internal_a38304b375b9c4b2e28b1d26fbc47fa11da42255811fdd765b4c0c0813f808fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:editIllnessSearch.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a38304b375b9c4b2e28b1d26fbc47fa11da42255811fdd765b4c0c0813f808fa->leave($__internal_a38304b375b9c4b2e28b1d26fbc47fa11da42255811fdd765b4c0c0813f808fa_prof);

    }

    // line 2
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_2b991df92d37a76c1f4dd1e8652fd14fa55f0f8cf763236f9b78c4e5908fdaee = $this->env->getExtension("native_profiler");
        $__internal_2b991df92d37a76c1f4dd1e8652fd14fa55f0f8cf763236f9b78c4e5908fdaee->enter($__internal_2b991df92d37a76c1f4dd1e8652fd14fa55f0f8cf763236f9b78c4e5908fdaee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "Edit Illness";
        
        $__internal_2b991df92d37a76c1f4dd1e8652fd14fa55f0f8cf763236f9b78c4e5908fdaee->leave($__internal_2b991df92d37a76c1f4dd1e8652fd14fa55f0f8cf763236f9b78c4e5908fdaee_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_52e61d5083a81251b955e4f3dbf0ba6a856d1544d904a0e94cf8bb5c38f65ef0 = $this->env->getExtension("native_profiler");
        $__internal_52e61d5083a81251b955e4f3dbf0ba6a856d1544d904a0e94cf8bb5c38f65ef0->enter($__internal_52e61d5083a81251b955e4f3dbf0ba6a856d1544d904a0e94cf8bb5c38f65ef0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    
                        <h3>Edit Illness</h3>
                       
                    <div class=\"x_content\">
                        <input class=\"form-control\" id='search' type=\"text\" name=\"query\" placeholder=\"Type a name\" />
                         
                                                    <center><img hidden id=\"loading\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("charisma/img/ajax-loaders/ajax-loader-1.gif"), "html", null, true);
        echo "\" ></center>
                                                    <table class=\"table table-striped\" id=\"resulttable\" class=\"hidden\">

                                                        <tbody id=\"resultbody\">

                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- form validation -->
<script>
                                   
   </script>
    <script>
\$('#search').keyup(function() {
    
     searchText = \$(this).val();
     if (this.value != ''){
         \$(\"#resultbody\").empty();
     }
     
     
     if (this.value.length >= 3 && this.value != '')
  {
     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 53
        echo $this->env->getExtension('routing')->getPath("admin_edit_illness_search");
        echo "',
        data: {searchText : searchText},
        beforeSend: function(){
        \$('#loading').show();
        },
        complete: function(){
        \$('#loading').hide();
        },
        success : function(data) 
           
           {if(data){
                \$(\"#resultbody\").empty();
                var len = data.length;
                var txt = \"\";
                  if(len > 0){
                    for(var i=0;i<len;i++){
                            //var button= \$('<button type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\"></i> Invite to Group </button>');
                            var entityId = data[i].id;
                            var showPath = '";
        // line 71
        echo $this->env->getExtension('routing')->getPath("admin_illness_edit", array("id" => "entityId"));
        echo "';
                            showPath = showPath.replace(\"entityId\", data[i].id);
                            txt += \"<tr><td>\"+data[i].illnessName+\"</td>\";
                            txt+= '<td><a href=\"'+showPath+'\" ><button id=\"edit-'+ data[i].id+'\" type=\"button\" class=\"btn btn-primary btn-xs btn-edit\"> <i class=\"fa fa-envelope\"></i> Edit Illness </button>';
                        
                    }
                    if(txt != \"\"){
                        \$(\"#resultbody\").append(txt).removeClass(\"hidden\");
                         
                    }
                }
            }
        },error: function() {
         alert('error');
          }
        });
    }
});
       
    </script>
    ";
        
        $__internal_52e61d5083a81251b955e4f3dbf0ba6a856d1544d904a0e94cf8bb5c38f65ef0->leave($__internal_52e61d5083a81251b955e4f3dbf0ba6a856d1544d904a0e94cf8bb5c38f65ef0_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:editIllnessSearch.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 71,  107 => 53,  70 => 19,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% block pagetitle %}Edit Illness{% endblock %}*/
/* {% block body -%}*/
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                     */
/*                         <h3>Edit Illness</h3>*/
/*                        */
/*                     <div class="x_content">*/
/*                         <input class="form-control" id='search' type="text" name="query" placeholder="Type a name" />*/
/*                          */
/*                                                     <center><img hidden id="loading" src="{{asset('charisma/img/ajax-loaders/ajax-loader-1.gif')}}" ></center>*/
/*                                                     <table class="table table-striped" id="resulttable" class="hidden">*/
/* */
/*                                                         <tbody id="resultbody">*/
/* */
/*                                                             </tr>*/
/*                                                         </tbody>*/
/*                                                     </table>*/
/* */
/*                                                 </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/* <script>*/
/*                                    */
/*    </script>*/
/*     <script>*/
/* $('#search').keyup(function() {*/
/*     */
/*      searchText = $(this).val();*/
/*      if (this.value != ''){*/
/*          $("#resultbody").empty();*/
/*      }*/
/*      */
/*      */
/*      if (this.value.length >= 3 && this.value != '')*/
/*   {*/
/*      $.ajax({*/
/*         type: "POST",*/
/*         dataType: "json",*/
/*         url: '{{ path('admin_edit_illness_search') }}',*/
/*         data: {searchText : searchText},*/
/*         beforeSend: function(){*/
/*         $('#loading').show();*/
/*         },*/
/*         complete: function(){*/
/*         $('#loading').hide();*/
/*         },*/
/*         success : function(data) */
/*            */
/*            {if(data){*/
/*                 $("#resultbody").empty();*/
/*                 var len = data.length;*/
/*                 var txt = "";*/
/*                   if(len > 0){*/
/*                     for(var i=0;i<len;i++){*/
/*                             //var button= $('<button type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user"></i> Invite to Group </button>');*/
/*                             var entityId = data[i].id;*/
/*                             var showPath = '{{ path("admin_illness_edit", {'id' : 'entityId' })}}';*/
/*                             showPath = showPath.replace("entityId", data[i].id);*/
/*                             txt += "<tr><td>"+data[i].illnessName+"</td>";*/
/*                             txt+= '<td><a href="'+showPath+'" ><button id="edit-'+ data[i].id+'" type="button" class="btn btn-primary btn-xs btn-edit"> <i class="fa fa-envelope"></i> Edit Illness </button>';*/
/*                         */
/*                     }*/
/*                     if(txt != ""){*/
/*                         $("#resultbody").append(txt).removeClass("hidden");*/
/*                          */
/*                     }*/
/*                 }*/
/*             }*/
/*         },error: function() {*/
/*          alert('error');*/
/*           }*/
/*         });*/
/*     }*/
/* });*/
/*        */
/*     </script>*/
/*     {% endblock %}*/
/* */
