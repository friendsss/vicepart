<?php

/* ResearchProjectMyProjectBundle:GroupPost:edit.html.twig */
class __TwigTemplate_a6c1f67d60125fc1274a95d9f59f48de94d095370f7f0aa520d03e877e9d8902 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:GroupPost:edit.html.twig", 1);
        $this->blocks = array(
            'doctor' => array($this, 'block_doctor'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e415c9025d0a2860763f63ddebd3c0aa07251cc195fb4fc2716298dbbf86f00 = $this->env->getExtension("native_profiler");
        $__internal_7e415c9025d0a2860763f63ddebd3c0aa07251cc195fb4fc2716298dbbf86f00->enter($__internal_7e415c9025d0a2860763f63ddebd3c0aa07251cc195fb4fc2716298dbbf86f00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:GroupPost:edit.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7e415c9025d0a2860763f63ddebd3c0aa07251cc195fb4fc2716298dbbf86f00->leave($__internal_7e415c9025d0a2860763f63ddebd3c0aa07251cc195fb4fc2716298dbbf86f00_prof);

    }

    // line 3
    public function block_doctor($context, array $blocks = array())
    {
        $__internal_53e0bc27cd53a394460533d14e20597c4bb4bbf0b80ec9465cc847e1b36c30c3 = $this->env->getExtension("native_profiler");
        $__internal_53e0bc27cd53a394460533d14e20597c4bb4bbf0b80ec9465cc847e1b36c30c3->enter($__internal_53e0bc27cd53a394460533d14e20597c4bb4bbf0b80ec9465cc847e1b36c30c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctor"));

        echo " class=\"active\" ";
        
        $__internal_53e0bc27cd53a394460533d14e20597c4bb4bbf0b80ec9465cc847e1b36c30c3->leave($__internal_53e0bc27cd53a394460533d14e20597c4bb4bbf0b80ec9465cc847e1b36c30c3_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_f4b15827a8b1586683d0c0f4cb2a069bf233e16b0e71fdfbff21cdcaabab5d71 = $this->env->getExtension("native_profiler");
        $__internal_f4b15827a8b1586683d0c0f4cb2a069bf233e16b0e71fdfbff21cdcaabab5d71->enter($__internal_f4b15827a8b1586683d0c0f4cb2a069bf233e16b0e71fdfbff21cdcaabab5d71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<center><h1><b>Edit Group Post</b></h1></center>
    <div id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">
        <br/>
        ";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "editPost")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Post Title: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "postTitle", array()), 'widget', array("attr" => array("name" => "postTitle", "placeholder" => "Post Title", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 15
        echo "
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Post Description: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "postContent", array()), 'widget', array("attr" => array("name" => "description", "placeholder" => "Post Content", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 23
        echo "
                </div>
            </div>
                
           
            
            <div class=\"ln_solid\"></div>
            
            <div class =\"form-group\">
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grouppost_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button class=\"btn btn-dark\" onclick=\"goBack()\">Cancel</button>
                </div>
            </div>
        ";
        // line 37
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
    </div>
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
    
    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
    <!-- form validation -->
    <script src=";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#editPost')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#editPost').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
";
        
        $__internal_f4b15827a8b1586683d0c0f4cb2a069bf233e16b0e71fdfbff21cdcaabab5d71->leave($__internal_f4b15827a8b1586683d0c0f4cb2a069bf233e16b0e71fdfbff21cdcaabab5d71_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:GroupPost:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 53,  103 => 37,  94 => 33,  82 => 23,  80 => 22,  71 => 15,  69 => 14,  61 => 9,  56 => 6,  50 => 5,  38 => 3,  31 => 1,  29 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% form_theme edit_form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {%block doctor%} class="active" {% endblock %}*/
/* */
/* {% block body -%}*/
/*     <center><h1><b>Edit Group Post</b></h1></center>*/
/*     <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">*/
/*         <br/>*/
/*         {{ form_start(edit_form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'editPost' } }) }}*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Post Title: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.postTitle, { 'attr' : { 'name': 'postTitle', 'placeholder' : 'Post Title', 'class' : 'form-control col-md-7 col-xs-12', */
/*                         'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Post Description: <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.postContent, { 'attr' : { 'name': 'description', 'placeholder' : 'Post Content', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 */
/*            */
/*             */
/*             <div class="ln_solid"></div>*/
/*             */
/*             <div class ="form-group">*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                     <a href="{{ path('grouppost_show', {'id': entity.id})}}"> {{ form_widget(edit_form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn'} }) }}</a>*/
/*                     <button class="btn btn-dark" onclick="goBack()">Cancel</button>*/
/*                 </div>*/
/*             </div>*/
/*         {{ form_end(edit_form) }}*/
/*     </div>*/
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/*     */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/*     */
/*     <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#editPost')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#editPost').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
