<?php

/* ::basesidebar.html.twig */
class __TwigTemplate_8f7bbb73dfaf89739664ede25be2a5cb26c9b5837f5b2e380da736b7dcfbedc0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    <!-- Bootstrap core CSS -->

    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/fonts/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom styling plus plugins -->
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/maps/jquery-jvectormap-2.0.1.css"), "html", null, true);
        echo "\" />
    <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/icheck/flat/green.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/floatexamples.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

    <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/nprogress.js"), "html", null, true);
        echo "\"></script>
    <script>
        NProgress.start();
    </script>
    
    <!--[if lt IE 9]>
        <script src=\"../assets/js/ie8-responsive-file-warning.js\"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
          <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->

</head>


<body class=\"nav-md\">

    <div class=\"container body\">


        <div class=\"main_container\">

            <div class=\"col-md-3 left_col\">
                <div class=\"left_col scroll-view\">

                    <div class=\"navbar nav_title\" style=\"border: 0;\">
                        <a href=\"index.html\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>FIMADILO</span></a>
                    </div>
                    <div class=\"clearfix\"></div>

                    <!-- menu prile quick info -->
                    <div class=\"profile\">
                        <div class=\"profile_pic\">
                            <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
        echo "\" alt=\"...\" class=\"img-circle profile_img\">
                        </div>
                        <div class=\"profile_info\">
                            <span>Welcome,</span>
                            <h2>";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "username"), "html", null, true);
        echo "</h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">

                        <div class=\"menu_section\">
                            <h3>General</h3>
                            <ul class=\"nav side-menu\">
                                <li><a><i class=\"fa fa-home\"></i> Profile <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"index.html\">Edit Profile</a>
                                        </li>
                                        <li><a href=\"index2.html\">View Profile</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-edit\"></i> Groups <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"form.html\">General Form</a>
                                        </li>
                                        <li><a href=\"form_advanced.html\">Advanced Components</a>
                                        </li>
                                        <li><a href=\"form_validation.html\">Form Validation</a>
                                        </li>
                                        <li><a href=\"form_wizards.html\">Form Wizard</a>
                                        </li>
                                        <li><a href=\"form_upload.html\">Form Upload</a>
                                        </li>
                                        <li><a href=\"form_buttons.html\">Form Buttons</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-desktop\"></i> UI Elements <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"general_elements.html\">General Elements</a>
                                        </li>
                                        <li><a href=\"media_gallery.html\">Media Gallery</a>
                                        </li>
                                        <li><a href=\"typography.html\">Typography</a>
                                        </li>
                                        <li><a href=\"icons.html\">Icons</a>
                                        </li>
                                        <li><a href=\"glyphicons.html\">Glyphicons</a>
                                        </li>
                                        <li><a href=\"widgets.html\">Widgets</a>
                                        </li>
                                        <li><a href=\"invoice.html\">Invoice</a>
                                        </li>
                                        <li><a href=\"inbox.html\">Inbox</a>
                                        </li>
                                        <li><a href=\"calender.html\">Calender</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-table\"></i> Tables <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"tables.html\">Tables</a>
                                        </li>
                                        <li><a href=\"tables_dynamic.html\">Table Dynamic</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-bar-chart-o\"></i> Data Presentation <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"chartjs.html\">Chart JS</a>
                                        </li>
                                        <li><a href=\"chartjs2.html\">Chart JS2</a>
                                        </li>
                                        <li><a href=\"morisjs.html\">Moris JS</a>
                                        </li>
                                        <li><a href=\"echarts.html\">ECharts </a>
                                        </li>
                                        <li><a href=\"other_charts.html\">Other Charts </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class=\"menu_section\">
                            <h3>Live On</h3>
                            <ul class=\"nav side-menu\">
                                <li><a><i class=\"fa fa-bug\"></i> Additional Pages <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"e_commerce.html\">E-commerce</a>
                                        </li>
                                        <li><a href=\"projects.html\">Projects</a>
                                        </li>
                                        <li><a href=\"project_detail.html\">Project Detail</a>
                                        </li>
                                        <li><a href=\"contacts.html\">Contacts</a>
                                        </li>
                                        <li><a href=\"profile.html\">Profile</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-windows\"></i> Extras <span class=\"fa fa-chevron-down\"></span></a>
                                    <ul class=\"nav child_menu\" style=\"display: none\">
                                        <li><a href=\"page_404.html\">404 Error</a>
                                        </li>
                                        <li><a href=\"page_500.html\">500 Error</a>
                                        </li>
                                        <li><a href=\"plain_page.html\">Plain Page</a>
                                        </li>
                                        <li><a href=\"login.html\">Login Page</a>
                                        </li>
                                        <li><a href=\"pricing_tables.html\">Pricing Tables</a>
                                        </li>

                                    </ul>
                                </li>
                                <li><a><i class=\"fa fa-laptop\"></i> Landing Page <span class=\"label label-success pull-right\">Coming Soon</span></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class=\"sidebar-footer hidden-small\">
                        <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">
                            <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
                        </a>
                        <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
                            <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>
                        </a>
                        <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">
                            <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
                        </a>
                        <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\">
                            <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class=\"top_nav\">

                <div class=\"nav_menu\">
                    <nav class=\"\" role=\"navigation\">
                        <div class=\"nav toggle\">
                            <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
                        </div>

                        <ul class=\"nav navbar-nav navbar-right\">
                            <li class=\"\">
                                <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                    <img src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
        echo "\" alt=\"\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "username"), "html", null, true);
        echo "
                                    <span class=\" fa fa-angle-down\"></span>
                                </a>
                                <ul class=\"dropdown-menu dropdown-usermenu animated fadeInDown pull-right\">
                                    <li><a href=\"javascript:;\">  Profile</a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:;\">
                                            <span class=\"badge bg-red pull-right\">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:;\">Help</a>
                                    </li>
                                    <li><a href=\"login.html\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>

                            <li role=\"presentation\" class=\"dropdown\">
                                <a href=\"javascript:;\" class=\"dropdown-toggle info-number\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                    <i class=\"fa fa-envelope-o\"></i>
                                    <span class=\"badge bg-green\">6</span>
                                </a>
                                <ul id=\"menu1\" class=\"dropdown-menu list-unstyled msg_list animated fadeInDown\" role=\"menu\">
                                    <li>
                                        <a>
                                            <span class=\"image\">
                                        <img src=\"images/img.jpg\" alt=\"Profile Image\" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class=\"time\">3 mins ago</span>
                                            </span>
                                            <span class=\"message\">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class=\"image\">
                                        <img src=\"images/img.jpg\" alt=\"Profile Image\" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class=\"time\">3 mins ago</span>
                                            </span>
                                            <span class=\"message\">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class=\"image\">
                                        <img src=\"images/img.jpg\" alt=\"Profile Image\" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class=\"time\">3 mins ago</span>
                                            </span>
                                            <span class=\"message\">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class=\"image\">
                                        <img src=\"images/img.jpg\" alt=\"Profile Image\" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class=\"time\">3 mins ago</span>
                                            </span>
                                            <span class=\"message\">
                                        Film festivals used to be do-or-die moments for movie makers. They were where... 
                                    </span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class=\"text-center\">
                                                <strong><a href=\"inbox.html\">See All Alerts</strong>
                                                <i class=\"fa fa-angle-right\"></i>
                                        </div>
                                <ul class=\"dropdown-menu dropdown-usermenu animated fadeInDown pull-right\">
                                    <li><a href=\"javascript:;\">  Profile</a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:;\">
                                            <span class=\"badge bg-red pull-right\">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:;\">Help</a>
                                    </li>
                                    <li><a href=\"login.html\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a>
                                    </li>
                                </ul>
                                    </li>
                                </ul>
                                
                            </li>
                            
                            <li>
                                <a>
                                 <div class=\"form-group top_search\">
                                    <form class=\"navbar-form navbar-right\" role=\"form\"  method=\"POST\">
                                    <div class=\"input-group\">
                                        <input type=\"text\"  class=\"form-control\" id=\"search\" name=\"search\" placeholder=\"Search\" >
                                        <span class=\"input-group-btn\">
                                        <button class=\"btn btn-default\" type=\"button\">Go!</button>
                                        </span>
                                    </div>
                                    
                                 </form>
                                     </div>
                                </a>     
                                
                            </li>

                        </ul>
                    </nav>
                </div>
                                        
                                        

            </div>
            <!-- /top navigation -->
            
<!-- page content -->
            <div class=\"right_col\" role=\"main\">
                <div class=\"\">
                    <div class=\"page-title\">
                       
                    </div>
                    <div class=\"clearfix\"></div>

                    <div class=\"row\">

                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\" style=\"height:900px;\">
                                <div class=\"x_title\">
                                    <h2>";
        // line 367
        $this->displayBlock('pagetitle', $context, $blocks);
        echo "</h2>
                                    
                                    <div class=\"clearfix\"></div>
                                </div>
                                ";
        // line 371
        $this->displayBlock('body', $context, $blocks);
        // line 372
        echo "                            </div>
                        </div>
                    </div>
                    
                                       
                </div>

                

            </div>
            <!-- /page content -->


                
           
        </div>

    </div>

    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>

    <script src=\"";
        // line 398
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

    <!-- gauge js -->
    <script type=\"text/javascript\" src=\"";
        // line 401
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/gauge/gauge.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 402
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/gauge/gauge_demo.js"), "html", null, true);
        echo "\"></script>
    <!-- chart js -->
    <script src=\"";
        // line 404
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/chartjs/chart.min.js"), "html", null, true);
        echo "\"></script>
    <!-- bootstrap progress js -->
    <script src=\"";
        // line 406
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/progressbar/bootstrap-progressbar.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 407
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/nicescroll/jquery.nicescroll.min.js"), "html", null, true);
        echo "\"></script>
    <!-- icheck -->
    <script src=\"";
        // line 409
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
    <!-- daterangepicker -->
    <script type=\"text/javascript\" src=\"";
        // line 411
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/moment.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 412
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/datepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 414
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "\"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type=\"text/javascript\" src=\"";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/excanvas.min.js"), "html", null, true);
        echo "\"></script><![endif]-->
    <script type=\"text/javascript\" src=\"";
        // line 418
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 419
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.pie.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 420
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.orderBars.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 421
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.time.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 422
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/date.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 423
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.spline.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.stack.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 425
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/curvedLines.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 426
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/flot/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(document).ready(function () {
            // [17, 74, 6, 39, 20, 85, 7]
            //[82, 23, 66, 9, 99, 6, 2]
            var data1 = [[gd(2012, 1, 1), 17], [gd(2012, 1, 2), 74], [gd(2012, 1, 3), 6], [gd(2012, 1, 4), 39], [gd(2012, 1, 5), 20], [gd(2012, 1, 6), 85], [gd(2012, 1, 7), 7]];

            var data2 = [[gd(2012, 1, 1), 82], [gd(2012, 1, 2), 23], [gd(2012, 1, 3), 66], [gd(2012, 1, 4), 9], [gd(2012, 1, 5), 119], [gd(2012, 1, 6), 6], [gd(2012, 1, 7), 9]];
            \$(\"#canvas_dahs\").length && \$.plot(\$(\"#canvas_dahs\"), [
                data1, data2
            ], {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    verticalLines: true,
                    hoverable: true,
                    clickable: true,
                    tickColor: \"#d5d5d5\",
                    borderWidth: 1,
                    color: '#fff'
                },
                colors: [\"rgba(38, 185, 154, 0.38)\", \"rgba(3, 88, 106, 0.38)\"],
                xaxis: {
                    tickColor: \"rgba(51, 51, 51, 0.06)\",
                    mode: \"time\",
                    tickSize: [1, \"day\"],
                    //tickLength: 10,
                    axisLabel: \"Date\",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                        //mode: \"time\", timeformat: \"%m/%d/%y\", minTickSize: [1, \"day\"]
                },
                yaxis: {
                    ticks: 8,
                    tickColor: \"rgba(51, 51, 51, 0.06)\",
                },
                tooltip: false
            });

            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }
        });
    </script>

    <!-- worldmap -->
    <script type=\"text/javascript\" src=\"";
        // line 489
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/maps/jquery-jvectormap-2.0.1.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 490
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/maps/gdp-data.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 491
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/maps/jquery-jvectormap-world-mill-en.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 492
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/maps/jquery-jvectormap-us-aea-en.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(function () {
            \$('#world-map-gdp').vectorMap({
                map: 'world_mill_en',
                backgroundColor: 'transparent',
                zoomOnScroll: false,
                series: {
                    regions: [{
                        values: gdpData,
                        scale: ['#E6F2F0', '#149B7E'],
                        normalizeFunction: 'polynomial'
                    }]
                },
                onRegionTipShow: function (e, el, code) {
                    el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
                }
            });
        });
    </script>
    <!-- skycons -->
    <script src=\"js/skycons/skycons.js\"></script>
    <script>
        var icons = new Skycons({
                \"color\": \"#73879C\"
            }),
            list = [
                \"clear-day\", \"clear-night\", \"partly-cloudy-day\",
                \"partly-cloudy-night\", \"cloudy\", \"rain\", \"sleet\", \"snow\", \"wind\",
                \"fog\"
            ],
            i;

        for (i = list.length; i--;)
            icons.set(list[i], list[i]);

        icons.play();
    </script>

    <!-- dashbord linegraph -->
    <script>
        var doughnutData = [
            {
                value: 30,
                color: \"#455C73\"
            },
            {
                value: 30,
                color: \"#9B59B6\"
            },
            {
                value: 60,
                color: \"#BDC3C7\"
            },
            {
                value: 100,
                color: \"#26B99A\"
            },
            {
                value: 120,
                color: \"#3498DB\"
            }
    ];
        var myDoughnut = new Chart(document.getElementById(\"canvas1\").getContext(\"2d\")).Doughnut(doughnutData);
    </script>
    <!-- /dashbord linegraph -->
    <!-- datepicker -->
    <script type=\"text/javascript\">
        \$(document).ready(function () {

            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                \$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                //alert(\"Callback has fired: [\" + start.format('MMMM D, YYYY') + \" to \" + end.format('MMMM D, YYYY') + \", label = \" + label + \"]\");
            }

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };
            \$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            \$('#reportrange').daterangepicker(optionSet1, cb);
            \$('#reportrange').on('show.daterangepicker', function () {
                console.log(\"show event fired\");
            });
            \$('#reportrange').on('hide.daterangepicker', function () {
                console.log(\"hide event fired\");
            });
            \$('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                console.log(\"apply event fired, start/end dates are \" + picker.startDate.format('MMMM D, YYYY') + \" to \" + picker.endDate.format('MMMM D, YYYY'));
            });
            \$('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                console.log(\"cancel event fired\");
            });
            \$('#options1').click(function () {
                \$('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
            });
            \$('#options2').click(function () {
                \$('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
            });
            \$('#destroy').click(function () {
                \$('#reportrange').data('daterangepicker').remove();
            });
        });
    </script>
    <script>
        NProgress.done();
    </script>
    <!-- /datepicker -->
    <!-- /footer content -->
</body>

</html>

";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        echo "FIMADILO";
    }

    // line 367
    public function block_pagetitle($context, array $blocks = array())
    {
    }

    // line 371
    public function block_body($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::basesidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  795 => 371,  790 => 367,  784 => 11,  631 => 492,  627 => 491,  623 => 490,  619 => 489,  553 => 426,  549 => 425,  545 => 424,  541 => 423,  537 => 422,  533 => 421,  529 => 420,  525 => 419,  521 => 418,  517 => 417,  511 => 414,  506 => 412,  502 => 411,  497 => 409,  492 => 407,  488 => 406,  483 => 404,  478 => 402,  474 => 401,  468 => 398,  440 => 372,  438 => 371,  431 => 367,  280 => 221,  123 => 67,  116 => 63,  77 => 27,  73 => 26,  68 => 24,  64 => 23,  60 => 22,  56 => 21,  50 => 18,  46 => 17,  41 => 15,  34 => 11,  22 => 1,  42 => 5,  36 => 4,  30 => 2,);
    }
}
