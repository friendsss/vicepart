<?php

/* ResearchProjectMyProjectBundle:DoctorGroup:edit.html.twig */
class __TwigTemplate_e23bd1f30e42b04ea299ec138fd842061ac4949ccd3868f5a686ae86fc9432dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_doctorgroup($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        echo "DOCTOR GROUP";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "<center><h1><b>Edit Group Information</b></h1>

    <div class=\"x_content\">
        <br/>
        <div data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "editGroup")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Group Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "groupName"), 'widget', array("attr" => array("name" => "groupName", "placeholder" => "Group Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 17
        echo "
                </div>
            </div>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Description <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "description"), 'widget', array("attr" => array("name" => "description", "placeholder" => "Description", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 25
        echo "
                </div>
            </div>
            <div class=\"item form-group\" hidden>
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Date Created: <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "dateCreated"), 'widget', array("attr" => array("name" => "dateCreated", "placeholder" => "Date Created", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 33
        echo "
                </div>
            </div>
            <div class=\"ln_solid\"></div>
            <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                <a href=\"\">";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "submit"), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn", "onClick" => "submitClicked()")));
        echo "</a>
                <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
            </div>
        </div>
        
        
        ";
        // line 44
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
    </div>
    
    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
    <!-- form validation -->
    <script src=";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#editGroup')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#editGroup').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    
    
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:DoctorGroup:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 55,  102 => 44,  93 => 38,  86 => 33,  84 => 32,  75 => 25,  73 => 24,  64 => 17,  62 => 16,  54 => 11,  47 => 6,  44 => 5,  38 => 4,  32 => 3,  27 => 2,);
    }
}
