<?php

/* ResearchProjectMyProjectBundle:Doctor:new.html.twig */
class __TwigTemplate_523db6c6afedcc7e959c89ba71c4d1c7efe9901baf9d4341f30661dc481a3806 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Doctor:new.html.twig", 1);
        $this->blocks = array(
            'doctor' => array($this, 'block_doctor'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_480f89ef307678a3ce88721dc1bcd7c43393471d5c93f153466c574b1dbc8ca0 = $this->env->getExtension("native_profiler");
        $__internal_480f89ef307678a3ce88721dc1bcd7c43393471d5c93f153466c574b1dbc8ca0->enter($__internal_480f89ef307678a3ce88721dc1bcd7c43393471d5c93f153466c574b1dbc8ca0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Doctor:new.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_480f89ef307678a3ce88721dc1bcd7c43393471d5c93f153466c574b1dbc8ca0->leave($__internal_480f89ef307678a3ce88721dc1bcd7c43393471d5c93f153466c574b1dbc8ca0_prof);

    }

    // line 3
    public function block_doctor($context, array $blocks = array())
    {
        $__internal_f7bfa3daf0938f3b0902241404f71d3c27adaeafdc918b440d3c33aa6df84d3b = $this->env->getExtension("native_profiler");
        $__internal_f7bfa3daf0938f3b0902241404f71d3c27adaeafdc918b440d3c33aa6df84d3b->enter($__internal_f7bfa3daf0938f3b0902241404f71d3c27adaeafdc918b440d3c33aa6df84d3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctor"));

        echo " class=\"active\" ";
        
        $__internal_f7bfa3daf0938f3b0902241404f71d3c27adaeafdc918b440d3c33aa6df84d3b->leave($__internal_f7bfa3daf0938f3b0902241404f71d3c27adaeafdc918b440d3c33aa6df84d3b_prof);

    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_7c7f14a7098744a54b03e3531ec96321d8dabd3a37f1b05912b4346b9652edcd = $this->env->getExtension("native_profiler");
        $__internal_7c7f14a7098744a54b03e3531ec96321d8dabd3a37f1b05912b4346b9652edcd->enter($__internal_7c7f14a7098744a54b03e3531ec96321d8dabd3a37f1b05912b4346b9652edcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "DOCTOR";
        
        $__internal_7c7f14a7098744a54b03e3531ec96321d8dabd3a37f1b05912b4346b9652edcd->leave($__internal_7c7f14a7098744a54b03e3531ec96321d8dabd3a37f1b05912b4346b9652edcd_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3bef5b03441d26daeb4d1aaa44bdabe52d5d45d781a6780234484ea521b490b3 = $this->env->getExtension("native_profiler");
        $__internal_3bef5b03441d26daeb4d1aaa44bdabe52d5d45d781a6780234484ea521b490b3->enter($__internal_3bef5b03441d26daeb4d1aaa44bdabe52d5d45d781a6780234484ea521b490b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<link href=";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/select/select2.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
</br>

    <center><h1><b>Register New Doctor</b></h1>
    <div>
        <br/>
        <br/>
        <div >
            ";
        // line 14
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "registerForm", "class" => "form-horizontal form-label-left")));
        echo "
            <div id='fn' class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >First Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "firstName", array()), 'widget', array("attr" => array("id" => "firstName", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 20
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "firstName", array()), 'errors');
        echo "</div></i>
            <div id='ln' class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Last Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lastName", array()), 'widget', array("attr" => array("name" => "lastName", "placeholder" => "Last Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "text")));
        // line 29
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lastName", array()), 'errors');
        echo "</div></i>
            <div id='mn' class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Middle Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "middleName", array()), 'widget', array("attr" => array("name" => "middleName", "placeholder" => "Middle Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "text")));
        // line 38
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "middleName", array()), 'errors');
        echo "</div></i>
            <div id='ea' class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Email Address <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailAddress", array()), 'widget', array("attr" => array("name" => "emailAddress", "placeholder" => "Email Address", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "10", "required" => "required", "type" => "email")));
        // line 47
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailAddress", array()), 'errors');
        echo "</div></i>
            <div id=\"uname\" class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Username <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("name" => "username", "placeholder" => "Username", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "5", "required" => "required", "type" => "text")));
        // line 56
        echo "
                </div>
               
            </div>
                 
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'errors');
        echo "</div></i>
            <div id='pass1' class=\"item form-group\">
                <label for=\"first\" class=\"control-label col-md-3\" >Password <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), "first", array()), 'widget', array("attr" => array("type" => "password", "placeholder" => "Password", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "8,15", "required" => "required")));
        // line 67
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), "first", array()), 'errors');
        echo "</div></i>
             <div id='pass2' class=\"item form-group\">
                <label for=\"second\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Repeat Password <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), "second", array()), 'widget', array("attr" => array("id" => "second", "type" => "password", "name" => "second", "placeholder" => "Repeat Password", "class" => "form-control col-md-7 col-xs-12", "data-validate-linked" => "researchproject_myprojectbundle_doctor_password_first", "required" => "required")));
        // line 76
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password", array()), "second", array()), 'errors');
        echo "</div></i>
            <div id=\"dob\" class=\"form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Date of Birth <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                   ";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateOfBirth", array()), 'widget', array("attr" => array("name" => "dateOfBirth", "placeholder" => "Date of Birth", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "5", "type" => "text")));
        // line 85
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateOfBirth", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Field of Practice <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 92
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fieldOfPractice", array()), 'widget', array("attr" => array("name" => "fieldOfPractice", "placeholder" => "Field of Practice", "class" => "form-control col-md-7 col-xs-12", "type" => "text")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fieldOfPractice", array()), 'errors');
        echo "</div></i>
                
            <div class=\"item form-group\" id=\"specialtyblock\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Specialty <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 100
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "specialties", array()), 'widget', array("attr" => array("name" => "specialties", "class" => "select2_multiple form-control", "type" => "text")));
        echo "
                    </div>
            </div>
                 <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "specialties", array()), 'errors');
        echo "</div></i>
            <div id='cn' class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Contact Numbers <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 108
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contactNumber", array()), 'widget', array("attr" => array("name" => "contactnumber", "placeholder" => "Contact Number", "class" => "form-control col-md-7 col-xs-12", "required" => "required", "type" => "text")));
        // line 109
        echo "
                    
                </div>
               
            </div>
                  <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 114
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contactNumber", array()), 'errors');
        echo "</div></i>
               
            <div id='fell' class=\"item form-group\" id=\"fellowshipblock\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Fellowship/s <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fellowships", array()), 'widget', array("attr" => array("name" => "fellowships", "class" => "select2_multiple form-control", "type" => "text")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fellowships", array()), 'errors');
        echo "</div></i>
            <div id='prc' class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">PRC Number <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 127
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prcNumber", array()), 'widget', array("attr" => array("name" => "prcNumber", "placeholder" => "PRC Number", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "7,7", "required" => "required", "type" => "number")));
        // line 128
        echo "  
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prcNumber", array()), 'errors');
        echo "</div></i>
            <div id=\"oid\" class=\" form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Original Issue Date <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                   ";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "origIssueDate", array()), 'widget', array("attr" => array("name" => "origIssueDate", "placeholder" => "Original Issue Date:", "class" => "form-control col-md-7 col-xs-12", "type" => "text")));
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 139
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "origIssueDate", array()), 'errors');
        echo "</div></i>
            <div id='amp' class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">About My Practice <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 143
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "aboutMyPractice", array()), 'widget', array("attr" => array("name" => "aboutMyPractice", "placeholder" => "About My Practice", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "6", "type" => "text")));
        // line 144
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 147
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "aboutMyPractice", array()), 'errors');
        echo "</div></i>
                <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Profile Picture <span class=\"required\">*</span>
                </label>
                    ";
        // line 151
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "file", array()), 'widget');
        echo "
            </div>
                </br>
            <div class=\"ln_solid\"></div>
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"\">";
        // line 156
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
                </div>
            </div>
           ";
        // line 160
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>

    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
<script type=\"text/javascript\" src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/external/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
     <script type=\"text/javascript\" src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
     <script src=";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/bootstrap.min.js"), "html", null, true);
        echo "></script>
    <!-- icheck -->
    <script src=";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "></script>

    <script src=";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "></script>
    <!-- form validation -->
    <script src=";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/select/select2.full.js"), "html", null, true);
        echo "></script>
    <!-- tags -->
        <script src=";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/tags/jquery.tagsinput.min.js"), "html", null, true);
        echo "></script>
    <script type=\"text/javascript\" src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("notify/notify.js"), "html", null, true);
        echo "\"></script>
    <!-- input tags -->
        
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
 
    <script>
        \$.notify.addStyle('formerror', {
                html: \"<div><span data-notify-text/></div>\",
                classes: {
                  base: {
                    \"color\": \"white\",
                    \"white-space\": \"nowrap\",
                    \"background-color\": \"#ce5454\",
                     \"padding\": \"3px 10px\",
                    \"border-radius\" : \"3px 4px 4px 3px\",
                    
                    \"border\": \"2px solid #ce5454\",
                    
                    
                  }
                }
              });
    
    // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#registerForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);
        function checkFormFields(){
            var isFalse = true;
            
            
            
        }
       

        \$('#registerForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }
            

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script type=\"text/javascript\">
            \$( \"#addcontact\" ).click(function() {
                alert( \"Handler for .click() called.\" );
              });
        
            var choice = document.getElementById(\"researchproject_myprojectbundle_doctor_fieldOfPractice\");
            choice.onchange=newChoice;
            function newChoice()
            {   
                var choice = document.getElementById(\"researchproject_myprojectbundle_doctor_fieldOfPractice\");
                var selectedValue = choice.options[choice.selectedIndex].value;


                if (selectedValue == \"Specialized\")
                {   document.getElementById(\"specialtyblock\").style.display = \"block\";
                }
                else
                {
                   document.getElementById(\"specialtyblock\").style.display = \"none\";
                }
            }
    </script>
    <script>
            \$(document).ready(function () {
                \$(\".select2_single\").select2({
                    allowClear: true
                });
                \$(\".select2_group\").select2({});
                \$(\".select2_multiple\").select2({
                    allowClear: true
                });
                
                
            });
        </script>
        <script>
        \$(document).ready(function () {
            \$('#researchproject_myprojectbundle_doctor_dateOfBirth').datepicker({
                maxDate: Date.now()
                
            }).on('change', function() {
               verifyDate( \$('#researchproject_myprojectbundle_doctor_dateOfBirth').val());
             });
            
            \$('#researchproject_myprojectbundle_doctor_origIssueDate').datepicker({
                maxDate: Date.now()
                
            }).on('change', function() {
               verifyIssueDate( \$('#researchproject_myprojectbundle_doctor_origIssueDate').val());
             }); 
            
            
            
            
            \$.notify.addStyle('formerror', {
                html: \"<div><span data-notify-text/></div>\",
                classes: {
                  base: {
                    \"color\": \"white\",
                    \"white-space\": \"nowrap\",
                    \"background-color\": \"#ce5454\",
                     \"padding\": \"3px 10px\",
                    \"border-radius\" : \"3px 4px 4px 3px\",
                    
                    \"border\": \"2px solid #ce5454\",
                    
                    
                  }
                }
              });
            
            
        });
        
        function showError(text, div){
      
        \$(div).notify(text,  
             { position: \"right\", autoHide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
        }

        function checkDate(date){
            var isTrue = true;
            var idate = document.getElementById(\"researchproject_myprojectbundle_doctor_dateOfBirth\"),
             dateReg = /^\\d{1,2}\\/\\d{1,2}\\/\\d{4}\$/;

        if(!dateReg.test(idate.value)){
            isTrue = false;
            }
            
            var now = new Date();
            if (Date.parse(idate.value) > now) {
              // selected date is in the past
              isTrue = false;
            }
            
             
            return isTrue;
        }
        
         function checkIssueDate(date){
            var isTrue = true;
            var idate = document.getElementById(\"researchproject_myprojectbundle_doctor_origIssueDate\"),
             dateReg = /^\\d{1,2}\\/\\d{1,2}\\/\\d{4}\$/;

        
        if(!dateReg.test(idate.value)){
            isTrue = false;
            }
            
            var now = new Date();
            if (Date.parse(idate.value) > now) {
              // selected date is in the past
              isTrue = false;
            }
            
             
            return isTrue;
        }
        
        function verifyDate(date){
            
            if(checkDate(date) === false){
                \$('#researchproject_myprojectbundle_doctor_dateOfBirth').addClass('parsley-error');
                 \$(\"#researchproject_myprojectbundle_doctor_dateOfBirth\").notify(\"Invalid Date\",  
                            { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            }else{
                \$('#researchproject_myprojectbundle_doctor_dateOfBirth').removeClass('parsley-error');
                \$('#dob .notifyjs-wrapper').trigger('notify-hide');
            }
           
        }
        
        function verifyIssueDate(date){
            
            if(checkIssueDate(date) === false){
                \$('#researchproject_myprojectbundle_doctor_origIssueDate').addClass('parsley-error');
                 \$(\"#researchproject_myprojectbundle_doctor_origIssueDate\").notify(\"Invalid Date\",  
                            { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            }else{
                \$('#researchproject_myprojectbundle_doctor_origIssueDate').removeClass('parsley-error');
                \$('#oid .notifyjs-wrapper').trigger('notify-hide');
            }
           
        }

    </script>
    <script>
  \$('#researchproject_myprojectbundle_doctor_username').keyup(function() {

     usernameText = \$(this).val();
    
     
     
     if (this.value.length >= 3 && this.value != '')
  {
     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 399
        echo $this->env->getExtension('routing')->getPath("username_check");
        echo "',
        data: {usernameText : usernameText },
        beforeSend: function(){
        \$('#loading').show();
        },
        complete: function(){
        \$('#loading').hide();
        },
        success : function(data){
            
            if(data.duplicate === 'true'){
                \$('#researchproject_myprojectbundle_doctor_username').addClass('parsley-error');
                 \$(\"#researchproject_myprojectbundle_doctor_username\").notify(\"Sorry! That username is already taken.\",  
                            { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            }else{
                \$('#researchproject_myprojectbundle_doctor_username').removeClass('parsley-error');
                \$('#uname .notifyjs-wrapper').trigger('notify-hide');
            }
            
        },error: function() {
         alert('error');
          }
        });
    }
});
       
    </script>
";
        
        $__internal_3bef5b03441d26daeb4d1aaa44bdabe52d5d45d781a6780234484ea521b490b3->leave($__internal_3bef5b03441d26daeb4d1aaa44bdabe52d5d45d781a6780234484ea521b490b3_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Doctor:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  595 => 399,  374 => 181,  370 => 180,  365 => 178,  361 => 177,  356 => 175,  351 => 173,  346 => 171,  342 => 170,  338 => 169,  326 => 160,  319 => 156,  311 => 151,  304 => 147,  299 => 144,  297 => 143,  290 => 139,  284 => 136,  276 => 131,  271 => 128,  269 => 127,  261 => 122,  255 => 119,  247 => 114,  240 => 109,  238 => 108,  230 => 103,  224 => 100,  216 => 95,  210 => 92,  203 => 88,  198 => 85,  196 => 84,  188 => 79,  183 => 76,  181 => 75,  173 => 70,  168 => 67,  166 => 66,  158 => 61,  151 => 56,  149 => 55,  141 => 50,  136 => 47,  134 => 46,  126 => 41,  121 => 38,  119 => 37,  111 => 32,  106 => 29,  104 => 28,  96 => 23,  91 => 20,  89 => 19,  81 => 14,  69 => 6,  63 => 5,  51 => 4,  39 => 3,  32 => 1,  30 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% form_theme form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {%block doctor%} class="active" {% endblock %}*/
/* {%block pagetitle%}DOCTOR{% endblock%}*/
/* {% block body -%}*/
/*  <link href={{asset('gentelella/css/select/select2.min.css')}} rel="stylesheet">*/
/* </br>*/
/* */
/*     <center><h1><b>Register New Doctor</b></h1>*/
/*     <div>*/
/*         <br/>*/
/*         <br/>*/
/*         <div >*/
/*             {{ form_start(form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'registerForm', 'class':'form-horizontal form-label-left' } }) }}*/
/*             <div id='fn' class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >First Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.firstName, { 'attr' : { 'id': 'firstName', 'name': 'firstName', 'placeholder' : 'First Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '1',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.firstName)}}</div></i>*/
/*             <div id='ln' class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Last Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.lastName, { 'attr' : { 'name': 'lastName', 'placeholder' : 'Last Name', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                    'data-validate-length-range' : '2', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.lastName)}}</div></i>*/
/*             <div id='mn' class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Middle Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.middleName, { 'attr' : { 'name': 'middleName', 'placeholder' : 'Middle Name', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                      'data-validate-length-range' : '2', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.middleName)}}</div></i>*/
/*             <div id='ea' class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Address <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.emailAddress, { 'attr' : { 'name': 'emailAddress', 'placeholder' : 'Email Address', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                      'data-validate-length-range' : '10', 'required': 'required', 'type' : 'email' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.emailAddress)}}</div></i>*/
/*             <div id="uname" class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Username <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.username, { 'attr' : { 'name': 'username', 'placeholder' : 'Username', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '5', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*                */
/*             </div>*/
/*                  */
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.username)}}</div></i>*/
/*             <div id='pass1' class="item form-group">*/
/*                 <label for="first" class="control-label col-md-3" >Password <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{  form_widget(form.password.first, { 'attr' : { 'type' : 'password', 'placeholder' : 'Password', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                             'data-validate-length-range' : '8,15', 'required': 'required'  } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.password.first)}}</div></i>*/
/*              <div id='pass2' class="item form-group">*/
/*                 <label for="second" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat Password <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{  form_widget(form.password.second, { 'attr' : {  'id' : 'second' ,'type' : 'password', 'name': 'second', 'placeholder' : 'Repeat Password', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                             'data-validate-linked' : 'researchproject_myprojectbundle_doctor_password_first', 'required': 'required' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.password.second)}}</div></i>*/
/*             <div id="dob" class="form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                    {{  form_widget(form.dateOfBirth, { 'attr' : { 'name': 'dateOfBirth', 'placeholder' : 'Date of Birth', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                         'data-validate-length-range' : '5',  'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.dateOfBirth)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Field of Practice <span class="required">*</span></label>*/
/*                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                         {{  form_widget(form.fieldOfPractice, { 'attr' : { 'name': 'fieldOfPractice', 'placeholder' : 'Field of Practice', 'class' : 'form-control col-md-7 col-xs-12', 'type' : 'text'} }) }}*/
/*                     </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.fieldOfPractice)}}</div></i>*/
/*                 */
/*             <div class="item form-group" id="specialtyblock">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Specialty <span class="required">*</span></label>*/
/*                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                         {{  form_widget(form.specialties, { 'attr' : { 'name': 'specialties', 'class' :'select2_multiple form-control','type' : 'text'} }) }}*/
/*                     </div>*/
/*             </div>*/
/*                  <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.specialties)}}</div></i>*/
/*             <div id='cn' class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Numbers <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(form.contactNumber, { 'attr' : { 'name': 'contactnumber', 'placeholder' : 'Contact Number', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                      'required': 'required', 'type' : 'text' } }) }}*/
/*                     */
/*                 </div>*/
/*                */
/*             </div>*/
/*                   <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.contactNumber)}}</div></i>*/
/*                */
/*             <div id='fell' class="item form-group" id="fellowshipblock">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Fellowship/s <span class="required">*</span></label>*/
/*                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                         {{  form_widget(form.fellowships, { 'attr' : { 'name': 'fellowships', 'class' :'select2_multiple form-control','type' : 'text'} }) }}*/
/*                     </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.fellowships)}}</div></i>*/
/*             <div id='prc' class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">PRC Number <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{  form_widget(form.prcNumber, { 'attr' : { 'name': 'prcNumber', 'placeholder' : 'PRC Number', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                        'data-validate-length-range' : '7,7',  'required': 'required', 'type' : 'number' } }) }}  */
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.prcNumber)}}</div></i>*/
/*             <div id="oid" class=" form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Original Issue Date <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                    {{  form_widget(form.origIssueDate, { 'attr' : { 'name': 'origIssueDate', 'placeholder' : 'Original Issue Date:', 'class' : 'form-control col-md-7 col-xs-12' ,'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.origIssueDate)}}</div></i>*/
/*             <div id='amp' class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">About My Practice <span class="required">*</span></label>*/
/*                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                         {{  form_widget(form.aboutMyPractice, { 'attr' : { 'name': 'aboutMyPractice', 'placeholder' : 'About My Practice', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                                  'data-validate-length-range' : '6',  'type' : 'text' } }) }}*/
/*                     </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(form.aboutMyPractice)}}</div></i>*/
/*                 <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Profile Picture <span class="required">*</span>*/
/*                 </label>*/
/*                     {{form_widget(form.file)}}*/
/*             </div>*/
/*                 </br>*/
/*             <div class="ln_solid"></div>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                     <a href="">{{ form_widget(form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn'} }) }}</a>*/
/*                     <button type="reset" class="btn btn-dark" onclick="goBack()" >Cancel</button>*/
/*                 </div>*/
/*             </div>*/
/*            {{ form_end(form)}}*/
/*     </div>*/
/* */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/* <script type="text/javascript" src="{{asset('gentelella/jquery-ui/external/jquery/jquery.js')}}"></script>*/
/*      <script type="text/javascript" src="{{asset('gentelella/jquery-ui/jquery-ui.min.js')}}"></script>*/
/*      <script src={{asset('gentelella/js/bootstrap.min.js')}}></script>*/
/*     <!-- icheck -->*/
/*     <script src={{asset('gentelella/js/icheck/icheck.min.js')}}></script>*/
/* */
/*     <script src={{asset('gentelella/js/custom.js')}}></script>*/
/*     <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script src={{asset('gentelella/js/select/select2.full.js')}}></script>*/
/*     <!-- tags -->*/
/*         <script src={{asset('gentelella/js/tags/jquery.tagsinput.min.js')}}></script>*/
/*     <script type="text/javascript" src="{{asset('notify/notify.js')}}"></script>*/
/*     <!-- input tags -->*/
/*         */
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/*  */
/*     <script>*/
/*         $.notify.addStyle('formerror', {*/
/*                 html: "<div><span data-notify-text/></div>",*/
/*                 classes: {*/
/*                   base: {*/
/*                     "color": "white",*/
/*                     "white-space": "nowrap",*/
/*                     "background-color": "#ce5454",*/
/*                      "padding": "3px 10px",*/
/*                     "border-radius" : "3px 4px 4px 3px",*/
/*                     */
/*                     "border": "2px solid #ce5454",*/
/*                     */
/*                     */
/*                   }*/
/*                 }*/
/*               });*/
/*     */
/*     // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#registerForm')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/*         function checkFormFields(){*/
/*             var isFalse = true;*/
/*             */
/*             */
/*             */
/*         }*/
/*        */
/* */
/*         $('#registerForm').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/*             */
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*     <script type="text/javascript">*/
/*             $( "#addcontact" ).click(function() {*/
/*                 alert( "Handler for .click() called." );*/
/*               });*/
/*         */
/*             var choice = document.getElementById("researchproject_myprojectbundle_doctor_fieldOfPractice");*/
/*             choice.onchange=newChoice;*/
/*             function newChoice()*/
/*             {   */
/*                 var choice = document.getElementById("researchproject_myprojectbundle_doctor_fieldOfPractice");*/
/*                 var selectedValue = choice.options[choice.selectedIndex].value;*/
/* */
/* */
/*                 if (selectedValue == "Specialized")*/
/*                 {   document.getElementById("specialtyblock").style.display = "block";*/
/*                 }*/
/*                 else*/
/*                 {*/
/*                    document.getElementById("specialtyblock").style.display = "none";*/
/*                 }*/
/*             }*/
/*     </script>*/
/*     <script>*/
/*             $(document).ready(function () {*/
/*                 $(".select2_single").select2({*/
/*                     allowClear: true*/
/*                 });*/
/*                 $(".select2_group").select2({});*/
/*                 $(".select2_multiple").select2({*/
/*                     allowClear: true*/
/*                 });*/
/*                 */
/*                 */
/*             });*/
/*         </script>*/
/*         <script>*/
/*         $(document).ready(function () {*/
/*             $('#researchproject_myprojectbundle_doctor_dateOfBirth').datepicker({*/
/*                 maxDate: Date.now()*/
/*                 */
/*             }).on('change', function() {*/
/*                verifyDate( $('#researchproject_myprojectbundle_doctor_dateOfBirth').val());*/
/*              });*/
/*             */
/*             $('#researchproject_myprojectbundle_doctor_origIssueDate').datepicker({*/
/*                 maxDate: Date.now()*/
/*                 */
/*             }).on('change', function() {*/
/*                verifyIssueDate( $('#researchproject_myprojectbundle_doctor_origIssueDate').val());*/
/*              }); */
/*             */
/*             */
/*             */
/*             */
/*             $.notify.addStyle('formerror', {*/
/*                 html: "<div><span data-notify-text/></div>",*/
/*                 classes: {*/
/*                   base: {*/
/*                     "color": "white",*/
/*                     "white-space": "nowrap",*/
/*                     "background-color": "#ce5454",*/
/*                      "padding": "3px 10px",*/
/*                     "border-radius" : "3px 4px 4px 3px",*/
/*                     */
/*                     "border": "2px solid #ce5454",*/
/*                     */
/*                     */
/*                   }*/
/*                 }*/
/*               });*/
/*             */
/*             */
/*         });*/
/*         */
/*         function showError(text, div){*/
/*       */
/*         $(div).notify(text,  */
/*              { position: "right", autoHide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*         }*/
/* */
/*         function checkDate(date){*/
/*             var isTrue = true;*/
/*             var idate = document.getElementById("researchproject_myprojectbundle_doctor_dateOfBirth"),*/
/*              dateReg = /^\d{1,2}\/\d{1,2}\/\d{4}$/;*/
/* */
/*         if(!dateReg.test(idate.value)){*/
/*             isTrue = false;*/
/*             }*/
/*             */
/*             var now = new Date();*/
/*             if (Date.parse(idate.value) > now) {*/
/*               // selected date is in the past*/
/*               isTrue = false;*/
/*             }*/
/*             */
/*              */
/*             return isTrue;*/
/*         }*/
/*         */
/*          function checkIssueDate(date){*/
/*             var isTrue = true;*/
/*             var idate = document.getElementById("researchproject_myprojectbundle_doctor_origIssueDate"),*/
/*              dateReg = /^\d{1,2}\/\d{1,2}\/\d{4}$/;*/
/* */
/*         */
/*         if(!dateReg.test(idate.value)){*/
/*             isTrue = false;*/
/*             }*/
/*             */
/*             var now = new Date();*/
/*             if (Date.parse(idate.value) > now) {*/
/*               // selected date is in the past*/
/*               isTrue = false;*/
/*             }*/
/*             */
/*              */
/*             return isTrue;*/
/*         }*/
/*         */
/*         function verifyDate(date){*/
/*             */
/*             if(checkDate(date) === false){*/
/*                 $('#researchproject_myprojectbundle_doctor_dateOfBirth').addClass('parsley-error');*/
/*                  $("#researchproject_myprojectbundle_doctor_dateOfBirth").notify("Invalid Date",  */
/*                             { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             }else{*/
/*                 $('#researchproject_myprojectbundle_doctor_dateOfBirth').removeClass('parsley-error');*/
/*                 $('#dob .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*            */
/*         }*/
/*         */
/*         function verifyIssueDate(date){*/
/*             */
/*             if(checkIssueDate(date) === false){*/
/*                 $('#researchproject_myprojectbundle_doctor_origIssueDate').addClass('parsley-error');*/
/*                  $("#researchproject_myprojectbundle_doctor_origIssueDate").notify("Invalid Date",  */
/*                             { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             }else{*/
/*                 $('#researchproject_myprojectbundle_doctor_origIssueDate').removeClass('parsley-error');*/
/*                 $('#oid .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*            */
/*         }*/
/* */
/*     </script>*/
/*     <script>*/
/*   $('#researchproject_myprojectbundle_doctor_username').keyup(function() {*/
/* */
/*      usernameText = $(this).val();*/
/*     */
/*      */
/*      */
/*      if (this.value.length >= 3 && this.value != '')*/
/*   {*/
/*      $.ajax({*/
/*         type: "POST",*/
/*         dataType: "json",*/
/*         url: '{{ path('username_check') }}',*/
/*         data: {usernameText : usernameText },*/
/*         beforeSend: function(){*/
/*         $('#loading').show();*/
/*         },*/
/*         complete: function(){*/
/*         $('#loading').hide();*/
/*         },*/
/*         success : function(data){*/
/*             */
/*             if(data.duplicate === 'true'){*/
/*                 $('#researchproject_myprojectbundle_doctor_username').addClass('parsley-error');*/
/*                  $("#researchproject_myprojectbundle_doctor_username").notify("Sorry! That username is already taken.",  */
/*                             { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             }else{*/
/*                 $('#researchproject_myprojectbundle_doctor_username').removeClass('parsley-error');*/
/*                 $('#uname .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*             */
/*         },error: function() {*/
/*          alert('error');*/
/*           }*/
/*         });*/
/*     }*/
/* });*/
/*        */
/*     </script>*/
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
