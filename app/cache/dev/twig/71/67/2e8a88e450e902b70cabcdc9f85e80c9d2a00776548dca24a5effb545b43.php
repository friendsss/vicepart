<?php

/* ResearchProjectMyProjectBundle:Default:clinic.html.twig */
class __TwigTemplate_71672e8a88e450e902b70cabcdc9f85e80c9d2a00776548dca24a5effb545b43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'clinic' => array($this, 'block_clinic'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " ";
    }

    // line 3
    public function block_clinic($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"\">
            <div class=\"page-title\">
                <div class=\"title\">
                    <h3>&nbsp;CLINIC</h3>
                </div>
            </div>
            <div class=\"clearfix\"></div>

            <center>
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12\">
                    ";
        // line 16
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 17
            echo "                    <a href=\"";
            echo $this->env->getExtension('routing')->getPath("clinic_new");
            echo "\">
                        <button class=\"btn btn-success right\" style=\"margin-bottom: 5px;\"><i class=\"fa fa-plus\"></i> Add New Clinic</button>
                    </a>
                    ";
        }
        // line 21
        echo "                    <div class=\"x_panel\">
                        <br/>
                        ";
        // line 23
        $context["clinicname"] = $this->getAttribute((isset($context["randomclinic1"]) ? $context["randomclinic1"] : $this->getContext($context, "randomclinic1")), "clinicName");
        // line 24
        echo "                        ";
        $context["clinicadd"] = $this->getAttribute((isset($context["randomclinic1"]) ? $context["randomclinic1"] : $this->getContext($context, "randomclinic1")), "clinicAddress");
        // line 25
        echo "                        
                        ";
        // line 26
        $context["clinicname1"] = $this->getAttribute((isset($context["randomclinic2"]) ? $context["randomclinic2"] : $this->getContext($context, "randomclinic2")), "clinicName");
        // line 27
        echo "                        ";
        $context["clinicadd1"] = $this->getAttribute((isset($context["randomclinic2"]) ? $context["randomclinic2"] : $this->getContext($context, "randomclinic2")), "clinicAddress");
        // line 28
        echo "                        
                        ";
        // line 29
        $context["clinicname2"] = $this->getAttribute((isset($context["randomclinic3"]) ? $context["randomclinic3"] : $this->getContext($context, "randomclinic3")), "clinicName");
        // line 30
        echo "                        ";
        $context["clinicadd2"] = $this->getAttribute((isset($context["randomclinic3"]) ? $context["randomclinic3"] : $this->getContext($context, "randomclinic3")), "clinicAddress");
        // line 31
        echo "                            
                            
                            
                                <div class=\"col-md-4 col-sm-4 col-xs-12 profile\">
                                        <div class=\"profile_img\">
                                            <!-- end of image cropping -->
                                            <div id=\"crop-avatar\">
                                                <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    <img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
        echo "\" alt=\"Avatar\">
                                                </div>
                                            </div>
                                            <!-- end of image cropping -->
                                        </div>
                                        <h3><a href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomclinic1"]) ? $context["randomclinic1"] : $this->getContext($context, "randomclinic1")), "id"))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["clinicname"]) ? $context["clinicname"] : $this->getContext($context, "clinicname")), "html", null, true);
        echo "</a></h3>

                                        <ul class=\"list-unstyled user_data\">
                                            <li><i class=\"fa fa-map-marker user-profile-icon\"></i> ";
        // line 48
        echo twig_escape_filter($this->env, (isset($context["clinicadd"]) ? $context["clinicadd"] : $this->getContext($context, "clinicadd")), "html", null, true);
        echo "
                                            </li>

                                        </ul>
                                        
                                     </div>
                                
                                                
                                <div class=\"col-md-4 col-sm-4 col-xs-12 profile\">
                                    
                                        <div class=\"profile_img\">
                                            <!-- end of image cropping -->
                                            <div id=\"crop-avatar\">
                                                <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
        echo "\" alt=\"Avatar\">
                                                </div>
                                            </div>
                                            <!-- end of image cropping -->
                                        </div>
                                        <h3><a href=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomclinic2"]) ? $context["randomclinic2"] : $this->getContext($context, "randomclinic2")), "id"))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["clinicname1"]) ? $context["clinicname1"] : $this->getContext($context, "clinicname1")), "html", null, true);
        echo "</a></h3>

                                        <ul class=\"list-unstyled user_data\">
                                            <li><i class=\"fa fa-map-marker user-profile-icon\"></i> ";
        // line 71
        echo twig_escape_filter($this->env, (isset($context["clinicadd1"]) ? $context["clinicadd1"] : $this->getContext($context, "clinicadd1")), "html", null, true);
        echo "
                                            </li>
                                        </ul>
                                </div>

                                <div class=\"col-md-4 col-sm-4 col-xs-12 profile\">
                                    
                                        <div class=\"profile_img\">
                                            <!-- end of image cropping -->
                                            <div id=\"crop-avatar\">
                                                <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    <img src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
        echo "\" alt=\"Avatar\">
                                                </div>
                                            </div>
                                            <!-- end of image cropping -->
                                        </div>
                                        <h3><a href=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomclinic3"]) ? $context["randomclinic3"] : $this->getContext($context, "randomclinic3")), "id"))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["clinicname2"]) ? $context["clinicname2"] : $this->getContext($context, "clinicname2")), "html", null, true);
        echo "</a></h3>

                                        <ul class=\"list-unstyled user_data\">
                                            <li><i class=\"fa fa-map-marker user-profile-icon\"></i> ";
        // line 91
        echo twig_escape_filter($this->env, (isset($context["clinicadd2"]) ? $context["clinicadd2"] : $this->getContext($context, "clinicadd2")), "html", null, true);
        echo "
                                            </li>
                                        </ul>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                  </center>
            
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12\">  
                    <center>
                    <div class=\"x_panel\">
                        <h3>CLINICS</h3>
                        <table class=\"table table-striped\">
                                        <thead>
                                            <tr>
                                                <th>Clinic Name</th>
                                                <th>Address</th>
                                                <th>Doctors</th>
                                                <th>View Profile</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ";
        // line 116
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 117
            echo "                                                <tr>
                                                    <td>";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicName"), "html", null, true);
            echo "</td>
                                                    <td>";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicAddress"), "html", null, true);
            echo "</td>
                                                    <td>
                                                    ";
            // line 121
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctors"));
            foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
                // line 122
                echo "                                                    
                                                        <a style=\"font-family: Century Gothic; color:#379DFF; font-size: 15px\" href=\"";
                // line 123
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id"))), "html", null, true);
                echo "\">
                                                        ";
                // line 124
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "firstName"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "middleName"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "lastName"), "html", null, true);
                echo "</a>
                                                        <br/>
                                                    
                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 128
            echo "                                                    </td>
                                                    <td><a href=\"";
            // line 129
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"><button class=\"btn btn-primary btn-xs\">View Clinic Profile</button></a></td>
                                                </tr>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "                                            
                                        </tbody>
                        </table>
                    </div>
                   </center>
                </div>
            </div>
        </div>

";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:clinic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  266 => 132,  257 => 129,  254 => 128,  240 => 124,  236 => 123,  233 => 122,  229 => 121,  224 => 119,  220 => 118,  217 => 117,  213 => 116,  185 => 91,  177 => 88,  169 => 83,  154 => 71,  146 => 68,  138 => 63,  120 => 48,  112 => 45,  104 => 40,  93 => 31,  90 => 30,  88 => 29,  85 => 28,  82 => 27,  80 => 26,  77 => 25,  74 => 24,  72 => 23,  68 => 21,  60 => 17,  58 => 16,  45 => 5,  42 => 4,  36 => 3,  30 => 2,);
    }
}
