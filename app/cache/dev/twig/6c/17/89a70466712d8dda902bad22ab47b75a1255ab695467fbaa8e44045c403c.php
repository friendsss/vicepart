<?php

/* ResearchProjectMyProjectBundle:Doctor:new.html.twig */
class __TwigTemplate_6c1789a70466712d8dda902bad22ab47b75a1255ab695467fbaa8e44045c403c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'doctor' => array($this, 'block_doctor'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_doctor($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        echo "DOCTOR";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "<link href=";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/select/select2.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
</br>

    <center><h1><b>Register New Doctor</b></h1>
    <div>
        <br/>
        <br/>
        <div >
            ";
        // line 14
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "registerForm", "class" => "form-horizontal form-label-left")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >First Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "firstName"), 'widget', array("attr" => array("id" => "firstName", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 20
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "firstName"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Last Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lastName"), 'widget', array("attr" => array("name" => "lastName", "placeholder" => "Last Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "text")));
        // line 29
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lastName"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Middle Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "middleName"), 'widget', array("attr" => array("name" => "middleName", "placeholder" => "Middle Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "text")));
        // line 38
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "middleName"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Email Address <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailAddress"), 'widget', array("attr" => array("name" => "emailAddress", "placeholder" => "Email Address", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "10", "required" => "required", "type" => "email")));
        // line 47
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailAddress"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Username <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username"), 'widget', array("attr" => array("name" => "username", "placeholder" => "Username", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "5", "required" => "required", "type" => "text")));
        // line 56
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label for=\"first\" class=\"control-label col-md-3\" >Password <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), "first"), 'widget', array("attr" => array("type" => "password", "placeholder" => "Password", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "8,15", "required" => "required")));
        // line 65
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), "first"), 'errors');
        echo "</div></i>
             <div class=\"item form-group\">
                <label for=\"second\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Repeat Password <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), "second"), 'widget', array("attr" => array("id" => "second", "type" => "password", "name" => "second", "placeholder" => "Repeat Password", "class" => "form-control col-md-7 col-xs-12", "data-validate-linked" => "researchproject_myprojectbundle_doctor_password_first", "required" => "required")));
        // line 74
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "password"), "second"), 'errors');
        echo "</div></i>
            <div id=\"dob\" class=\"form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Date of Birth <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                   ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateOfBirth"), 'widget', array("attr" => array("name" => "dateOfBirth", "placeholder" => "Date of Birth", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "5", "required" => "required", "type" => "text")));
        // line 83
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "dateOfBirth"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Field of Practice <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fieldOfPractice"), 'widget', array("attr" => array("name" => "fieldOfPractice", "placeholder" => "Field of Practice", "class" => "form-control col-md-7 col-xs-12")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fieldOfPractice"), 'errors');
        echo "</div></i>
                
            <div class=\"item form-group\" id=\"specialtyblock\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Specialty <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "specialties"), 'widget', array("attr" => array("name" => "specialties", "class" => "select2_multiple form-control", "required" => "required")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 101
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "specialties"), 'errors');
        echo "</div></i>
             <div class=\"item form-group\" id=\"contactnumberblock\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Contact Number/s <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 105
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contactNumbers"), 'widget', array("attr" => array("name" => "contactNumbers", "class" => "tags form-control", "required" => "required", "type" => "number")));
        echo "
                    </div>
            </div>
            <div class=\"item form-group\" id=\"fellowshipblock\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Fellowship/s <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 111
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fellowships"), 'widget', array("attr" => array("name" => "fellowships", "class" => "select2_multiple form-control", "required" => "required")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 114
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "fellowships"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">PRC Number <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prcNumber"), 'widget', array("attr" => array("name" => "prcNumber", "placeholder" => "PRC Number", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "7,7", "required" => "required", "type" => "number")));
        // line 120
        echo "  
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 123
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prcNumber"), 'errors');
        echo "</div></i>
            <div id=\"oid\" class=\"form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Original Issue Date <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                   ";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "origIssueDate"), 'widget', array("attr" => array("name" => "origIssueDate", "placeholder" => "Original Issue Date:", "class" => "form-control col-md-7 col-xs-12")));
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "origIssueDate"), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">About My Practice <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 135
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "aboutMyPractice"), 'widget', array("attr" => array("name" => "aboutMyPractice", "placeholder" => "About My Practice", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "6", "required" => "required", "type" => "text")));
        // line 136
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 139
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "aboutMyPractice"), 'errors');
        echo "</div></i>
</br>
            <div class=\"ln_solid\"></div>
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"\">";
        // line 143
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit"), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button type=\"reset\" class=\"btn btn-dark\" onclick=\"goBack()\" >Cancel</button>
                </div>
            </div>
           ";
        // line 147
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
    </div>

    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
<script type=\"text/javascript\" src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/jquery-ui/external/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
     <script type=\"text/javascript\" src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/jquery-ui/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
     <script src=";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/bootstrap.min.js"), "html", null, true);
        echo "></script>
    <!-- icheck -->
    <script src=";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "></script>

    <script src=";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "></script>
    <!-- form validation -->
    <script src=";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script src=";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/select/select2.full.js"), "html", null, true);
        echo "></script>
    <!-- tags -->
        <script src=";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/tags/jquery.tagsinput.min.js"), "html", null, true);
        echo "></script>
    <script type=\"text/javascript\" src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("notify/notify.js"), "html", null, true);
        echo "\"></script>
    <!-- input tags -->
        <script>
            function onAddTag(tag) {
                alert(\"Added a tag: \" + tag);
            }

            function onRemoveTag(tag) {
                alert(\"Removed a tag: \" + tag);
            }

            function onChangeTag(input, tag) {
                alert(\"Changed a tag: \" + tag);
            }

            \$(function () {
                \$('#researchproject_myprojectbundle_doctor_contactNumbers').tagsInput({
                    width: 'auto'
                });
            });
        </script>
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
 
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#registerForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

       

        \$('#registerForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }
            

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script type=\"text/javascript\">
            var choice = document.getElementById(\"researchproject_myprojectbundle_doctor_fieldOfPractice\");
            choice.onchange=newChoice;
            function newChoice()
            {   
                var choice = document.getElementById(\"researchproject_myprojectbundle_doctor_fieldOfPractice\");
                var selectedValue = choice.options[choice.selectedIndex].value;


                if (selectedValue == \"Specialized\")
                {   document.getElementById(\"specialtyblock\").style.display = \"block\";
                }
                else
                {
                   document.getElementById(\"specialtyblock\").style.display = \"none\";
                }
            }
    </script>
    <script>
            \$(document).ready(function () {
                \$(\".select2_single\").select2({
                    allowClear: true
                });
                \$(\".select2_group\").select2({});
                \$(\".select2_multiple\").select2({
                    allowClear: true
                });
                
                
            });
        </script>
        <script>
        \$(document).ready(function () {
            \$('#researchproject_myprojectbundle_doctor_dateOfBirth').datepicker({
                maxDate: Date.now()
                
            }).on('change', function() {
               verifyDate( \$('#researchproject_myprojectbundle_doctor_dateOfBirth').val());
             });
            
            \$('#researchproject_myprojectbundle_doctor_origIssueDate').datepicker({
                maxDate: Date.now()
                
            }).on('change', function() {
               verifyIssueDate( \$('#researchproject_myprojectbundle_doctor_origIssueDate').val());
             });
            
            \$.notify.addStyle('formerror', {
                html: \"<div><span data-notify-text/></div>\",
                classes: {
                  base: {
                    \"color\": \"white\",
                    \"white-space\": \"nowrap\",
                    \"background-color\": \"#ce5454\",
                     \"padding\": \"3px 10px\",
                    \"border-radius\" : \"3px 4px 4px 3px\",
                    
                    \"border\": \"2px solid #ce5454\",
                    
                    
                  }
                }
              });
            
            
        });
        
        function showError(text, div){
      
        \$(div).notify(text,  
             { position: \"right\", autoHide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
        }

        function checkDate(date){
            var isTrue = true;
            var idate = document.getElementById(\"researchproject_myprojectbundle_doctor_dateOfBirth\"),
             dateReg = /(0[1-9]|[12][0-9]|3[01])[\\/](0[1-9]|1[012])[\\/]201[4-9]|20[2-9][0-9]/;

        if(!dateReg.test(idate.value)){
            isTrue = false;
            }
            
             
            return isTrue;
        }
        
         function checkIssueDate(date){
            var isTrue = true;
            var idate = document.getElementById(\"researchproject_myprojectbundle_doctor_origIssueDate\"),
             dateReg = /(0[1-9]|[12][0-9]|3[01])[\\/](0[1-9]|1[012])[\\/]201[4-9]|20[2-9][0-9]/;

        if(!dateReg.test(idate.value)){
            isTrue = false;
            }
             
            return isTrue;
        }
        
        function verifyDate(date){
            
            if(checkDate(date) === false){
                \$('#researchproject_myprojectbundle_doctor_dateOfBirth').addClass('parsley-error');
                 \$(\"#researchproject_myprojectbundle_doctor_dateOfBirth\").notify(\"Invalid Date\",  
                            { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            }else{
                \$('#researchproject_myprojectbundle_doctor_dateOfBirth').removeClass('parsley-error');
                \$('#dob .notifyjs-wrapper').trigger('notify-hide');
            }
           
        }
        
        function verifyIssueDate(date){
            
            if(checkIssueDate(date) === false){
                \$('#researchproject_myprojectbundle_doctor_origIssueDate').addClass('parsley-error');
                 \$(\"#researchproject_myprojectbundle_doctor_origIssueDate\").notify(\"Invalid Date\",  
                            { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            }else{
                \$('#researchproject_myprojectbundle_doctor_origIssueDate').removeClass('parsley-error');
                \$('#oid .notifyjs-wrapper').trigger('notify-hide');
            }
           
        }

    </script>
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Doctor:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  333 => 168,  329 => 167,  324 => 165,  320 => 164,  315 => 162,  310 => 160,  305 => 158,  301 => 157,  297 => 156,  285 => 147,  278 => 143,  271 => 139,  266 => 136,  264 => 135,  257 => 131,  251 => 128,  243 => 123,  238 => 120,  236 => 119,  228 => 114,  222 => 111,  213 => 105,  206 => 101,  200 => 98,  192 => 93,  186 => 90,  179 => 86,  174 => 83,  172 => 82,  164 => 77,  159 => 74,  157 => 73,  149 => 68,  144 => 65,  142 => 64,  134 => 59,  129 => 56,  127 => 55,  119 => 50,  114 => 47,  112 => 46,  104 => 41,  99 => 38,  97 => 37,  89 => 32,  84 => 29,  82 => 28,  74 => 23,  69 => 20,  67 => 19,  59 => 14,  47 => 6,  44 => 5,  38 => 4,  32 => 3,  27 => 2,);
    }
}
