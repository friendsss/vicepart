<?php

/* ResearchProjectMyProjectBundle:DoctorGroup:show.html.twig */
class __TwigTemplate_82a74830231c17aa5a589f40d8202062c1ea7569f2eb5c9c463499a1e3b305a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_doctorgroup($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        echo "DOCTOR GROUP";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                <h3>&nbsp;DOCTOR GROUP</h3>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    
                        <h3>Group Profile</h3>
                       
                    <div class=\"x_content\">
                        <center>
                            <h3 style=\"font-size: 30px;\"><b> ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "groupName"), "html", null, true);
        echo "</b></h3>
                            <h4 style=\"font-family: Century Gothic; font-size: 20px;\"><i> ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "description"), "html", null, true);
        echo "</i></h4>
                            <h4 style=\"font-family: Century Gothic; font-size: 15px;\"><i> Created on: ";
        // line 22
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "dateCreated"), "m-d-Y"), "html", null, true);
        echo "</i></h4>
                            
                                ";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getAdminDoctors"));
        foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
            // line 25
            echo "                                    ";
            if (((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")) == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"))) {
                // line 26
                echo "                                        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\"><button class=\"btn btn-primary\">Edit Information</button></a>
                                       <button id =\"imodal\" type=\"button\" class=\"btn btn-success btn\" data-toggle=\"modal\" data-target=\"#invitemodal\" data-backdrop=\"static\" data-keyboard=\"false\">Invite Member to Group</button>
                                    ";
            } else {
                // line 29
                echo "                                        <div></div>
                                    ";
            }
            // line 31
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo " 
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    <div align=\"right\" >
                        <h3 style=\"font-size: 20px;\">Group Discussions</h3>
                        ";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctors"));
        foreach ($context['_seq'] as $context["_key"] => $context["member"]) {
            // line 44
            echo "                            ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user") == (isset($context["member"]) ? $context["member"] : $this->getContext($context, "member")))) {
                // line 45
                echo "                                <button type=\"button\" class=\"btn btn-success right\" data-toggle=\"modal\" data-target=\"#postmodal\">New Post</button>
                            ";
            } else {
                // line 47
                echo "                                <div></div>
                            ";
            }
            // line 49
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['member'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                                <div id=\"postmodal\" class=\"modal fade bs-example-modal-lg\" align=\"left\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog modal-lg\">
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                                </button>
                                                <h3 class=\"modal-title\" id=\"myModalLabel\">New Post</h3>
                                            </div>
                                            <div class=\"modal-body\">
                                                
                                                ";
        // line 60
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "newPost", "class" => "form-horizontal form-label-left")));
        echo "
                                                <div class=\"item form-group\">
                                                <label class=\"control-label col-md-2 col-sm-2 col-xs-2 left-align\" >Post Title: </label>
                                                    <div class=\"col-md-7\">
                                                        ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "postTitle"), 'widget', array("attr" => array("name" => "postTitle", "placeholder" => "Post Title", "class" => "form-control col-md-5 col-xs-5", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 65
        echo "
                                                    </div>
                                                </div>
                                                <div class=\"item form-group\">
                                                <label class=\"control-label col-md-2 col-sm-2 col-xs-2\" >Post Content: </label>
                                                    <div class=\"col-md-7\">
                                                        ";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "postContent"), 'widget', array("attr" => array("name" => "postContent", "placeholder" => "Post Content", "class" => "form-control col-md-5 col-xs-5", "data-validate-length-range" => "5", "required" => "required", "type" => "text")));
        // line 72
        echo "
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                            <div class=\"modal-footer\">
                                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                <a href=\"\">";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Post"), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn", "onClick" => "submitClicked()")));
        echo "</a> 
                                            </div>
                                            ";
        // line 81
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                                        </div>
                                    </div>
                                </div>
                                <div class=\"clearfix\"></div>
                        
                    </div>
                    <div class=\"x_content\" align=\"center\">
                        ";
        // line 89
        if (twig_test_empty($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getGroupPosts"))) {
            // line 90
            echo "                            <h4 style=\"font-family: Century Gothic;font-size: 16px;\"> <i>No discussions started yet.</i> </h4>
                        ";
        } else {
            // line 92
            echo "                        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getGroupPosts"));
            foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                // line 93
                echo "                        
                        <h3>Post Title: ";
                // line 94
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "postTitle"), "html", null, true);
                echo " </h3>
                        <a style =\"color: white;\"href=\"";
                // line 95
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grouppost_show", array("id" => $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "id"))), "html", null, true);
                echo "\"><button class='btn btn-info'>View group post</button></a>
                        ";
                // line 96
                if (($this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "doctorAuthor") == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"))) {
                    echo " 
                        <a  id=\"dlt_post-";
                    // line 97
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "id"), "html", null, true);
                    echo "\" class=\"btn btn-danger btn-delete-post\" href=\"#\">
                                                            <i class=\"glyphicon glyphicon-trash icon-white\"></i>
                                                            Delete
                                                        </a>
                        <a  id=\"edit_post-";
                    // line 101
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["post"]) ? $context["post"] : $this->getContext($context, "post")), "id"), "html", null, true);
                    echo "\" class=\"btn btn-primary btn-edit-post\" href=\"#\">
                                                            <i class=\"glyphicon glyphicon-edit icon-white\"></i>
                                                            Edit
                                                        </a>
                        ";
                }
                // line 106
                echo "                        <div class='ln solid'></div>
                        </br>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 109
            echo "                        ";
        }
        // line 110
        echo "                    </div>
                </div>
            </div>
        </div>
        <div class=\"clearfix\"></div>
        
        <div id=\"invitemodal\" class=\"modal fade bs-example-modal-lg\" align=\"left\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" data-backdrop=\"static\" >
                                    <div class=\"modal-dialog modal-lg\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                                </button>
                                                <h4 class=\"modal-title\" style=\"font-family: Century Gothic;\" id=\"myModalLabel\">Invite members to group</h4>
                                            </div>
                                            <div class=\"modal-body\">
                                           

                                        
                                                
                                            </div>
                               
                                        </div>
                                    </div>
                                </div>
        
        
        <div id=\"delete\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" >
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h2 class=\"modal-title\" id=\"deleteModal\">Delete Post</h2>
                                            </div>

                                            <div class=\"modal-body confirm\">
                                                Are you sure you wan't to remove this post?
                                                <div class=\"modal-footer\">
                                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                    <button type=\"button\" id='con_delete_post' class=\"marginTop btn btn-primary col-sm-2 col-sm-offset-5\" >Yes</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
        
        <div id=\"edit\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" >
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h2 class=\"modal-title\" id=\"editModal\">Edit Post</h2>
                                            </div>

                                            <div class=\"modal-body confirm\">
                                            ";
        // line 166
        if (array_key_exists("editForm", $context)) {
            echo "   
                                               ";
            // line 167
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["editForm"]) ? $context["editForm"] : $this->getContext($context, "editForm")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "newPost", "class" => "form-horizontal form-label-left")));
            echo "
                                                <div class=\"item form-group\">
                                                <label class=\"control-label col-md-2 col-sm-2 col-xs-2 left-align\" >Post Title: </label>
                                                    <div class=\"col-md-7\">
                                                        ";
            // line 171
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["editForm"]) ? $context["editForm"] : $this->getContext($context, "editForm")), "postTitle"), 'widget', array("attr" => array("name" => "postTitle", "placeholder" => "Post Title", "class" => "form-control col-md-5 col-xs-5", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
            // line 172
            echo "
                                                    </div>
                                                </div>
                                                <div class=\"item form-group\">
                                                <label class=\"control-label col-md-2 col-sm-2 col-xs-2\" >Post Content: </label>
                                                    <div class=\"col-md-7\">
                                                        ";
            // line 178
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["editForm"]) ? $context["editForm"] : $this->getContext($context, "editForm")), "postContent"), 'widget', array("attr" => array("name" => "postContent", "placeholder" => "Post Content", "class" => "form-control col-md-5 col-xs-5", "data-validate-length-range" => "5", "required" => "required", "type" => "text")));
            // line 179
            echo "
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                            <div class=\"modal-footer\">
                                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                <a href=\"\">";
            // line 186
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["editForm"]) ? $context["editForm"] : $this->getContext($context, "editForm")), "submit"), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
            echo "</a> 
                                            </div>
                                            ";
            // line 188
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
            echo "
                                                
                                            ";
        }
        // line 190
        echo "    
                                             <div class=\"modal-footer\">
                                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                    <button type=\"button\" id='con_edit_post' class=\"marginTop btn btn-primary col-sm-2 col-sm-offset-5\" >Edit</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
          </div>
        
    </div>


<div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
<!-- form validation -->
    <script src=";
        // line 212
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#newPost')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#newPost').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script>
                                    var dltPostBtnId;
                                    \$(\".btn-delete-post\").click(function(x) {
                                        x.preventDefault();
                                        dltPostBtnId = this.id;
                                        dltPostBtnId = dltPostBtnId.replace('dlt_post-', '');
                                        \$('#delete').modal();
                                    });
                                    \$(\"#con_delete_post\").click(function(d) {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 250
        echo $this->env->getExtension('routing')->getPath("post_delete");
        echo "',
                                            data: {id: dltPostBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                               
                                                console.log(response);
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    });
                                   
                                   
</script>
    ";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:DoctorGroup:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  408 => 250,  367 => 212,  343 => 190,  337 => 188,  332 => 186,  323 => 179,  321 => 178,  313 => 172,  311 => 171,  304 => 167,  300 => 166,  242 => 110,  239 => 109,  231 => 106,  223 => 101,  216 => 97,  212 => 96,  208 => 95,  204 => 94,  201 => 93,  196 => 92,  192 => 90,  190 => 89,  179 => 81,  174 => 79,  165 => 72,  163 => 71,  155 => 65,  153 => 64,  146 => 60,  134 => 50,  128 => 49,  124 => 47,  120 => 45,  117 => 44,  113 => 43,  100 => 32,  94 => 31,  90 => 29,  83 => 26,  80 => 25,  76 => 24,  71 => 22,  67 => 21,  63 => 20,  47 => 6,  44 => 5,  38 => 4,  32 => 3,  27 => 2,);
    }
}
