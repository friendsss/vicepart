<?php

/* ResearchProjectMyProjectBundle:Default:login.html.twig */
class __TwigTemplate_f021011f4997c94bf681a1d96e2f474092d8bdd68ebc0c338ee7dcc7c6a0577f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Gentallela Alela! | </title>

    <!-- Bootstrap core CSS -->

    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom styling plus plugins -->
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/css/icheck/flat/green.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">


    <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/jquery.min.js"), "html", null, true);
        echo "\"></script>

    <!--[if lt IE 9]>
        <script src=\"../assets/js/ie8-responsive-file-warning.js\"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
          <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->

</head>

<body style=\"background:#F7F7F7;\">
    
    <div class=\"\">
        <a class=\"hiddenanchor\" id=\"toregister\"></a>
        <a class=\"hiddenanchor\" id=\"tologin\"></a>

        <div id=\"wrapper\">
            <div id=\"login\" class=\"animate form\">
                <section class=\"login_content\">
                    <form id=\"loginform\" class=\"form-signin\" role=\"form\" action=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
                        <h1>Login Form</h1>
                        <div>
                            <input type=\"text\" class=\"form-control\" placeholder=\"Username\" required autofocus id=\"username\" name=\"_username\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" />
                        </div>
                        <div>
                            <input type=\"password\" class=\"form-control\" placeholder=\"Password\" id=\"password\" name=\"_password\" required />
                        </div>
                        <div>
                            
                            <button type=\"submit\" id=\"submit\" class=\"btn btn-default submit\" >Log in</button>
                            <a class=\"reset_pass\" href=\"#\">Lost your password?</a>
                        </div>
                        <div class=\"clearfix\"></div>
                        <div class=\"separator\">

                            <p class=\"change_link\">New to site?
                                <a href=\"#toregister\" class=\"to_register\"> Create Account </a>
                            </p>
                            <div class=\"clearfix\"></div>
                            <br />
                            <div>
                                <h1><i class=\"fa fa-paw\" style=\"font-size: 26px;\"></i> Gentelella Alela!</h1>

                                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id=\"register\" class=\"animate form\">
                <section class=\"login_content\">
                    <form>
                        <h1>Create Account</h1>
                        <div>
                            <input type=\"text\" class=\"form-control\" placeholder=\"Username\" required=\"\" />
                        </div>
                        <div>
                            <input type=\"email\" class=\"form-control\" placeholder=\"Email\" required=\"\" />
                        </div>
                        <div>
                            <input type=\"password\" class=\"form-control\" placeholder=\"Password\" required=\"\" />
                        </div>
                        <div>
                            <a class=\"btn btn-default submit\" href=\"index.html\">Submit</a>
                        </div>
                        <div class=\"clearfix\"></div>
                        <div class=\"separator\">

                            <p class=\"change_link\">Already a member ?
                                <a href=\"#tologin\" class=\"to_register\"> Log in </a>
                            </p>
                            <div class=\"clearfix\"></div>
                            <br />
                            <div>
                                <h1><i class=\"fa fa-paw\" style=\"font-size: 26px;\"></i> Gentelella Alela!</h1>

                                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

</body>

</html>";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 51,  86 => 48,  60 => 25,  54 => 22,  50 => 21,  44 => 18,  40 => 17,  35 => 15,  19 => 1,);
    }
}
