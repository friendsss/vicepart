<?php

/* ResearchProjectMyProjectBundle:Admin:deleteDoctor.html.twig */
class __TwigTemplate_3aa9e6c123a380bd7146a646ae7f2be4f96d1c3e455cd7822a5ad7c9809576ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:deleteDoctor.html.twig", 1);
        $this->blocks = array(
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1799cf3405caabb5475ad22f6214e3941e6fec48d09b811c04a61efbd8db3731 = $this->env->getExtension("native_profiler");
        $__internal_1799cf3405caabb5475ad22f6214e3941e6fec48d09b811c04a61efbd8db3731->enter($__internal_1799cf3405caabb5475ad22f6214e3941e6fec48d09b811c04a61efbd8db3731_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:deleteDoctor.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1799cf3405caabb5475ad22f6214e3941e6fec48d09b811c04a61efbd8db3731->leave($__internal_1799cf3405caabb5475ad22f6214e3941e6fec48d09b811c04a61efbd8db3731_prof);

    }

    // line 2
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_e60de48664f419d7e672aa26efd69946ca07314ba08984ebd36071d8fe3eb110 = $this->env->getExtension("native_profiler");
        $__internal_e60de48664f419d7e672aa26efd69946ca07314ba08984ebd36071d8fe3eb110->enter($__internal_e60de48664f419d7e672aa26efd69946ca07314ba08984ebd36071d8fe3eb110_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "Search Doctors";
        
        $__internal_e60de48664f419d7e672aa26efd69946ca07314ba08984ebd36071d8fe3eb110->leave($__internal_e60de48664f419d7e672aa26efd69946ca07314ba08984ebd36071d8fe3eb110_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_7f51c818399f51563d38e834f7ce56f0a5b90442c34ee6ac2501ffaf738d6de0 = $this->env->getExtension("native_profiler");
        $__internal_7f51c818399f51563d38e834f7ce56f0a5b90442c34ee6ac2501ffaf738d6de0->enter($__internal_7f51c818399f51563d38e834f7ce56f0a5b90442c34ee6ac2501ffaf738d6de0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    
                        <h3>Search Doctors</h3>
                       
                    <div class=\"x_content\">
                        <input class=\"form-control\" id='search' type=\"text\" name=\"query\" placeholder=\"Type a name\" />
                         
                                                    <center><img hidden id=\"loading\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("charisma/img/ajax-loaders/ajax-loader-1.gif"), "html", null, true);
        echo "\" ></center>
                                                    <table class=\"table table-striped\" id=\"resulttable\" class=\"hidden\">

                                                        <tbody id=\"resultbody\">

                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- form validation -->
<script>
                                    var deleteBtnId;
                                    \$(document).on(\"click\", \".btn-remove\", function(x){
                                         x.preventDefault();
                                        deleteBtnId = this.id;
                                        deleteBtnId = deleteBtnId.replace('remove-', '');
                                         \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 44
        echo $this->env->getExtension('routing')->getPath("admin_delete_doctor_click");
        echo "',
                                            data: {id: deleteBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                               location.reload();
                                                console.log(response);
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    });
    
   </script>
    <script>
\$('#search').keyup(function() {
    
     searchText = \$(this).val();
     if (this.value != ''){
         \$(\"#resultbody\").empty();
     }
     
     
     if (this.value.length >= 3 && this.value != '')
  {
     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 72
        echo $this->env->getExtension('routing')->getPath("admin_delete_doctor");
        echo "',
        data: {searchText : searchText},
        beforeSend: function(){
        \$('#loading').show();
        },
        complete: function(){
        \$('#loading').hide();
        },
        success : function(data) 
           
           {if(data){
                \$(\"#resultbody\").empty();
                var len = data.length;
                var txt = \"\";
                  if(len > 0){
                    for(var i=0;i<len;i++){
                            //var button= \$('<button type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\"></i> Invite to Group </button>');
                            var entityId = data[i].id;
                            var showPath = '";
        // line 90
        echo $this->env->getExtension('routing')->getPath("doctor_show", array("id" => "entityId"));
        echo "';
                            showPath = showPath.replace(\"entityId\", data[i].id);
                            txt += \"<tr><td>\"+data[i].firstName+\" \"+data[i].middleName+\" \"+data[i].lastName +\"</td>\";
                            txt+= '<td><a href=\"#\" ><button id=\"remove-'+ data[i].id+'\" type=\"button\" class=\"btn btn-danger btn-xs btn-remove\"> <i class=\"fa fa-trash\"></i> Remove Account </button></td>';
                        
                    }
                    if(txt != \"\"){
                        \$(\"#resultbody\").append(txt).removeClass(\"hidden\");
                         
                    }
                }
            }
        },error: function() {
         alert('error');
          }
        });
    }
});
       
    </script>
    ";
        
        $__internal_7f51c818399f51563d38e834f7ce56f0a5b90442c34ee6ac2501ffaf738d6de0->leave($__internal_7f51c818399f51563d38e834f7ce56f0a5b90442c34ee6ac2501ffaf738d6de0_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:deleteDoctor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 90,  129 => 72,  98 => 44,  70 => 19,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% block pagetitle %}Search Doctors{% endblock %}*/
/* {% block body -%}*/
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                     */
/*                         <h3>Search Doctors</h3>*/
/*                        */
/*                     <div class="x_content">*/
/*                         <input class="form-control" id='search' type="text" name="query" placeholder="Type a name" />*/
/*                          */
/*                                                     <center><img hidden id="loading" src="{{asset('charisma/img/ajax-loaders/ajax-loader-1.gif')}}" ></center>*/
/*                                                     <table class="table table-striped" id="resulttable" class="hidden">*/
/* */
/*                                                         <tbody id="resultbody">*/
/* */
/*                                                             </tr>*/
/*                                                         </tbody>*/
/*                                                     </table>*/
/* */
/*                                                 </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/* <script>*/
/*                                     var deleteBtnId;*/
/*                                     $(document).on("click", ".btn-remove", function(x){*/
/*                                          x.preventDefault();*/
/*                                         deleteBtnId = this.id;*/
/*                                         deleteBtnId = deleteBtnId.replace('remove-', '');*/
/*                                          $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('admin_delete_doctor_click') }}',*/
/*                                             data: {id: deleteBtnId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                location.reload();*/
/*                                                 console.log(response);*/
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*     */
/*    </script>*/
/*     <script>*/
/* $('#search').keyup(function() {*/
/*     */
/*      searchText = $(this).val();*/
/*      if (this.value != ''){*/
/*          $("#resultbody").empty();*/
/*      }*/
/*      */
/*      */
/*      if (this.value.length >= 3 && this.value != '')*/
/*   {*/
/*      $.ajax({*/
/*         type: "POST",*/
/*         dataType: "json",*/
/*         url: '{{ path('admin_delete_doctor') }}',*/
/*         data: {searchText : searchText},*/
/*         beforeSend: function(){*/
/*         $('#loading').show();*/
/*         },*/
/*         complete: function(){*/
/*         $('#loading').hide();*/
/*         },*/
/*         success : function(data) */
/*            */
/*            {if(data){*/
/*                 $("#resultbody").empty();*/
/*                 var len = data.length;*/
/*                 var txt = "";*/
/*                   if(len > 0){*/
/*                     for(var i=0;i<len;i++){*/
/*                             //var button= $('<button type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user"></i> Invite to Group </button>');*/
/*                             var entityId = data[i].id;*/
/*                             var showPath = '{{ path("doctor_show", {'id' : 'entityId' })}}';*/
/*                             showPath = showPath.replace("entityId", data[i].id);*/
/*                             txt += "<tr><td>"+data[i].firstName+" "+data[i].middleName+" "+data[i].lastName +"</td>";*/
/*                             txt+= '<td><a href="#" ><button id="remove-'+ data[i].id+'" type="button" class="btn btn-danger btn-xs btn-remove"> <i class="fa fa-trash"></i> Remove Account </button></td>';*/
/*                         */
/*                     }*/
/*                     if(txt != ""){*/
/*                         $("#resultbody").append(txt).removeClass("hidden");*/
/*                          */
/*                     }*/
/*                 }*/
/*             }*/
/*         },error: function() {*/
/*          alert('error');*/
/*           }*/
/*         });*/
/*     }*/
/* });*/
/*        */
/*     </script>*/
/*     {% endblock %}*/
/* */
