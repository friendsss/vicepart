<?php

/* ResearchProjectMyProjectBundle:Admin:index.html.twig */
class __TwigTemplate_66c904ec20f78b87a0af91f81b72e8a6c72365c44519adcd3ddae82615a6763a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee3e882ee3b9e2f0e6953bd7251b072c7788608c707a11137577fa9d741ae197 = $this->env->getExtension("native_profiler");
        $__internal_ee3e882ee3b9e2f0e6953bd7251b072c7788608c707a11137577fa9d741ae197->enter($__internal_ee3e882ee3b9e2f0e6953bd7251b072c7788608c707a11137577fa9d741ae197_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ee3e882ee3b9e2f0e6953bd7251b072c7788608c707a11137577fa9d741ae197->leave($__internal_ee3e882ee3b9e2f0e6953bd7251b072c7788608c707a11137577fa9d741ae197_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_776aae246498d64c94a8f719392cc452b2b8b95ef16c6a669fae50d453352a16 = $this->env->getExtension("native_profiler");
        $__internal_776aae246498d64c94a8f719392cc452b2b8b95ef16c6a669fae50d453352a16->enter($__internal_776aae246498d64c94a8f719392cc452b2b8b95ef16c6a669fae50d453352a16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"x_panel\">
    <div class=\"x_content\">    
<div class=\"jumbotron\">
    <center>
                                        <h1>Hello ";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo "!</h1>
                                        <p>Welcome to the Admin Panel.</p>
    </center>
                                    </div>
        </div>
   </div>
    ";
        
        $__internal_776aae246498d64c94a8f719392cc452b2b8b95ef16c6a669fae50d453352a16->leave($__internal_776aae246498d64c94a8f719392cc452b2b8b95ef16c6a669fae50d453352a16_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 8,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* */
/* {% block body -%}*/
/* <div class="x_panel">*/
/*     <div class="x_content">    */
/* <div class="jumbotron">*/
/*     <center>*/
/*                                         <h1>Hello {{app.user.username}}!</h1>*/
/*                                         <p>Welcome to the Admin Panel.</p>*/
/*     </center>*/
/*                                     </div>*/
/*         </div>*/
/*    </div>*/
/*     {% endblock %}*/
/* */
