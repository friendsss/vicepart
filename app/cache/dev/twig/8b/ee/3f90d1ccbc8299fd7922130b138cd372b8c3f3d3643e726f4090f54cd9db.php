<?php

/* ResearchProjectMyProjectBundle:Default:doctorgroup.html.twig */
class __TwigTemplate_8bee3f90d1ccbc8299fd7922130b138cd372b8c3f3d3643e726f4090f54cd9db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " ";
    }

    // line 3
    public function block_doctorgroup($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"\">
            <div class=\"page-title\">
                <div class=\"title\">
                    <h3>&nbsp;DOCTOR GROUP</h3>
                </div>
            </div>
            <div class=\"clearfix\"></div>
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12\">
                    
            <a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("doctorgroup_new");
        echo "\">
                <button class=\"btn btn-success right\" style=\"margin-bottom: 5px;\"><i class=\"fa fa-plus\"></i> Create New Group</button>
            </a>
                        
            
            <div class=\"x_panel\">
            <br/>            
            <form action=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("doctorgrouphomepage");
        echo "\" method=\"POST\" role=\"search\">
                <div class=\"col-md-10 col-sm-10 col-xs-12 form-group top_search\" style='margin-left: 60px;'>
                    <div class=\"input-group\">
                        
                        <input action=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("doctorgrouphomepage");
        echo "\" method=\"POST\" role=\"search\" id=\"searchfield\" name=\"searchfield\" type=\"text\" class=\"form-control\" placeholder=\"Find a doctor group.\">
                            <span class=\"input-group-btn\">
                                <button  action=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("doctorgrouphomepage");
        echo "\" method=\"POST\" role=\"search\" class=\"btn btn-default\" type=\"submit\">Go!</button>
                            </span>
                    </div>
                </div>
            </form>
            <h3></h3>
                            
            ";
        // line 35
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 36
            echo "                <center><i><h3>Find a doctor group maybe?</h3></i></center>
                <br/>
                ";
            // line 38
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 39
                echo "                <div class=\"col-md-3 col-sm-3 col-xs-3\">
                <div class=\"item\" style=\"vertical-align: top; display: inline-block; text-align: center; width: 120px;\">
                    <span style=\"display: block;\">
                        <a href=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\">
                            <img src=\"";
                // line 43
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/homepage/Group.png"), "html", null, true);
                echo "\" alt=\"Doctor Group\">
                            <h3 style=\"color:#379DFF;\"><i class=\"fa fa-circle\"></i><b> &emsp;";
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "groupName"), "html", null, true);
                echo "</b></h3>
                        </a>
                        ";
                // line 46
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getAdminDoctors"));
                foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
                    // line 47
                    echo "                            <h4 style=\"font-family: Century Gothic; font-size: 12px;\">Created by: ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "firstName"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "lastName"), "html", null, true);
                    echo "</h4>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo "                    </span>
                </div>
                </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "                <br/>
            ";
        } elseif (twig_test_empty((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")))) {
            // line 55
            echo "                <center><i><h3>No Doctor Group Found. Find another one.</h3></i></center>
                <br/>
            ";
        } else {
            // line 58
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")));
            foreach ($context['_seq'] as $context["_key"] => $context["doctorgroup"]) {
                // line 59
                echo "                <center>
                <div class=\"col-md-3 col-sm-3 col-xs-3\">
                <div class=\"item\" style=\"vertical-align: top; display: inline-block; text-align: center; width: 120px;\">
                    <span style=\"display: block;\">
                        <a href=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute((isset($context["doctorgroup"]) ? $context["doctorgroup"] : $this->getContext($context, "doctorgroup")), "id"))), "html", null, true);
                echo "\">
                            <img src=\"";
                // line 64
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/homepage/Group.png"), "html", null, true);
                echo "\" alt=\"Doctor Group\">
                            <h3 style=\"color:#379DFF;\"><i class=\"fa fa-circle\"></i><b> &emsp;";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctorgroup"]) ? $context["doctorgroup"] : $this->getContext($context, "doctorgroup")), "groupName"), "html", null, true);
                echo "</b></h3>
                        </a>
                        ";
                // line 67
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["doctorgroup"]) ? $context["doctorgroup"] : $this->getContext($context, "doctorgroup")), "getAdminDoctors"));
                foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
                    // line 68
                    echo "                            <h4 style=\"font-family: Century Gothic; font-size: 12px;\">Created by: ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "firstName"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "lastName"), "html", null, true);
                    echo "</h4>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 70
                echo "                    </span>
                </div>
                </div>
                </center>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctorgroup'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "            ";
        }
        // line 76
        echo "            
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:doctorgroup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 76,  199 => 75,  189 => 70,  178 => 68,  174 => 67,  169 => 65,  165 => 64,  161 => 63,  155 => 59,  150 => 58,  145 => 55,  141 => 53,  132 => 49,  121 => 47,  117 => 46,  112 => 44,  108 => 43,  104 => 42,  99 => 39,  95 => 38,  91 => 36,  89 => 35,  79 => 28,  74 => 26,  67 => 22,  57 => 15,  45 => 5,  42 => 4,  36 => 3,  30 => 2,);
    }
}
