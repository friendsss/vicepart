<?php

/* ResearchProjectMyProjectBundle:Search:searchResult.html.twig */
class __TwigTemplate_ccdbd2c75cbd44c40e0629374c4477cb2c5f2cd1a07b2df1fccad0258b6d67e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Search:searchResult.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ed8c44b25faf0ff6468b06eb2cf145f12ae6edb4900f2f9f1014b76ef0687ef = $this->env->getExtension("native_profiler");
        $__internal_9ed8c44b25faf0ff6468b06eb2cf145f12ae6edb4900f2f9f1014b76ef0687ef->enter($__internal_9ed8c44b25faf0ff6468b06eb2cf145f12ae6edb4900f2f9f1014b76ef0687ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Search:searchResult.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ed8c44b25faf0ff6468b06eb2cf145f12ae6edb4900f2f9f1014b76ef0687ef->leave($__internal_9ed8c44b25faf0ff6468b06eb2cf145f12ae6edb4900f2f9f1014b76ef0687ef_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_f99a832f2bec888a281bdd392ceb22d24f6d53d63d6835e0385f4044816744f8 = $this->env->getExtension("native_profiler");
        $__internal_f99a832f2bec888a281bdd392ceb22d24f6d53d63d6835e0385f4044816744f8->enter($__internal_f99a832f2bec888a281bdd392ceb22d24f6d53d63d6835e0385f4044816744f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "
";
        // line 4
        $context["doctorcount"] = "";
        // line 5
        $context["cliniccount"] = "";
        // line 6
        $context["illnesscount"] = "";
        // line 7
        $context["doctorgroupcount"] = "";
        // line 8
        echo "<div>
\t<div class=\"row clearfix\">
\t\t<div class=\"col-md-12 column\">
                        
\t\t\t<div class=\"jumbotron\">
                                
                                                ";
        // line 14
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 15
            echo "                                                    <center><h1 style=\"font-family: Century Gothic; font-size: 50px\">Search results for <b style=\"color:#379DFF;\"><i>________</i></b></h1></center>
                                                ";
        } else {
            // line 17
            echo "                                                    <center><h1 style=\"font-family: Century Gothic; font-size: 50px\">Search results for <b style=\"color:#379DFF;\"><i>";
            echo twig_escape_filter($this->env, (isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")), "html", null, true);
            echo "</i></b></h1></center>
                                                ";
        }
        // line 19
        echo "                        </div>
                      
                        
                        <div class=\"x_content\">
                            
                            <form action=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("search");
        echo "\" method=\"POST\" role=\"search\">
                                <div class=\"col-md-10 col-sm-10 col-xs-12 form-group top_search\" style='margin-left: 60px;'>
                                    
                                    <div class=\"input-group\">
                                        <input action=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("search");
        echo "\" method=\"POST\" role=\"search\" id=\"searchfield\" name=\"searchfield\" type=\"text\" class=\"form-control\" placeholder=\"Search something.\">
                                            <span class=\"input-group-btn\">
                                                <button  action=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("search");
        echo "\" method=\"POST\" role=\"search\" class=\"btn btn-default\" type=\"submit\">Go!</button>
                                            </span>
                                    </div>
                                    
                                </div>
                                    </form>
                                    <div class=\"col-xs-12\">
                                        <!-- required for floating -->
                                        <!-- Nav tabs -->
                                        <ul class=\"nav nav-tabs tabs-right\">
                                            
                                            ";
        // line 41
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 42
            echo "                                                     ";
            $context["doctorcount"] = "0";
            // line 43
            echo "                                            ";
        } elseif (twig_test_empty((isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")))) {
            // line 44
            echo "                                                    ";
            $context["doctorcount"] = "0";
            // line 45
            echo "                                            ";
        } else {
            // line 46
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")));
            foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
                // line 47
                echo "                                                    ";
                $context["doctorcount"] = twig_length_filter($this->env, (isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")));
                // line 48
                echo "                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 49
            echo "                                            ";
        }
        // line 50
        echo "                                            <li class=\"active\"><a href=\"#home-r\" data-toggle=\"tab\"><button class=\"btn-lg btn-info\" type=\"button\"><center><i class =\"fa fa-stethoscope\"></i> Doctors (";
        echo twig_escape_filter($this->env, (isset($context["doctorcount"]) ? $context["doctorcount"] : $this->getContext($context, "doctorcount")), "html", null, true);
        echo ")</center></button></a>
                                            </li>
                                            
                                            ";
        // line 53
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 54
            echo "                                                     ";
            $context["cliniccount"] = "0";
            // line 55
            echo "                                            ";
        } elseif (twig_test_empty((isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")))) {
            // line 56
            echo "                                                    ";
            $context["cliniccount"] = "0";
            // line 57
            echo "                                            ";
        } else {
            // line 58
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")));
            foreach ($context['_seq'] as $context["_key"] => $context["clinic"]) {
                // line 59
                echo "                                                    ";
                $context["cliniccount"] = twig_length_filter($this->env, (isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")));
                // line 60
                echo "                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clinic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "                                            ";
        }
        // line 62
        echo "                                            <li><a href=\"#profile-r\" data-toggle=\"tab\"><button class=\"btn-lg btn-info\" type=\"button\"><center><i class =\"fa fa-hospital-o\"></i> Clinics (";
        echo twig_escape_filter($this->env, (isset($context["cliniccount"]) ? $context["cliniccount"] : $this->getContext($context, "cliniccount")), "html", null, true);
        echo ")</center></button></a>
                                            </li>
                                            
                                            ";
        // line 65
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 66
            echo "                                                     ";
            $context["illnesscount"] = "0";
            // line 67
            echo "                                            ";
        } elseif (twig_test_empty((isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")))) {
            // line 68
            echo "                                                    ";
            $context["illnesscount"] = "0";
            // line 69
            echo "                                            ";
        } else {
            // line 70
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")));
            foreach ($context['_seq'] as $context["_key"] => $context["illness"]) {
                // line 71
                echo "                                                    ";
                $context["illnesscount"] = twig_length_filter($this->env, (isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")));
                // line 72
                echo "                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['illness'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "                                            ";
        }
        // line 74
        echo "                                            <li><a href=\"#messages-r\" data-toggle=\"tab\"><button class=\"btn-lg btn-info\" type=\"button\"><center><i class =\"fa fa-wheelchair\"></i> Illnesses (";
        echo twig_escape_filter($this->env, (isset($context["illnesscount"]) ? $context["illnesscount"] : $this->getContext($context, "illnesscount")), "html", null, true);
        echo ")</center></button></a>
                                            </li>
                                            ";
        // line 76
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 77
            echo "                                            ";
            if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
                // line 78
                echo "                                                     ";
                $context["doctorgroupcount"] = "0";
                // line 79
                echo "                                            ";
            } elseif (twig_test_empty((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")))) {
                // line 80
                echo "                                                    ";
                $context["doctorgroupcount"] = "0";
                // line 81
                echo "                                            ";
            } else {
                // line 82
                echo "                                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")));
                foreach ($context['_seq'] as $context["_key"] => $context["doctorgroup"]) {
                    // line 83
                    echo "                                                    ";
                    $context["doctorgroupcount"] = twig_length_filter($this->env, (isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")));
                    // line 84
                    echo "                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctorgroup'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 85
                echo "                                            ";
            }
            // line 86
            echo "                                            <li><a href=\"#doctorgroup-r\" data-toggle=\"tab\"><button class=\"btn-lg btn-info\" type=\"button\"><center><i class =\"fa fa-users\"></i> Doctor Groups (";
            echo twig_escape_filter($this->env, (isset($context["doctorgroupcount"]) ? $context["doctorgroupcount"] : $this->getContext($context, "doctorgroupcount")), "html", null, true);
            echo ")</center></button></a>
                                            </li>
                                            ";
        }
        // line 89
        echo "                                        </ul>
                                    </div>

                                    <div class=\"col-xs-12\">
                                        <!-- Tab panes -->
                                        <div class=\"tab-content\">
                                            <div class=\"tab-pane active\" id=\"home-r\">
                                                </br>
                                                </br>
                                                <div style=\"overflow-y: scroll; height:300px;\">
                                                ";
        // line 99
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 100
            echo "                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                     <center><i><h3>Search something... A doctor maybe?</h3></i></center>
                                                ";
        } elseif (twig_test_empty(        // line 105
(isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")))) {
            // line 106
            echo "                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                   <center><i><h3>No Doctor Found</h3></i></center>
                                                ";
        } else {
            // line 112
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["doctors"]) ? $context["doctors"] : $this->getContext($context, "doctors")));
            foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
                // line 113
                echo "                                                        <center><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
                echo "\"><i style=\"font-size: 25px; color: gray\"class =\"fa fa-user-md\"></i><h3>
                                                                    ";
                // line 114
                if (($this->getAttribute($context["doctor"], "fieldOfPractice", array()) == "Specialized")) {
                    // line 115
                    echo "                                                                        <b style=\"color:#379DFF;\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "firstName", array()), "html", null, true);
                    echo "  ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "middleName", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "lastName", array()), "html", null, true);
                    echo "</b> </a>
                                                                        <br>";
                    // line 116
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "fieldOfPractice", array()), "html", null, true);
                    echo " - 
                                                                            ";
                    // line 117
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["doctor"], "specialties", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                        // line 118
                        echo "                                                                                | ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["specialty"], "specialtyName", array()), "html", null, true);
                        echo " |
                                                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 120
                    echo "                                                                    ";
                } else {
                    // line 121
                    echo "                                                                        <b style=\"color:#379DFF;\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "firstName", array()), "html", null, true);
                    echo "  ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "middleName", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "lastName", array()), "html", null, true);
                    echo "</b> </a>
                                                                        <br>";
                    // line 122
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "fieldOfPractice", array()), "html", null, true);
                    echo "
                                                                            
                                                                    ";
                }
                // line 125
                echo "                                                           </h3></center>
                                                    
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 128
            echo "                                                ";
        }
        // line 129
        echo "                                                </div>
                                            </div>
                                            <div class=\"tab-pane\" id=\"profile-r\">
                                                </br>
                                                </br>
                                                <div style=\"overflow-y: scroll; height:300px;\">
                                                ";
        // line 135
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 136
            echo "                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                     <center><i><h3>Search something... A clinic maybe?</h3></i></center>
                                                ";
        } elseif (twig_test_empty(        // line 141
(isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")))) {
            // line 142
            echo "                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                   <center><i><h3>No Clinic Found</h3></i></center>
                                                ";
        } else {
            // line 148
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")));
            foreach ($context['_seq'] as $context["_key"] => $context["clinic"]) {
                // line 149
                echo "                                                    
                                                        <center><a href=\"";
                // line 150
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute($context["clinic"], "id", array()))), "html", null, true);
                echo "\"><i style=\"font-size: 25px; color: gray\" class =\"fa fa-map-marker\"></i><h3>
                                                                    <b style=\"color:#379DFF;\"> ";
                // line 151
                echo twig_escape_filter($this->env, $this->getAttribute($context["clinic"], "clinicName", array()), "html", null, true);
                echo "</b></a>
                                                                    <br/>";
                // line 152
                echo twig_escape_filter($this->env, $this->getAttribute($context["clinic"], "clinicAddress", array()), "html", null, true);
                echo "</h3></center>                  
                                                    
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clinic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 155
            echo "                                                ";
        }
        // line 156
        echo "                                                </div>
                                            </div>
                                            <div class=\"tab-pane\" id=\"messages-r\">
                                                </br>
                                                </br>
                                                 <div style=\"overflow-y: scroll; height:300px;\">
                                                ";
        // line 162
        if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
            // line 163
            echo "                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                     <center><i><h3>Search something... An illness maybe?</h3></i></center>
                                                ";
        } elseif (twig_test_empty(        // line 169
(isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")))) {
            // line 170
            echo "                                                   <center><i><h3>No Illness Found</h3></i></center>
                                                   </br>
                                                   </br>
                                                ";
        } else {
            // line 174
            echo "                                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")));
            foreach ($context['_seq'] as $context["_key"] => $context["illness"]) {
                // line 175
                echo "                                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("illness_show", array("id" => $this->getAttribute($context["illness"], "id", array()))), "html", null, true);
                echo "\"><center><i style=\" font-size: 25px; color: gray\" class =\"fa fa-tint\"></i><h3> 
                                                                    <b style=\"color:#379DFF;\">";
                // line 176
                echo twig_escape_filter($this->env, $this->getAttribute($context["illness"], "illnessName", array()), "html", null, true);
                echo "</b></a>
                                                                    <br/>\"";
                // line 177
                echo twig_escape_filter($this->env, $this->getAttribute($context["illness"], "illnessCommonName", array()), "html", null, true);
                echo "\"
                                                                    <br/>";
                // line 178
                echo twig_escape_filter($this->env, $this->getAttribute($context["illness"], "illnessDefinition", array()), "html", null, true);
                echo " </h3></center>
                                                    
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['illness'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 181
            echo "                                                ";
        }
        echo "  
                                                </div>
                                            </div>
                                            ";
        // line 184
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 185
            echo "                                            <div class=\"tab-pane\" id=\"doctorgroup-r\">
                                                </br>
                                                </br>
                                                <div style=\"overflow-y: scroll; height:300px;\">
                                                ";
            // line 189
            if (((isset($context["searchword"]) ? $context["searchword"] : $this->getContext($context, "searchword")) == "")) {
                // line 190
                echo "                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                     <center><i><h3>Search something... A doctor group maybe?</h3></i></center>
                                                ";
            } elseif (twig_test_empty(            // line 195
(isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")))) {
                // line 196
                echo "                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                   <center><i><h3>No Doctor Group Found</h3></i></center>
                                                ";
            } else {
                // line 202
                echo "                                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["doctorgroups"]) ? $context["doctorgroups"] : $this->getContext($context, "doctorgroups")));
                foreach ($context['_seq'] as $context["_key"] => $context["doctorgroup"]) {
                    // line 203
                    echo "                                                    
                                                        <center><a href=\"";
                    // line 204
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_show", array("id" => $this->getAttribute($context["doctorgroup"], "id", array()))), "html", null, true);
                    echo "\"><i style=\"font-size: 25px; color: gray\"class =\"fa fa-users\"></i>
                                                            <h3>
                                                                <b style=\"color:#379DFF;\">";
                    // line 206
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctorgroup"], "groupName", array()), "html", null, true);
                    echo " </b><br/>
                                                                ";
                    // line 207
                    echo twig_escape_filter($this->env, $this->getAttribute($context["doctorgroup"], "description", array()), "html", null, true);
                    echo "
                                                            </h3>
                                                        </a></center>
                                                    
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctorgroup'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 212
                echo "                                                ";
            }
            // line 213
            echo "                                                </div>
                                            </div>
                                            ";
        }
        // line 216
        echo "                                        </div>
                                    </div>

                                    <div class=\"clearfix\"></div>
                        </div>
                        
                </div>
\t</div>
</div>

";
        
        $__internal_f99a832f2bec888a281bdd392ceb22d24f6d53d63d6835e0385f4044816744f8->leave($__internal_f99a832f2bec888a281bdd392ceb22d24f6d53d63d6835e0385f4044816744f8_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Search:searchResult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  534 => 216,  529 => 213,  526 => 212,  515 => 207,  511 => 206,  506 => 204,  503 => 203,  498 => 202,  490 => 196,  488 => 195,  481 => 190,  479 => 189,  473 => 185,  471 => 184,  464 => 181,  455 => 178,  451 => 177,  447 => 176,  442 => 175,  437 => 174,  431 => 170,  429 => 169,  421 => 163,  419 => 162,  411 => 156,  408 => 155,  399 => 152,  395 => 151,  391 => 150,  388 => 149,  383 => 148,  375 => 142,  373 => 141,  366 => 136,  364 => 135,  356 => 129,  353 => 128,  345 => 125,  339 => 122,  330 => 121,  327 => 120,  318 => 118,  314 => 117,  310 => 116,  301 => 115,  299 => 114,  294 => 113,  289 => 112,  281 => 106,  279 => 105,  272 => 100,  270 => 99,  258 => 89,  251 => 86,  248 => 85,  242 => 84,  239 => 83,  234 => 82,  231 => 81,  228 => 80,  225 => 79,  222 => 78,  219 => 77,  217 => 76,  211 => 74,  208 => 73,  202 => 72,  199 => 71,  194 => 70,  191 => 69,  188 => 68,  185 => 67,  182 => 66,  180 => 65,  173 => 62,  170 => 61,  164 => 60,  161 => 59,  156 => 58,  153 => 57,  150 => 56,  147 => 55,  144 => 54,  142 => 53,  135 => 50,  132 => 49,  126 => 48,  123 => 47,  118 => 46,  115 => 45,  112 => 44,  109 => 43,  106 => 42,  104 => 41,  90 => 30,  85 => 28,  78 => 24,  71 => 19,  65 => 17,  61 => 15,  59 => 14,  51 => 8,  49 => 7,  47 => 6,  45 => 5,  43 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {%block body%}*/
/* */
/* {% set doctorcount = "" %}*/
/* {% set cliniccount = "" %}*/
/* {% set illnesscount = "" %}*/
/* {% set doctorgroupcount = "" %}*/
/* <div>*/
/* 	<div class="row clearfix">*/
/* 		<div class="col-md-12 column">*/
/*                         */
/* 			<div class="jumbotron">*/
/*                                 */
/*                                                 {% if searchword == "" %}*/
/*                                                     <center><h1 style="font-family: Century Gothic; font-size: 50px">Search results for <b style="color:#379DFF;"><i>________</i></b></h1></center>*/
/*                                                 {% else %}*/
/*                                                     <center><h1 style="font-family: Century Gothic; font-size: 50px">Search results for <b style="color:#379DFF;"><i>{{searchword}}</i></b></h1></center>*/
/*                                                 {% endif %}*/
/*                         </div>*/
/*                       */
/*                         */
/*                         <div class="x_content">*/
/*                             */
/*                             <form action="{{path('search')}}" method="POST" role="search">*/
/*                                 <div class="col-md-10 col-sm-10 col-xs-12 form-group top_search" style='margin-left: 60px;'>*/
/*                                     */
/*                                     <div class="input-group">*/
/*                                         <input action="{{path('search')}}" method="POST" role="search" id="searchfield" name="searchfield" type="text" class="form-control" placeholder="Search something.">*/
/*                                             <span class="input-group-btn">*/
/*                                                 <button  action="{{path('search')}}" method="POST" role="search" class="btn btn-default" type="submit">Go!</button>*/
/*                                             </span>*/
/*                                     </div>*/
/*                                     */
/*                                 </div>*/
/*                                     </form>*/
/*                                     <div class="col-xs-12">*/
/*                                         <!-- required for floating -->*/
/*                                         <!-- Nav tabs -->*/
/*                                         <ul class="nav nav-tabs tabs-right">*/
/*                                             */
/*                                             {% if searchword == ""%}*/
/*                                                      {% set doctorcount = "0" %}*/
/*                                             {% elseif doctors is empty%}*/
/*                                                     {% set doctorcount = "0" %}*/
/*                                             {%else%}*/
/*                                                 {% for doctor in doctors %}*/
/*                                                     {% set doctorcount = (doctors|length) %}*/
/*                                                 {% endfor %}*/
/*                                             {% endif %}*/
/*                                             <li class="active"><a href="#home-r" data-toggle="tab"><button class="btn-lg btn-info" type="button"><center><i class ="fa fa-stethoscope"></i> Doctors ({{doctorcount}})</center></button></a>*/
/*                                             </li>*/
/*                                             */
/*                                             {% if searchword == ""%}*/
/*                                                      {% set cliniccount = "0" %}*/
/*                                             {% elseif clinics is empty%}*/
/*                                                     {% set cliniccount = "0" %}*/
/*                                             {%else%}*/
/*                                                 {% for clinic in clinics %}*/
/*                                                     {% set cliniccount = (clinics|length) %}*/
/*                                                 {% endfor %}*/
/*                                             {% endif %}*/
/*                                             <li><a href="#profile-r" data-toggle="tab"><button class="btn-lg btn-info" type="button"><center><i class ="fa fa-hospital-o"></i> Clinics ({{cliniccount}})</center></button></a>*/
/*                                             </li>*/
/*                                             */
/*                                             {% if searchword == ""%}*/
/*                                                      {% set illnesscount = "0" %}*/
/*                                             {% elseif illnesses is empty%}*/
/*                                                     {% set illnesscount = "0" %}*/
/*                                             {%else%}*/
/*                                                 {% for illness in illnesses %}*/
/*                                                     {% set illnesscount = (illnesses|length) %}*/
/*                                                 {% endfor %}*/
/*                                             {% endif %}*/
/*                                             <li><a href="#messages-r" data-toggle="tab"><button class="btn-lg btn-info" type="button"><center><i class ="fa fa-wheelchair"></i> Illnesses ({{illnesscount}})</center></button></a>*/
/*                                             </li>*/
/*                                             {% if app.user %}*/
/*                                             {% if searchword == ""%}*/
/*                                                      {% set doctorgroupcount = "0" %}*/
/*                                             {% elseif doctorgroups is empty%}*/
/*                                                     {% set doctorgroupcount = "0" %}*/
/*                                             {%else%}*/
/*                                                 {% for doctorgroup in doctorgroups %}*/
/*                                                     {% set doctorgroupcount = (doctorgroups|length) %}*/
/*                                                 {% endfor %}*/
/*                                             {% endif %}*/
/*                                             <li><a href="#doctorgroup-r" data-toggle="tab"><button class="btn-lg btn-info" type="button"><center><i class ="fa fa-users"></i> Doctor Groups ({{doctorgroupcount}})</center></button></a>*/
/*                                             </li>*/
/*                                             {% endif %}*/
/*                                         </ul>*/
/*                                     </div>*/
/* */
/*                                     <div class="col-xs-12">*/
/*                                         <!-- Tab panes -->*/
/*                                         <div class="tab-content">*/
/*                                             <div class="tab-pane active" id="home-r">*/
/*                                                 </br>*/
/*                                                 </br>*/
/*                                                 <div style="overflow-y: scroll; height:300px;">*/
/*                                                 {% if searchword == ""%}*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                      <center><i><h3>Search something... A doctor maybe?</h3></i></center>*/
/*                                                 {%elseif doctors is empty%}*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                    <center><i><h3>No Doctor Found</h3></i></center>*/
/*                                                 {%else%}*/
/*                                                 {% for doctor in doctors %}*/
/*                                                         <center><a href="{{ path('doctor_show', {'id':doctor.id})}}"><i style="font-size: 25px; color: gray"class ="fa fa-user-md"></i><h3>*/
/*                                                                     {% if doctor.fieldOfPractice == "Specialized" %}*/
/*                                                                         <b style="color:#379DFF;">{{doctor.firstName}}  {{doctor.middleName}} {{doctor.lastName}}</b> </a>*/
/*                                                                         <br>{{doctor.fieldOfPractice}} - */
/*                                                                             {% for specialty in doctor.specialties%}*/
/*                                                                                 | {{specialty.specialtyName}} |*/
/*                                                                             {% endfor %}*/
/*                                                                     {% else %}*/
/*                                                                         <b style="color:#379DFF;">{{doctor.firstName}}  {{doctor.middleName}} {{doctor.lastName}}</b> </a>*/
/*                                                                         <br>{{doctor.fieldOfPractice}}*/
/*                                                                             */
/*                                                                     {% endif %}*/
/*                                                            </h3></center>*/
/*                                                     */
/*                                                 {% endfor %}*/
/*                                                 {% endif %}*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <div class="tab-pane" id="profile-r">*/
/*                                                 </br>*/
/*                                                 </br>*/
/*                                                 <div style="overflow-y: scroll; height:300px;">*/
/*                                                 {% if searchword == ""%}*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                      <center><i><h3>Search something... A clinic maybe?</h3></i></center>*/
/*                                                 {%elseif clinics is empty%}*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                    <center><i><h3>No Clinic Found</h3></i></center>*/
/*                                                 {%else%}*/
/*                                                 {% for clinic in clinics %}*/
/*                                                     */
/*                                                         <center><a href="{{ path('clinic_show', {'id':clinic.id})}}"><i style="font-size: 25px; color: gray" class ="fa fa-map-marker"></i><h3>*/
/*                                                                     <b style="color:#379DFF;"> {{clinic.clinicName}}</b></a>*/
/*                                                                     <br/>{{clinic.clinicAddress}}</h3></center>                  */
/*                                                     */
/*                                                 {% endfor %}*/
/*                                                 {% endif %}*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             <div class="tab-pane" id="messages-r">*/
/*                                                 </br>*/
/*                                                 </br>*/
/*                                                  <div style="overflow-y: scroll; height:300px;">*/
/*                                                 {% if searchword == ""%}*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                      <center><i><h3>Search something... An illness maybe?</h3></i></center>*/
/*                                                 {%elseif illnesses is empty%}*/
/*                                                    <center><i><h3>No Illness Found</h3></i></center>*/
/*                                                    </br>*/
/*                                                    </br>*/
/*                                                 {%else%}*/
/*                                                 {% for illness in illnesses %}*/
/*                                                     <a href="{{ path('illness_show', {'id':illness.id})}}"><center><i style=" font-size: 25px; color: gray" class ="fa fa-tint"></i><h3> */
/*                                                                     <b style="color:#379DFF;">{{illness.illnessName}}</b></a>*/
/*                                                                     <br/>"{{illness.illnessCommonName}}"*/
/*                                                                     <br/>{{illness.illnessDefinition}} </h3></center>*/
/*                                                     */
/*                                                 {% endfor %}*/
/*                                                 {% endif %}  */
/*                                                 </div>*/
/*                                             </div>*/
/*                                             {% if app.user %}*/
/*                                             <div class="tab-pane" id="doctorgroup-r">*/
/*                                                 </br>*/
/*                                                 </br>*/
/*                                                 <div style="overflow-y: scroll; height:300px;">*/
/*                                                 {% if searchword == ""%}*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                      <center><i><h3>Search something... A doctor group maybe?</h3></i></center>*/
/*                                                 {%elseif doctorgroups is empty%}*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                     <br/>*/
/*                                                    <center><i><h3>No Doctor Group Found</h3></i></center>*/
/*                                                 {%else%}*/
/*                                                 {% for doctorgroup in doctorgroups %}*/
/*                                                     */
/*                                                         <center><a href="{{ path('doctorgroup_show', {'id':doctorgroup.id})}}"><i style="font-size: 25px; color: gray"class ="fa fa-users"></i>*/
/*                                                             <h3>*/
/*                                                                 <b style="color:#379DFF;">{{doctorgroup.groupName}} </b><br/>*/
/*                                                                 {{doctorgroup.description}}*/
/*                                                             </h3>*/
/*                                                         </a></center>*/
/*                                                     */
/*                                                 {% endfor %}*/
/*                                                 {% endif %}*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                             {% endif %}*/
/*                                         </div>*/
/*                                     </div>*/
/* */
/*                                     <div class="clearfix"></div>*/
/*                         </div>*/
/*                         */
/*                 </div>*/
/* 	</div>*/
/* </div>*/
/* */
/* {% endblock %}*/
/* */
