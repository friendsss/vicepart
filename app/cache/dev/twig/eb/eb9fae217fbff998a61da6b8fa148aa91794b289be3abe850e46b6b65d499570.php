<?php

/* ResearchProjectMyProjectBundle:DoctorGroup:invite.html.twig */
class __TwigTemplate_c588907e32f2d94621d2c003a3753667f2e9c5c6fe4cbaea8e7d4adcdab5e41b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:DoctorGroup:invite.html.twig", 1);
        $this->blocks = array(
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_02d42a9f586201c4c13b1e064c2458338b9f5bd6bcf8e5bd4359593086a39b4b = $this->env->getExtension("native_profiler");
        $__internal_02d42a9f586201c4c13b1e064c2458338b9f5bd6bcf8e5bd4359593086a39b4b->enter($__internal_02d42a9f586201c4c13b1e064c2458338b9f5bd6bcf8e5bd4359593086a39b4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:DoctorGroup:invite.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_02d42a9f586201c4c13b1e064c2458338b9f5bd6bcf8e5bd4359593086a39b4b->leave($__internal_02d42a9f586201c4c13b1e064c2458338b9f5bd6bcf8e5bd4359593086a39b4b_prof);

    }

    // line 2
    public function block_doctorgroup($context, array $blocks = array())
    {
        $__internal_886521442f97b5b29812de487b90401a5c8c5fb5b86b3f75225385eed5ac5b18 = $this->env->getExtension("native_profiler");
        $__internal_886521442f97b5b29812de487b90401a5c8c5fb5b86b3f75225385eed5ac5b18->enter($__internal_886521442f97b5b29812de487b90401a5c8c5fb5b86b3f75225385eed5ac5b18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctorgroup"));

        echo " class=\"active\" ";
        
        $__internal_886521442f97b5b29812de487b90401a5c8c5fb5b86b3f75225385eed5ac5b18->leave($__internal_886521442f97b5b29812de487b90401a5c8c5fb5b86b3f75225385eed5ac5b18_prof);

    }

    // line 3
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_156457045f955b63543654fa46412bca61015920b359cbe973538ce60e98b711 = $this->env->getExtension("native_profiler");
        $__internal_156457045f955b63543654fa46412bca61015920b359cbe973538ce60e98b711->enter($__internal_156457045f955b63543654fa46412bca61015920b359cbe973538ce60e98b711_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "DOCTOR GROUP";
        
        $__internal_156457045f955b63543654fa46412bca61015920b359cbe973538ce60e98b711->leave($__internal_156457045f955b63543654fa46412bca61015920b359cbe973538ce60e98b711_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_0774a04c2907de094b2cfb4be6ec63d7ef6cfac64c2e15a5bbed4d5618c67ec1 = $this->env->getExtension("native_profiler");
        $__internal_0774a04c2907de094b2cfb4be6ec63d7ef6cfac64c2e15a5bbed4d5618c67ec1->enter($__internal_0774a04c2907de094b2cfb4be6ec63d7ef6cfac64c2e15a5bbed4d5618c67ec1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                <h3>&nbsp;Invite Members to Group</h3>
            </div>
        </div>
        <div >
            <div>
                <div  >
                    
                    <div >
                        <center>
                                 <div class=\"\">
                                    
                                        <input id='search' type=\"text\" name=\"query\" />
                                        
                                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\">
                                <div class=\"x_title\">
                                    <h2>Search result</h2>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div class=\"x_content\">
                                     <img hidden id=\"loading\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("charisma/img/ajax-loaders/ajax-loader-1.gif"), "html", null, true);
        echo "\" >
                                    <table class=\"table table-striped\" id=\"resulttable\" class=\"hidden\">
                                       
                                        <tbody id=\"resultbody\">
                                            
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                                        
                                </div>
                                
                        </center>
                    </div>
                </div>
            </div>
        </div>
        


<div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
<!-- form validation -->
    <script src=";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script  
  src=\"//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\">
</script>
    <script>
\$('#search').keyup(function() {

     searchText = \$(this).val();
     if (this.value != ''){
         \$(\"#resultbody\").empty();
     }
     
     
     if (this.value.length >= 3 && this.value != '')
  {
     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 77
        echo $this->env->getExtension('routing')->getPath("invite_doctor");
        echo "',
        data: {searchText : searchText},
        beforeSend: function(){
        \$('#loading').show();
        },
        complete: function(){
        \$('#loading').hide();
        },
        success : function(data) 
            
            {if(data){
                \$(\"#resultbody\").empty();
                var len = data.length;
                var txt = \"\";
                  if(len > 0){
                    for(var i=0;i<len;i++){
                            //var button= \$('<button type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\"></i> Invite to Group </button>');
                            txt += \"<tr><td>\"+data[i].firstName+\" \"+data[i].middleName+\" \"+data[i].lastName +\"</td>\";
                            txt+= '<td><button id=\"add-'+ data[i].id+'\" type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\"></i> Invite to Group </button>' + '</td></tr>';
                        
                    }
                    if(txt != \"\"){
                        \$(\"#resultbody\").append(txt).removeClass(\"hidden\");
                        
                    }
                }
            }
        },error: function() {
         alert('error');
          }
        });
    }
});
    </script>
    ";
        
        $__internal_0774a04c2907de094b2cfb4be6ec63d7ef6cfac64c2e15a5bbed4d5618c67ec1->leave($__internal_0774a04c2907de094b2cfb4be6ec63d7ef6cfac64c2e15a5bbed4d5618c67ec1_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:DoctorGroup:invite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 77,  125 => 59,  91 => 28,  66 => 5,  60 => 4,  48 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block doctorgroup %} class="active" {% endblock %}*/
/* {% block pagetitle %}DOCTOR GROUP{% endblock %}*/
/* {% block body -%}*/
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 <h3>&nbsp;Invite Members to Group</h3>*/
/*             </div>*/
/*         </div>*/
/*         <div >*/
/*             <div>*/
/*                 <div  >*/
/*                     */
/*                     <div >*/
/*                         <center>*/
/*                                  <div class="">*/
/*                                     */
/*                                         <input id='search' type="text" name="query" />*/
/*                                         */
/*                                         <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                             <div class="x_panel">*/
/*                                 <div class="x_title">*/
/*                                     <h2>Search result</h2>*/
/*                                     <div class="clearfix"></div>*/
/*                                 </div>*/
/*                                 <div class="x_content">*/
/*                                      <img hidden id="loading" src="{{asset('charisma/img/ajax-loaders/ajax-loader-1.gif')}}" >*/
/*                                     <table class="table table-striped" id="resulttable" class="hidden">*/
/*                                        */
/*                                         <tbody id="resultbody">*/
/*                                             */
/*                                             </tr>*/
/*                                         </tbody>*/
/*                                     </table>*/
/* */
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                                         */
/*                                 </div>*/
/*                                 */
/*                         </center>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         */
/* */
/* */
/* <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script  */
/*   src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js">*/
/* </script>*/
/*     <script>*/
/* $('#search').keyup(function() {*/
/* */
/*      searchText = $(this).val();*/
/*      if (this.value != ''){*/
/*          $("#resultbody").empty();*/
/*      }*/
/*      */
/*      */
/*      if (this.value.length >= 3 && this.value != '')*/
/*   {*/
/*      $.ajax({*/
/*         type: "POST",*/
/*         dataType: "json",*/
/*         url: '{{ path('invite_doctor') }}',*/
/*         data: {searchText : searchText},*/
/*         beforeSend: function(){*/
/*         $('#loading').show();*/
/*         },*/
/*         complete: function(){*/
/*         $('#loading').hide();*/
/*         },*/
/*         success : function(data) */
/*             */
/*             {if(data){*/
/*                 $("#resultbody").empty();*/
/*                 var len = data.length;*/
/*                 var txt = "";*/
/*                   if(len > 0){*/
/*                     for(var i=0;i<len;i++){*/
/*                             //var button= $('<button type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user"></i> Invite to Group </button>');*/
/*                             txt += "<tr><td>"+data[i].firstName+" "+data[i].middleName+" "+data[i].lastName +"</td>";*/
/*                             txt+= '<td><button id="add-'+ data[i].id+'" type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user"></i> Invite to Group </button>' + '</td></tr>';*/
/*                         */
/*                     }*/
/*                     if(txt != ""){*/
/*                         $("#resultbody").append(txt).removeClass("hidden");*/
/*                         */
/*                     }*/
/*                 }*/
/*             }*/
/*         },error: function() {*/
/*          alert('error');*/
/*           }*/
/*         });*/
/*     }*/
/* });*/
/*     </script>*/
/*     {% endblock %}*/
