<?php

/* ResearchProjectMyProjectBundle:Admin:editClinicSearch.html.twig */
class __TwigTemplate_61e7beaac7fd9888d30a8f6c98d0b505c11bc28a3bcf982aeeeddb3ea676b53d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:editClinicSearch.html.twig", 1);
        $this->blocks = array(
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04c549589982888ec536f2f02dbaab28b7ef4602712db659ae2529554dfc7cff = $this->env->getExtension("native_profiler");
        $__internal_04c549589982888ec536f2f02dbaab28b7ef4602712db659ae2529554dfc7cff->enter($__internal_04c549589982888ec536f2f02dbaab28b7ef4602712db659ae2529554dfc7cff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:editClinicSearch.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_04c549589982888ec536f2f02dbaab28b7ef4602712db659ae2529554dfc7cff->leave($__internal_04c549589982888ec536f2f02dbaab28b7ef4602712db659ae2529554dfc7cff_prof);

    }

    // line 2
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_8a05efea3b8456966411dfc1d71946d6ed3aaf7bb0161d97cf042285d68fbf25 = $this->env->getExtension("native_profiler");
        $__internal_8a05efea3b8456966411dfc1d71946d6ed3aaf7bb0161d97cf042285d68fbf25->enter($__internal_8a05efea3b8456966411dfc1d71946d6ed3aaf7bb0161d97cf042285d68fbf25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "Edit Clinic";
        
        $__internal_8a05efea3b8456966411dfc1d71946d6ed3aaf7bb0161d97cf042285d68fbf25->leave($__internal_8a05efea3b8456966411dfc1d71946d6ed3aaf7bb0161d97cf042285d68fbf25_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_be9dbc05a0c246723130a77e00d37429e0b42bf270a11f4b83aadfb26b47c346 = $this->env->getExtension("native_profiler");
        $__internal_be9dbc05a0c246723130a77e00d37429e0b42bf270a11f4b83aadfb26b47c346->enter($__internal_be9dbc05a0c246723130a77e00d37429e0b42bf270a11f4b83aadfb26b47c346_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    
                        <h3>Edit Clinic</h3>
                       
                    <div class=\"x_content\">
                        <input class=\"form-control\" id='search' type=\"text\" name=\"query\" placeholder=\"Type a name\" />
                         
                                                    <center><img hidden id=\"loading\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("charisma/img/ajax-loaders/ajax-loader-1.gif"), "html", null, true);
        echo "\" ></center>
                                                    <table class=\"table table-striped\" id=\"resulttable\" class=\"hidden\">

                                                        <tbody id=\"resultbody\">

                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- form validation -->
<script>
                                   
   </script>
    <script>
\$('#search').keyup(function() {
    
     searchText = \$(this).val();
     if (this.value != ''){
         \$(\"#resultbody\").empty();
     }
     
     
     if (this.value.length >= 3 && this.value != '')
  {
     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 53
        echo $this->env->getExtension('routing')->getPath("admin_edit_clinic_search");
        echo "',
        data: {searchText : searchText},
        beforeSend: function(){
        \$('#loading').show();
        },
        complete: function(){
        \$('#loading').hide();
        },
        success : function(data) 
           
           {if(data){
                \$(\"#resultbody\").empty();
                var len = data.length;
                var txt = \"\";
                  if(len > 0){
                    for(var i=0;i<len;i++){
                            //var button= \$('<button type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\"></i> Invite to Group </button>');
                            var entityId = data[i].id;
                            var showPath = '";
        // line 71
        echo $this->env->getExtension('routing')->getPath("admin_clinic_edit", array("id" => "entityId"));
        echo "';
                            showPath = showPath.replace(\"entityId\", data[i].id);
                             var showPath2 = '";
        // line 73
        echo $this->env->getExtension('routing')->getPath("clinic_show", array("id" => "entityId"));
        echo "';
                            showPath2 = showPath2.replace(\"entityId\", data[i].id);
                            txt += \"<tr><td>\"+data[i].clinicName+\"</td>\";
                            txt+= '<td><a href=\"'+showPath+'\" ><button id=\"edit-'+ data[i].id+'\" type=\"button\" class=\"btn btn-success btn-xs btn-edit\"> <i class=\"fa fa-edit\"></i> Edit Clinic </button><td><a href=\"'+showPath2+'\" ><button id=\"view-'+ data[i].id+'\" type=\"button\" class=\"btn btn-primary btn-xs btn-view\"> <i class=\"fa fa-hospital-o\"></i> View Clinic </button>';
                        
                    }
                    if(txt != \"\"){
                        \$(\"#resultbody\").append(txt).removeClass(\"hidden\");
                         
                    }
                }
            }
        },error: function() {
         alert('error');
          }
        });
    }
});
       
    </script>
    ";
        
        $__internal_be9dbc05a0c246723130a77e00d37429e0b42bf270a11f4b83aadfb26b47c346->leave($__internal_be9dbc05a0c246723130a77e00d37429e0b42bf270a11f4b83aadfb26b47c346_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:editClinicSearch.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 73,  128 => 71,  107 => 53,  70 => 19,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% block pagetitle %}Edit Clinic{% endblock %}*/
/* {% block body -%}*/
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                     */
/*                         <h3>Edit Clinic</h3>*/
/*                        */
/*                     <div class="x_content">*/
/*                         <input class="form-control" id='search' type="text" name="query" placeholder="Type a name" />*/
/*                          */
/*                                                     <center><img hidden id="loading" src="{{asset('charisma/img/ajax-loaders/ajax-loader-1.gif')}}" ></center>*/
/*                                                     <table class="table table-striped" id="resulttable" class="hidden">*/
/* */
/*                                                         <tbody id="resultbody">*/
/* */
/*                                                             </tr>*/
/*                                                         </tbody>*/
/*                                                     </table>*/
/* */
/*                                                 </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/* <script>*/
/*                                    */
/*    </script>*/
/*     <script>*/
/* $('#search').keyup(function() {*/
/*     */
/*      searchText = $(this).val();*/
/*      if (this.value != ''){*/
/*          $("#resultbody").empty();*/
/*      }*/
/*      */
/*      */
/*      if (this.value.length >= 3 && this.value != '')*/
/*   {*/
/*      $.ajax({*/
/*         type: "POST",*/
/*         dataType: "json",*/
/*         url: '{{ path('admin_edit_clinic_search') }}',*/
/*         data: {searchText : searchText},*/
/*         beforeSend: function(){*/
/*         $('#loading').show();*/
/*         },*/
/*         complete: function(){*/
/*         $('#loading').hide();*/
/*         },*/
/*         success : function(data) */
/*            */
/*            {if(data){*/
/*                 $("#resultbody").empty();*/
/*                 var len = data.length;*/
/*                 var txt = "";*/
/*                   if(len > 0){*/
/*                     for(var i=0;i<len;i++){*/
/*                             //var button= $('<button type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user"></i> Invite to Group </button>');*/
/*                             var entityId = data[i].id;*/
/*                             var showPath = '{{ path("admin_clinic_edit", {'id' : 'entityId' })}}';*/
/*                             showPath = showPath.replace("entityId", data[i].id);*/
/*                              var showPath2 = '{{ path("clinic_show", {'id' : 'entityId' })}}';*/
/*                             showPath2 = showPath2.replace("entityId", data[i].id);*/
/*                             txt += "<tr><td>"+data[i].clinicName+"</td>";*/
/*                             txt+= '<td><a href="'+showPath+'" ><button id="edit-'+ data[i].id+'" type="button" class="btn btn-success btn-xs btn-edit"> <i class="fa fa-edit"></i> Edit Clinic </button><td><a href="'+showPath2+'" ><button id="view-'+ data[i].id+'" type="button" class="btn btn-primary btn-xs btn-view"> <i class="fa fa-hospital-o"></i> View Clinic </button>';*/
/*                         */
/*                     }*/
/*                     if(txt != ""){*/
/*                         $("#resultbody").append(txt).removeClass("hidden");*/
/*                          */
/*                     }*/
/*                 }*/
/*             }*/
/*         },error: function() {*/
/*          alert('error');*/
/*           }*/
/*         });*/
/*     }*/
/* });*/
/*        */
/*     </script>*/
/*     {% endblock %}*/
/* */
