<?php

/* ResearchProjectMyProjectBundle:DoctorGroup:show.html.twig */
class __TwigTemplate_569ebf8a06fb40bf274153838a88f9e9d56a7d899651fdd35a879b6c9833b0fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:DoctorGroup:show.html.twig", 1);
        $this->blocks = array(
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a46f014c0b91155375c7cb081f7e06ba9f7f4d392c09d3a889d50ecc7b5d2f2 = $this->env->getExtension("native_profiler");
        $__internal_4a46f014c0b91155375c7cb081f7e06ba9f7f4d392c09d3a889d50ecc7b5d2f2->enter($__internal_4a46f014c0b91155375c7cb081f7e06ba9f7f4d392c09d3a889d50ecc7b5d2f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:DoctorGroup:show.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4a46f014c0b91155375c7cb081f7e06ba9f7f4d392c09d3a889d50ecc7b5d2f2->leave($__internal_4a46f014c0b91155375c7cb081f7e06ba9f7f4d392c09d3a889d50ecc7b5d2f2_prof);

    }

    // line 3
    public function block_doctorgroup($context, array $blocks = array())
    {
        $__internal_f2c83833a2ff02444ee0604c3bdb0e0856f140b7f0e82d1e9eba5742a4398675 = $this->env->getExtension("native_profiler");
        $__internal_f2c83833a2ff02444ee0604c3bdb0e0856f140b7f0e82d1e9eba5742a4398675->enter($__internal_f2c83833a2ff02444ee0604c3bdb0e0856f140b7f0e82d1e9eba5742a4398675_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctorgroup"));

        echo " class=\"active\" ";
        
        $__internal_f2c83833a2ff02444ee0604c3bdb0e0856f140b7f0e82d1e9eba5742a4398675->leave($__internal_f2c83833a2ff02444ee0604c3bdb0e0856f140b7f0e82d1e9eba5742a4398675_prof);

    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_15f5c992e31353f374c9f12cef93bc23e955fde6f3c3ea00741b0095bfa66b07 = $this->env->getExtension("native_profiler");
        $__internal_15f5c992e31353f374c9f12cef93bc23e955fde6f3c3ea00741b0095bfa66b07->enter($__internal_15f5c992e31353f374c9f12cef93bc23e955fde6f3c3ea00741b0095bfa66b07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "DOCTOR GROUP";
        
        $__internal_15f5c992e31353f374c9f12cef93bc23e955fde6f3c3ea00741b0095bfa66b07->leave($__internal_15f5c992e31353f374c9f12cef93bc23e955fde6f3c3ea00741b0095bfa66b07_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_51b1d513f50e12053225d7f17bd36ddfdf4a51f1a6dd5ed6ecfdff60e9fa19f5 = $this->env->getExtension("native_profiler");
        $__internal_51b1d513f50e12053225d7f17bd36ddfdf4a51f1a6dd5ed6ecfdff60e9fa19f5->enter($__internal_51b1d513f50e12053225d7f17bd36ddfdf4a51f1a6dd5ed6ecfdff60e9fa19f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                <h3>&nbsp;DOCTOR GROUP</h3>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    
                        <h3>Group Profile</h3>
                       
                    <div class=\"x_content\">
                        <center>
                            <div hidden id=\"groupId\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()), "html", null, true);
        echo "</div>
                            <h3 style=\"font-size: 30px;\"><b> ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "groupName", array()), "html", null, true);
        echo "</b></h3>
                            <h4 style=\"font-family: Century Gothic; font-size: 20px;\"><i> ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "description", array()), "html", null, true);
        echo "</i></h4>
                            <h4 style=\"font-family: Century Gothic; font-size: 15px;\"><i> Created on: ";
        // line 23
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "dateCreated", array()), "m-d-Y"), "html", null, true);
        echo "</i></h4>
                            
                                ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getAdminDoctors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
            // line 26
            echo "                                    ";
            if (($context["admin"] == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()))) {
                // line 27
                echo "                                        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctorgroup_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
                echo "\"><button class=\"btn btn-primary\"><i class=\"glyphicon glyphicon-edit\"></i> Edit Information</button></a>
                                       <button id =\"imodal\" type=\"button\" class=\"btn btn-success btn\" data-toggle=\"modal\" data-target=\"#invite\" data-backdrop=\"static\" data-keyboard=\"false\"><i class=\"glyphicon glyphicon-user\"></i> Invite Member to Group</button>
                                       <button id =\"view\" type=\"button\" class=\"btn btn-success btn\" data-toggle=\"modal\" data-target=\"#viewmembers\" data-backdrop=\"static\" data-keyboard=\"false\"><i class=\"glyphicon glyphicon-eye-open\"></i> View Members</button>
                                       <button id =\"deletegroup-";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()), "html", null, true);
                echo "\" type=\"button\" class=\"btn btn-danger btn-delete-group\" data-toggle=\"modal\" data-target=\"#deletegroupmodal\" data-backdrop=\"static\" data-keyboard=\"false\"><i class=\"glyphicon glyphicon-trash\"></i> Delete Group</button>
                                    ";
            }
            // line 32
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    <div align=\"right\" >
                        <h3 style=\"font-size: 20px;\">Group Discussions</h3>
                        ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["member"]) {
            // line 44
            echo "                            ";
            if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()) == $context["member"])) {
                // line 45
                echo "                                <button type=\"button\" class=\"btn btn-success right\" data-toggle=\"modal\" data-target=\"#postmodal\"><i class=\"glyphicon glyphicon-plus-sign icon-white\"></i>
                                    New Post</button>
                            ";
            }
            // line 48
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['member'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                                <div id=\"postmodal\" class=\"modal fade bs-example-modal-lg\" align=\"left\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog modal-lg\">
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                                </button>
                                                <h3 class=\"modal-title\" id=\"myModalLabel\">New Post</h3>
                                            </div>
                                            <div class=\"modal-body\">
                                                
                                                ";
        // line 59
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "newPost", "class" => "form-horizontal form-label-left")));
        echo "
                                                <div class=\"item form-group\">
                                                <label class=\"control-label col-md-2 col-sm-2 col-xs-2 left-align\" >Post Title: </label>
                                                    <div class=\"col-md-7\">
                                                        ";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "postTitle", array()), 'widget', array("attr" => array("name" => "postTitle", "placeholder" => "Post Title", "class" => "form-control col-md-5 col-xs-5", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 64
        echo "
                                                    </div>
                                                </div>
                                                <div class=\"item form-group\">
                                                <label class=\"control-label col-md-2 col-sm-2 col-xs-2\" >Post Content: </label>
                                                    <div class=\"col-md-7\">
                                                        ";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "postContent", array()), 'widget', array("attr" => array("name" => "postContent", "placeholder" => "Post Content", "class" => "form-control col-md-5 col-xs-5", "data-validate-length-range" => "5", "required" => "required", "type" => "text")));
        // line 71
        echo "
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                            <div class=\"modal-footer\">
                                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                <a href=\"\">";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Post", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn", "onClick" => "submitClicked()")));
        echo "</a> 
                                            </div>
                                            ";
        // line 80
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                                        </div>
                                    </div>
                                </div>
                                <div class=\"clearfix\"></div>
                        
                    </div>
                    <div class=\"x_content\" align=\"center\">
                        ";
        // line 88
        if (twig_test_empty($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getGroupPosts", array()))) {
            // line 89
            echo "                            <h4 style=\"font-family: Century Gothic;font-size: 16px;\"> <i>No discussions started yet.</i> </h4>
                        ";
        } else {
            // line 91
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getGroupPosts", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                // line 92
                echo "                        
                        <h3>Post Title: ";
                // line 93
                echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "postTitle", array()), "html", null, true);
                echo " </h3>
                        <a style =\"color: white;\"   href=\"";
                // line 94
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grouppost_show", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
                echo "\"><button data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"View post\" class='btn btn-info'><i class=\"glyphicon glyphicon-eye-open icon-white\"></i>
                                </button></a>
                        ";
                // line 96
                if (($this->getAttribute($context["post"], "doctorAuthor", array()) == $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()))) {
                    echo " 
                        <a  id=\"dlt_post-";
                    // line 97
                    echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "id", array()), "html", null, true);
                    echo "\"  data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Delete post\" class=\"btn btn-danger btn-delete-post\" href=\"#\">
                                                            <i class=\"glyphicon glyphicon-trash icon-white\"></i>
                                                            
                                                        </a>
                        <a  id=\"edit_post-";
                    // line 101
                    echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "id", array()), "html", null, true);
                    echo "\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Edit post\" class=\"btn btn-primary btn-edit-post\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grouppost_edit", array("id" => $this->getAttribute($context["post"], "id", array()))), "html", null, true);
                    echo "\">
                                                            <i class=\"glyphicon glyphicon-edit icon-white\" ></i>
                                                            
                                                        </a>
                        ";
                }
                // line 106
                echo "                        <div class='ln solid'></div>
                        </br>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 109
            echo "                        ";
        }
        // line 110
        echo "                    </div>
                </div>
            </div>
        </div>
        <div class=\"clearfix\"></div>
        
        <div id=\"invitemodal\" class=\"modal fade bs-example-modal-lg\" align=\"left\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" data-backdrop=\"static\" >
                                    <div class=\"modal-dialog modal-lg\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                                </button>
                                                <h4 class=\"modal-title\" style=\"font-family: Century Gothic;\" id=\"myModalLabel\">Invite members to group</h4>
                                            </div>
                                            <div class=\"modal-body\">
                                                
                                            </div>
                               
                                        </div>
                                    </div>
                                </div>
        
        
        <div id=\"delete\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" >
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                 <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 style=\"font-family: Century Gothic; font-size: 16px;\"class=\"modal-title\" id=\"deleteModal\">Delete Post</h4>
                                            </div>

                                            <div class=\"modal-body\">
                                                <center><h4 style=\"font-family: Century Gothic; font-size: 16px;\">
                                                        <i class=\"fa fa-exclamation-triangle\" style=\"color:#ff4d4d; font-size: 30px;\"></i>&emsp;Are you sure you want to remove this post?</h4></center>
                                            </div>
                                            
                                            <div class=\"modal-footer\">
                                                    <button type=\"button\" id='con_delete_post' class=\"btn btn-primary\" >Yes</button>
                                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
        
        <div id=\"invite\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" >
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                 <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 style=\"font-family: Century Gothic; font-size: 16px;\"class=\"modal-title\" id=\"editModal\">Invite Members</h4>
                                            </div>

                                            <div class=\"modal-body confirm\">
                                            <div class=\"x_panel\">
                                                <div class=\"x_title\">
                                                    <center>
                                                        <h3 style=\"font-size: 20px;\">Find member</h3>
                                                    <div class=\"clearfix\"></div>
                                                    <input class='form-control'id='search' placeholder=\"Search name of doctor\" type=\"text\" name=\"query\" />
                                                    </center>
                                                </div>
                                                <div class=\"x_content\" style=\"overflow-y: scroll; height:300px;\">
                                                    <center><img hidden id=\"loading\" src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("charisma/img/ajax-loaders/ajax-loader-1.gif"), "html", null, true);
        echo "\" ></center>
                                                    <table class=\"table table-striped\" id=\"resulttable\" class=\"hidden\">

                                                        <tbody id=\"resultbody\">
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            </div>
                                          
                                               
                                             <div class=\"modal-footer\">
                                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                    
                                                </div>
                                            </div>

                                        </div>
                                    </div>
       <div id=\"deletegroupmodal\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" >
                                        <div class=\"modal-content\">
                                            
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 style=\"font-family: Century Gothic; font-size: 16px;\"class=\"modal-title\" id=\"deletegroupmodal\">Delete Group</h4>
                                            </div>

                                            <div class=\"modal-body\">
                                                <center><h4 style=\"font-family: Century Gothic; font-size: 16px;\">
                                                        <i class=\"fa fa-exclamation-triangle\" style=\"color:#ff4d4d; font-size: 30px;\"></i>&emsp;Are you sure you want to remove this group?</h4></center>
                                            </div>
                                            
                                            <div class=\"modal-footer\">
                                                    <button type=\"button\" id='con_delete_post' class=\"btn btn-primary\" >Yes</button>
                                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
         <div id=\"viewmembers\" class=\"modal fade bs-example-modal-lg\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" >
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                               <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 style=\"font-family: Century Gothic; font-size: 16px;\"class=\"modal-title\" id=\"viewmodal\">Remove Members</h4>
                                            </div>

                                            <div class=\"modal-body\">
                                               <div class=\"x_panel\">
                                                    <center><h3 style=\"font-size: 20px;\">MEMBERS</h3></center>
                                                <div class=\"x_content\" style=\"overflow-y: scroll; height:400px;\">
                                                   
                                                    <table class=\"table table-striped\" id=\"membertable\" class=\"hidden\">

                                                        <tbody id=\"viewbody\">
                                                            ";
        // line 231
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "doctors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["member"]) {
            // line 232
            echo "                                                            <tr>
                                                                <td>
                                                                    <center>
                                                                    <a href=\"";
            // line 235
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute($context["member"], "id", array()))), "html", null, true);
            echo "\">     ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["member"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["member"], "lastName", array()), "html", null, true);
            echo " </a>
                                                                    </center>
                                                                </td>
                                                                <td>
                                                                    <center>
                                                                    <button id=\"deletemember-";
            // line 240
            echo twig_escape_filter($this->env, $this->getAttribute($context["member"], "id", array()), "html", null, true);
            echo "\" type=\"button\" class=\"btn btn-danger btn-xs btn-removefromgroup\"> <i class=\"fa fa-close\"></i> Remove from Group</button> 
                                                                    </center>
                                                                </td>
                                                            </tr>
                                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['member'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 245
        echo "                                                            <div id=\"noresults\"></div>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                                
                                                
                                                
                                            </div>

                                        </div>
                                    </div>
                                </div>
          </div>
        
    </div>


<div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>
    
<!-- form validation -->
    <script src=";
        // line 272
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#newPost')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#newPost').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script>
                                    var dltPostBtnId;
                                    \$(\".btn-delete-post\").click(function(x) {
                                        x.preventDefault();
                                        dltPostBtnId = this.id;
                                        dltPostBtnId = dltPostBtnId.replace('dlt_post-', '');
                                        \$('#delete').modal();
                                    });
                                    \$(\"#con_delete_post\").click(function(d) {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 310
        echo $this->env->getExtension('routing')->getPath("post_delete");
        echo "',
                                            data: {id: dltPostBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                               location.reload();
                                                console.log(response);
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    });
                                    
                                    var dltGroupBtnId;
                                    \$(\".btn-delete-group\").click(function(x) {
                                        x.preventDefault();
                                        dltGroupBtnId = this.id;
                                        dltGroupBtnId = dltGroupBtnId.replace('deletegroup-', '');
                                        
                                    });
                                    \$(\"#con_delete_group\").click(function(d) {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 333
        echo $this->env->getExtension('routing')->getPath("delete_group");
        echo "',
                                            data: {id: dltGroupBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                                 window.location.href = response;
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    }); 
                                    
                                    var inviteBtnId;
                                    var groupId = \$('#groupId').text();
                                    \$(document).on(\"click\", \".btn-invite\", function(x){
                                         x.preventDefault();
                                        inviteBtnId = this.id;
                                        inviteBtnId = inviteBtnId.replace('add-', '');
                                         \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 353
        echo $this->env->getExtension('routing')->getPath("doctorgroup_invite");
        echo "',
                                            data: {id: inviteBtnId, groupId : groupId},
                                            dataType: 'json',
                                            success: function(response) {
                                               location.reload();
                                                console.log(response);
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    });
                                    
                                     var memberBtnId;
                                     var groupId = \$('#groupId').text();
                                    \$(\".btn-removefromgroup\").click(function(x) {
                                        x.preventDefault();
                                        memberBtnId = this.id;
                                        memberBtnId = memberBtnId.replace('deletemember-', '');
                                        
                                       \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 375
        echo $this->env->getExtension('routing')->getPath("delete_member");
        echo "',
                                            data: {id: memberBtnId, groupId : groupId},
                                            dataType: 'json',
                                            success: function(response) {
                                                //\$('#viewbody').load(document.URL +  ' #viewbody');
                                                console.log(response);
                                                location.reload();
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    });
                                   
                                   
</script>
   <script>
\$('#search').keyup(function() {

     searchText = \$(this).val();
     groupId = \$('#groupId').text();
     if (this.value != ''){
         \$(\"#resultbody\").empty();
     }
     
     
     if (this.value.length >= 3 && this.value != '')
  {
     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 406
        echo $this->env->getExtension('routing')->getPath("invite_doctor");
        echo "',
        data: {searchText : searchText, groupId : groupId },
        beforeSend: function(){
        \$('#loading').show();
        },
        complete: function(){
        \$('#loading').hide();
        },
        success : function(data) 
           
            {if(data){
                \$(\"#resultbody\").empty();
                var len = data.length;
                var txt = \"\";
                  if(len > 0){
                    for(var i=0;i<len;i++){
                            //var button= \$('<button type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\"></i> Invite to Group </button>');
                            var entityId = data[i].id;
                            var showPath = '";
        // line 424
        echo $this->env->getExtension('routing')->getPath("doctor_show", array("id" => "entityId"));
        echo "';
                            showPath = showPath.replace(\"entityId\", data[i].id);
                            txt += \"<tr><td>\"+data[i].firstName+\" \"+data[i].middleName+\" \"+data[i].lastName +\"</td>\";
                            txt+= '<td><a href=\"#\" ><button id=\"add-'+ data[i].id+'\" type=\"button\" class=\"btn btn-primary btn-xs btn-invite\"> <i class=\"fa fa-envelope\"></i> Invite to Group </button>' + '</td><td><a href=\"'+showPath+'\" ><button type=\"button\" class=\"btn btn-success btn-xs\"> <i class=\"fa fa-user\"></i>View Profile</button>' + '</td></tr>';
                        
                    }
                    if(txt != \"\"){
                        \$(\"#resultbody\").append(txt).removeClass(\"hidden\");
                         
                    }
                }
            }
        },error: function() {
         alert('error');
          }
        });
    }
});

       
    </script>
    ";
        
        $__internal_51b1d513f50e12053225d7f17bd36ddfdf4a51f1a6dd5ed6ecfdff60e9fa19f5->leave($__internal_51b1d513f50e12053225d7f17bd36ddfdf4a51f1a6dd5ed6ecfdff60e9fa19f5_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:DoctorGroup:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  625 => 424,  604 => 406,  570 => 375,  545 => 353,  522 => 333,  496 => 310,  455 => 272,  426 => 245,  415 => 240,  403 => 235,  398 => 232,  394 => 231,  334 => 174,  268 => 110,  265 => 109,  257 => 106,  247 => 101,  240 => 97,  236 => 96,  231 => 94,  227 => 93,  224 => 92,  219 => 91,  215 => 89,  213 => 88,  202 => 80,  197 => 78,  188 => 71,  186 => 70,  178 => 64,  176 => 63,  169 => 59,  157 => 49,  151 => 48,  146 => 45,  143 => 44,  139 => 43,  127 => 33,  121 => 32,  116 => 30,  109 => 27,  106 => 26,  102 => 25,  97 => 23,  93 => 22,  89 => 21,  85 => 20,  69 => 6,  63 => 5,  51 => 4,  39 => 3,  32 => 1,  30 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% form_theme form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {% block doctorgroup %} class="active" {% endblock %}*/
/* {% block pagetitle %}DOCTOR GROUP{% endblock %}*/
/* {% block body -%}*/
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 <h3>&nbsp;DOCTOR GROUP</h3>*/
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                     */
/*                         <h3>Group Profile</h3>*/
/*                        */
/*                     <div class="x_content">*/
/*                         <center>*/
/*                             <div hidden id="groupId">{{entity.id}}</div>*/
/*                             <h3 style="font-size: 30px;"><b> {{entity.groupName}}</b></h3>*/
/*                             <h4 style="font-family: Century Gothic; font-size: 20px;"><i> {{entity.description}}</i></h4>*/
/*                             <h4 style="font-family: Century Gothic; font-size: 15px;"><i> Created on: {{ entity.dateCreated | date('m-d-Y')}}</i></h4>*/
/*                             */
/*                                 {% for admin in entity.getAdminDoctors%}*/
/*                                     {% if admin == app.user %}*/
/*                                         <a href="{{path('doctorgroup_edit', {'id': entity.id})}}"><button class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit Information</button></a>*/
/*                                        <button id ="imodal" type="button" class="btn btn-success btn" data-toggle="modal" data-target="#invite" data-backdrop="static" data-keyboard="false"><i class="glyphicon glyphicon-user"></i> Invite Member to Group</button>*/
/*                                        <button id ="view" type="button" class="btn btn-success btn" data-toggle="modal" data-target="#viewmembers" data-backdrop="static" data-keyboard="false"><i class="glyphicon glyphicon-eye-open"></i> View Members</button>*/
/*                                        <button id ="deletegroup-{{entity.id}}" type="button" class="btn btn-danger btn-delete-group" data-toggle="modal" data-target="#deletegroupmodal" data-backdrop="static" data-keyboard="false"><i class="glyphicon glyphicon-trash"></i> Delete Group</button>*/
/*                                     {% endif %}*/
/*                                 {% endfor %}*/
/*                         </center>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                     <div align="right" >*/
/*                         <h3 style="font-size: 20px;">Group Discussions</h3>*/
/*                         {% for member in entity.getDoctors%}*/
/*                             {% if app.user == member %}*/
/*                                 <button type="button" class="btn btn-success right" data-toggle="modal" data-target="#postmodal"><i class="glyphicon glyphicon-plus-sign icon-white"></i>*/
/*                                     New Post</button>*/
/*                             {% endif %}*/
/*                         {% endfor %}*/
/*                                 <div id="postmodal" class="modal fade bs-example-modal-lg" align="left" tabindex="-1" role="dialog" aria-hidden="true">*/
/*                                     <div class="modal-dialog modal-lg">*/
/*                                         <div class="modal-content">*/
/*                                             <div class="modal-header">*/
/*                                                 <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>*/
/*                                                 </button>*/
/*                                                 <h3 class="modal-title" id="myModalLabel">New Post</h3>*/
/*                                             </div>*/
/*                                             <div class="modal-body">*/
/*                                                 */
/*                                                 {{ form_start(form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'newPost', 'class':'form-horizontal form-label-left' } }) }}*/
/*                                                 <div class="item form-group">*/
/*                                                 <label class="control-label col-md-2 col-sm-2 col-xs-2 left-align" >Post Title: </label>*/
/*                                                     <div class="col-md-7">*/
/*                                                         {{ form_widget(form.postTitle, { 'attr' : { 'name': 'postTitle', 'placeholder' : 'Post Title', 'class' : 'form-control col-md-5 col-xs-5', */
/*                                                         'data-validate-length-range' : '1', 'required': 'required', 'type' : 'text' } }) }}*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <div class="item form-group">*/
/*                                                 <label class="control-label col-md-2 col-sm-2 col-xs-2" >Post Content: </label>*/
/*                                                     <div class="col-md-7">*/
/*                                                         {{ form_widget(form.postContent, { 'attr' : { 'name': 'postContent', 'placeholder' : 'Post Content', 'class' : 'form-control col-md-5 col-xs-5', */
/*                                                         'data-validate-length-range' : '5', 'required': 'required', 'type' : 'text' } }) }}*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                     */
/*                                             </div>*/
/*                                             <div class="modal-footer">*/
/*                                                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>*/
/*                                                 <a href="">{{ form_widget(form.Post, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn', 'onClick' : 'submitClicked()'} }) }}</a> */
/*                                             </div>*/
/*                                             {{ form_end(form) }}*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="clearfix"></div>*/
/*                         */
/*                     </div>*/
/*                     <div class="x_content" align="center">*/
/*                         {% if entity.getGroupPosts is empty %}*/
/*                             <h4 style="font-family: Century Gothic;font-size: 16px;"> <i>No discussions started yet.</i> </h4>*/
/*                         {% else %}*/
/*                         {% for post in entity.getGroupPosts %}*/
/*                         */
/*                         <h3>Post Title: {{post.postTitle}} </h3>*/
/*                         <a style ="color: white;"   href="{{path('grouppost_show', {'id' : post.id})}}"><button data-toggle="tooltip" data-placement="bottom" title="View post" class='btn btn-info'><i class="glyphicon glyphicon-eye-open icon-white"></i>*/
/*                                 </button></a>*/
/*                         {% if post.doctorAuthor == app.user %} */
/*                         <a  id="dlt_post-{{post.id}}"  data-toggle="tooltip" data-placement="bottom" title="Delete post" class="btn btn-danger btn-delete-post" href="#">*/
/*                                                             <i class="glyphicon glyphicon-trash icon-white"></i>*/
/*                                                             */
/*                                                         </a>*/
/*                         <a  id="edit_post-{{post.id}}" data-toggle="tooltip" data-placement="bottom" title="Edit post" class="btn btn-primary btn-edit-post" href="{{ path('grouppost_edit', {'id': post.id})}}">*/
/*                                                             <i class="glyphicon glyphicon-edit icon-white" ></i>*/
/*                                                             */
/*                                                         </a>*/
/*                         {% endif%}*/
/*                         <div class='ln solid'></div>*/
/*                         </br>*/
/*                         {% endfor %}*/
/*                         {% endif %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="clearfix"></div>*/
/*         */
/*         <div id="invitemodal" class="modal fade bs-example-modal-lg" align="left" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" >*/
/*                                     <div class="modal-dialog modal-lg">*/
/*                                         <div class="modal-content">*/
/* */
/*                                             <div class="modal-header">*/
/*                                                 <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>*/
/*                                                 </button>*/
/*                                                 <h4 class="modal-title" style="font-family: Century Gothic;" id="myModalLabel">Invite members to group</h4>*/
/*                                             </div>*/
/*                                             <div class="modal-body">*/
/*                                                 */
/*                                             </div>*/
/*                                */
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*         */
/*         */
/*         <div id="delete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">*/
/*                                     <div class="modal-dialog" >*/
/*                                         <div class="modal-content">*/
/*                                             <div class="modal-header">*/
/*                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 16px;"class="modal-title" id="deleteModal">Delete Post</h4>*/
/*                                             </div>*/
/* */
/*                                             <div class="modal-body">*/
/*                                                 <center><h4 style="font-family: Century Gothic; font-size: 16px;">*/
/*                                                         <i class="fa fa-exclamation-triangle" style="color:#ff4d4d; font-size: 30px;"></i>&emsp;Are you sure you want to remove this post?</h4></center>*/
/*                                             </div>*/
/*                                             */
/*                                             <div class="modal-footer">*/
/*                                                     <button type="button" id='con_delete_post' class="btn btn-primary" >Yes</button>*/
/*                                                     <button type="button" class="btn btn-default" data-dismiss="modal">No</button>*/
/*                                             </div>*/
/* */
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*         */
/*         <div id="invite" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">*/
/*                                     <div class="modal-dialog" >*/
/*                                         <div class="modal-content">*/
/*                                             <div class="modal-header">*/
/*                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 16px;"class="modal-title" id="editModal">Invite Members</h4>*/
/*                                             </div>*/
/* */
/*                                             <div class="modal-body confirm">*/
/*                                             <div class="x_panel">*/
/*                                                 <div class="x_title">*/
/*                                                     <center>*/
/*                                                         <h3 style="font-size: 20px;">Find member</h3>*/
/*                                                     <div class="clearfix"></div>*/
/*                                                     <input class='form-control'id='search' placeholder="Search name of doctor" type="text" name="query" />*/
/*                                                     </center>*/
/*                                                 </div>*/
/*                                                 <div class="x_content" style="overflow-y: scroll; height:300px;">*/
/*                                                     <center><img hidden id="loading" src="{{asset('charisma/img/ajax-loaders/ajax-loader-1.gif')}}" ></center>*/
/*                                                     <table class="table table-striped" id="resulttable" class="hidden">*/
/* */
/*                                                         <tbody id="resultbody">*/
/*                                                         </tbody>*/
/*                                                     </table>*/
/* */
/*                                                 </div>*/
/*                                             </div>*/
/*                                             </div>*/
/*                                           */
/*                                                */
/*                                              <div class="modal-footer">*/
/*                                                     <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>*/
/*                                                     */
/*                                                 </div>*/
/*                                             </div>*/
/* */
/*                                         </div>*/
/*                                     </div>*/
/*        <div id="deletegroupmodal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">*/
/*                                     <div class="modal-dialog" >*/
/*                                         <div class="modal-content">*/
/*                                             */
/*                                             <div class="modal-header">*/
/*                                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 16px;"class="modal-title" id="deletegroupmodal">Delete Group</h4>*/
/*                                             </div>*/
/* */
/*                                             <div class="modal-body">*/
/*                                                 <center><h4 style="font-family: Century Gothic; font-size: 16px;">*/
/*                                                         <i class="fa fa-exclamation-triangle" style="color:#ff4d4d; font-size: 30px;"></i>&emsp;Are you sure you want to remove this group?</h4></center>*/
/*                                             </div>*/
/*                                             */
/*                                             <div class="modal-footer">*/
/*                                                     <button type="button" id='con_delete_post' class="btn btn-primary" >Yes</button>*/
/*                                                     <button type="button" class="btn btn-default" data-dismiss="modal">No</button>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*          <div id="viewmembers" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">*/
/*                                     <div class="modal-dialog" >*/
/*                                         <div class="modal-content">*/
/*                                             <div class="modal-header">*/
/*                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 16px;"class="modal-title" id="viewmodal">Remove Members</h4>*/
/*                                             </div>*/
/* */
/*                                             <div class="modal-body">*/
/*                                                <div class="x_panel">*/
/*                                                     <center><h3 style="font-size: 20px;">MEMBERS</h3></center>*/
/*                                                 <div class="x_content" style="overflow-y: scroll; height:400px;">*/
/*                                                    */
/*                                                     <table class="table table-striped" id="membertable" class="hidden">*/
/* */
/*                                                         <tbody id="viewbody">*/
/*                                                             {%for member in entity.doctors%}*/
/*                                                             <tr>*/
/*                                                                 <td>*/
/*                                                                     <center>*/
/*                                                                     <a href="{{path("doctor_show", {'id' : member.id})}}">     {{member.firstName}} {{member.lastName}} </a>*/
/*                                                                     </center>*/
/*                                                                 </td>*/
/*                                                                 <td>*/
/*                                                                     <center>*/
/*                                                                     <button id="deletemember-{{member.id}}" type="button" class="btn btn-danger btn-xs btn-removefromgroup"> <i class="fa fa-close"></i> Remove from Group</button> */
/*                                                                     </center>*/
/*                                                                 </td>*/
/*                                                             </tr>*/
/*                                                             {% endfor %}*/
/*                                                             <div id="noresults"></div>*/
/*                                                         </tbody>*/
/*                                                     </table>*/
/* */
/*                                                 </div>*/
/*                                             </div>*/
/*                                                 */
/*                                                 */
/*                                                 */
/*                                             </div>*/
/* */
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*           </div>*/
/*         */
/*     </div>*/
/* */
/* */
/* <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#newPost')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#newPost').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*     <script>*/
/*                                     var dltPostBtnId;*/
/*                                     $(".btn-delete-post").click(function(x) {*/
/*                                         x.preventDefault();*/
/*                                         dltPostBtnId = this.id;*/
/*                                         dltPostBtnId = dltPostBtnId.replace('dlt_post-', '');*/
/*                                         $('#delete').modal();*/
/*                                     });*/
/*                                     $("#con_delete_post").click(function(d) {*/
/*                                         $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('post_delete') }}',*/
/*                                             data: {id: dltPostBtnId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                location.reload();*/
/*                                                 console.log(response);*/
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*                                     */
/*                                     var dltGroupBtnId;*/
/*                                     $(".btn-delete-group").click(function(x) {*/
/*                                         x.preventDefault();*/
/*                                         dltGroupBtnId = this.id;*/
/*                                         dltGroupBtnId = dltGroupBtnId.replace('deletegroup-', '');*/
/*                                         */
/*                                     });*/
/*                                     $("#con_delete_group").click(function(d) {*/
/*                                         $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{path('delete_group')}}',*/
/*                                             data: {id: dltGroupBtnId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                  window.location.href = response;*/
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     }); */
/*                                     */
/*                                     var inviteBtnId;*/
/*                                     var groupId = $('#groupId').text();*/
/*                                     $(document).on("click", ".btn-invite", function(x){*/
/*                                          x.preventDefault();*/
/*                                         inviteBtnId = this.id;*/
/*                                         inviteBtnId = inviteBtnId.replace('add-', '');*/
/*                                          $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('doctorgroup_invite') }}',*/
/*                                             data: {id: inviteBtnId, groupId : groupId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                location.reload();*/
/*                                                 console.log(response);*/
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*                                     */
/*                                      var memberBtnId;*/
/*                                      var groupId = $('#groupId').text();*/
/*                                     $(".btn-removefromgroup").click(function(x) {*/
/*                                         x.preventDefault();*/
/*                                         memberBtnId = this.id;*/
/*                                         memberBtnId = memberBtnId.replace('deletemember-', '');*/
/*                                         */
/*                                        $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('delete_member') }}',*/
/*                                             data: {id: memberBtnId, groupId : groupId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                 //$('#viewbody').load(document.URL +  ' #viewbody');*/
/*                                                 console.log(response);*/
/*                                                 location.reload();*/
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*                                    */
/*                                    */
/* </script>*/
/*    <script>*/
/* $('#search').keyup(function() {*/
/* */
/*      searchText = $(this).val();*/
/*      groupId = $('#groupId').text();*/
/*      if (this.value != ''){*/
/*          $("#resultbody").empty();*/
/*      }*/
/*      */
/*      */
/*      if (this.value.length >= 3 && this.value != '')*/
/*   {*/
/*      $.ajax({*/
/*         type: "POST",*/
/*         dataType: "json",*/
/*         url: '{{ path('invite_doctor') }}',*/
/*         data: {searchText : searchText, groupId : groupId },*/
/*         beforeSend: function(){*/
/*         $('#loading').show();*/
/*         },*/
/*         complete: function(){*/
/*         $('#loading').hide();*/
/*         },*/
/*         success : function(data) */
/*            */
/*             {if(data){*/
/*                 $("#resultbody").empty();*/
/*                 var len = data.length;*/
/*                 var txt = "";*/
/*                   if(len > 0){*/
/*                     for(var i=0;i<len;i++){*/
/*                             //var button= $('<button type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user"></i> Invite to Group </button>');*/
/*                             var entityId = data[i].id;*/
/*                             var showPath = '{{ path("doctor_show", {'id' : 'entityId' })}}';*/
/*                             showPath = showPath.replace("entityId", data[i].id);*/
/*                             txt += "<tr><td>"+data[i].firstName+" "+data[i].middleName+" "+data[i].lastName +"</td>";*/
/*                             txt+= '<td><a href="#" ><button id="add-'+ data[i].id+'" type="button" class="btn btn-primary btn-xs btn-invite"> <i class="fa fa-envelope"></i> Invite to Group </button>' + '</td><td><a href="'+showPath+'" ><button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user"></i>View Profile</button>' + '</td></tr>';*/
/*                         */
/*                     }*/
/*                     if(txt != ""){*/
/*                         $("#resultbody").append(txt).removeClass("hidden");*/
/*                          */
/*                     }*/
/*                 }*/
/*             }*/
/*         },error: function() {*/
/*          alert('error');*/
/*           }*/
/*         });*/
/*     }*/
/* });*/
/* */
/*        */
/*     </script>*/
/*     {% endblock %}*/
