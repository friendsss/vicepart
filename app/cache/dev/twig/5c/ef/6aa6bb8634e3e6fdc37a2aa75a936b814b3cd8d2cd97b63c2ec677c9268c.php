<?php

/* ResearchProjectMyProjectBundle:Default:index.html.twig */
class __TwigTemplate_5cef6aa6bb8634e3e6fdc37a2aa75a936b814b3cd8d2cd97b63c2ec677c9268c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'home' => array($this, 'block_home'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " FIMADILO - Welcome! ";
    }

    // line 3
    public function block_home($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        echo " Home ";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    <br/>
    
    </br>
    <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
    <!-- Indicators -->
    <ol class=\"carousel-indicators\">
      <li class=\"item1 active\"></li>
      <li class=\"item2\"></li>
      <li class=\"item3\"></li>
      <li class=\"item4\"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class=\"carousel-inner\" role=\"listbox\">

      <div class=\"item active\">
        <a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("doctorhomepage");
        echo "\">
        <img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/carousel/Doctor.png"), "html", null, true);
        echo "\" alt=\"Doctor\">
        <div class=\"carousel-caption\" style=\"color: black;\">
              <br/>
              <br/>
              Find the best doctors in Iloilo.
        </div>
        </a>
      </div>

      <div class=\"item\">
        <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("clinichomepage");
        echo "\">
        <img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/carousel/Clinic.png"), "html", null, true);
        echo "\" alt=\"Clinic\">
        <div class=\"carousel-caption\" style=\"color: black;\">
              <br/>
              <br/>
              Locate the clinics in Iloilo.
        </div>
        </a>
      </div>
    
      <div class=\"item\">
        <a href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("clinichomepage");
        echo "\">
        <img src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/carousel/Illness.png"), "html", null, true);
        echo "\" alt=\"Illness\">
        <div class=\"carousel-caption\" style=\"color: black;\">
              <br/>
              <br/>
              Know more about your illness.
        </div>
        </a>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\">
      <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Previous</span>
    </a>
    <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\">
      <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Next</span>
    </a>
  </div>
        
        <center>
        <a href=\"";
        // line 68
        echo $this->env->getExtension('routing')->getPath("doctorhomepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/homepage/Doctor.png"), "html", null, true);
        echo "\"></a>
        <a href=\"";
        // line 69
        echo $this->env->getExtension('routing')->getPath("clinichomepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/homepage/Clinic.png"), "html", null, true);
        echo "\"></a>
        <a href=\"";
        // line 70
        echo $this->env->getExtension('routing')->getPath("illnesshomepage");
        echo "\"><img  src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/homepage/Illness.png"), "html", null, true);
        echo "\"></a>
        
        </br>
        </br>
        <h3></h3>
        <h3 style=\" font-size: 16px;\"><b><i class=\"fa fa-user-md\"></i>&emsp;DOCTOR</b></h3>
        
        </br>
                       
            ";
        // line 79
        $context["firstname1"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "firstName");
        // line 80
        echo "            ";
        $context["lastname1"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "lastName");
        // line 81
        echo "            ";
        $context["firstname2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "firstName");
        // line 82
        echo "            ";
        $context["lastname2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "lastName");
        // line 83
        echo "            ";
        $context["firstname3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "firstName");
        // line 84
        echo "            ";
        $context["lastname3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "lastName");
        // line 85
        echo "                
            <button type=\"bYutton\" class=\"btn-lg btn-primary\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "fieldOfPractice"), "html", null, true);
        echo "\"><center><a style=\"color:white; font-family: Century Gothic; font-size: 14px;\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "id"))), "html", null, true);
        echo "\"><i style=\"font-size: 25px; color:white\" class = \"fa fa-stethoscope\"></i>
                     &emsp;";
        // line 87
        echo twig_escape_filter($this->env, (isset($context["firstname1"]) ? $context["firstname1"] : $this->getContext($context, "firstname1")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lastname1"]) ? $context["lastname1"] : $this->getContext($context, "lastname1")), "html", null, true);
        echo "</a></center></button>&emsp;
            <button type=\"bYutton\" class=\"btn-lg btn-primary\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "fieldOfPractice"), "html", null, true);
        echo "\"><center><a style=\"color:white; font-family: Century Gothic; font-size: 14px;\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "id"))), "html", null, true);
        echo "\"><i style=\"font-size: 25px; color:white\" class = \"fa fa-stethoscope\"></i>
                     &emsp;";
        // line 89
        echo twig_escape_filter($this->env, (isset($context["firstname2"]) ? $context["firstname2"] : $this->getContext($context, "firstname2")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lastname2"]) ? $context["lastname2"] : $this->getContext($context, "lastname2")), "html", null, true);
        echo "</a></center></button>&emsp; 
            <button type=\"bYutton\" class=\"btn-lg btn-primary\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "fieldOfPractice"), "html", null, true);
        echo "\"><center><a style=\"color:white; font-family: Century Gothic; font-size: 14px;\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "id"))), "html", null, true);
        echo "\"><i style=\"font-size: 25px; color:white\" class = \"fa fa-stethoscope\"></i>
                     &emsp;";
        // line 91
        echo twig_escape_filter($this->env, (isset($context["firstname3"]) ? $context["firstname3"] : $this->getContext($context, "firstname3")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lastname3"]) ? $context["lastname3"] : $this->getContext($context, "lastname3")), "html", null, true);
        echo "</a></center></button>&emsp;
                     
        </br>
        
        <h3></h3>
        <h3 style=\" font-size: 16px;\"><b><i class=\"fa fa-hospital-o\"></i>&emsp;CLINIC</b></h3> 
        
        </br>    
            ";
        // line 99
        $context["clinicname1"] = $this->getAttribute((isset($context["randomcli1"]) ? $context["randomcli1"] : $this->getContext($context, "randomcli1")), "clinicName");
        // line 100
        echo "            ";
        $context["clinicadd1"] = $this->getAttribute((isset($context["randomcli1"]) ? $context["randomcli1"] : $this->getContext($context, "randomcli1")), "clinicAddress");
        // line 101
        echo "            ";
        $context["clinicname2"] = $this->getAttribute((isset($context["randomcli2"]) ? $context["randomcli2"] : $this->getContext($context, "randomcli2")), "clinicName");
        // line 102
        echo "            ";
        $context["clinicadd2"] = $this->getAttribute((isset($context["randomcli2"]) ? $context["randomcli2"] : $this->getContext($context, "randomcli2")), "clinicAddress");
        // line 103
        echo "            ";
        $context["clinicname3"] = $this->getAttribute((isset($context["randomcli3"]) ? $context["randomcli3"] : $this->getContext($context, "randomcli3")), "clinicName");
        // line 104
        echo "            ";
        $context["clinicadd3"] = $this->getAttribute((isset($context["randomcli3"]) ? $context["randomcli3"] : $this->getContext($context, "randomcli3")), "clinicAddress");
        // line 105
        echo "                
             
            <button type=\"button\" class=\"btn-lg btn-round btn-info\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
        // line 107
        echo twig_escape_filter($this->env, (isset($context["clinicadd1"]) ? $context["clinicadd1"] : $this->getContext($context, "clinicadd1")), "html", null, true);
        echo "\"><center><a style=\" font-family: Century Gothic; font-size: 14px; color: white\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomcli1"]) ? $context["randomcli1"] : $this->getContext($context, "randomcli1")), "id"))), "html", null, true);
        echo "\"><i style=\"font-size: 25px; color:white\" class = \"fa fa-map-marker\"></i>
                     &emsp;";
        // line 108
        echo twig_escape_filter($this->env, (isset($context["clinicname1"]) ? $context["clinicname1"] : $this->getContext($context, "clinicname1")), "html", null, true);
        echo "</a></center></button>&emsp;
            <button type=\"button\" class=\"btn-lg btn-round btn-info\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
        // line 109
        echo twig_escape_filter($this->env, (isset($context["clinicadd2"]) ? $context["clinicadd2"] : $this->getContext($context, "clinicadd2")), "html", null, true);
        echo "\"><center><a style=\" font-family: Century Gothic; font-size: 14px; color: white\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomcli2"]) ? $context["randomcli2"] : $this->getContext($context, "randomcli2")), "id"))), "html", null, true);
        echo "\"><i style=\"font-size: 25px; color:white\" class = \"fa fa-map-marker\"></i>
                     &emsp;";
        // line 110
        echo twig_escape_filter($this->env, (isset($context["clinicname2"]) ? $context["clinicname2"] : $this->getContext($context, "clinicname2")), "html", null, true);
        echo "</a></center></button>&emsp;
            <button type=\"button\" class=\"btn-lg btn-round btn-info\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
        // line 111
        echo twig_escape_filter($this->env, (isset($context["clinicadd3"]) ? $context["clinicadd3"] : $this->getContext($context, "clinicadd3")), "html", null, true);
        echo "\"><center><a style=\" font-family: Century Gothic; font-size: 14px; color: white\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomcli3"]) ? $context["randomcli3"] : $this->getContext($context, "randomcli3")), "id"))), "html", null, true);
        echo "\"><i style=\"font-size: 25px; color:white\" class = \"fa fa-map-marker\"></i>
                     &emsp;";
        // line 112
        echo twig_escape_filter($this->env, (isset($context["clinicname3"]) ? $context["clinicname3"] : $this->getContext($context, "clinicname3")), "html", null, true);
        echo "</a></center></button>&emsp;
        
        </br>
        
        <h3></h3>
        <h3 style=\" font-size: 16px;\"><b><i class=\"fa fa-medkit\"></i>&emsp;ILLNESS</b></h3> 
        
        </br>
        ";
        // line 120
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, (isset($context["illnesses"]) ? $context["illnesses"] : $this->getContext($context, "illnesses")), 0, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["illness"]) {
            echo "                
            ";
            // line 121
            $context["illnessname"] = $this->getAttribute((isset($context["illness"]) ? $context["illness"] : $this->getContext($context, "illness")), "illnessName");
            // line 122
            echo "             
            <button type=\"button\" class=\"btn-lg btn-round btn-dark\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["illness"]) ? $context["illness"] : $this->getContext($context, "illness")), "illnessCommonName"), "html", null, true);
            echo "\"><center><a style=\" font-family: Century Gothic; font-size: 14px; color: white\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("illness_show", array("id" => $this->getAttribute((isset($context["illness"]) ? $context["illness"] : $this->getContext($context, "illness")), "id"))), "html", null, true);
            echo "\"><i style=\"font-size: 25px; color:white\" class = \"fa fa-eyedropper\"></i>
                     &emsp;";
            // line 124
            echo twig_escape_filter($this->env, (isset($context["illnessname"]) ? $context["illnessname"] : $this->getContext($context, "illnessname")));
            echo "</a></center></button>&emsp;
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['illness'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "        <br/>
        <h3></h3>
            
        ";
        // line 131
        echo "        <br/>
        </center>
        
        <script>
            \$(document).ready(function(){
            // Activate Carousel
            \$(\"#myCarousel\").carousel();
    
            // Enable Carousel Indicators
            \$(\".item1\").click(function(){
                \$(\"#myCarousel\").carousel(0);
            });
            \$(\".item2\").click(function(){
                \$(\"#myCarousel\").carousel(1);
            });
            \$(\".item3\").click(function(){
                \$(\"#myCarousel\").carousel(2);
            });
    
            // Enable Carousel Controls
            \$(\".left\").click(function(){
                \$(\"#myCarousel\").carousel(\"prev\");
            });
            \$(\".right\").click(function(){
                \$(\"#myCarousel\").carousel(\"next\");
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  311 => 131,  306 => 126,  298 => 124,  292 => 123,  289 => 122,  287 => 121,  281 => 120,  270 => 112,  264 => 111,  260 => 110,  254 => 109,  250 => 108,  244 => 107,  240 => 105,  237 => 104,  234 => 103,  231 => 102,  228 => 101,  225 => 100,  223 => 99,  210 => 91,  204 => 90,  198 => 89,  192 => 88,  186 => 87,  180 => 86,  177 => 85,  174 => 84,  171 => 83,  168 => 82,  165 => 81,  162 => 80,  160 => 79,  146 => 70,  140 => 69,  134 => 68,  108 => 45,  104 => 44,  91 => 34,  87 => 33,  74 => 23,  70 => 22,  52 => 6,  49 => 5,  43 => 4,  37 => 3,  31 => 2,);
    }
}
