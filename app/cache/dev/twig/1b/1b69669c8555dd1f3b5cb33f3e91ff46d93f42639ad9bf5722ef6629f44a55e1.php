<?php

/* ResearchProjectMyProjectBundle:Admin:editIllness.html.twig */
class __TwigTemplate_c82c00324d91c7c044a7d49aac88d9ece7146eab655ffd054ad9fbc122ced0e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:editIllness.html.twig", 1);
        $this->blocks = array(
            'illness' => array($this, 'block_illness'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d09a05828da6095b1fc3a0c325dc754869024d682be17e293bd0e10c7785a43 = $this->env->getExtension("native_profiler");
        $__internal_4d09a05828da6095b1fc3a0c325dc754869024d682be17e293bd0e10c7785a43->enter($__internal_4d09a05828da6095b1fc3a0c325dc754869024d682be17e293bd0e10c7785a43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:editIllness.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4d09a05828da6095b1fc3a0c325dc754869024d682be17e293bd0e10c7785a43->leave($__internal_4d09a05828da6095b1fc3a0c325dc754869024d682be17e293bd0e10c7785a43_prof);

    }

    // line 3
    public function block_illness($context, array $blocks = array())
    {
        $__internal_09a839a84c338fba6181666ad098b45c80a9c3d260963382bae06b7d36b50280 = $this->env->getExtension("native_profiler");
        $__internal_09a839a84c338fba6181666ad098b45c80a9c3d260963382bae06b7d36b50280->enter($__internal_09a839a84c338fba6181666ad098b45c80a9c3d260963382bae06b7d36b50280_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "illness"));

        echo " class=\"active\" ";
        
        $__internal_09a839a84c338fba6181666ad098b45c80a9c3d260963382bae06b7d36b50280->leave($__internal_09a839a84c338fba6181666ad098b45c80a9c3d260963382bae06b7d36b50280_prof);

    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_9ccef8d7515e53f84511ac8bea5f1ecbe3272939b6f7941e3cd3b5b244a812b1 = $this->env->getExtension("native_profiler");
        $__internal_9ccef8d7515e53f84511ac8bea5f1ecbe3272939b6f7941e3cd3b5b244a812b1->enter($__internal_9ccef8d7515e53f84511ac8bea5f1ecbe3272939b6f7941e3cd3b5b244a812b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "ILLNESS";
        
        $__internal_9ccef8d7515e53f84511ac8bea5f1ecbe3272939b6f7941e3cd3b5b244a812b1->leave($__internal_9ccef8d7515e53f84511ac8bea5f1ecbe3272939b6f7941e3cd3b5b244a812b1_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_ee1268e5be452ecbac4fd90d5be16f1aa725716de0dd8465ab7def92ba25394a = $this->env->getExtension("native_profiler");
        $__internal_ee1268e5be452ecbac4fd90d5be16f1aa725716de0dd8465ab7def92ba25394a->enter($__internal_ee1268e5be452ecbac4fd90d5be16f1aa725716de0dd8465ab7def92ba25394a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<center><h1><b>Edit Illness Information</b></h1>
    <div class=\"x_content\">
        <br />
        <div id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 10
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "editIllnessForm")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Illness Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "illnessName", array()), 'widget', array("attr" => array("id" => "illnessName", "name" => "illnessName", "placeholder" => "Illness Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 16
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "illnessName", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Illness Common Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "illnessCommonName", array()), 'widget', array("attr" => array("id" => "illnessCommonName", "name" => "illnessCommonName", "placeholder" => "Illness Common Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 25
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "illnessCommonName", array()), 'errors');
        echo "</div></i>
             <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Define Illness <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "illnessDefinition", array()), 'widget', array("attr" => array("id" => "illnessDefinition", "name" => "illnessDefinition", "placeholder" => "Define Illness", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 34
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "illnessDefinition", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Describe Illness <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "illnessDescription", array()), 'widget', array("attr" => array("id" => "illnessDescription", "name" => "illnessDescription", "placeholder" => "Describe illness", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 43
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "illnessDescription", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Symptoms <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "symptoms", array()), 'widget', array("attr" => array("id" => "symptoms", "name" => "symptoms", "placeholder" => "Describe symptoms", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 52
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "symptoms", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Home Remedies <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "homeRemedies", array()), 'widget', array("attr" => array("id" => "homeRemedies", "name" => "homeRemedies", "placeholder" => "Alternatives found at home / First Aid", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 61
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "homeRemedies", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Commonly Prescribed Drugs <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "commonlyPrescribedDrugs", array()), 'widget', array("attr" => array("id" => "commonlyPrescribedDrugs", "name" => "commonlyPrescribedDrugs", "placeholder" => "Commonly Prescribed Drugs", "class" => "resizable_textarea form-control", "data-validate-length-range" => "3", "required" => "required", "type" => "text")));
        // line 70
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px;\" >";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "commonlyPrescribedDrugs", array()), 'errors');
        echo "</div></i>
            
                
            <div class=\"ln_solid\"></div>
            <div class=\"form group\">
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("illness_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button class=\"btn btn-primary\" onclick=\"goBack()\">Cancel</button>
                </div>
            </div>
            ";
        // line 83
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
            </div>
           
    </div>

    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>

    
    <!-- icheck -->
    <script src=";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "></script>

    <script src=";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "></script>
    <!-- form validation -->
    <script src=";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#editIllnessForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#editIllnessForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script>
        function goBack() {
        window.history.back();
        }
    </script>
";
        
        $__internal_ee1268e5be452ecbac4fd90d5be16f1aa725716de0dd8465ab7def92ba25394a->leave($__internal_ee1268e5be452ecbac4fd90d5be16f1aa725716de0dd8465ab7def92ba25394a_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:editIllness.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 101,  220 => 99,  215 => 97,  198 => 83,  189 => 79,  180 => 73,  175 => 70,  173 => 69,  165 => 64,  160 => 61,  158 => 60,  150 => 55,  145 => 52,  143 => 51,  135 => 46,  130 => 43,  128 => 42,  120 => 37,  115 => 34,  113 => 33,  105 => 28,  100 => 25,  98 => 24,  90 => 19,  85 => 16,  83 => 15,  75 => 10,  69 => 6,  63 => 5,  51 => 4,  39 => 3,  32 => 1,  30 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% form_theme edit_form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {%block illness%} class="active" {% endblock %}*/
/* {%block pagetitle%}ILLNESS{% endblock%}*/
/* {% block body -%}*/
/*     <center><h1><b>Edit Illness Information</b></h1>*/
/*     <div class="x_content">*/
/*         <br />*/
/*         <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">*/
/*             {{ form_start(edit_form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'editIllnessForm'} }) }}*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Illness Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.illnessName, { 'attr' : { 'id': 'illnessName', 'name': 'illnessName', 'placeholder' : 'Illness Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.illnessName)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Illness Common Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.illnessCommonName, { 'attr' : { 'id': 'illnessCommonName', 'name': 'illnessCommonName', 'placeholder' : 'Illness Common Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.illnessCommonName)}}</div></i>*/
/*              <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Define Illness <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.illnessDefinition, { 'attr' : { 'id': 'illnessDefinition', 'name': 'illnessDefinition', 'placeholder' : 'Define Illness', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.illnessDefinition)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Describe Illness <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.illnessDescription, { 'attr' : { 'id': 'illnessDescription', 'name': 'illnessDescription', 'placeholder' : 'Describe illness', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.illnessDescription)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Symptoms <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.symptoms, { 'attr' : { 'id': 'symptoms', 'name': 'symptoms', 'placeholder' : 'Describe symptoms', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.symptoms)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Home Remedies <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.homeRemedies, { 'attr' : { 'id': 'homeRemedies', 'name': 'homeRemedies', 'placeholder' : 'Alternatives found at home / First Aid', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.homeRemedies)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Commonly Prescribed Drugs <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.commonlyPrescribedDrugs, { 'attr' : { 'id': 'commonlyPrescribedDrugs', 'name': 'commonlyPrescribedDrugs', 'placeholder' : 'Commonly Prescribed Drugs', 'class' : 'resizable_textarea form-control', */
/*                                                     'data-validate-length-range' : '3',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px;" >{{form_errors(edit_form.commonlyPrescribedDrugs)}}</div></i>*/
/*             */
/*                 */
/*             <div class="ln_solid"></div>*/
/*             <div class="form group">*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                     <a href="{{ path('illness_show', {'id': entity.id})}}"> {{ form_widget(edit_form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn'} }) }}</a>*/
/*                     <button class="btn btn-primary" onclick="goBack()">Cancel</button>*/
/*                 </div>*/
/*             </div>*/
/*             {{ form_end(edit_form)}}*/
/*             </div>*/
/*            */
/*     </div>*/
/* */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/* */
/*     */
/*     <!-- icheck -->*/
/*     <script src={{asset('gentelella/js/icheck/icheck.min.js')}}></script>*/
/* */
/*     <script src={{asset('gentelella/js/custom.js')}}></script>*/
/*     <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#editIllnessForm')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#editIllnessForm').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
