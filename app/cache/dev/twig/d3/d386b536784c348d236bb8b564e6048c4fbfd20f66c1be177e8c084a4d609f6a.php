<?php

/* ResearchProjectMyProjectBundle:Clinic:show.html.twig */
class __TwigTemplate_ecf9558634b949b264a30257a5bd96148abf4b56e5299a50aaabadffcbb4afb9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Clinic:show.html.twig", 1);
        $this->blocks = array(
            'clinic' => array($this, 'block_clinic'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b634b92d3fcfba95a274abdb52090bff17ab00500d19eba93129b7a5eccc058 = $this->env->getExtension("native_profiler");
        $__internal_3b634b92d3fcfba95a274abdb52090bff17ab00500d19eba93129b7a5eccc058->enter($__internal_3b634b92d3fcfba95a274abdb52090bff17ab00500d19eba93129b7a5eccc058_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Clinic:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3b634b92d3fcfba95a274abdb52090bff17ab00500d19eba93129b7a5eccc058->leave($__internal_3b634b92d3fcfba95a274abdb52090bff17ab00500d19eba93129b7a5eccc058_prof);

    }

    // line 2
    public function block_clinic($context, array $blocks = array())
    {
        $__internal_f8f9873339caa6722671094b3ba7ce2da84dd84b453a5c45b3ec938dbb5731ad = $this->env->getExtension("native_profiler");
        $__internal_f8f9873339caa6722671094b3ba7ce2da84dd84b453a5c45b3ec938dbb5731ad->enter($__internal_f8f9873339caa6722671094b3ba7ce2da84dd84b453a5c45b3ec938dbb5731ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "clinic"));

        echo " class=\"active\" ";
        
        $__internal_f8f9873339caa6722671094b3ba7ce2da84dd84b453a5c45b3ec938dbb5731ad->leave($__internal_f8f9873339caa6722671094b3ba7ce2da84dd84b453a5c45b3ec938dbb5731ad_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c321e3eeee92beb1ac44b4daa159ae0fa8012f64ee478aad3160d3f6b3dfd158 = $this->env->getExtension("native_profiler");
        $__internal_c321e3eeee92beb1ac44b4daa159ae0fa8012f64ee478aad3160d3f6b3dfd158->enter($__internal_c321e3eeee92beb1ac44b4daa159ae0fa8012f64ee478aad3160d3f6b3dfd158_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <meta name=\"viewport\" content=\"initial-scale=1.0\">
    <meta charset=\"utf-8\">
    <title>Simple Map</title>
    <meta name=\"viewport\" content=\"initial-scale=1.0\">
    <meta charset=\"utf-8\">
    <style>
      html, body, #map-canvas  {
  margin: 0;
  padding: 0;
  height: 100%;
}

#map-canvas {
  width:500px;
  height:480px;
}
    </style>
";
        
        $__internal_c321e3eeee92beb1ac44b4daa159ae0fa8012f64ee478aad3160d3f6b3dfd158->leave($__internal_c321e3eeee92beb1ac44b4daa159ae0fa8012f64ee478aad3160d3f6b3dfd158_prof);

    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        $__internal_b804c6d131af4c170394e2aeaf3926168ceaee36cae8ca3d239b1dd0b142b367 = $this->env->getExtension("native_profiler");
        $__internal_b804c6d131af4c170394e2aeaf3926168ceaee36cae8ca3d239b1dd0b142b367->enter($__internal_b804c6d131af4c170394e2aeaf3926168ceaee36cae8ca3d239b1dd0b142b367_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 25
        echo "<div class=\"\">
                    <div class=\"page-title\">
                        <div class=\"title\">
                            <h3>&nbsp;CLINIC</h3>
                        </div>
                    </div>
                    <div class=\"clearfix\"></div>

                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\">
                                   <h3>Clinic Information</h3>
                               
                                <div class=\"x_content\">
                                    <br/>
                                    <br/>
                                    <div class=\"col-md-7 col-sm-7 col-xs-12\">
                                        <div class=\"product-image\">
                                            <div id=\"map\" style=\"width: 475px; height: 500px;\"></div>
                                        </div>
                                            <center><h3>Location of <i>";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicName", array()), "html", null, true);
        echo "</i></h3></center>
                                    </div>
                             
                                    
                                    
                                    <div class=\"col-md-5 col-sm-5 col-xs-12\" style=\"border:0px solid #e5e5e5;\">

                                        <div class=\"\" role=\"tabpanel\" data-example-id=\"togglable-tabs\">
                                        <ul id=\"myTab1\" class=\"nav nav-tabs bar_tabs right\" role=\"tablist\">
                                            <li role=\"presentation\" class=\"active\"><a href=\"#tab_content11\" id=\"home-tabb\" role=\"tab\" data-toggle=\"tab\" aria-controls=\"home\" aria-expanded=\"true\">Profile</a>
                                            </li>
                                            <li role=\"presentation\" class=\"\"><a href=\"#tab_content22\" role=\"tab\" id=\"profile-tabb\" data-toggle=\"tab\" aria-controls=\"profile\" aria-expanded=\"false\">Schedule</a>
                                            </li>
                                        </ul>
                                        <div id=\"myTabContent2\" class=\"tab-content\">
                                            <div role=\"tabpanel\" class=\"tab-pane fade active in\" id=\"tab_content11\" aria-labelledby=\"home-tab\">
                                                <center>
                                                    
                                                <div class=\"profile_img\">
                                                    <!-- end of image cropping -->
                                                <div id=\"crop-avatar\">
                                                    <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    ";
        // line 68
        if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getWebPath", array()) == null)) {
            // line 69
            echo "                                                        <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
            echo "\" alt=\"\">
                                                    ";
        } else {
            // line 71
            echo "                                                    <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getWebPath", array())), "html", null, true);
            echo "\" alt=\"Avatar\">
                                                    ";
        }
        // line 73
        echo "                                                </div>

                                                <!-- Cropping modal -->
                                                <div class=\"modal fade\" id=\"avatar-modal\" aria-hidden=\"true\" aria-labelledby=\"avatar-modal-label\" role=\"dialog\" tabindex=\"-1\">
                                                    <div class=\"modal-dialog modal-lg\">
                                                        <div class=\"modal-content\">
                                                            <form class=\"avatar-form\" action=\"crop.php\" enctype=\"multipart/form-data\" method=\"post\">
                                                                <div class=\"modal-header\">
                                                                    <button class=\"close\" data-dismiss=\"modal\" type=\"button\">&times;</button>
                                                                    <h4 class=\"modal-title\" id=\"avatar-modal-label\">Change Avatar</h4>
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"avatar-body\">

                                                                        <!-- Upload image and data -->
                                                                        <div class=\"avatar-upload\">
                                                                            <input class=\"avatar-src\" name=\"avatar_src\" type=\"hidden\">
                                                                            <input class=\"avatar-data\" name=\"avatar_data\" type=\"hidden\">
                                                                            <label for=\"avatarInput\">Local upload</label>
                                                                            <input class=\"avatar-input\" id=\"avatarInput\" name=\"avatar_file\" type=\"file\">
                                                                        </div>

                                                                        <!-- Crop and preview -->
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-9\">
                                                                                <div class=\"avatar-wrapper\"></div>
                                                                            </div>
                                                                            <div class=\"col-md-3\">
                                                                                <div class=\"avatar-preview preview-lg\"></div>
                                                                                <div class=\"avatar-preview preview-md\"></div>
                                                                                <div class=\"avatar-preview preview-sm\"></div>
                                                                            </div>
                                                                        </div>

                                                                        <div class=\"row avatar-btns\">
                                                                            <div class=\"col-md-9\">
                                                                                <div class=\"btn-group\">
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-90\" type=\"button\" title=\"Rotate -90 degrees\">Rotate Left</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-15\" type=\"button\">-15deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-30\" type=\"button\">-30deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-45\" type=\"button\">-45deg</button>
                                                                                </div>
                                                                                <div class=\"btn-group\">
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"90\" type=\"button\" title=\"Rotate 90 degrees\">Rotate Right</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"15\" type=\"button\">15deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"30\" type=\"button\">30deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"45\" type=\"button\">45deg</button>
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"col-md-3\">
                                                                                <button class=\"btn btn-primary btn-block avatar-save\" type=\"submit\">Done</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class=\"modal-footer\">
                                                                <button class=\"btn btn-default\" data-dismiss=\"modal\" type=\"button\">Close</button>
                                                            </div> -->
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.modal -->

                                                <!-- Loading state -->
                                                <div class=\"loading\" aria-label=\"Loading\" role=\"img\" tabindex=\"-1\"></div>
                                                </div>
                                                <!-- end of image cropping -->
                                        
                                            </div>
                                              ";
        // line 143
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 144
            echo "
                                                <div hidden id='successmessage'>
                                                    ";
            // line 146
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                                                </div>

                                              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 150
        echo "                                          ";
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 151
            echo "                                              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "doctors", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
                // line 152
                echo "                                                 ";
                if (($this->getAttribute($context["doctor"], "id", array()) == $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))) {
                    // line 153
                    echo "                                                    <div id='addressError'>
                                                       .
                                                    </div>
                                                  ";
                }
                // line 156
                echo "  
                                              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 157
            echo "  
                                        ";
        }
        // line 159
        echo "                                            <center>
                                            <h3 style=\"font-size: 20px\"><b>";
        // line 160
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicName", array()), "html", null, true);
        echo "</b></h3>
                                            
                                            ";
        // line 162
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctors", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
            // line 163
            echo "                                                    <a style=\"font-family: Century Gothic; color:#379DFF; font-size: 15px\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
            echo "\">
                                                        <i class=\"fa fa-user\"></i>&emsp;";
            // line 164
            echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "middleName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "lastName", array()), "html", null, true);
            echo "</a>
                                                    <br/>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 167
        echo "                                            
                                            <h4><div style=\"font-family: Century Gothic; font-size: 15px \" id=\"address\"><i class=\"fa fa-map-marker\"></i>&emsp;";
        // line 168
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicAddress", array()), "html", null, true);
        echo "</div></h4>
                                            
                                            <div style=\"font-family: Century Gothic; font-size: 15px\"><i class=\"fa fa-pencil-square-o\"></i>&emsp;";
        // line 170
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicDescription", array()), "html", null, true);
        echo "</div>
                                            <div style=\"font-family: Century Gothic; font-size: 15px\"><i class=\"fa fa-phone-square-o\"></i>&emsp;";
        // line 171
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "contactNumber", array()), "html", null, true);
        echo "</div>
                                            <div id=\"latitude\" hidden></div>
                                            <div id=\"longitude\" hidden></div>
                                            <br/>
                                        
                                            <div class=\"\">
                                                ";
        // line 177
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 178
            echo "                                                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
            echo "\"><button class=\"btn btn-primary\"><i class=\"glyphicon glyphicon-edit\"></i> Edit Information</button></a>
                                                ";
        }
        // line 180
        echo "                                                <button id =\"amodal\" type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#appointmentmodal\" data-backdrop=\"static\" data-keyboard=\"false\">
                                                    <i class=\"glyphicon glyphicon-calendar\"></i> Set an Appointment</button>
                                            </div>
                                          
                                            </center>
                                        </div>
                                        <div role=\"tabpanel\" class=\"tab-pane fade\" id=\"tab_content22\" aria-labelledby=\"profile-tab\">
                                            <div>
                                            <ul class=\"list-inline prod_color\"> 
                                                <center>
                                                <li><h3><i class=\"fa fa-calendar\"></i><b>&emsp;CLINIC SCHEDULE</b></h3></li>
                                                <div style=\"overflow-y: scroll; height:450px;\">
                                                    <li>
                                                        <center>
                                                            <h3>Sunday</h3>
                                                            ";
        // line 195
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunCloseTime", array()), "H:i") == "00:00"))) {
            // line 196
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 198
            echo "                                                                <div class=\"color bg-green\"></div>
                                                                <h3><i><b>";
            // line 199
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 201
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Monday</h3>
                                                            ";
        // line 206
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monCloseTime", array()), "H:i") == "00:00"))) {
            // line 207
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 209
            echo "                                                                <div class=\"color bg-green\"></div>
                                                                <h3><i><b>";
            // line 210
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 212
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Tuesday</h3>
                                                            ";
        // line 217
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueCloseTime", array()), "H:i") == "00:00"))) {
            // line 218
            echo "                                                               <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 220
            echo "                                                                <div class=\"color bg-green\"></div>
                                                                <h3><i><b>";
            // line 221
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 223
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Wednesday</h3> 
                                                            ";
        // line 228
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedCloseTime", array()), "H:i") == "00:00"))) {
            // line 229
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 231
            echo "                                                                <div class=\"color bg-green\"></div>
                                                                <h3><i><b>";
            // line 232
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 234
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Thursday </h3>
                                                            ";
        // line 239
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuCloseTime", array()), "H:i") == "00:00"))) {
            // line 240
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 242
            echo "                                                                <div class=\"color bg-green\"></div>
                                                                <h3><i><b>";
            // line 243
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 245
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Friday </h3>
                                                            ";
        // line 250
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friCloseTime", array()), "H:i") == "00:00"))) {
            // line 251
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 253
            echo "                                                                <div class=\"color bg-green\"></div>
                                                                <h3><i><b>";
            // line 254
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 255
        echo "</h4>
                                                        <center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Saturday </h3> 
                                                            ";
        // line 261
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satCloseTime", array()), "H:i") == "00:00"))) {
            // line 262
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 264
            echo "                                                                <div class=\"color bg-green\"></div>
                                                                <h3><i><b>";
            // line 265
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 266
        echo "</h4>
                                                        </center>
                                                    </li>
                                                    <div class=\"\">
                                                        <button id =\"amodal\" type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#appointmentmodal\" data-backdrop=\"static\" data-keyboard=\"false\">
                                                            <i class=\"glyphicon glyphicon-calendar\"></i> Set an Appointment</button>
                                                    </div>
                                                    </center>
                                                </ul>
                                               </div>
                                            </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    
               <div id=\"appointmentmodal\" class=\"modal fade bs-example-modal-lg\" align=\"left\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" data-backdrop=\"static\" >
                                    <div class=\"modal-dialog modal-lg\" role=\"document\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                                </button>
                                                <h4 class=\"modal-title\" style=\"font-family: Century Gothic;\" id=\"myModalLabel\">Set an Appointment for <i>";
        // line 294
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicName", array()), "html", null, true);
        echo "</i></h4>
                                            </div>
                                            <div class=\"modal-body\">
                                                <!-- Smart Wizard -->
                                                <div id=\"wizard\" class=\"form_wizard wizard_horizontal\">
                                                <ul class=\"wizard_steps\">
                                                    <li>
                                                        <a href=\"#step-1\">
                                                            <span class=\"step_no\">1</span>
                                                            <span class=\"step_descr\">
                                                                1<br/>
                                                                <small style=\"font-family: Century Gothic; font-size: 14px;\">Fill Up Information</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"#step-2\">
                                                            <span class=\"step_no\">2</span>
                                                            <span class=\"step_descr\">
                                                                2<br />
                                                                <small style=\"font-family: Century Gothic; font-size: 14px;\">Clinic Schedule</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"#step-3\">
                                                            <span class=\"step_no\">3</span>
                                                            <span class=\"step_descr\">
                                                                3<br />
                                                                <small style=\"font-family: Century Gothic; font-size: 14px;\">Confirm Appointment</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"#step-4\">
                                                            <span class=\"step_no\">4</span>
                                                            <span class=\"step_descr\">
                                                                4<br />
                                                                <small style=\"font-family: Century Gothic; font-size: 14px;\">Success!</small>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                        
                                           ";
        // line 338
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "appointmentForm", "class" => "form-horizontal form-label-left")));
        echo "
                                            
                                        <div id=\"step-1\">
                                            <h3> </h3>
                                            <center><h3 style=\"font-size: 20px;\"class=\"StepTitle\"><b>FILL UP INFORMATION</b></h3></center>
                                                <div id='fn' class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >First Name <span class=\"required\">*</span>
                                                    </label>
                                                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                        ";
        // line 347
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "firstName", array()), 'widget', array("attr" => array("id" => "firstName", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 348
        echo "
                                                    </div>
                                                </div>
                                                <div id='ln' class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Last Name <span class=\"required\">*</span>
                                                    </label>
                                                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                        ";
        // line 355
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "lastName", array()), 'widget', array("attr" => array("id" => "lastName", "name" => "lastName", "placeholder" => "Last Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 356
        echo "
                                                    </div>
                                                </div>
                                                <div id='ea' class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Email Address <span class=\"required\">*</span>
                                                    </label>
                                                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                        ";
        // line 363
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "emailAddress", array()), 'widget', array("attr" => array("id" => "emailAddress", "name" => "emailAddress", "placeholder" => "Email Address", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "10", "required" => "required", "type" => "email")));
        // line 364
        echo "
                                                    </div>
                                                </div>
                                                <div id='cn' class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Contact Number  <span class=\"required\">*</span>
                                                    </label>
                                                    <div class=\"col-md-6 col-sm-6 col-xsw-12\">
                                                        ";
        // line 371
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "contactNumber", array()), 'widget', array("attr" => array("id" => "contactNumber", "name" => "contactNumber", "placeholder" => "Contact Number", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "7, 11", "required" => "required", "type" => "number")));
        // line 372
        echo "
                                                    </div>
                                                </div>
                                                <div id='n' class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Note 
                                                    </label>
                                                    <div class=\" col-sm-6 col-xs-12\">
                                                       ";
        // line 379
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "note", array()), 'widget', array("attr" => array("id" => "note", "name" => "note", "placeholder" => "Write a note", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 380
        echo "
                                                    
                                                    </div>
                                                </div>
                                        </div>
                                        <div id=\"step-2\">
                                            <h3> </h3>
                                            <center><h3 style=\"font-size: 20px;\"class=\"StepTitle\"><b>CLINIC SCHEDULE</b></h3>
                                                    <div class=\"form-group\">
                                                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Pick a Schedule  <span class=\"required\">*</span>
                                                        </label>
                                                        <div id='da' class=\" col-sm-6 col-xs-12\">
                                                               ";
        // line 392
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "dateAppointed", array()), 'widget', array("attr" => array("id" => "dateOfAppointment", "name" => "dateOfAppointment", "placeholder" => "View Calendar", "class" => "form-control has-feedback-left", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 393
        echo "
                                                        </div>
                                                    </div>
                                            <div class=\"item form-group\">
                                                 <div id=\"clinicsched\" class=\" col-sm-12 col-xs-12\"></div>
                                            </div>
                                            </center>
                                        </div>
                                        <div id=\"step-3\">
                                            <h3> </h3>
                                            <center><h3 style=\"font-size: 20px;\"class=\"StepTitle\"><b>CONFIRM APPOINTMENT</b></h3></center>
                                            <div style=\"overflow-y: scroll; height:250px;\">
                                                <center>
                                                <h4 style=\"font-family: Century Gothic; font-size: 13px;\"><i class=\"fa fa-pencil\"></i> <u>First Name </u></h4>
                                                <h4 class=\"color bg-success\" style=\"font-family: Century Gothic; font-size: 16px;\"><b><div id='firstNameConfirm'></div></b></h4>
                                                <h4 style=\"font-family: Century Gothic; font-size: 13px;\"><i class=\"fa fa-pencil\"></i> <u>Last Name </u></h4>
                                                <h4 class=\"color bg-success\" style=\"font-family: Century Gothic; font-size: 16px;\"><b><div id='lastNameConfirm'></div></b></h4>
                                                <h4 style=\"font-family: Century Gothic; font-size: 13px;\"><i class=\"fa fa-envelope\"></i> <u>Email Address </u></h4>
                                                <h4 class=\"color bg-success\" style=\"font-family: Century Gothic; font-size: 16px;\"><b><div id='emailAddressConfirm'></div></b></h4>
                                                <h4 style=\"font-family: Century Gothic; font-size: 13px;\"><i class=\"fa fa-phone\"></i> <u>Contact Number </u></h4>
                                                <h4 class=\"color bg-success\" style=\"font-family: Century Gothic; font-size: 16px;\"><b><div id='contactNumberConfirm'></div></b></h4>
                                                <h4 style=\"font-family: Century Gothic; font-size: 13px;\"><i class=\"fa fa-calendar\"></i> <u>Appointment Date</u></i></h4>
                                                <h4 class=\"color bg-success\" style=\"font-family: Century Gothic; font-size: 16px;\"><b><div id='appointmentDateConfirm'></div></b></h4>
                                                <h4 style=\"font-family: Century Gothic; font-size: 13px;\"><i class=\"fa fa-qrcode\"></i> <u>Appointment Code</u></h4>
                                                <h4 class=\"color bg-success\" style=\"font-family: Century Gothic; font-size: 18px;\"><b><div id='appointmentCodeConfirm'>";
        // line 417
        echo twig_escape_filter($this->env, (isset($context["appointmentCode"]) ? $context["appointmentCode"] : $this->getContext($context, "appointmentCode")), "html", null, true);
        echo "</div></b></h4>
                                                <h4 style=\"font-family: Century Gothic; font-size: 12px;\"><i>(to be presented on appointment date)</i></h4>
                                                <h4 style=\"font-family: Century Gothic; font-size: 13px;\"><i class=\"fa fa-circle\"></i> <u>Note</u></h4>
                                                <h4 class=\"color bg-success\" style=\"font-family: Century Gothic; font-size: 16px;\"><b><div id='noteConfirm'></div></b></h4>
                                                <br/>
                                                </center>
                                            </div>
                                            
                                        </div>
                                        <div id=\"step-4\">
                                            <h3> </h3>
                                            <center><h3 style=\"font-size: 20px;\"class=\"StepTitle\"><b>FINISH APPOINTMENT</b></h3></center>
                                            <div class=\"jumbotron\">
                                              <a href=\"\" hidden>";
        // line 430
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "Save", array()), 'widget', array("attr" => array("class" => "btn btn-info modal-submit", "id" => "submitbtn")));
        echo "</a>
                                              <center>
                                              <h1 style=\"font-size: 50px;\"><b><i>Appointment Created</i></b></h1>
                                              <h3 style=\"font-size: 18px;\">Click finish to save appointment.</h3>
                                              </center>
                                            </div>
                                            
                                         ";
        // line 437
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), 'form_end');
        echo "
                                        
                                    </div>
                                    
                                    <!-- End SmartWizard Content -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
               </div>    
                    <div id=\"schedulemodal\" class=\"modal fade bs-example-modal-lg\" align=\"left\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" data-backdrop=\"static\" >
                        <div class=\"modal-dialog modal-lg\">
                            <div class=\"modal-content\">
                                <div class=\"modal-header\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                        </button>
                                    <h4 class=\"modal-title\" style=\"font-family: Century Gothic;\" id=\"myModalLabel\">Clinic Schedule</h4>
                                </div>
                                <div class=\"modal-body\">
                                    k
                                    <div id=\"results\"></div>
                                </div>
                            </div>
                        </div>
                   </div>
                    
                    ";
        // line 464
        echo "                    <div id=\"appointmentsched\" hidden>
                        
                        <div id=\"sundaysched\">
                            ";
        // line 467
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunCloseTime", array()), "H:i") == "00:00"))) {
            // line 468
            echo "                                <div class=\"color bg-red\"> <div id='sundaystatus'>CLOSED</div></div>
                            ";
        } else {
            // line 470
            echo "                                <div class=\"color bg-green\"><div id='sundaystatus'>OPEN</div>
                                   <i><b>";
            // line 471
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i>
                                </div>
                            ";
        }
        // line 473
        echo "                                 
                        </div>
                        <div id=\"mondaysched\">
                            ";
        // line 476
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monCloseTime", array()), "H:i") == "00:00"))) {
            // line 477
            echo "                                <div class=\"color bg-red\"> <div id='mondaystatus'>CLOSED</div></div>
                            ";
        } else {
            // line 479
            echo "                                <div class=\"color bg-green\"><div id='mondaystatus'>OPEN</div>
                                   <i><b>";
            // line 480
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i>
                                </div>
                            ";
        }
        // line 482
        echo "                                 
                        </div>
                        <div id=\"tuesdaysched\">
                            ";
        // line 485
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueCloseTime", array()), "H:i") == "00:00"))) {
            // line 486
            echo "                                <div class=\"color bg-red\"> <div id='tuesdaystatus'>CLOSED</div></div>
                            ";
        } else {
            // line 488
            echo "                                <div class=\"color bg-green\"><div id='tuesdaystatus'>OPEN</div>
                                   <i><b>";
            // line 489
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i>
                                </div>
                            ";
        }
        // line 491
        echo "                                 
                        </div>
                        <div id=\"wednesdaysched\">
                            ";
        // line 494
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedCloseTime", array()), "H:i") == "00:00"))) {
            // line 495
            echo "                                <div class=\"color bg-red\"><div id='wednesdaystatus'>CLOSED</div></div>
                            ";
        } else {
            // line 497
            echo "                                <div class=\"color bg-green\"><div id='wednesdaystatus'>OPEN</div>
                                   <i><b>";
            // line 498
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i>
                                </div>
                            ";
        }
        // line 500
        echo "                                 
                        </div>
                        <div id=\"thursdaysched\">
                            ";
        // line 503
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuCloseTime", array()), "H:i") == "00:00"))) {
            // line 504
            echo "                                <div class=\"color bg-red\"> <div id='thursdaystatus'>CLOSED</div></div>
                            ";
        } else {
            // line 506
            echo "                                <div class=\"color bg-green\"><div id='thursdaystatus'>OPEN</div>
                                   <i><b>";
            // line 507
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i>
                                </div>
                            ";
        }
        // line 509
        echo "                                 
                        </div>
                        <div id=\"fridaysched\">
                            ";
        // line 512
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friCloseTime", array()), "H:i") == "00:00"))) {
            // line 513
            echo "                                <div class=\"color bg-red\"><div id='fridaystatus'>CLOSED</div></div>
                            ";
        } else {
            // line 515
            echo "                                <div class=\"color bg-green\"><div id='fridaystatus'>OPEN</div>
                                   <i><b>";
            // line 516
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i>
                                </div>
                            ";
        }
        // line 518
        echo "                                 
                        </div>
                        <div id=\"saturdaysched\">
                            ";
        // line 521
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satOpenTime", array()), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satCloseTime", array()), "H:i") == "00:00"))) {
            // line 522
            echo "                                  <div class=\"color bg-red\"> <div id='saturdaystatus'>CLOSED</div></div>
                            ";
        } else {
            // line 524
            echo "                                <div class=\"color bg-green\"><div id='saturdaystatus'>OPEN</div>
                                   <i><b>";
            // line 525
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satOpenTime", array()), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satCloseTime", array()), "H:i"), "html", null, true);
            echo "</b></i>
                                </div>
                            ";
        }
        // line 527
        echo "                                 
                        </div>
                    </div>
        
                    
               <div hidden id=\"directionsPanel\">
  Enter a starting point and click \"Calculate route\".
</div>     
        

    <script type=\"text/javascript\" src=\"";
        // line 537
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/external/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
     <script type=\"text/javascript\" src=\"";
        // line 538
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 539
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("notify/notify.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 540
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/parsley/parsley.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 541
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/moment.min2.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 542
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/datepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 543
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/jquery.geocomplete.js"), "html", null, true);
        echo "\"></script>
     <script async defer src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDRJhfHphrYhN10mIoK_5CtgJQ0M9qbO14&callback=initMap&libraries=places&sensor=false\"></script> 
      <script src=";
        // line 545
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
        
<script type=\"text/javascript\">
        \$(document).ready(function () {
            \$('#form_dateAppointed').datepicker({
                minDate: Date.now(),
                onSelect: function(dateText, evnt) {
                    
                    onSelectDate(dateText);
                   

                }
            });
        });
        
        function onSelectDate(dateText){ 
         var dateee = new Date(dateText);
         var weekday = new Array(7);
            weekday[0]=  \"Sunday\";
            weekday[1] = \"Monday\";
            weekday[2] = \"Tuesday\";
            weekday[3] = \"Wednesday\";
            weekday[4] = \"Thursday\";
            weekday[5] = \"Friday\";
            weekday[6] = \"Saturday\";
            
            var s = \$('#sundaysched').html();
            var m = \$('#mondaysched').html();
            var t = \$('#tuesdaysched').html();
            var w = \$('#wednesdaysched').html();
            var th = \$('#thursdaysched').html();
            var f = \$('#fridaysched').html();
            var sa = \$('#saturdaysched').html();
          
            var daydate = weekday[dateee.getDay()];
        
        
            if(daydate === 'Sunday'){
                 \$('#clinicsched').html(s);
            }
            if(daydate === 'Monday'){
                 \$('#clinicsched').html(m);
            }
            if(daydate === 'Tuesday'){
                 \$('#clinicsched').html(t);
            }
            if(daydate === 'Wednesday'){
                 \$('#clinicsched').html(w);
            }
            if(daydate === 'Thursday'){
                 \$('#clinicsched').html(th);
            }
            if(daydate === 'Friday'){
                 \$('#clinicsched').html(f);
            }
            if(daydate === 'Saturday'){
                 \$('#clinicsched').html(sa);
            }
            
        
        
        
        
       
        
        
        
    }
    </script>     
<script>
    
function initMap() {
   
  var long = ";
        // line 618
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicLong", array()), "html", null, true);
        echo ";
  var lat = ";
        // line 619
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicLat", array()), "html", null, true);
        echo ";
  var currloc;
  var latLng = new google.maps.LatLng( ";
        // line 621
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicLong", array()), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicLat", array()), "html", null, true);
        echo ");
  var myLatLng = {lat: lat, lng: long};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: myLatLng
  });
 
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer({
        map: map
      });
      
    
    
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      currloc = position.coords.latitude + \",  \" + position.coords.longitude;
      calcRoute(currloc, myLatLng);
      var marker2 = new google.maps.Marker({
        position: pos,
        map: map,

      });
      infoWindow.setPosition(pos);
      infoWindow.setContent('Location found.');
      map.setCenter(pos);
      
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
  
  
  
  
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\\'t support geolocation.');
}


function calcRoute(start, end) {
    
      
      var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
      };
      directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(result);
        }
      });
    };


    </script>
 
<script>
    function isFutureDate(idate){
    var today = new Date().getTime(),
        idate = idate.split(\"/\");

    idate = new Date(idate[2], idate[1] - 1, idate[0]).getTime();
    return (today - idate) < 0 ? true : false;
}

function checkDate(){
    var idate = document.getElementById(\"form_dateAppointed\"),
        dateReg = /(0[1-9]|[12][0-9]|3[01])[\\/](0[1-9]|1[012])[\\/]201[4-9]|20[2-9][0-9]/;

    if(!dateReg.test(idate.value)){
       \$(\"#form_dateAppointed\").notify(\"Invalid Date\",  
  { position: \"right\", autohide: \"false\" });
    }
}

\$('#appointmentmodal').on('hidden.bs.modal', function (e) {
  \$(this)
    .find(\"input,textarea,select\")
       .val('')
       .end()
    .find(\"input[type=checkbox], input[type=radio]\")
       .prop(\"checked\", \"\")
       .end();
});


\$('#amodal').on('click', function()
    {
        \$('#appointmentForm').find('input:text, input:password, select, textarea').val('');
        \$('#appointmentForm').find('input:radio, input:checkbox').prop('checked', false);
        \$('#form_dateAppointed').val('');
        
});





</script>
  <script type=\"text/javascript\" src=\"";
        // line 734
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/wizard/jquery.smartWizard.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\">
\$(document).ready(function(){
    \$.notify.addStyle('formerror', {
                html: \"<div><span data-notify-text/></div>\",
                classes: {
                  base: {
                    \"color\": \"white\",
                    \"white-space\": \"nowrap\",
                    \"background-color\": \"#ce5454\",
                     \"padding\": \"3px 10px\",
                    \"border-radius\" : \"3px 4px 4px 3px\",
                    
                    \"border\": \"2px solid #ce5454\",
                    
                    
                  }
                }
              });

    // Smart Wizard         
    \$('#wizard').smartWizard({
        onLeaveStep:leaveAStepCallback,
        onFinish:onFinishCallback
    });

    function leaveAStepCallback(obj, context){


        if(context.fromStep === 2){
           fillInfo();
        }


        return validateSteps(context.fromStep); // return false to stay on step and true to continue navigation 
    }

    function onFinishCallback(objs, context){
        if(validateAllSteps()){
            \$('form').submit();
            
        }
    }

    // Your Step validation logic
    function validateSteps(stepnumber){
        var isStepValid = true;
        // validate step 1
        if(stepnumber == 1){
            if(\$('#form_firstName').val() == '' ){
                isStepValid = false;
                \$(\"#form_firstName\").notify(\"Please fill out this field\",  
                   { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            
            }else{
                 \$('#fn .notifyjs-wrapper').trigger('notify-hide');
            }
            if(\$('#form_lastName').val() == '' ){
                isStepValid = false;
                \$(\"#form_lastName\").notify(\"Please fill out this field\",  
                   { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            
            }else{
                 \$('#ln .notifyjs-wrapper').trigger('notify-hide');
            }
            if(\$('#form_contactNumber').val() == '' ){
                isStepValid = false;
                \$(\"#form_contactNumber\").notify(\"Please fill out this field\",  
                   { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            
            }else{
                 \$('#cn .notifyjs-wrapper').trigger('notify-hide');
            }
            if(\$('#form_emailAddress').val() == '' ){
                isStepValid = false;
                \$(\"#form_emailAddress\").notify(\"Please fill out this field\",  
                   { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            
            }else{
                 \$('#ea .notifyjs-wrapper').trigger('notify-hide');
            }
            if(\$('#form_note').val() == '' ){
                isStepValid = false;
                \$(\"#form_note\").notify(\"Please fill out this field\",  
                   { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            
            }else{
                 \$('#n .notifyjs-wrapper').trigger('notify-hide');
            }
            var re = /^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))\$/;
            if(re.test(\$('#form_emailAddress').val()) == false ){
                isStepValid = false;
                
                \$(\"#form_emailAddress\").notify(\"Invalid email\",  
                   { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            
            }else{
                 \$('#ea .notifyjs-wrapper').trigger('notify-hide');
            }
            var cn = /^(\\d{7}|\\d{10}|09\\d{9})\$/;
            if(cn.test(\$('#form_contactNumber').val()) == false ){
                isStepValid = false;
                
                \$(\"#form_contactNumber\").notify(\"Invalid contact\",  
                   { position: \"right\", autohide: \"false\", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });
            
            }else{
                 \$('#cn .notifyjs-wrapper').trigger('notify-hide');
            }
            
        }
        if(stepnumber == 2){
            if(\$('#form_dateAppointed').val() == ''){
                \$(\"#form_dateAppointed\").notify(\"Plese pick a date\",  
                  { position: \"right\", autohide: \"false\" });
                  isStepValid = false;
                 }
                var idate = document.getElementById(\"form_dateAppointed\"),
                dateReg = /^\\d{1,2}\\/\\d{1,2}\\/\\d{4}\$/;

        
             if(!dateReg.test(idate.value)){
               isStepValid = false;
                }

                var now = new Date();
                if (Date.parse(idate.value) < now) {
                  // selected date is in the past
                  isStepValid = false;
                }


              
                            
                          
            var dateee = new Date( \$('#form_dateAppointed').val());
            var weekday = new Array(7);
            weekday[0]=  \"Sunday\";
            weekday[1] = \"Monday\";
            weekday[2] = \"Tuesday\";
            weekday[3] = \"Wednesday\";
            weekday[4] = \"Thursday\";
            weekday[5] = \"Friday\";
            weekday[6] = \"Saturday\";

            var daydate = weekday[dateee.getDay()];
            
            if(daydate === 'Sunday'){
                if(\$('#sundaystatus').text() === 'CLOSED'){
                   isStepValid = false;
                }
              } if(daydate === 'Monday'){
                  if(\$('#mondaystatus').text() === 'CLOSED'){
                   isStepValid = false;
                }
              }if(daydate === 'Tuesday'){
                  if(\$('#tuesdaystatus').text() === 'CLOSED'){
                   isStepValid = false;
                }
              }if(daydate === 'Wednesday'){
                  if(\$('#wednesdaystatus').text() === 'CLOSED'){
                   isStepValid = false;
                }
              }if(daydate === 'Thursday'){
                  if(\$('#thursdaystatus').text() === 'CLOSED'){
                   isStepValid = false;
                }
              }if(daydate === 'Friday'){
                  if(\$('#fridaystatus').text() === 'CLOSED'){
                   isStepValid = false;
                }
              }if(daydate === 'Saturday'){
                  if(\$('#saturdaystatus').text() === 'CLOSED'){
                   isStepValid = false;
                }
              }
            
              
        }
        // ... 
        return isStepValid;
    }
    function validateAllSteps(){
        var isStepValid = true;
        // all step validation logic     
        return isStepValid;
    }
    
    function fillInfo(){
         \$('#firstNameConfirm').html(\$(\"#form_firstName\").val());
         \$('#lastNameConfirm').html(\$(\"#form_lastName\").val());
         \$('#contactNumberConfirm').html(\$(\"#form_contactNumber\").val());
         \$('#emailAddressConfirm').html(\$(\"#form_emailAddress\").val());
         \$('#noteConfirm').html(\$(\"#form_note\").val());
         \$('#appointmentDateConfirm').html(\$(\"#form_dateAppointed\").val());
        
    }
    
    
    
});
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){
    
    if (\$('#successmessage').length){
        
            new PNotify({
                                title: 'Success!',
                                text: 'You have successfully created an appointment!',
                                type: 'success'
                            });
    }
    
    
    
});
</script>

   
";
        
        $__internal_b804c6d131af4c170394e2aeaf3926168ceaee36cae8ca3d239b1dd0b142b367->leave($__internal_b804c6d131af4c170394e2aeaf3926168ceaee36cae8ca3d239b1dd0b142b367_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Clinic:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1125 => 734,  1007 => 621,  1002 => 619,  998 => 618,  922 => 545,  917 => 543,  913 => 542,  909 => 541,  905 => 540,  901 => 539,  897 => 538,  893 => 537,  881 => 527,  873 => 525,  870 => 524,  866 => 522,  864 => 521,  859 => 518,  851 => 516,  848 => 515,  844 => 513,  842 => 512,  837 => 509,  829 => 507,  826 => 506,  822 => 504,  820 => 503,  815 => 500,  807 => 498,  804 => 497,  800 => 495,  798 => 494,  793 => 491,  785 => 489,  782 => 488,  778 => 486,  776 => 485,  771 => 482,  763 => 480,  760 => 479,  756 => 477,  754 => 476,  749 => 473,  741 => 471,  738 => 470,  734 => 468,  732 => 467,  727 => 464,  698 => 437,  688 => 430,  672 => 417,  646 => 393,  644 => 392,  630 => 380,  628 => 379,  619 => 372,  617 => 371,  608 => 364,  606 => 363,  597 => 356,  595 => 355,  586 => 348,  584 => 347,  572 => 338,  525 => 294,  495 => 266,  488 => 265,  485 => 264,  481 => 262,  479 => 261,  471 => 255,  464 => 254,  461 => 253,  457 => 251,  455 => 250,  448 => 245,  441 => 243,  438 => 242,  434 => 240,  432 => 239,  425 => 234,  418 => 232,  415 => 231,  411 => 229,  409 => 228,  402 => 223,  395 => 221,  392 => 220,  388 => 218,  386 => 217,  379 => 212,  372 => 210,  369 => 209,  365 => 207,  363 => 206,  356 => 201,  349 => 199,  346 => 198,  342 => 196,  340 => 195,  323 => 180,  317 => 178,  315 => 177,  306 => 171,  302 => 170,  297 => 168,  294 => 167,  281 => 164,  276 => 163,  272 => 162,  267 => 160,  264 => 159,  260 => 157,  253 => 156,  247 => 153,  244 => 152,  239 => 151,  236 => 150,  226 => 146,  222 => 144,  218 => 143,  146 => 73,  140 => 71,  134 => 69,  132 => 68,  106 => 45,  84 => 25,  78 => 23,  54 => 4,  48 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {%block clinic%} class="active" {% endblock %}*/
/* {% block head %}*/
/*     <meta name="viewport" content="initial-scale=1.0">*/
/*     <meta charset="utf-8">*/
/*     <title>Simple Map</title>*/
/*     <meta name="viewport" content="initial-scale=1.0">*/
/*     <meta charset="utf-8">*/
/*     <style>*/
/*       html, body, #map-canvas  {*/
/*   margin: 0;*/
/*   padding: 0;*/
/*   height: 100%;*/
/* }*/
/* */
/* #map-canvas {*/
/*   width:500px;*/
/*   height:480px;*/
/* }*/
/*     </style>*/
/* {% endblock %}*/
/* */
/* {% block body -%}*/
/* */
/*                 <div class="">*/
/*                     <div class="page-title">*/
/*                         <div class="title">*/
/*                             <h3>&nbsp;CLINIC</h3>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="clearfix"></div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                             <div class="x_panel">*/
/*                                    <h3>Clinic Information</h3>*/
/*                                */
/*                                 <div class="x_content">*/
/*                                     <br/>*/
/*                                     <br/>*/
/*                                     <div class="col-md-7 col-sm-7 col-xs-12">*/
/*                                         <div class="product-image">*/
/*                                             <div id="map" style="width: 475px; height: 500px;"></div>*/
/*                                         </div>*/
/*                                             <center><h3>Location of <i>{{entity.clinicName}}</i></h3></center>*/
/*                                     </div>*/
/*                              */
/*                                     */
/*                                     */
/*                                     <div class="col-md-5 col-sm-5 col-xs-12" style="border:0px solid #e5e5e5;">*/
/* */
/*                                         <div class="" role="tabpanel" data-example-id="togglable-tabs">*/
/*                                         <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">*/
/*                                             <li role="presentation" class="active"><a href="#tab_content11" id="home-tabb" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Profile</a>*/
/*                                             </li>*/
/*                                             <li role="presentation" class=""><a href="#tab_content22" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false">Schedule</a>*/
/*                                             </li>*/
/*                                         </ul>*/
/*                                         <div id="myTabContent2" class="tab-content">*/
/*                                             <div role="tabpanel" class="tab-pane fade active in" id="tab_content11" aria-labelledby="home-tab">*/
/*                                                 <center>*/
/*                                                     */
/*                                                 <div class="profile_img">*/
/*                                                     <!-- end of image cropping -->*/
/*                                                 <div id="crop-avatar">*/
/*                                                     <!-- Current avatar -->*/
/*                                                 <div class="avatar-view" title="Change the avatar">*/
/*                                                     {% if entity.getWebPath == null%}*/
/*                                                         <img src="{{asset('gentelella/images/picture.jpg')}}" alt="">*/
/*                                                     {% else %}*/
/*                                                     <img src="{{asset(entity.getWebPath)}}" alt="Avatar">*/
/*                                                     {% endif %}*/
/*                                                 </div>*/
/* */
/*                                                 <!-- Cropping modal -->*/
/*                                                 <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">*/
/*                                                     <div class="modal-dialog modal-lg">*/
/*                                                         <div class="modal-content">*/
/*                                                             <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">*/
/*                                                                 <div class="modal-header">*/
/*                                                                     <button class="close" data-dismiss="modal" type="button">&times;</button>*/
/*                                                                     <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>*/
/*                                                                 </div>*/
/*                                                                 <div class="modal-body">*/
/*                                                                     <div class="avatar-body">*/
/* */
/*                                                                         <!-- Upload image and data -->*/
/*                                                                         <div class="avatar-upload">*/
/*                                                                             <input class="avatar-src" name="avatar_src" type="hidden">*/
/*                                                                             <input class="avatar-data" name="avatar_data" type="hidden">*/
/*                                                                             <label for="avatarInput">Local upload</label>*/
/*                                                                             <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">*/
/*                                                                         </div>*/
/* */
/*                                                                         <!-- Crop and preview -->*/
/*                                                                         <div class="row">*/
/*                                                                             <div class="col-md-9">*/
/*                                                                                 <div class="avatar-wrapper"></div>*/
/*                                                                             </div>*/
/*                                                                             <div class="col-md-3">*/
/*                                                                                 <div class="avatar-preview preview-lg"></div>*/
/*                                                                                 <div class="avatar-preview preview-md"></div>*/
/*                                                                                 <div class="avatar-preview preview-sm"></div>*/
/*                                                                             </div>*/
/*                                                                         </div>*/
/* */
/*                                                                         <div class="row avatar-btns">*/
/*                                                                             <div class="col-md-9">*/
/*                                                                                 <div class="btn-group">*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>*/
/*                                                                                 </div>*/
/*                                                                                 <div class="btn-group">*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>*/
/*                                                                                     <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>*/
/*                                                                                 </div>*/
/*                                                                             </div>*/
/*                                                                             <div class="col-md-3">*/
/*                                                                                 <button class="btn btn-primary btn-block avatar-save" type="submit">Done</button>*/
/*                                                                             </div>*/
/*                                                                         </div>*/
/*                                                                     </div>*/
/*                                                                 </div>*/
/*                                                                 <!-- <div class="modal-footer">*/
/*                                                                 <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>*/
/*                                                             </div> -->*/
/*                                                             </form>*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <!-- /.modal -->*/
/* */
/*                                                 <!-- Loading state -->*/
/*                                                 <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>*/
/*                                                 </div>*/
/*                                                 <!-- end of image cropping -->*/
/*                                         */
/*                                             </div>*/
/*                                               {% for flashMessage in app.session.flashbag.get('notice') %}*/
/* */
/*                                                 <div hidden id='successmessage'>*/
/*                                                     {{ flashMessage }}*/
/*                                                 </div>*/
/* */
/*                                               {% endfor %}*/
/*                                           {% if app.user %}*/
/*                                               {% for doctor in entity.doctors %}*/
/*                                                  {% if doctor.id == app.user.id %}*/
/*                                                     <div id='addressError'>*/
/*                                                        .*/
/*                                                     </div>*/
/*                                                   {% endif %}  */
/*                                               {% endfor %}  */
/*                                         {% endif %}*/
/*                                             <center>*/
/*                                             <h3 style="font-size: 20px"><b>{{entity.clinicName}}</b></h3>*/
/*                                             */
/*                                             {% for doctor in entity.getDoctors %}*/
/*                                                     <a style="font-family: Century Gothic; color:#379DFF; font-size: 15px" href="{{path('doctor_show', {'id': doctor.id})}}">*/
/*                                                         <i class="fa fa-user"></i>&emsp;{{doctor.firstName}} {{doctor.middleName}} {{doctor.lastName}}</a>*/
/*                                                     <br/>*/
/*                                             {% endfor %}*/
/*                                             */
/*                                             <h4><div style="font-family: Century Gothic; font-size: 15px " id="address"><i class="fa fa-map-marker"></i>&emsp;{{entity.clinicAddress}}</div></h4>*/
/*                                             */
/*                                             <div style="font-family: Century Gothic; font-size: 15px"><i class="fa fa-pencil-square-o"></i>&emsp;{{entity.clinicDescription}}</div>*/
/*                                             <div style="font-family: Century Gothic; font-size: 15px"><i class="fa fa-phone-square-o"></i>&emsp;{{entity.contactNumber}}</div>*/
/*                                             <div id="latitude" hidden></div>*/
/*                                             <div id="longitude" hidden></div>*/
/*                                             <br/>*/
/*                                         */
/*                                             <div class="">*/
/*                                                 {% if app.user %}*/
/*                                                         <a href="{{path('clinic_edit',{'id':entity.id})}}"><button class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit Information</button></a>*/
/*                                                 {% endif %}*/
/*                                                 <button id ="amodal" type="button" class="btn btn-success" data-toggle="modal" data-target="#appointmentmodal" data-backdrop="static" data-keyboard="false">*/
/*                                                     <i class="glyphicon glyphicon-calendar"></i> Set an Appointment</button>*/
/*                                             </div>*/
/*                                           */
/*                                             </center>*/
/*                                         </div>*/
/*                                         <div role="tabpanel" class="tab-pane fade" id="tab_content22" aria-labelledby="profile-tab">*/
/*                                             <div>*/
/*                                             <ul class="list-inline prod_color"> */
/*                                                 <center>*/
/*                                                 <li><h3><i class="fa fa-calendar"></i><b>&emsp;CLINIC SCHEDULE</b></h3></li>*/
/*                                                 <div style="overflow-y: scroll; height:450px;">*/
/*                                                     <li>*/
/*                                                         <center>*/
/*                                                             <h3>Sunday</h3>*/
/*                                                             {% if entity.sunOpenTime | date('H:i') == '00:00' and entity.sunCloseTime | date('H:i') == '00:00' %}*/
/*                                                                 <div class="color bg-red"></div> <h3>CLOSED</h3>*/
/*                                                             {% else %}*/
/*                                                                 <div class="color bg-green"></div>*/
/*                                                                 <h3><i><b>{{entity.sunOpenTime | date('H:i')}} up to {{entity.sunCloseTime | date('H:i')}}</b></i></h3>*/
/*                                                             {% endif %}*/
/*                                                         </center>*/
/*                                                     </li>*/
/*                                                     <li>*/
/*                                                         <center>*/
/*                                                             <h3>Monday</h3>*/
/*                                                             {% if entity.monOpenTime | date('H:i') == '00:00' and entity.monCloseTime | date('H:i') == '00:00' %}*/
/*                                                                 <div class="color bg-red"></div> <h3>CLOSED</h3>*/
/*                                                             {% else %}*/
/*                                                                 <div class="color bg-green"></div>*/
/*                                                                 <h3><i><b>{{entity.monOpenTime | date('H:i')}} up to {{entity.monCloseTime | date('H:i')}}</b></i></h3>*/
/*                                                             {% endif %}*/
/*                                                         </center>*/
/*                                                     </li>*/
/*                                                     <li>*/
/*                                                         <center>*/
/*                                                             <h3>Tuesday</h3>*/
/*                                                             {% if entity.tueOpenTime | date('H:i') == '00:00' and entity.tueCloseTime | date('H:i') == '00:00' %}*/
/*                                                                <div class="color bg-red"></div> <h3>CLOSED</h3>*/
/*                                                             {% else %}*/
/*                                                                 <div class="color bg-green"></div>*/
/*                                                                 <h3><i><b>{{entity.tueOpenTime | date('H:i')}} up to {{entity.tueCloseTime | date('H:i')}}</b></i></h3>*/
/*                                                             {% endif %}*/
/*                                                         </center>*/
/*                                                     </li>*/
/*                                                     <li>*/
/*                                                         <center>*/
/*                                                             <h3>Wednesday</h3> */
/*                                                             {% if entity.wedOpenTime | date('H:i') == '00:00' and entity.wedCloseTime | date('H:i') == '00:00' %}*/
/*                                                                 <div class="color bg-red"></div> <h3>CLOSED</h3>*/
/*                                                             {% else %}*/
/*                                                                 <div class="color bg-green"></div>*/
/*                                                                 <h3><i><b>{{entity.wedOpenTime | date('H:i')}} up to {{entity.wedCloseTime | date('H:i')}}</b></i></h3>*/
/*                                                             {% endif %}*/
/*                                                         </center>*/
/*                                                     </li>*/
/*                                                     <li>*/
/*                                                         <center>*/
/*                                                             <h3>Thursday </h3>*/
/*                                                             {% if entity.thuOpenTime | date('H:i') == '00:00' and entity.thuCloseTime | date('H:i') == '00:00' %}*/
/*                                                                 <div class="color bg-red"></div> <h3>CLOSED</h3>*/
/*                                                             {% else %}*/
/*                                                                 <div class="color bg-green"></div>*/
/*                                                                 <h3><i><b>{{entity.thuOpenTime | date('H:i')}} up to {{entity.thuCloseTime | date('H:i')}}</b></i></h3>*/
/*                                                             {% endif %}*/
/*                                                         </center>*/
/*                                                     </li>*/
/*                                                     <li>*/
/*                                                         <center>*/
/*                                                             <h3>Friday </h3>*/
/*                                                             {% if entity.friOpenTime | date('H:i') == '00:00' and entity.friCloseTime | date('H:i') == '00:00' %}*/
/*                                                                 <div class="color bg-red"></div> <h3>CLOSED</h3>*/
/*                                                             {% else %}*/
/*                                                                 <div class="color bg-green"></div>*/
/*                                                                 <h3><i><b>{{entity.friOpenTime | date('H:i')}} up to {{entity.friCloseTime | date('H:i')}}</b></i></h3>*/
/*                                                             {% endif %}</h4>*/
/*                                                         <center>*/
/*                                                     </li>*/
/*                                                     <li>*/
/*                                                         <center>*/
/*                                                             <h3>Saturday </h3> */
/*                                                             {% if entity.satOpenTime | date('H:i') == '00:00' and entity.satCloseTime | date('H:i') == '00:00' %}*/
/*                                                                 <div class="color bg-red"></div> <h3>CLOSED</h3>*/
/*                                                             {% else %}*/
/*                                                                 <div class="color bg-green"></div>*/
/*                                                                 <h3><i><b>{{entity.satOpenTime | date('H:i')}} up to {{entity.satCloseTime | date('H:i')}}</b></i></h3>*/
/*                                                             {% endif %}</h4>*/
/*                                                         </center>*/
/*                                                     </li>*/
/*                                                     <div class="">*/
/*                                                         <button id ="amodal" type="button" class="btn btn-success" data-toggle="modal" data-target="#appointmentmodal" data-backdrop="static" data-keyboard="false">*/
/*                                                             <i class="glyphicon glyphicon-calendar"></i> Set an Appointment</button>*/
/*                                                     </div>*/
/*                                                     </center>*/
/*                                                 </ul>*/
/*                                                </div>*/
/*                                             </ul>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*                     */
/*                <div id="appointmentmodal" class="modal fade bs-example-modal-lg" align="left" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" >*/
/*                                     <div class="modal-dialog modal-lg" role="document">*/
/*                                         <div class="modal-content">*/
/* */
/*                                             <div class="modal-header">*/
/*                                                 <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>*/
/*                                                 </button>*/
/*                                                 <h4 class="modal-title" style="font-family: Century Gothic;" id="myModalLabel">Set an Appointment for <i>{{entity.clinicName}}</i></h4>*/
/*                                             </div>*/
/*                                             <div class="modal-body">*/
/*                                                 <!-- Smart Wizard -->*/
/*                                                 <div id="wizard" class="form_wizard wizard_horizontal">*/
/*                                                 <ul class="wizard_steps">*/
/*                                                     <li>*/
/*                                                         <a href="#step-1">*/
/*                                                             <span class="step_no">1</span>*/
/*                                                             <span class="step_descr">*/
/*                                                                 1<br/>*/
/*                                                                 <small style="font-family: Century Gothic; font-size: 14px;">Fill Up Information</small>*/
/*                                                             </span>*/
/*                                                         </a>*/
/*                                                     </li>*/
/*                                                     <li>*/
/*                                                         <a href="#step-2">*/
/*                                                             <span class="step_no">2</span>*/
/*                                                             <span class="step_descr">*/
/*                                                                 2<br />*/
/*                                                                 <small style="font-family: Century Gothic; font-size: 14px;">Clinic Schedule</small>*/
/*                                                             </span>*/
/*                                                         </a>*/
/*                                                     </li>*/
/*                                                     <li>*/
/*                                                         <a href="#step-3">*/
/*                                                             <span class="step_no">3</span>*/
/*                                                             <span class="step_descr">*/
/*                                                                 3<br />*/
/*                                                                 <small style="font-family: Century Gothic; font-size: 14px;">Confirm Appointment</small>*/
/*                                                             </span>*/
/*                                                         </a>*/
/*                                                     </li>*/
/*                                                     <li>*/
/*                                                         <a href="#step-4">*/
/*                                                             <span class="step_no">4</span>*/
/*                                                             <span class="step_descr">*/
/*                                                                 4<br />*/
/*                                                                 <small style="font-family: Century Gothic; font-size: 14px;">Success!</small>*/
/*                                                             </span>*/
/*                                                         </a>*/
/*                                                     </li>*/
/*                                                 </ul>*/
/*                                         */
/*                                            {{ form_start(appointmentForm, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'appointmentForm', 'class':'form-horizontal form-label-left' } }) }}*/
/*                                             */
/*                                         <div id="step-1">*/
/*                                             <h3> </h3>*/
/*                                             <center><h3 style="font-size: 20px;"class="StepTitle"><b>FILL UP INFORMATION</b></h3></center>*/
/*                                                 <div id='fn' class="item form-group">*/
/*                                                     <label class="control-label col-md-3 col-sm-3 col-xs-12" >First Name <span class="required">*</span>*/
/*                                                     </label>*/
/*                                                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                                                         {{ form_widget(appointmentForm.firstName, { 'attr' : { 'id': 'firstName', 'name': 'firstName', 'placeholder' : 'First Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                                                         'data-validate-length-range' : '1',  'required': 'required', 'type' : 'text' } }) }}*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <div id='ln' class="item form-group">*/
/*                                                     <label class="control-label col-md-3 col-sm-3 col-xs-12" >Last Name <span class="required">*</span>*/
/*                                                     </label>*/
/*                                                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                                                         {{ form_widget(appointmentForm.lastName, { 'attr' : { 'id': 'lastName', 'name': 'lastName', 'placeholder' : 'Last Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                                                         'data-validate-length-range' : '1',  'required': 'required', 'type' : 'text' } }) }}*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <div id='ea' class="item form-group">*/
/*                                                     <label class="control-label col-md-3 col-sm-3 col-xs-12" >Email Address <span class="required">*</span>*/
/*                                                     </label>*/
/*                                                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                                                         {{ form_widget(appointmentForm.emailAddress, { 'attr' : { 'id': 'emailAddress', 'name': 'emailAddress', 'placeholder' : 'Email Address', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                                                         'data-validate-length-range' : '10',  'required': 'required', 'type' : 'email' } }) }}*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <div id='cn' class="item form-group">*/
/*                                                     <label class="control-label col-md-3 col-sm-3 col-xs-12" >Contact Number  <span class="required">*</span>*/
/*                                                     </label>*/
/*                                                     <div class="col-md-6 col-sm-6 col-xsw-12">*/
/*                                                         {{ form_widget(appointmentForm.contactNumber, { 'attr' : { 'id': 'contactNumber', 'name': 'contactNumber', 'placeholder' : 'Contact Number', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                                                         'data-validate-length-range' : '7, 11',  'required': 'required', 'type' : 'number' } }) }}*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <div id='n' class="item form-group">*/
/*                                                     <label class="control-label col-md-3 col-sm-3 col-xs-12" >Note */
/*                                                     </label>*/
/*                                                     <div class=" col-sm-6 col-xs-12">*/
/*                                                        {{ form_widget(appointmentForm.note, { 'attr' : { 'id': 'note', 'name': 'note', 'placeholder' : 'Write a note', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                                                         'data-validate-length-range' : '1',  'required': 'required', 'type' : 'text' } }) }}*/
/*                                                     */
/*                                                     </div>*/
/*                                                 </div>*/
/*                                         </div>*/
/*                                         <div id="step-2">*/
/*                                             <h3> </h3>*/
/*                                             <center><h3 style="font-size: 20px;"class="StepTitle"><b>CLINIC SCHEDULE</b></h3>*/
/*                                                     <div class="form-group">*/
/*                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" >Pick a Schedule  <span class="required">*</span>*/
/*                                                         </label>*/
/*                                                         <div id='da' class=" col-sm-6 col-xs-12">*/
/*                                                                {{ form_widget(appointmentForm.dateAppointed, { 'attr' : { 'id': 'dateOfAppointment', 'name': 'dateOfAppointment', 'placeholder' : 'View Calendar', 'class' : 'form-control has-feedback-left', */
/*                                                                                            'data-validate-length-range' : '1',  'required': 'required', 'type' : 'text' } }) }}*/
/*                                                         </div>*/
/*                                                     </div>*/
/*                                             <div class="item form-group">*/
/*                                                  <div id="clinicsched" class=" col-sm-12 col-xs-12"></div>*/
/*                                             </div>*/
/*                                             </center>*/
/*                                         </div>*/
/*                                         <div id="step-3">*/
/*                                             <h3> </h3>*/
/*                                             <center><h3 style="font-size: 20px;"class="StepTitle"><b>CONFIRM APPOINTMENT</b></h3></center>*/
/*                                             <div style="overflow-y: scroll; height:250px;">*/
/*                                                 <center>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 13px;"><i class="fa fa-pencil"></i> <u>First Name </u></h4>*/
/*                                                 <h4 class="color bg-success" style="font-family: Century Gothic; font-size: 16px;"><b><div id='firstNameConfirm'></div></b></h4>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 13px;"><i class="fa fa-pencil"></i> <u>Last Name </u></h4>*/
/*                                                 <h4 class="color bg-success" style="font-family: Century Gothic; font-size: 16px;"><b><div id='lastNameConfirm'></div></b></h4>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 13px;"><i class="fa fa-envelope"></i> <u>Email Address </u></h4>*/
/*                                                 <h4 class="color bg-success" style="font-family: Century Gothic; font-size: 16px;"><b><div id='emailAddressConfirm'></div></b></h4>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 13px;"><i class="fa fa-phone"></i> <u>Contact Number </u></h4>*/
/*                                                 <h4 class="color bg-success" style="font-family: Century Gothic; font-size: 16px;"><b><div id='contactNumberConfirm'></div></b></h4>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 13px;"><i class="fa fa-calendar"></i> <u>Appointment Date</u></i></h4>*/
/*                                                 <h4 class="color bg-success" style="font-family: Century Gothic; font-size: 16px;"><b><div id='appointmentDateConfirm'></div></b></h4>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 13px;"><i class="fa fa-qrcode"></i> <u>Appointment Code</u></h4>*/
/*                                                 <h4 class="color bg-success" style="font-family: Century Gothic; font-size: 18px;"><b><div id='appointmentCodeConfirm'>{{appointmentCode}}</div></b></h4>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 12px;"><i>(to be presented on appointment date)</i></h4>*/
/*                                                 <h4 style="font-family: Century Gothic; font-size: 13px;"><i class="fa fa-circle"></i> <u>Note</u></h4>*/
/*                                                 <h4 class="color bg-success" style="font-family: Century Gothic; font-size: 16px;"><b><div id='noteConfirm'></div></b></h4>*/
/*                                                 <br/>*/
/*                                                 </center>*/
/*                                             </div>*/
/*                                             */
/*                                         </div>*/
/*                                         <div id="step-4">*/
/*                                             <h3> </h3>*/
/*                                             <center><h3 style="font-size: 20px;"class="StepTitle"><b>FINISH APPOINTMENT</b></h3></center>*/
/*                                             <div class="jumbotron">*/
/*                                               <a href="" hidden>{{ form_widget(appointmentForm.Save, { 'attr' : { 'class' : 'btn btn-info modal-submit', 'id' : 'submitbtn'} }) }}</a>*/
/*                                               <center>*/
/*                                               <h1 style="font-size: 50px;"><b><i>Appointment Created</i></b></h1>*/
/*                                               <h3 style="font-size: 18px;">Click finish to save appointment.</h3>*/
/*                                               </center>*/
/*                                             </div>*/
/*                                             */
/*                                          {{ form_end(appointmentForm)}}*/
/*                                         */
/*                                     </div>*/
/*                                     */
/*                                     <!-- End SmartWizard Content -->*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                </div>    */
/*                     <div id="schedulemodal" class="modal fade bs-example-modal-lg" align="left" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" >*/
/*                         <div class="modal-dialog modal-lg">*/
/*                             <div class="modal-content">*/
/*                                 <div class="modal-header">*/
/*                                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>*/
/*                                         </button>*/
/*                                     <h4 class="modal-title" style="font-family: Century Gothic;" id="myModalLabel">Clinic Schedule</h4>*/
/*                                 </div>*/
/*                                 <div class="modal-body">*/
/*                                     k*/
/*                                     <div id="results"></div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                    </div>*/
/*                     */
/*                     {#FOR APPOINTMENT CLINIC SCHED UI#}*/
/*                     <div id="appointmentsched" hidden>*/
/*                         */
/*                         <div id="sundaysched">*/
/*                             {% if entity.sunOpenTime | date('H:i') == '00:00' and entity.sunCloseTime | date('H:i') == '00:00' %}*/
/*                                 <div class="color bg-red"> <div id='sundaystatus'>CLOSED</div></div>*/
/*                             {% else %}*/
/*                                 <div class="color bg-green"><div id='sundaystatus'>OPEN</div>*/
/*                                    <i><b>{{entity.sunOpenTime | date('H:i')}} up to {{entity.sunCloseTime | date('H:i')}}</b></i>*/
/*                                 </div>*/
/*                             {% endif %}                                 */
/*                         </div>*/
/*                         <div id="mondaysched">*/
/*                             {% if entity.monOpenTime | date('H:i') == '00:00' and entity.monCloseTime | date('H:i') == '00:00' %}*/
/*                                 <div class="color bg-red"> <div id='mondaystatus'>CLOSED</div></div>*/
/*                             {% else %}*/
/*                                 <div class="color bg-green"><div id='mondaystatus'>OPEN</div>*/
/*                                    <i><b>{{entity.monOpenTime | date('H:i')}} up to {{entity.monCloseTime | date('H:i')}}</b></i>*/
/*                                 </div>*/
/*                             {% endif %}                                 */
/*                         </div>*/
/*                         <div id="tuesdaysched">*/
/*                             {% if entity.tueOpenTime | date('H:i') == '00:00' and entity.tueCloseTime | date('H:i') == '00:00' %}*/
/*                                 <div class="color bg-red"> <div id='tuesdaystatus'>CLOSED</div></div>*/
/*                             {% else %}*/
/*                                 <div class="color bg-green"><div id='tuesdaystatus'>OPEN</div>*/
/*                                    <i><b>{{entity.tueOpenTime | date('H:i')}} up to {{entity.tueCloseTime | date('H:i')}}</b></i>*/
/*                                 </div>*/
/*                             {% endif %}                                 */
/*                         </div>*/
/*                         <div id="wednesdaysched">*/
/*                             {% if entity.wedOpenTime | date('H:i') == '00:00' and entity.wedCloseTime | date('H:i') == '00:00' %}*/
/*                                 <div class="color bg-red"><div id='wednesdaystatus'>CLOSED</div></div>*/
/*                             {% else %}*/
/*                                 <div class="color bg-green"><div id='wednesdaystatus'>OPEN</div>*/
/*                                    <i><b>{{entity.wedOpenTime | date('H:i')}} up to {{entity.wedCloseTime | date('H:i')}}</b></i>*/
/*                                 </div>*/
/*                             {% endif %}                                 */
/*                         </div>*/
/*                         <div id="thursdaysched">*/
/*                             {% if entity.thuOpenTime | date('H:i') == '00:00' and entity.thuCloseTime | date('H:i') == '00:00' %}*/
/*                                 <div class="color bg-red"> <div id='thursdaystatus'>CLOSED</div></div>*/
/*                             {% else %}*/
/*                                 <div class="color bg-green"><div id='thursdaystatus'>OPEN</div>*/
/*                                    <i><b>{{entity.thuOpenTime | date('H:i')}} up to {{entity.thuCloseTime | date('H:i')}}</b></i>*/
/*                                 </div>*/
/*                             {% endif %}                                 */
/*                         </div>*/
/*                         <div id="fridaysched">*/
/*                             {% if entity.friOpenTime | date('H:i') == '00:00' and entity.friCloseTime | date('H:i') == '00:00' %}*/
/*                                 <div class="color bg-red"><div id='fridaystatus'>CLOSED</div></div>*/
/*                             {% else %}*/
/*                                 <div class="color bg-green"><div id='fridaystatus'>OPEN</div>*/
/*                                    <i><b>{{entity.friOpenTime | date('H:i')}} up to {{entity.friCloseTime | date('H:i')}}</b></i>*/
/*                                 </div>*/
/*                             {% endif %}                                 */
/*                         </div>*/
/*                         <div id="saturdaysched">*/
/*                             {% if entity.satOpenTime | date('H:i') == '00:00' and entity.satCloseTime | date('H:i') == '00:00' %}*/
/*                                   <div class="color bg-red"> <div id='saturdaystatus'>CLOSED</div></div>*/
/*                             {% else %}*/
/*                                 <div class="color bg-green"><div id='saturdaystatus'>OPEN</div>*/
/*                                    <i><b>{{entity.satOpenTime | date('H:i')}} up to {{entity.satCloseTime | date('H:i')}}</b></i>*/
/*                                 </div>*/
/*                             {% endif %}                                 */
/*                         </div>*/
/*                     </div>*/
/*         */
/*                     */
/*                <div hidden id="directionsPanel">*/
/*   Enter a starting point and click "Calculate route".*/
/* </div>     */
/*         */
/* */
/*     <script type="text/javascript" src="{{asset('gentelella/jquery-ui/external/jquery/jquery.js')}}"></script>*/
/*      <script type="text/javascript" src="{{asset('gentelella/jquery-ui/jquery-ui.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('notify/notify.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/parsley/parsley.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/moment.min2.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/datepicker/daterangepicker.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/jquery.geocomplete.js')}}"></script>*/
/*      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRJhfHphrYhN10mIoK_5CtgJQ0M9qbO14&callback=initMap&libraries=places&sensor=false"></script> */
/*       <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*         */
/* <script type="text/javascript">*/
/*         $(document).ready(function () {*/
/*             $('#form_dateAppointed').datepicker({*/
/*                 minDate: Date.now(),*/
/*                 onSelect: function(dateText, evnt) {*/
/*                     */
/*                     onSelectDate(dateText);*/
/*                    */
/* */
/*                 }*/
/*             });*/
/*         });*/
/*         */
/*         function onSelectDate(dateText){ */
/*          var dateee = new Date(dateText);*/
/*          var weekday = new Array(7);*/
/*             weekday[0]=  "Sunday";*/
/*             weekday[1] = "Monday";*/
/*             weekday[2] = "Tuesday";*/
/*             weekday[3] = "Wednesday";*/
/*             weekday[4] = "Thursday";*/
/*             weekday[5] = "Friday";*/
/*             weekday[6] = "Saturday";*/
/*             */
/*             var s = $('#sundaysched').html();*/
/*             var m = $('#mondaysched').html();*/
/*             var t = $('#tuesdaysched').html();*/
/*             var w = $('#wednesdaysched').html();*/
/*             var th = $('#thursdaysched').html();*/
/*             var f = $('#fridaysched').html();*/
/*             var sa = $('#saturdaysched').html();*/
/*           */
/*             var daydate = weekday[dateee.getDay()];*/
/*         */
/*         */
/*             if(daydate === 'Sunday'){*/
/*                  $('#clinicsched').html(s);*/
/*             }*/
/*             if(daydate === 'Monday'){*/
/*                  $('#clinicsched').html(m);*/
/*             }*/
/*             if(daydate === 'Tuesday'){*/
/*                  $('#clinicsched').html(t);*/
/*             }*/
/*             if(daydate === 'Wednesday'){*/
/*                  $('#clinicsched').html(w);*/
/*             }*/
/*             if(daydate === 'Thursday'){*/
/*                  $('#clinicsched').html(th);*/
/*             }*/
/*             if(daydate === 'Friday'){*/
/*                  $('#clinicsched').html(f);*/
/*             }*/
/*             if(daydate === 'Saturday'){*/
/*                  $('#clinicsched').html(sa);*/
/*             }*/
/*             */
/*         */
/*         */
/*         */
/*         */
/*        */
/*         */
/*         */
/*         */
/*     }*/
/*     </script>     */
/* <script>*/
/*     */
/* function initMap() {*/
/*    */
/*   var long = {{entity.clinicLong}};*/
/*   var lat = {{entity.clinicLat}};*/
/*   var currloc;*/
/*   var latLng = new google.maps.LatLng( {{entity.clinicLong}}, {{entity.clinicLat}});*/
/*   var myLatLng = {lat: lat, lng: long};*/
/*   var map = new google.maps.Map(document.getElementById('map'), {*/
/*     zoom: 16,*/
/*     center: myLatLng*/
/*   });*/
/*  */
/*   directionsService = new google.maps.DirectionsService;*/
/*   directionsDisplay = new google.maps.DirectionsRenderer({*/
/*         map: map*/
/*       });*/
/*       */
/*     */
/*     */
/*   if (navigator.geolocation) {*/
/*     navigator.geolocation.getCurrentPosition(function(position) {*/
/*       var pos = {*/
/*         lat: position.coords.latitude,*/
/*         lng: position.coords.longitude*/
/*       };*/
/*       currloc = position.coords.latitude + ",  " + position.coords.longitude;*/
/*       calcRoute(currloc, myLatLng);*/
/*       var marker2 = new google.maps.Marker({*/
/*         position: pos,*/
/*         map: map,*/
/* */
/*       });*/
/*       infoWindow.setPosition(pos);*/
/*       infoWindow.setContent('Location found.');*/
/*       map.setCenter(pos);*/
/*       */
/*     }, function() {*/
/*       handleLocationError(true, infoWindow, map.getCenter());*/
/*     });*/
/*   } else {*/
/*     // Browser doesn't support Geolocation*/
/*     handleLocationError(false, infoWindow, map.getCenter());*/
/*   }*/
/*   */
/*   */
/*   */
/*   */
/* }*/
/* */
/* function handleLocationError(browserHasGeolocation, infoWindow, pos) {*/
/*   infoWindow.setPosition(pos);*/
/*   infoWindow.setContent(browserHasGeolocation ?*/
/*                         'Error: The Geolocation service failed.' :*/
/*                         'Error: Your browser doesn\'t support geolocation.');*/
/* }*/
/* */
/* */
/* function calcRoute(start, end) {*/
/*     */
/*       */
/*       var request = {*/
/*         origin: start,*/
/*         destination: end,*/
/*         travelMode: google.maps.TravelMode.DRIVING*/
/*       };*/
/*       directionsService.route(request, function(result, status) {*/
/*         if (status == google.maps.DirectionsStatus.OK) {*/
/*           directionsDisplay.setDirections(result);*/
/*         }*/
/*       });*/
/*     };*/
/* */
/* */
/*     </script>*/
/*  */
/* <script>*/
/*     function isFutureDate(idate){*/
/*     var today = new Date().getTime(),*/
/*         idate = idate.split("/");*/
/* */
/*     idate = new Date(idate[2], idate[1] - 1, idate[0]).getTime();*/
/*     return (today - idate) < 0 ? true : false;*/
/* }*/
/* */
/* function checkDate(){*/
/*     var idate = document.getElementById("form_dateAppointed"),*/
/*         dateReg = /(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/]201[4-9]|20[2-9][0-9]/;*/
/* */
/*     if(!dateReg.test(idate.value)){*/
/*        $("#form_dateAppointed").notify("Invalid Date",  */
/*   { position: "right", autohide: "false" });*/
/*     }*/
/* }*/
/* */
/* $('#appointmentmodal').on('hidden.bs.modal', function (e) {*/
/*   $(this)*/
/*     .find("input,textarea,select")*/
/*        .val('')*/
/*        .end()*/
/*     .find("input[type=checkbox], input[type=radio]")*/
/*        .prop("checked", "")*/
/*        .end();*/
/* });*/
/* */
/* */
/* $('#amodal').on('click', function()*/
/*     {*/
/*         $('#appointmentForm').find('input:text, input:password, select, textarea').val('');*/
/*         $('#appointmentForm').find('input:radio, input:checkbox').prop('checked', false);*/
/*         $('#form_dateAppointed').val('');*/
/*         */
/* });*/
/* */
/* */
/* */
/* */
/* */
/* </script>*/
/*   <script type="text/javascript" src="{{asset('gentelella/js/wizard/jquery.smartWizard.js')}}"></script>*/
/*   <script type="text/javascript">*/
/* $(document).ready(function(){*/
/*     $.notify.addStyle('formerror', {*/
/*                 html: "<div><span data-notify-text/></div>",*/
/*                 classes: {*/
/*                   base: {*/
/*                     "color": "white",*/
/*                     "white-space": "nowrap",*/
/*                     "background-color": "#ce5454",*/
/*                      "padding": "3px 10px",*/
/*                     "border-radius" : "3px 4px 4px 3px",*/
/*                     */
/*                     "border": "2px solid #ce5454",*/
/*                     */
/*                     */
/*                   }*/
/*                 }*/
/*               });*/
/* */
/*     // Smart Wizard         */
/*     $('#wizard').smartWizard({*/
/*         onLeaveStep:leaveAStepCallback,*/
/*         onFinish:onFinishCallback*/
/*     });*/
/* */
/*     function leaveAStepCallback(obj, context){*/
/* */
/* */
/*         if(context.fromStep === 2){*/
/*            fillInfo();*/
/*         }*/
/* */
/* */
/*         return validateSteps(context.fromStep); // return false to stay on step and true to continue navigation */
/*     }*/
/* */
/*     function onFinishCallback(objs, context){*/
/*         if(validateAllSteps()){*/
/*             $('form').submit();*/
/*             */
/*         }*/
/*     }*/
/* */
/*     // Your Step validation logic*/
/*     function validateSteps(stepnumber){*/
/*         var isStepValid = true;*/
/*         // validate step 1*/
/*         if(stepnumber == 1){*/
/*             if($('#form_firstName').val() == '' ){*/
/*                 isStepValid = false;*/
/*                 $("#form_firstName").notify("Please fill out this field",  */
/*                    { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             */
/*             }else{*/
/*                  $('#fn .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*             if($('#form_lastName').val() == '' ){*/
/*                 isStepValid = false;*/
/*                 $("#form_lastName").notify("Please fill out this field",  */
/*                    { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             */
/*             }else{*/
/*                  $('#ln .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*             if($('#form_contactNumber').val() == '' ){*/
/*                 isStepValid = false;*/
/*                 $("#form_contactNumber").notify("Please fill out this field",  */
/*                    { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             */
/*             }else{*/
/*                  $('#cn .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*             if($('#form_emailAddress').val() == '' ){*/
/*                 isStepValid = false;*/
/*                 $("#form_emailAddress").notify("Please fill out this field",  */
/*                    { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             */
/*             }else{*/
/*                  $('#ea .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*             if($('#form_note').val() == '' ){*/
/*                 isStepValid = false;*/
/*                 $("#form_note").notify("Please fill out this field",  */
/*                    { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             */
/*             }else{*/
/*                  $('#n .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*             var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;*/
/*             if(re.test($('#form_emailAddress').val()) == false ){*/
/*                 isStepValid = false;*/
/*                 */
/*                 $("#form_emailAddress").notify("Invalid email",  */
/*                    { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             */
/*             }else{*/
/*                  $('#ea .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*             var cn = /^(\d{7}|\d{10}|09\d{9})$/;*/
/*             if(cn.test($('#form_contactNumber').val()) == false ){*/
/*                 isStepValid = false;*/
/*                 */
/*                 $("#form_contactNumber").notify("Invalid contact",  */
/*                    { position: "right", autohide: "false", style: 'formerror', arrowSize: '6',  showAnimation: 'fadeIn' });*/
/*             */
/*             }else{*/
/*                  $('#cn .notifyjs-wrapper').trigger('notify-hide');*/
/*             }*/
/*             */
/*         }*/
/*         if(stepnumber == 2){*/
/*             if($('#form_dateAppointed').val() == ''){*/
/*                 $("#form_dateAppointed").notify("Plese pick a date",  */
/*                   { position: "right", autohide: "false" });*/
/*                   isStepValid = false;*/
/*                  }*/
/*                 var idate = document.getElementById("form_dateAppointed"),*/
/*                 dateReg = /^\d{1,2}\/\d{1,2}\/\d{4}$/;*/
/* */
/*         */
/*              if(!dateReg.test(idate.value)){*/
/*                isStepValid = false;*/
/*                 }*/
/* */
/*                 var now = new Date();*/
/*                 if (Date.parse(idate.value) < now) {*/
/*                   // selected date is in the past*/
/*                   isStepValid = false;*/
/*                 }*/
/* */
/* */
/*               */
/*                             */
/*                           */
/*             var dateee = new Date( $('#form_dateAppointed').val());*/
/*             var weekday = new Array(7);*/
/*             weekday[0]=  "Sunday";*/
/*             weekday[1] = "Monday";*/
/*             weekday[2] = "Tuesday";*/
/*             weekday[3] = "Wednesday";*/
/*             weekday[4] = "Thursday";*/
/*             weekday[5] = "Friday";*/
/*             weekday[6] = "Saturday";*/
/* */
/*             var daydate = weekday[dateee.getDay()];*/
/*             */
/*             if(daydate === 'Sunday'){*/
/*                 if($('#sundaystatus').text() === 'CLOSED'){*/
/*                    isStepValid = false;*/
/*                 }*/
/*               } if(daydate === 'Monday'){*/
/*                   if($('#mondaystatus').text() === 'CLOSED'){*/
/*                    isStepValid = false;*/
/*                 }*/
/*               }if(daydate === 'Tuesday'){*/
/*                   if($('#tuesdaystatus').text() === 'CLOSED'){*/
/*                    isStepValid = false;*/
/*                 }*/
/*               }if(daydate === 'Wednesday'){*/
/*                   if($('#wednesdaystatus').text() === 'CLOSED'){*/
/*                    isStepValid = false;*/
/*                 }*/
/*               }if(daydate === 'Thursday'){*/
/*                   if($('#thursdaystatus').text() === 'CLOSED'){*/
/*                    isStepValid = false;*/
/*                 }*/
/*               }if(daydate === 'Friday'){*/
/*                   if($('#fridaystatus').text() === 'CLOSED'){*/
/*                    isStepValid = false;*/
/*                 }*/
/*               }if(daydate === 'Saturday'){*/
/*                   if($('#saturdaystatus').text() === 'CLOSED'){*/
/*                    isStepValid = false;*/
/*                 }*/
/*               }*/
/*             */
/*               */
/*         }*/
/*         // ... */
/*         return isStepValid;*/
/*     }*/
/*     function validateAllSteps(){*/
/*         var isStepValid = true;*/
/*         // all step validation logic     */
/*         return isStepValid;*/
/*     }*/
/*     */
/*     function fillInfo(){*/
/*          $('#firstNameConfirm').html($("#form_firstName").val());*/
/*          $('#lastNameConfirm').html($("#form_lastName").val());*/
/*          $('#contactNumberConfirm').html($("#form_contactNumber").val());*/
/*          $('#emailAddressConfirm').html($("#form_emailAddress").val());*/
/*          $('#noteConfirm').html($("#form_note").val());*/
/*          $('#appointmentDateConfirm').html($("#form_dateAppointed").val());*/
/*         */
/*     }*/
/*     */
/*     */
/*     */
/* });*/
/* </script>*/
/* */
/* <script type="text/javascript">*/
/* $(document).ready(function(){*/
/*     */
/*     if ($('#successmessage').length){*/
/*         */
/*             new PNotify({*/
/*                                 title: 'Success!',*/
/*                                 text: 'You have successfully created an appointment!',*/
/*                                 type: 'success'*/
/*                             });*/
/*     }*/
/*     */
/*     */
/*     */
/* });*/
/* </script>*/
/* */
/*    */
/* {% endblock %}*/
/* */
/* */
