<?php

/* ResearchProjectMyProjectBundle:Illness:show.html.twig */
class __TwigTemplate_af7fb091fa6cd062c7f5209dfe43417c19b439373d255583955349abe4dc3297 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Illness:show.html.twig", 1);
        $this->blocks = array(
            'illness' => array($this, 'block_illness'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5a38e6f5940f217328e18dc987ae75b7747c5963aab6004832980f561f33366a = $this->env->getExtension("native_profiler");
        $__internal_5a38e6f5940f217328e18dc987ae75b7747c5963aab6004832980f561f33366a->enter($__internal_5a38e6f5940f217328e18dc987ae75b7747c5963aab6004832980f561f33366a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Illness:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a38e6f5940f217328e18dc987ae75b7747c5963aab6004832980f561f33366a->leave($__internal_5a38e6f5940f217328e18dc987ae75b7747c5963aab6004832980f561f33366a_prof);

    }

    // line 2
    public function block_illness($context, array $blocks = array())
    {
        $__internal_7a6404c8545300dd681914ccd4fa5587846a62e8587b7519049cf680c2736602 = $this->env->getExtension("native_profiler");
        $__internal_7a6404c8545300dd681914ccd4fa5587846a62e8587b7519049cf680c2736602->enter($__internal_7a6404c8545300dd681914ccd4fa5587846a62e8587b7519049cf680c2736602_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "illness"));

        echo " class=\"active\" ";
        
        $__internal_7a6404c8545300dd681914ccd4fa5587846a62e8587b7519049cf680c2736602->leave($__internal_7a6404c8545300dd681914ccd4fa5587846a62e8587b7519049cf680c2736602_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_05115d949b37aff6bdccc1c2cacaa49a806e3be7429b73f556edac8b190012dd = $this->env->getExtension("native_profiler");
        $__internal_05115d949b37aff6bdccc1c2cacaa49a806e3be7429b73f556edac8b190012dd->enter($__internal_05115d949b37aff6bdccc1c2cacaa49a806e3be7429b73f556edac8b190012dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"\">
            <div class=\"page-title\">
                <div class=\"title\">
                    <h3>&nbsp;ILLNESS</h3>
                </div>
            </div>
                    <div class=\"clearfix\"></div>

                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\">
                                <div class=\"x_title\">
                                    <h3>Illness Information</h3>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div class=\"x_content\">
                                    <center>
                                        <h3 style=\"font-size: 50px;\"><b>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessName", array()), "html", null, true);
        echo "<b></h3>       
                                        <h3 style=\"font-size: 20px; font-family: Century Gothic;\"><i class =\"fa fa-quote-left\"></i>&emsp; <b><i>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessCommonName", array()), "html", null, true);
        echo "</i></b> &emsp;<i class =\"fa fa-quote-right\"></i></h3>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class =\"fa fa-file-text\"></i>&emsp;<b>Illness Definition:</b> ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessDefinition", array()), "html", null, true);
        echo "</h4>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class =\"fa fa-clipboard\"></i>&emsp;<b>Illness Description:</b> ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "illnessDescription", array()), "html", null, true);
        echo "</h4>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"><i class =\"fa fa-exclamation-triangle\"></i>&emsp;<b>Symptoms:</b> ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "symptoms", array()), "html", null, true);
        echo "</h4>
                                        
                                        <h3></h3>
                                        <h3><i class=\"fa fa-medkit\"></i><b>&emsp;MEDICATIONS</b></h3>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"> <i class =\"fa fa-chevron-circle-right\"></i>&emsp; <b>Home Remedies:</b> ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "homeRemedies", array()), "html", null, true);
        echo "</h4>
                                        <h4 style=\"font-family: Century Gothic; font-size: 16px\"> <i class =\"fa fa-chevron-circle-right\"></i>&emsp; <b>Commonly Prescribed Drugs:</b> ";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "commonlyPrescribedDrugs", array()), "html", null, true);
        echo "</h4>
                                        <br/>
                                    ";
        // line 32
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 33
            echo "                                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("illness_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
            echo "\"><button class=\"btn btn-primary\">Edit Information</button></a>
                                    ";
        }
        // line 35
        echo "                                    </center>
                                </div>
                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                        
";
        
        $__internal_05115d949b37aff6bdccc1c2cacaa49a806e3be7429b73f556edac8b190012dd->leave($__internal_05115d949b37aff6bdccc1c2cacaa49a806e3be7429b73f556edac8b190012dd_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Illness:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 35,  106 => 33,  104 => 32,  99 => 30,  95 => 29,  88 => 25,  84 => 24,  80 => 23,  76 => 22,  72 => 21,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {%block illness%} class="active" {% endblock %}*/
/* {% block body -%}*/
/*         <div class="">*/
/*             <div class="page-title">*/
/*                 <div class="title">*/
/*                     <h3>&nbsp;ILLNESS</h3>*/
/*                 </div>*/
/*             </div>*/
/*                     <div class="clearfix"></div>*/
/* */
/*                     <div class="row">*/
/*                         <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                             <div class="x_panel">*/
/*                                 <div class="x_title">*/
/*                                     <h3>Illness Information</h3>*/
/*                                     <div class="clearfix"></div>*/
/*                                 </div>*/
/*                                 <div class="x_content">*/
/*                                     <center>*/
/*                                         <h3 style="font-size: 50px;"><b>{{ entity.illnessName }}<b></h3>       */
/*                                         <h3 style="font-size: 20px; font-family: Century Gothic;"><i class ="fa fa-quote-left"></i>&emsp; <b><i>{{entity.illnessCommonName}}</i></b> &emsp;<i class ="fa fa-quote-right"></i></h3>*/
/*                                         <h4 style="font-family: Century Gothic; font-size: 16px"><i class ="fa fa-file-text"></i>&emsp;<b>Illness Definition:</b> {{entity.illnessDefinition}}</h4>*/
/*                                         <h4 style="font-family: Century Gothic; font-size: 16px"><i class ="fa fa-clipboard"></i>&emsp;<b>Illness Description:</b> {{entity.illnessDescription}}</h4>*/
/*                                         <h4 style="font-family: Century Gothic; font-size: 16px"><i class ="fa fa-exclamation-triangle"></i>&emsp;<b>Symptoms:</b> {{entity.symptoms}}</h4>*/
/*                                         */
/*                                         <h3></h3>*/
/*                                         <h3><i class="fa fa-medkit"></i><b>&emsp;MEDICATIONS</b></h3>*/
/*                                         <h4 style="font-family: Century Gothic; font-size: 16px"> <i class ="fa fa-chevron-circle-right"></i>&emsp; <b>Home Remedies:</b> {{entity.homeRemedies}}</h4>*/
/*                                         <h4 style="font-family: Century Gothic; font-size: 16px"> <i class ="fa fa-chevron-circle-right"></i>&emsp; <b>Commonly Prescribed Drugs:</b> {{entity.commonlyPrescribedDrugs}}</h4>*/
/*                                         <br/>*/
/*                                     {% if app.user %}*/
/*                                         <a href="{{path('illness_edit',{'id':entity.id})}}"><button class="btn btn-primary">Edit Information</button></a>*/
/*                                     {% endif %}*/
/*                                     </center>*/
/*                                 </div>*/
/*                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             */
/*                         */
/* {% endblock %}*/
/* */
