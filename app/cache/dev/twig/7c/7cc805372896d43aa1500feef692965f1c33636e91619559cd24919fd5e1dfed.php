<?php

/* ResearchProjectMyProjectBundle:Default:index.html.twig */
class __TwigTemplate_efde800b2128db4792195eb7c258b7b6e44dffc8e31cc558c82463b87715c9fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'home' => array($this, 'block_home'),
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f239379b98da32533bec32c7a3bd2f1bc5324779b2801aa5541403295e2bf2cc = $this->env->getExtension("native_profiler");
        $__internal_f239379b98da32533bec32c7a3bd2f1bc5324779b2801aa5541403295e2bf2cc->enter($__internal_f239379b98da32533bec32c7a3bd2f1bc5324779b2801aa5541403295e2bf2cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f239379b98da32533bec32c7a3bd2f1bc5324779b2801aa5541403295e2bf2cc->leave($__internal_f239379b98da32533bec32c7a3bd2f1bc5324779b2801aa5541403295e2bf2cc_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_b876cd13eba88ec677dd8f3604906d37e23ca243055f4b9eda61dac1c483ff17 = $this->env->getExtension("native_profiler");
        $__internal_b876cd13eba88ec677dd8f3604906d37e23ca243055f4b9eda61dac1c483ff17->enter($__internal_b876cd13eba88ec677dd8f3604906d37e23ca243055f4b9eda61dac1c483ff17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " FIMADILO - Welcome! ";
        
        $__internal_b876cd13eba88ec677dd8f3604906d37e23ca243055f4b9eda61dac1c483ff17->leave($__internal_b876cd13eba88ec677dd8f3604906d37e23ca243055f4b9eda61dac1c483ff17_prof);

    }

    // line 3
    public function block_home($context, array $blocks = array())
    {
        $__internal_5107204d84903e4e35c2bd1d3a9df5d4621305aef6a436ffe44eae180b1683b3 = $this->env->getExtension("native_profiler");
        $__internal_5107204d84903e4e35c2bd1d3a9df5d4621305aef6a436ffe44eae180b1683b3->enter($__internal_5107204d84903e4e35c2bd1d3a9df5d4621305aef6a436ffe44eae180b1683b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "home"));

        echo " class=\"active\" ";
        
        $__internal_5107204d84903e4e35c2bd1d3a9df5d4621305aef6a436ffe44eae180b1683b3->leave($__internal_5107204d84903e4e35c2bd1d3a9df5d4621305aef6a436ffe44eae180b1683b3_prof);

    }

    // line 4
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_b67cefd8a07cd7fc902bf761605f3c572cf1d917e74c349e4677802fa8799b5c = $this->env->getExtension("native_profiler");
        $__internal_b67cefd8a07cd7fc902bf761605f3c572cf1d917e74c349e4677802fa8799b5c->enter($__internal_b67cefd8a07cd7fc902bf761605f3c572cf1d917e74c349e4677802fa8799b5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo " Home ";
        
        $__internal_b67cefd8a07cd7fc902bf761605f3c572cf1d917e74c349e4677802fa8799b5c->leave($__internal_b67cefd8a07cd7fc902bf761605f3c572cf1d917e74c349e4677802fa8799b5c_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_47ca5c0ff04260d8ddce1189e64c7df4d09b863851bd6a451e20f080ea5e36f7 = $this->env->getExtension("native_profiler");
        $__internal_47ca5c0ff04260d8ddce1189e64c7df4d09b863851bd6a451e20f080ea5e36f7->enter($__internal_47ca5c0ff04260d8ddce1189e64c7df4d09b863851bd6a451e20f080ea5e36f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<style>
.crop {
  position: relative;
  width: 100px;
  height: 100px;
  overflow: hidden;
}

.crop img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: 100%;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
</style>    
    

    <br/>
    
    </br>
    <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
    <!-- Indicators -->
    <ol class=\"carousel-indicators\">
      <li class=\"item1 active\"></li>
      <li class=\"item2\"></li>
      <li class=\"item3\"></li>
      <li class=\"item4\"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class=\"carousel-inner\" role=\"listbox\">

      <div class=\"item active\">
        <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("doctorhomepage");
        echo "\">
        <img src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/carousel/Doctor.png"), "html", null, true);
        echo "\" alt=\"Doctor\">
        <div class=\"carousel-caption\" style=\"color: black;\">
              <br/>
              <br/>
              Find the best doctors in Iloilo.
        </div>
        </a>
      </div>

      <div class=\"item\">
        <a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("clinichomepage");
        echo "\">
        <img src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/carousel/Clinic.png"), "html", null, true);
        echo "\" alt=\"Clinic\">
        <div class=\"carousel-caption\" style=\"color: black;\">
              <br/>
              <br/>
              Locate the clinics in Iloilo.
        </div>
        </a>
      </div>
    
      <div class=\"item\">
        <a href=\"";
        // line 65
        echo $this->env->getExtension('routing')->getPath("clinichomepage");
        echo "\">
        <img src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/carousel/Illness.png"), "html", null, true);
        echo "\" alt=\"Illness\">
        <div class=\"carousel-caption\" style=\"color: black;\">
              <br/>
              <br/>
              Know more about your illness.
        </div>
        </a>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\">
      <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Previous</span>
    </a>
    <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\">
      <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
      <span class=\"sr-only\">Next</span>
    </a>
  </div>
        <br/>
        <br/>
        <center>
        <a href=\"";
        // line 90
        echo $this->env->getExtension('routing')->getPath("doctorhomepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/homepage/Doctor.png"), "html", null, true);
        echo "\"></a>
        <a href=\"";
        // line 91
        echo $this->env->getExtension('routing')->getPath("clinichomepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/homepage/Clinic.png"), "html", null, true);
        echo "\"></a>
        <a href=\"";
        // line 92
        echo $this->env->getExtension('routing')->getPath("illnesshomepage");
        echo "\"><img  src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/homepage/Illness.png"), "html", null, true);
        echo "\"></a>
        
        </br>
        </br>
        <div style=\"height: 325px;\">
        <h3></h3>
        <h3 style=\" font-size: 16px;\"><b><i class=\"fa fa-user-md\"></i>&emsp;DOCTOR</b></h3>
        
        <br/>
        <br/>
                       
            ";
        // line 103
        $context["firstname1"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "firstName", array());
        // line 104
        echo "            ";
        $context["lastname1"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "lastName", array());
        // line 105
        echo "            ";
        $context["fop1"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "fieldOfPractice", array());
        // line 106
        echo "            ";
        $context["firstname2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "firstName", array());
        // line 107
        echo "            ";
        $context["lastname2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "lastName", array());
        // line 108
        echo "            ";
        $context["fop2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "fieldOfPractice", array());
        // line 109
        echo "            ";
        $context["firstname3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "firstName", array());
        // line 110
        echo "            ";
        $context["lastname3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "lastName", array());
        // line 111
        echo "            ";
        $context["fop3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "fieldOfPractice", array());
        // line 112
        echo "                
            <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
        // line 117
        echo twig_escape_filter($this->env, (isset($context["firstname1"]) ? $context["firstname1"] : $this->getContext($context, "firstname1")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lastname1"]) ? $context["lastname1"] : $this->getContext($context, "lastname1")), "html", null, true);
        echo "</b></h3></i>
                                        
                                        ";
        // line 119
        if (((isset($context["fop1"]) ? $context["fop1"] : $this->getContext($context, "fop1")) == "General")) {
            // line 120
            echo "                                             <h4>";
            echo twig_escape_filter($this->env, (isset($context["fop1"]) ? $context["fop1"] : $this->getContext($context, "fop1")), "html", null, true);
            echo "</h4>
                                        ";
        } elseif ((        // line 121
(isset($context["fop1"]) ? $context["fop1"] : $this->getContext($context, "fop1")) == "Specialized")) {
            // line 122
            echo "                                            <h4>";
            echo twig_escape_filter($this->env, (isset($context["fop1"]) ? $context["fop1"] : $this->getContext($context, "fop1")), "html", null, true);
            echo "</h4>
                                            ";
            // line 123
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "specialties", array()), 0, 1));
            foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                // line 124
                echo "                                            <button class=\"btn-info btn-xs\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["specialty"], "specialtyName", array()), "html", null, true);
                echo "\">See Specialties</button>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 126
            echo "                                        ";
        }
        // line 127
        echo "                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        ";
        // line 132
        if (($this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "getWebPath", array()) == null)) {
            // line 133
            echo "                                        <div class=\"crop\">
                                        <img src=\"";
            // line 134
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
            echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        </div>
                                        <br/>
                                        <a href=\"";
            // line 137
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                        ";
        } else {
            // line 141
            echo "                                            <div class=\"crop\">
                                            <img style=\"margin-bottom: 17px;\" src=\"";
            // line 142
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "getWebPath", array())), "html", null, true);
            echo "\"alt=\"\" class=\"img-circle img-responsive\" height=\"42\" width=\"42\">
                                            </div>
                                            <br/>
                                            <a href=\"";
            // line 145
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                        ";
        }
        // line 149
        echo "                                        <br/>
                                        
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
        // line 163
        echo twig_escape_filter($this->env, (isset($context["firstname2"]) ? $context["firstname2"] : $this->getContext($context, "firstname2")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lastname2"]) ? $context["lastname2"] : $this->getContext($context, "lastname2")), "html", null, true);
        echo "</b></h3></i>
                                        
                                        ";
        // line 165
        if (((isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")) == "General")) {
            // line 166
            echo "                                             <h4>";
            echo twig_escape_filter($this->env, (isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")), "html", null, true);
            echo "</h4>
                                        ";
        } elseif ((        // line 167
(isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")) == "Specialized")) {
            // line 168
            echo "                                            <h4>";
            echo twig_escape_filter($this->env, (isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")), "html", null, true);
            echo "</h4>
                                            ";
            // line 169
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "specialties", array()), 0, 1));
            foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                // line 170
                echo "                                                <button class=\"btn-info btn-xs\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["specialty"], "specialtyName", array()), "html", null, true);
                echo "\">See Specialties</button>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 172
            echo "                                        ";
        }
        // line 173
        echo "                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        ";
        // line 178
        if (($this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "getWebPath", array()) == null)) {
            // line 179
            echo "                                        <div class=\"crop\">
                                        <img src=\"";
            // line 180
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
            echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        </div>
                                        <br/>
                                        <a href=\"";
            // line 183
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                        ";
        } else {
            // line 187
            echo "                                            <div class=\"crop\">
                                            <img style=\"margin-bottom: 17px;\" src=\"";
            // line 188
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "getWebPath", array())), "html", null, true);
            echo "\"alt=\"\" class=\"img-circle img-responsive\" height=\"42\" width=\"42\">
                                            </div>
                                            <br/>
                                            <a href=\"";
            // line 191
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                        ";
        }
        // line 195
        echo "                                        <br/>
                                        
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
        // line 209
        echo twig_escape_filter($this->env, (isset($context["firstname3"]) ? $context["firstname3"] : $this->getContext($context, "firstname3")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["lastname3"]) ? $context["lastname3"] : $this->getContext($context, "lastname3")), "html", null, true);
        echo "</b></h3></i>
                                        
                                        ";
        // line 211
        if (((isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")) == "General")) {
            // line 212
            echo "                                            <h4>";
            echo twig_escape_filter($this->env, (isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")), "html", null, true);
            echo "</h4>
                                        ";
        } elseif ((        // line 213
(isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")) == "Specialized")) {
            // line 214
            echo "                                            <h4>";
            echo twig_escape_filter($this->env, (isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")), "html", null, true);
            echo "</h4>
                                            ";
            // line 215
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "specialties", array()), 0, 1));
            foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                echo "              
                                            <button class=\"btn-info btn-xs\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                // line 216
                echo twig_escape_filter($this->env, $this->getAttribute($context["specialty"], "specialtyName", array()), "html", null, true);
                echo "\">See Specialties</button>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 218
            echo "                                        ";
        }
        // line 219
        echo "                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        ";
        // line 224
        if (($this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "getWebPath", array()) == null)) {
            // line 225
            echo "                                        <div class=\"crop\">
                                        <img src=\"";
            // line 226
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
            echo "\" alt=\"\" class=\"img-circle img-responsive\" height=\"42\" width=\"42\">
                                        </div>
                                        <br/>
                                        <a href=\"";
            // line 229
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                               <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                            <br/>
                                        ";
        } else {
            // line 234
            echo "                                            <div class=\"crop\">
                                            <img style=\"margin-bottom: 17px;\" src=\"";
            // line 235
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "getWebPath", array())), "html", null, true);
            echo "\"alt=\"\" class=\"img-circle img-responsive\">
                                            </div>
                                            <br/>
                                            <a href=\"";
            // line 238
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                               <i class=\"fa fa-user\"></i> View Profile </button></a>
                                               <br/>
                                        ";
        }
        // line 242
        echo "                                        
                                        
                                            
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        
        </br>    
            
                
             
            <div style=\"height: 325px;\">
        <h3></h3>
        <h3 style=\" font-size: 16px;\"><b><i class=\"fa fa-user-md\"></i>&emsp;CLINIC</b></h3>
        
        </br>
                       
            ";
        // line 265
        $context["clinicname1"] = $this->getAttribute((isset($context["randomcli1"]) ? $context["randomcli1"] : $this->getContext($context, "randomcli1")), "clinicName", array());
        // line 266
        echo "            ";
        $context["clinicadd1"] = $this->getAttribute((isset($context["randomcli1"]) ? $context["randomcli1"] : $this->getContext($context, "randomcli1")), "clinicAddress", array());
        // line 267
        echo "            ";
        $context["clinicname2"] = $this->getAttribute((isset($context["randomcli2"]) ? $context["randomcli2"] : $this->getContext($context, "randomcli2")), "clinicName", array());
        // line 268
        echo "            ";
        $context["clinicadd2"] = $this->getAttribute((isset($context["randomcli2"]) ? $context["randomcli2"] : $this->getContext($context, "randomcli2")), "clinicAddress", array());
        // line 269
        echo "            ";
        $context["clinicname3"] = $this->getAttribute((isset($context["randomcli3"]) ? $context["randomcli3"] : $this->getContext($context, "randomcli3")), "clinicName", array());
        // line 270
        echo "            ";
        $context["clinicadd3"] = $this->getAttribute((isset($context["randomcli3"]) ? $context["randomcli3"] : $this->getContext($context, "randomcli3")), "clinicAddress", array());
        // line 271
        echo "                
            <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
        // line 276
        echo twig_escape_filter($this->env, (isset($context["clinicname1"]) ? $context["clinicname1"] : $this->getContext($context, "clinicname1")), "html", null, true);
        echo "</b></h3></i>
                                        
                                        ";
        // line 278
        echo twig_escape_filter($this->env, (isset($context["clinicadd1"]) ? $context["clinicadd1"] : $this->getContext($context, "clinicadd1")), "html", null, true);
        echo "
                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        ";
        // line 284
        if (($this->getAttribute((isset($context["randomcli1"]) ? $context["randomcli1"] : $this->getContext($context, "randomcli1")), "getWebPath", array()) == null)) {
            // line 285
            echo "                                        <div class=\"crop\">
                                        <img src=\"";
            // line 286
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
            echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        </div>
                                        <br/>
                                        <a href=\"";
            // line 289
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomcli1"]) ? $context["randomcli1"] : $this->getContext($context, "randomcli1")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                        ";
        } else {
            // line 293
            echo "                                            <div class=\"crop\">
                                            <img style=\"margin-bottom: 17px;\" src=\"";
            // line 294
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["randomcli1"]) ? $context["randomcli1"] : $this->getContext($context, "randomcli1")), "getWebPath", array())), "html", null, true);
            echo "\"alt=\"\" class=\"img-circle img-responsive\">
                                            </div>
                                            <br/>
                                            <a href=\"";
            // line 297
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomcli1"]) ? $context["randomcli1"] : $this->getContext($context, "randomcli1")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br>
                                        ";
        }
        // line 301
        echo "                                        <br/>
                                        
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
        // line 315
        echo twig_escape_filter($this->env, (isset($context["clinicname2"]) ? $context["clinicname2"] : $this->getContext($context, "clinicname2")), "html", null, true);
        echo "</b></h3></i>
                                        ";
        // line 316
        echo twig_escape_filter($this->env, (isset($context["clinicadd2"]) ? $context["clinicadd2"] : $this->getContext($context, "clinicadd2")), "html", null, true);
        echo "
                                        
                                        
                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        ";
        // line 324
        if (($this->getAttribute((isset($context["randomcli2"]) ? $context["randomcli2"] : $this->getContext($context, "randomcli2")), "getWebPath", array()) == null)) {
            // line 325
            echo "                                        <div class=\"crop\">
                                        <img src=\"";
            // line 326
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
            echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        </div>
                                        <br/>
                                        <a href=\"";
            // line 329
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomcli2"]) ? $context["randomcli2"] : $this->getContext($context, "randomcli2")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                        ";
        } else {
            // line 333
            echo "                                            <div class=\"crop\">
                                            <img style=\"margin-bottom: 17px;\" src=\"";
            // line 334
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["randomcli2"]) ? $context["randomcli2"] : $this->getContext($context, "randomcli2")), "getWebPath", array())), "html", null, true);
            echo "\"alt=\"\" class=\"img-circle img-responsive\">
                                            </div>
                                            <br/>
                                            <a href=\"";
            // line 337
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomcli2"]) ? $context["randomcli2"] : $this->getContext($context, "randomcli2")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br>
                                        ";
        }
        // line 341
        echo "                                        <br/>
                                        
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
        // line 355
        echo twig_escape_filter($this->env, (isset($context["clinicname3"]) ? $context["clinicname3"] : $this->getContext($context, "clinicname3")), "html", null, true);
        echo "</b></h3></i>
                                        ";
        // line 356
        echo twig_escape_filter($this->env, (isset($context["clinicadd3"]) ? $context["clinicadd3"] : $this->getContext($context, "clinicadd3")), "html", null, true);
        echo "
                                       
                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        ";
        // line 363
        if (($this->getAttribute((isset($context["randomcli3"]) ? $context["randomcli3"] : $this->getContext($context, "randomcli3")), "getWebPath", array()) == null)) {
            // line 364
            echo "                                        <div class=\"crop\">
                                        <img src=\"";
            // line 365
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
            echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        </div>
                                        <br/>
                                        <a href=\"";
            // line 368
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomcli3"]) ? $context["randomcli3"] : $this->getContext($context, "randomcli3")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                               <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                            <br/>
                                        ";
        } else {
            // line 373
            echo "                                            <div class=\"crop\">
                                            <img style=\"margin-bottom: 17px;\" src=\"";
            // line 374
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["randomcli3"]) ? $context["randomcli3"] : $this->getContext($context, "randomcli3")), "getWebPath", array())), "html", null, true);
            echo "\"alt=\"\" class=\"img-circle img-responsive\">
                                            </div>
                                            <br/>
                                            <a href=\"";
            // line 377
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("clinic_show", array("id" => $this->getAttribute((isset($context["randomcli3"]) ? $context["randomcli3"] : $this->getContext($context, "randomcli3")), "id", array()))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                               <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br>
                                        ";
        }
        // line 381
        echo "                                        
                                        
                                            
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        
        </br>
        
            
        ";
        // line 399
        echo "        <br/>
        </center>
        
        <script>
            \$(document).ready(function(){
            // Activate Carousel
            \$(\"#myCarousel\").carousel();
    
            // Enable Carousel Indicators
            \$(\".item1\").click(function(){
                \$(\"#myCarousel\").carousel(0);
            });
            \$(\".item2\").click(function(){
                \$(\"#myCarousel\").carousel(1);
            });
            \$(\".item3\").click(function(){
                \$(\"#myCarousel\").carousel(2);
            });
    
            // Enable Carousel Controls
            \$(\".left\").click(function(){
                \$(\"#myCarousel\").carousel(\"prev\");
            });
            \$(\".right\").click(function(){
                \$(\"#myCarousel\").carousel(\"next\");
            });
        });
    </script>
";
        
        $__internal_47ca5c0ff04260d8ddce1189e64c7df4d09b863851bd6a451e20f080ea5e36f7->leave($__internal_47ca5c0ff04260d8ddce1189e64c7df4d09b863851bd6a451e20f080ea5e36f7_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  757 => 399,  739 => 381,  732 => 377,  726 => 374,  723 => 373,  715 => 368,  709 => 365,  706 => 364,  704 => 363,  694 => 356,  690 => 355,  674 => 341,  667 => 337,  661 => 334,  658 => 333,  651 => 329,  645 => 326,  642 => 325,  640 => 324,  629 => 316,  625 => 315,  609 => 301,  602 => 297,  596 => 294,  593 => 293,  586 => 289,  580 => 286,  577 => 285,  575 => 284,  566 => 278,  561 => 276,  554 => 271,  551 => 270,  548 => 269,  545 => 268,  542 => 267,  539 => 266,  537 => 265,  512 => 242,  505 => 238,  499 => 235,  496 => 234,  488 => 229,  482 => 226,  479 => 225,  477 => 224,  470 => 219,  467 => 218,  459 => 216,  453 => 215,  448 => 214,  446 => 213,  441 => 212,  439 => 211,  432 => 209,  416 => 195,  409 => 191,  403 => 188,  400 => 187,  393 => 183,  387 => 180,  384 => 179,  382 => 178,  375 => 173,  372 => 172,  363 => 170,  359 => 169,  354 => 168,  352 => 167,  347 => 166,  345 => 165,  338 => 163,  322 => 149,  315 => 145,  309 => 142,  306 => 141,  299 => 137,  293 => 134,  290 => 133,  288 => 132,  281 => 127,  278 => 126,  269 => 124,  265 => 123,  260 => 122,  258 => 121,  253 => 120,  251 => 119,  244 => 117,  237 => 112,  234 => 111,  231 => 110,  228 => 109,  225 => 108,  222 => 107,  219 => 106,  216 => 105,  213 => 104,  211 => 103,  195 => 92,  189 => 91,  183 => 90,  156 => 66,  152 => 65,  139 => 55,  135 => 54,  122 => 44,  118 => 43,  79 => 6,  73 => 5,  61 => 4,  49 => 3,  37 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block title %} FIMADILO - Welcome! {% endblock %}*/
/* {%block home%} class="active" {% endblock %}*/
/* {% block pagetitle %} Home {% endblock %}*/
/* {% block body %}*/
/* <style>*/
/* .crop {*/
/*   position: relative;*/
/*   width: 100px;*/
/*   height: 100px;*/
/*   overflow: hidden;*/
/* }*/
/* */
/* .crop img {*/
/*   position: absolute;*/
/*   left: 50%;*/
/*   top: 50%;*/
/*   height: 100%;*/
/*   width: 100%;*/
/*   -webkit-transform: translate(-50%,-50%);*/
/*       -ms-transform: translate(-50%,-50%);*/
/*           transform: translate(-50%,-50%);*/
/* }*/
/* </style>    */
/*     */
/* */
/*     <br/>*/
/*     */
/*     </br>*/
/*     <div id="myCarousel" class="carousel slide" data-ride="carousel">*/
/*     <!-- Indicators -->*/
/*     <ol class="carousel-indicators">*/
/*       <li class="item1 active"></li>*/
/*       <li class="item2"></li>*/
/*       <li class="item3"></li>*/
/*       <li class="item4"></li>*/
/*     </ol>*/
/* */
/*     <!-- Wrapper for slides -->*/
/*     <div class="carousel-inner" role="listbox">*/
/* */
/*       <div class="item active">*/
/*         <a href="{{path('doctorhomepage')}}">*/
/*         <img src="{{asset('images/carousel/Doctor.png')}}" alt="Doctor">*/
/*         <div class="carousel-caption" style="color: black;">*/
/*               <br/>*/
/*               <br/>*/
/*               Find the best doctors in Iloilo.*/
/*         </div>*/
/*         </a>*/
/*       </div>*/
/* */
/*       <div class="item">*/
/*         <a href="{{path('clinichomepage')}}">*/
/*         <img src="{{asset('images/carousel/Clinic.png')}}" alt="Clinic">*/
/*         <div class="carousel-caption" style="color: black;">*/
/*               <br/>*/
/*               <br/>*/
/*               Locate the clinics in Iloilo.*/
/*         </div>*/
/*         </a>*/
/*       </div>*/
/*     */
/*       <div class="item">*/
/*         <a href="{{path('clinichomepage')}}">*/
/*         <img src="{{asset('images/carousel/Illness.png')}}" alt="Illness">*/
/*         <div class="carousel-caption" style="color: black;">*/
/*               <br/>*/
/*               <br/>*/
/*               Know more about your illness.*/
/*         </div>*/
/*         </a>*/
/*       </div>*/
/*   */
/*     </div>*/
/* */
/*     <!-- Left and right controls -->*/
/*     <a class="left carousel-control" href="#myCarousel" role="button">*/
/*       <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>*/
/*       <span class="sr-only">Previous</span>*/
/*     </a>*/
/*     <a class="right carousel-control" href="#myCarousel" role="button">*/
/*       <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>*/
/*       <span class="sr-only">Next</span>*/
/*     </a>*/
/*   </div>*/
/*         <br/>*/
/*         <br/>*/
/*         <center>*/
/*         <a href="{{path('doctorhomepage')}}"><img src="{{asset('images/homepage/Doctor.png')}}"></a>*/
/*         <a href="{{path('clinichomepage')}}"><img src="{{asset('images/homepage/Clinic.png')}}"></a>*/
/*         <a href="{{path('illnesshomepage')}}"><img  src="{{asset('images/homepage/Illness.png')}}"></a>*/
/*         */
/*         </br>*/
/*         </br>*/
/*         <div style="height: 325px;">*/
/*         <h3></h3>*/
/*         <h3 style=" font-size: 16px;"><b><i class="fa fa-user-md"></i>&emsp;DOCTOR</b></h3>*/
/*         */
/*         <br/>*/
/*         <br/>*/
/*                        */
/*             {% set firstname1 = randomdoc1.firstName %}*/
/*             {% set lastname1 = randomdoc1.lastName %}*/
/*             {% set fop1 = randomdoc1.fieldOfPractice %}*/
/*             {% set firstname2 = randomdoc2.firstName %}*/
/*             {% set lastname2 = randomdoc2.lastName %}*/
/*             {% set fop2 = randomdoc2.fieldOfPractice %}*/
/*             {% set firstname3 = randomdoc3.firstName %}*/
/*             {% set lastname3 = randomdoc3.lastName %}*/
/*             {% set fop3 = randomdoc3.fieldOfPractice %}*/
/*                 */
/*             <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">*/
/*                         <div class="well profile_view">*/
/*                             <div class="col-sm-12">*/
/*                                     <div class="left col-xs-7">*/
/*                                         <i><h3><b>{{firstname1}} {{lastname1}}</b></h3></i>*/
/*                                         */
/*                                         {% if fop1 == "General"%}*/
/*                                              <h4>{{fop1}}</h4>*/
/*                                         {% elseif fop1 == "Specialized" %}*/
/*                                             <h4>{{fop1}}</h4>*/
/*                                             {% for specialty in randomdoc1.specialties | slice(0,1)%}*/
/*                                             <button class="btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="{{specialty.specialtyName}}">See Specialties</button>*/
/*                                             {% endfor %}*/
/*                                         {% endif %}*/
/*                                         */
/*                                     </div>*/
/*                                     <div class="right col-xs-5 text-center">*/
/*                                         */
/*                                         <br/>*/
/*                                         {% if randomdoc1.getWebPath == null%}*/
/*                                         <div class="crop">*/
/*                                         <img src="{{asset('gentelella/images/img.jpg')}}" alt="" class="img-circle img-responsive">*/
/*                                         </div>*/
/*                                         <br/>*/
/*                                         <a href="{{path('doctor_show', {'id': randomdoc1.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                         {% else %}*/
/*                                             <div class="crop">*/
/*                                             <img style="margin-bottom: 17px;" src="{{asset(randomdoc1.getWebPath)}}"alt="" class="img-circle img-responsive" height="42" width="42">*/
/*                                             </div>*/
/*                                             <br/>*/
/*                                             <a href="{{path('doctor_show', {'id': randomdoc1.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                         {% endif %}*/
/*                                         <br/>*/
/*                                         */
/*                                     </div>*/
/*                             </div>*/
/*                             <div class="col-xs-12 bottom text-center right">*/
/*                                 <div class="col-xs-12 col-sm-6 emphasis">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">*/
/*                         <div class="well profile_view">*/
/*                             <div class="col-sm-12">*/
/*                                     <div class="left col-xs-7">*/
/*                                         <i><h3><b>{{firstname2}} {{lastname2}}</b></h3></i>*/
/*                                         */
/*                                         {% if fop2 == "General"%}*/
/*                                              <h4>{{fop2}}</h4>*/
/*                                         {% elseif fop2 == "Specialized" %}*/
/*                                             <h4>{{fop2}}</h4>*/
/*                                             {% for specialty in randomdoc2.specialties | slice(0,1)%}*/
/*                                                 <button class="btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="{{specialty.specialtyName}}">See Specialties</button>*/
/*                                             {% endfor %}*/
/*                                         {% endif %}*/
/*                                         */
/*                                     </div>*/
/*                                     <div class="right col-xs-5 text-center">*/
/*                                         */
/*                                         <br/>*/
/*                                         {% if randomdoc2.getWebPath == null%}*/
/*                                         <div class="crop">*/
/*                                         <img src="{{asset('gentelella/images/img.jpg')}}" alt="" class="img-circle img-responsive">*/
/*                                         </div>*/
/*                                         <br/>*/
/*                                         <a href="{{path('doctor_show', {'id': randomdoc2.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                         {% else %}*/
/*                                             <div class="crop">*/
/*                                             <img style="margin-bottom: 17px;" src="{{asset(randomdoc2.getWebPath)}}"alt="" class="img-circle img-responsive" height="42" width="42">*/
/*                                             </div>*/
/*                                             <br/>*/
/*                                             <a href="{{path('doctor_show', {'id': randomdoc2.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                         {% endif %}*/
/*                                         <br/>*/
/*                                         */
/*                                     </div>*/
/*                             </div>*/
/*                             <div class="col-xs-12 bottom text-center right">*/
/*                                 <div class="col-xs-12 col-sm-6 emphasis">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">*/
/*                         <div class="well profile_view">*/
/*                             <div class="col-sm-12">*/
/*                                     <div class="left col-xs-7">*/
/*                                         <i><h3><b>{{firstname3}} {{lastname3}}</b></h3></i>*/
/*                                         */
/*                                         {% if fop3 == "General"%}*/
/*                                             <h4>{{fop3}}</h4>*/
/*                                         {% elseif fop3 == "Specialized" %}*/
/*                                             <h4>{{fop3}}</h4>*/
/*                                             {% for specialty in randomdoc3.specialties | slice(0, 1) %}              */
/*                                             <button class="btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="{{specialty.specialtyName}}">See Specialties</button>*/
/*                                             {% endfor %}*/
/*                                         {% endif %}*/
/*                                         */
/*                                     </div>*/
/*                                     <div class="right col-xs-5 text-center">*/
/*                                         */
/*                                         <br/>*/
/*                                         {% if randomdoc3.getWebPath == null%}*/
/*                                         <div class="crop">*/
/*                                         <img src="{{asset('gentelella/images/img.jpg')}}" alt="" class="img-circle img-responsive" height="42" width="42">*/
/*                                         </div>*/
/*                                         <br/>*/
/*                                         <a href="{{path('doctor_show', {'id': randomdoc3.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                                <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                             <br/>*/
/*                                         {% else %}*/
/*                                             <div class="crop">*/
/*                                             <img style="margin-bottom: 17px;" src="{{asset(randomdoc3.getWebPath)}}"alt="" class="img-circle img-responsive">*/
/*                                             </div>*/
/*                                             <br/>*/
/*                                             <a href="{{path('doctor_show', {'id': randomdoc3.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                                <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                                <br/>*/
/*                                         {% endif %}*/
/*                                         */
/*                                         */
/*                                             */
/*                                     </div>*/
/*                             </div>*/
/*                             <div class="col-xs-12 bottom text-center right">*/
/*                                 <div class="col-xs-12 col-sm-6 emphasis">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*         </div>*/
/*         */
/*         </br>    */
/*             */
/*                 */
/*              */
/*             <div style="height: 325px;">*/
/*         <h3></h3>*/
/*         <h3 style=" font-size: 16px;"><b><i class="fa fa-user-md"></i>&emsp;CLINIC</b></h3>*/
/*         */
/*         </br>*/
/*                        */
/*             {% set clinicname1 = randomcli1.clinicName %}*/
/*             {% set clinicadd1 = randomcli1.clinicAddress %}*/
/*             {% set clinicname2 = randomcli2.clinicName %}*/
/*             {% set clinicadd2 = randomcli2.clinicAddress %}*/
/*             {% set clinicname3 = randomcli3.clinicName %}*/
/*             {% set clinicadd3 = randomcli3.clinicAddress %}*/
/*                 */
/*             <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">*/
/*                         <div class="well profile_view">*/
/*                             <div class="col-sm-12">*/
/*                                     <div class="left col-xs-7">*/
/*                                         <i><h3><b>{{clinicname1}}</b></h3></i>*/
/*                                         */
/*                                         {{clinicadd1}}*/
/*                                         */
/*                                     </div>*/
/*                                     <div class="right col-xs-5 text-center">*/
/*                                         */
/*                                         <br/>*/
/*                                         {% if randomcli1.getWebPath == null%}*/
/*                                         <div class="crop">*/
/*                                         <img src="{{asset('gentelella/images/img.jpg')}}" alt="" class="img-circle img-responsive">*/
/*                                         </div>*/
/*                                         <br/>*/
/*                                         <a href="{{path('clinic_show', {'id': randomcli1.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                         {% else %}*/
/*                                             <div class="crop">*/
/*                                             <img style="margin-bottom: 17px;" src="{{asset(randomcli1.getWebPath)}}"alt="" class="img-circle img-responsive">*/
/*                                             </div>*/
/*                                             <br/>*/
/*                                             <a href="{{path('clinic_show', {'id': randomcli1.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br>*/
/*                                         {% endif %}*/
/*                                         <br/>*/
/*                                         */
/*                                     </div>*/
/*                             </div>*/
/*                             <div class="col-xs-12 bottom text-center right">*/
/*                                 <div class="col-xs-12 col-sm-6 emphasis">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">*/
/*                         <div class="well profile_view">*/
/*                             <div class="col-sm-12">*/
/*                                     <div class="left col-xs-7">*/
/*                                         <i><h3><b>{{clinicname2}}</b></h3></i>*/
/*                                         {{clinicadd2}}*/
/*                                         */
/*                                         */
/*                                         */
/*                                     </div>*/
/*                                     <div class="right col-xs-5 text-center">*/
/*                                         */
/*                                         <br/>*/
/*                                         {% if randomcli2.getWebPath == null%}*/
/*                                         <div class="crop">*/
/*                                         <img src="{{asset('gentelella/images/img.jpg')}}" alt="" class="img-circle img-responsive">*/
/*                                         </div>*/
/*                                         <br/>*/
/*                                         <a href="{{path('clinic_show', {'id': randomcli2.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                         {% else %}*/
/*                                             <div class="crop">*/
/*                                             <img style="margin-bottom: 17px;" src="{{asset(randomcli2.getWebPath)}}"alt="" class="img-circle img-responsive">*/
/*                                             </div>*/
/*                                             <br/>*/
/*                                             <a href="{{path('clinic_show', {'id': randomcli2.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br>*/
/*                                         {% endif %}*/
/*                                         <br/>*/
/*                                         */
/*                                     </div>*/
/*                             </div>*/
/*                             <div class="col-xs-12 bottom text-center right">*/
/*                                 <div class="col-xs-12 col-sm-6 emphasis">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">*/
/*                         <div class="well profile_view">*/
/*                             <div class="col-sm-12">*/
/*                                     <div class="left col-xs-7">*/
/*                                         <i><h3><b>{{clinicname3}}</b></h3></i>*/
/*                                         {{clinicadd3}}*/
/*                                        */
/*                                         */
/*                                     </div>*/
/*                                     <div class="right col-xs-5 text-center">*/
/*                                         */
/*                                         <br/>*/
/*                                         {% if randomcli3.getWebPath == null%}*/
/*                                         <div class="crop">*/
/*                                         <img src="{{asset('gentelella/images/img.jpg')}}" alt="" class="img-circle img-responsive">*/
/*                                         </div>*/
/*                                         <br/>*/
/*                                         <a href="{{path('clinic_show', {'id': randomcli3.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                                <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                             <br/>*/
/*                                         {% else %}*/
/*                                             <div class="crop">*/
/*                                             <img style="margin-bottom: 17px;" src="{{asset(randomcli3.getWebPath)}}"alt="" class="img-circle img-responsive">*/
/*                                             </div>*/
/*                                             <br/>*/
/*                                             <a href="{{path('clinic_show', {'id': randomcli3.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                                <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br>*/
/*                                         {% endif %}*/
/*                                         */
/*                                         */
/*                                             */
/*                                     </div>*/
/*                             </div>*/
/*                             <div class="col-xs-12 bottom text-center right">*/
/*                                 <div class="col-xs-12 col-sm-6 emphasis">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*         </div>*/
/*         */
/*         </br>*/
/*         */
/*             */
/*         {#<button type="button" class="btn btn-primary btn-lg"><a href="{{ path('clinic_new') }}"></a>Register New Clinic</button>*/
/*         <button type="button" class="btn btn-success btn-lg"><a href="{{ path('doctor_group') }}"></a>Register New Group</button>#}*/
/*         <br/>*/
/*         </center>*/
/*         */
/*         <script>*/
/*             $(document).ready(function(){*/
/*             // Activate Carousel*/
/*             $("#myCarousel").carousel();*/
/*     */
/*             // Enable Carousel Indicators*/
/*             $(".item1").click(function(){*/
/*                 $("#myCarousel").carousel(0);*/
/*             });*/
/*             $(".item2").click(function(){*/
/*                 $("#myCarousel").carousel(1);*/
/*             });*/
/*             $(".item3").click(function(){*/
/*                 $("#myCarousel").carousel(2);*/
/*             });*/
/*     */
/*             // Enable Carousel Controls*/
/*             $(".left").click(function(){*/
/*                 $("#myCarousel").carousel("prev");*/
/*             });*/
/*             $(".right").click(function(){*/
/*                 $("#myCarousel").carousel("next");*/
/*             });*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
