<?php

/* ResearchProjectMyProjectBundle:Admin:deleteFellowship.html.twig */
class __TwigTemplate_ea2ae73d0e0571f0807b47060ae18478f4d7f501dd88cf1cf4a77654fcc5e33f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:deleteFellowship.html.twig", 1);
        $this->blocks = array(
            'pagetitle' => array($this, 'block_pagetitle'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c862fcaa3937ea97fc0b6390e021352a12b35654cdf2be42c090e2d3a689020f = $this->env->getExtension("native_profiler");
        $__internal_c862fcaa3937ea97fc0b6390e021352a12b35654cdf2be42c090e2d3a689020f->enter($__internal_c862fcaa3937ea97fc0b6390e021352a12b35654cdf2be42c090e2d3a689020f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:deleteFellowship.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c862fcaa3937ea97fc0b6390e021352a12b35654cdf2be42c090e2d3a689020f->leave($__internal_c862fcaa3937ea97fc0b6390e021352a12b35654cdf2be42c090e2d3a689020f_prof);

    }

    // line 2
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_6406dcdc22cdbe6fae0d491b909380adc1acb90cf9d9720ec4e653f615f969b4 = $this->env->getExtension("native_profiler");
        $__internal_6406dcdc22cdbe6fae0d491b909380adc1acb90cf9d9720ec4e653f615f969b4->enter($__internal_6406dcdc22cdbe6fae0d491b909380adc1acb90cf9d9720ec4e653f615f969b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "Delete Fellowship";
        
        $__internal_6406dcdc22cdbe6fae0d491b909380adc1acb90cf9d9720ec4e653f615f969b4->leave($__internal_6406dcdc22cdbe6fae0d491b909380adc1acb90cf9d9720ec4e653f615f969b4_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3c2ace966f528b252ed948c248daa2387dc849076c9f6c494de7d3b24b2f81dd = $this->env->getExtension("native_profiler");
        $__internal_3c2ace966f528b252ed948c248daa2387dc849076c9f6c494de7d3b24b2f81dd->enter($__internal_3c2ace966f528b252ed948c248daa2387dc849076c9f6c494de7d3b24b2f81dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                    
                        <h3>Delete Fellowship</h3>
                       
                    <div class=\"x_content\">
                        <input class=\"form-control\" id='search' type=\"text\" name=\"query\" placeholder=\"Type a name\" />
                         
                                                    <center><img hidden id=\"loading\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("charisma/img/ajax-loaders/ajax-loader-1.gif"), "html", null, true);
        echo "\" ></center>
                                                    <table class=\"table table-striped\" id=\"resulttable\" class=\"hidden\">

                                                        <tbody id=\"resultbody\">

                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!-- form validation -->
<script>
                                    var deleteBtnId;
                                    \$(document).on(\"click\", \".btn-remove\", function(x){
                                         x.preventDefault();
                                        deleteBtnId = this.id;
                                        deleteBtnId = deleteBtnId.replace('remove-', '');
                                         \$.ajax({
                                            type: \"POST\",
                                            url: '";
        // line 44
        echo $this->env->getExtension('routing')->getPath("admin_delete_fellowship_click");
        echo "',
                                            data: {id: deleteBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                               location.reload();
                                                console.log(response);
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    });
    
   </script>
    <script>
\$('#search').keyup(function() {
    
     searchText = \$(this).val();
     if (this.value != ''){
         \$(\"#resultbody\").empty();
     }
     
     
     if (this.value.length >= 3 && this.value != '')
  {
     \$.ajax({
        type: \"POST\",
        dataType: \"json\",
        url: '";
        // line 72
        echo $this->env->getExtension('routing')->getPath("admin_delete_fellowship");
        echo "',
        data: {searchText : searchText},
        beforeSend: function(){
        \$('#loading').show();
        },
        complete: function(){
        \$('#loading').hide();
        },
        success : function(data) 
           
           {if(data){
                  
                \$(\"#resultbody\").empty();
                var len = data.length;
                var txt = \"\";
                  if(len > 0){
                    for(var i=0;i<len;i++){
                            //var button= \$('<button type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\"></i> Invite to Group </button>');
                            var entityId = data[i].id;
                            var showPath = '";
        // line 91
        echo $this->env->getExtension('routing')->getPath("doctor_show", array("id" => "entityId"));
        echo "';
                            showPath = showPath.replace(\"entityId\", data[i].id);
                            txt += \"<tr><td>\"+data[i].fellowshipName+\"</td>\";
                            txt+= '<td><a href=\"#\" ><button id=\"remove-'+ data[i].id+'\" type=\"button\" class=\"btn btn-danger btn-xs btn-remove\"> <i class=\"fa fa-envelope\"></i> Remove Specialty </button>';
                        
                    }
                    if(txt != \"\"){
                        \$(\"#resultbody\").append(txt).removeClass(\"hidden\");
                         
                    }
                }
            }
        },error: function() {
         alert('error');
          }
        });
    }
});
       
    </script>
    ";
        
        $__internal_3c2ace966f528b252ed948c248daa2387dc849076c9f6c494de7d3b24b2f81dd->leave($__internal_3c2ace966f528b252ed948c248daa2387dc849076c9f6c494de7d3b24b2f81dd_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:deleteFellowship.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 91,  129 => 72,  98 => 44,  70 => 19,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% block pagetitle %}Delete Fellowship{% endblock %}*/
/* {% block body -%}*/
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 */
/*             </div>*/
/*         </div>*/
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                     */
/*                         <h3>Delete Fellowship</h3>*/
/*                        */
/*                     <div class="x_content">*/
/*                         <input class="form-control" id='search' type="text" name="query" placeholder="Type a name" />*/
/*                          */
/*                                                     <center><img hidden id="loading" src="{{asset('charisma/img/ajax-loaders/ajax-loader-1.gif')}}" ></center>*/
/*                                                     <table class="table table-striped" id="resulttable" class="hidden">*/
/* */
/*                                                         <tbody id="resultbody">*/
/* */
/*                                                             </tr>*/
/*                                                         </tbody>*/
/*                                                     </table>*/
/* */
/*                                                 </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     */
/* <!-- form validation -->*/
/* <script>*/
/*                                     var deleteBtnId;*/
/*                                     $(document).on("click", ".btn-remove", function(x){*/
/*                                          x.preventDefault();*/
/*                                         deleteBtnId = this.id;*/
/*                                         deleteBtnId = deleteBtnId.replace('remove-', '');*/
/*                                          $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('admin_delete_fellowship_click') }}',*/
/*                                             data: {id: deleteBtnId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                location.reload();*/
/*                                                 console.log(response);*/
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*     */
/*    </script>*/
/*     <script>*/
/* $('#search').keyup(function() {*/
/*     */
/*      searchText = $(this).val();*/
/*      if (this.value != ''){*/
/*          $("#resultbody").empty();*/
/*      }*/
/*      */
/*      */
/*      if (this.value.length >= 3 && this.value != '')*/
/*   {*/
/*      $.ajax({*/
/*         type: "POST",*/
/*         dataType: "json",*/
/*         url: '{{ path('admin_delete_fellowship') }}',*/
/*         data: {searchText : searchText},*/
/*         beforeSend: function(){*/
/*         $('#loading').show();*/
/*         },*/
/*         complete: function(){*/
/*         $('#loading').hide();*/
/*         },*/
/*         success : function(data) */
/*            */
/*            {if(data){*/
/*                   */
/*                 $("#resultbody").empty();*/
/*                 var len = data.length;*/
/*                 var txt = "";*/
/*                   if(len > 0){*/
/*                     for(var i=0;i<len;i++){*/
/*                             //var button= $('<button type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user"></i> Invite to Group </button>');*/
/*                             var entityId = data[i].id;*/
/*                             var showPath = '{{ path("doctor_show", {'id' : 'entityId' })}}';*/
/*                             showPath = showPath.replace("entityId", data[i].id);*/
/*                             txt += "<tr><td>"+data[i].fellowshipName+"</td>";*/
/*                             txt+= '<td><a href="#" ><button id="remove-'+ data[i].id+'" type="button" class="btn btn-danger btn-xs btn-remove"> <i class="fa fa-envelope"></i> Remove Specialty </button>';*/
/*                         */
/*                     }*/
/*                     if(txt != ""){*/
/*                         $("#resultbody").append(txt).removeClass("hidden");*/
/*                          */
/*                     }*/
/*                 }*/
/*             }*/
/*         },error: function() {*/
/*          alert('error');*/
/*           }*/
/*         });*/
/*     }*/
/* });*/
/*        */
/*     </script>*/
/*     {% endblock %}*/
/* */
