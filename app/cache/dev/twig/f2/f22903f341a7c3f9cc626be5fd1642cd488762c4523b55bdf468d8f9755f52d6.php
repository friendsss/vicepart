<?php

/* ResearchProjectMyProjectBundle:Admin:editDoctor.html.twig */
class __TwigTemplate_6d6595f393e3a565306a32edfe70b2b5d82b850e92fdce491ca6a4206fddbe8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::admin.html.twig", "ResearchProjectMyProjectBundle:Admin:editDoctor.html.twig", 1);
        $this->blocks = array(
            'pagetitle' => array($this, 'block_pagetitle'),
            'doctor' => array($this, 'block_doctor'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22b49ae60f0902d013ce3b1f55e3bdbee427b1f1bd87f62487939dba56bb795a = $this->env->getExtension("native_profiler");
        $__internal_22b49ae60f0902d013ce3b1f55e3bdbee427b1f1bd87f62487939dba56bb795a->enter($__internal_22b49ae60f0902d013ce3b1f55e3bdbee427b1f1bd87f62487939dba56bb795a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Admin:editDoctor.html.twig"));

        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), array(0 => "ResearchProjectMyProjectBundle:Form:field.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_22b49ae60f0902d013ce3b1f55e3bdbee427b1f1bd87f62487939dba56bb795a->leave($__internal_22b49ae60f0902d013ce3b1f55e3bdbee427b1f1bd87f62487939dba56bb795a_prof);

    }

    // line 3
    public function block_pagetitle($context, array $blocks = array())
    {
        $__internal_b7aec7ce18cf86286148e1fc8a7b0657a3d0bc11e87702473cc6b95d8727f008 = $this->env->getExtension("native_profiler");
        $__internal_b7aec7ce18cf86286148e1fc8a7b0657a3d0bc11e87702473cc6b95d8727f008->enter($__internal_b7aec7ce18cf86286148e1fc8a7b0657a3d0bc11e87702473cc6b95d8727f008_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagetitle"));

        echo "DOCTOR";
        
        $__internal_b7aec7ce18cf86286148e1fc8a7b0657a3d0bc11e87702473cc6b95d8727f008->leave($__internal_b7aec7ce18cf86286148e1fc8a7b0657a3d0bc11e87702473cc6b95d8727f008_prof);

    }

    // line 4
    public function block_doctor($context, array $blocks = array())
    {
        $__internal_a519af222e4b216ce056c3176bb0962c9bbb6d3664d335ce36758b3a196830ce = $this->env->getExtension("native_profiler");
        $__internal_a519af222e4b216ce056c3176bb0962c9bbb6d3664d335ce36758b3a196830ce->enter($__internal_a519af222e4b216ce056c3176bb0962c9bbb6d3664d335ce36758b3a196830ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctor"));

        echo " class=\"active\" ";
        
        $__internal_a519af222e4b216ce056c3176bb0962c9bbb6d3664d335ce36758b3a196830ce->leave($__internal_a519af222e4b216ce056c3176bb0962c9bbb6d3664d335ce36758b3a196830ce_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_d22d3973b70da3dc1b5cccea950315738f9d86265c4d749240aee02e4f8e5dfa = $this->env->getExtension("native_profiler");
        $__internal_d22d3973b70da3dc1b5cccea950315738f9d86265c4d749240aee02e4f8e5dfa->enter($__internal_d22d3973b70da3dc1b5cccea950315738f9d86265c4d749240aee02e4f8e5dfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<link href=";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/select/select2.min.css"), "html", null, true);
        echo " rel=\"stylesheet\">
<center><h1><b>Edit Doctor Information</b></h1></center>
    <div class=\"x_content\">
        <br />
        <div id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">
            ";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "editForm")));
        echo "
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >First Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "firstName", array()), 'widget', array("attr" => array("id" => "firstName", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 17
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "firstName", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Last Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "lastName", array()), 'widget', array("attr" => array("name" => "lastName", "placeholder" => "Last Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "text")));
        // line 26
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "lastName", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Middle Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "middleName", array()), 'widget', array("attr" => array("name" => "middleName", "placeholder" => "Middle Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "text")));
        // line 35
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "middleName", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Email Address <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "emailAddress", array()), 'widget', array("attr" => array("name" => "emailAddress", "placeholder" => "Email Address", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "2", "required" => "required", "type" => "email")));
        // line 44
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "emailAddress", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group \">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Username <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "username", array()), 'widget', array("attr" => array("name" => "username", "placeholder" => "Username", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "6", "required" => "required", "type" => "text")));
        // line 53
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "username", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Date of Birth <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                   ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "dateOfBirth", array()), 'widget', array("attr" => array("name" => "dateOfBirth", "placeholder" => "Date of Birth", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "6", "required" => "required", "type" => "password")));
        // line 62
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "dateOfBirth", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Field of Practice <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "fieldOfPractice", array()), 'widget', array("attr" => array("name" => "fieldOfPractice", "placeholder" => "Field of Practice", "class" => "form-control col-md-7 col-xs-12")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "fieldOfPractice", array()), 'errors');
        echo "</div></i>
            <div id =\"specialtyblock\" class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Specialty <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "specialties", array()), 'widget', array("attr" => array("name" => "specialty", "placeholder" => "Specialty", "class" => "select2_multiple form-control")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "specialties", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Contact Number <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "contactNumber", array()), 'widget', array("attr" => array("id" => "cnum", "name" => "cnum", "placeholder" => "ContactNumber", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 85
        echo "
                </div>
            </div><div class=\"item form-group\" id=\"fellowshipblock\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Fellowship/s <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "fellowships", array()), 'widget', array("attr" => array("name" => "fellowships", "class" => "select2_multiple form-control", "required" => "required")));
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "fellowships", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">PRC Number <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "prcNumber", array()), 'widget', array("attr" => array("name" => "prcNumber", "placeholder" => "PRC Number", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "7,7", "required" => "required", "type" => "number")));
        // line 99
        echo "  
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "prcNumber", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Original Issue Date <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                   ";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "origIssueDate", array()), 'widget', array("attr" => array("name" => "origIssueDate", "placeholder" => "Original Issue Date:", "class" => "form-control col-md-7 col-xs-12")));
        echo "
                </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 110
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "origIssueDate", array()), 'errors');
        echo "</div></i>
            <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">About My Practice <span class=\"required\">*</span></label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        ";
        // line 114
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "aboutMyPractice", array()), 'widget', array("attr" => array("name" => "aboutMyPractice", "placeholder" => "About My Practice", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "6", "required" => "required", "type" => "text")));
        // line 115
        echo "
                    </div>
            </div>
                <i><div style = \"color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;\" >";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "aboutMyPractice", array()), 'errors');
        echo "</div></i>
             <div class=\"item form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >First Name <span class=\"required\">*</span>
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 123
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "file", array()), 'widget');
        echo "
                </div>
            </div>   
            <div class=\"ln_solid\"></div>
            
            <div class =\"form-group\">
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <a href=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\"> ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info", "id" => "submitbtn")));
        echo "</a>
                    <button class=\"btn btn-dark\" onclick=\"goBack()\">Cancel</button>
                </div>
            </div>
                
            ";
        // line 135
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
            </div>
    </div>
    
    <div id=\"custom_notifications\" class=\"custom-notifications dsp_none\">
        <ul class=\"list-unstyled notifications clearfix\" data-tabbed_notifications=\"notif-group\">
        </ul>
        <div class=\"clearfix\"></div>
        <div id=\"notif-group\" class=\"tabbed_notifications\"></div>
    </div>

    <script>
        function goBack() {
        window.history.back();
        }
    </script>
    <!-- icheck -->
    <script src=";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "></script>

    <script src=";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "></script>
    <!-- form validation -->
    <script src=";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/validator/validator.js"), "html", null, true);
        echo "></script>
     <script src=";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/select/select2.full.js"), "html", null, true);
        echo "></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on \"blur\" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        \$('#editForm')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            

        // bind the validation to the form submit event
        //\$('#send').click('submit');//.prop('disabled', true);

        \$('#editForm').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll(\$(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script type=\"text/javascript\">
            var choice = document.getElementById(\"researchproject_myprojectbundle_doctor_fieldOfPractice\");
            choice.onchange=newChoice;
            function newChoice()
            {   
                var choice = document.getElementById(\"researchproject_myprojectbundle_doctor_fieldOfPractice\");
                var selectedValue = choice.options[choice.selectedIndex].value;


                if (selectedValue == \"Specialized\")
                {   document.getElementById(\"specialtyblock\").style.display = \"block\";
                }
                else
                {
                   document.getElementById(\"specialtyblock\").style.display = \"none\";
                }
            }
    </script>
     <script>
            \$(document).ready(function () {
                \$(\".select2_single\").select2({
                    allowClear: true
                });
                \$(\".select2_group\").select2({});
                \$(\".select2_multiple\").select2({
                    allowClear: true
                });
                
                
            });
        </script>

";
        
        $__internal_d22d3973b70da3dc1b5cccea950315738f9d86265c4d749240aee02e4f8e5dfa->leave($__internal_d22d3973b70da3dc1b5cccea950315738f9d86265c4d749240aee02e4f8e5dfa_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Admin:editDoctor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  322 => 157,  318 => 156,  313 => 154,  308 => 152,  288 => 135,  278 => 130,  268 => 123,  260 => 118,  255 => 115,  253 => 114,  246 => 110,  240 => 107,  232 => 102,  227 => 99,  225 => 98,  217 => 93,  211 => 90,  204 => 85,  202 => 84,  194 => 79,  188 => 76,  181 => 72,  175 => 69,  168 => 65,  163 => 62,  161 => 61,  153 => 56,  148 => 53,  146 => 52,  138 => 47,  133 => 44,  131 => 43,  123 => 38,  118 => 35,  116 => 34,  108 => 29,  103 => 26,  101 => 25,  93 => 20,  88 => 17,  86 => 16,  78 => 11,  69 => 6,  63 => 5,  51 => 4,  39 => 3,  32 => 1,  30 => 2,  11 => 1,);
    }
}
/* {% extends '::admin.html.twig' %}*/
/* {% form_theme edit_form 'ResearchProjectMyProjectBundle:Form:field.html.twig' %}*/
/* {% block pagetitle %}DOCTOR{% endblock %}*/
/* {%block doctor%} class="active" {% endblock %}*/
/* {% block body -%}*/
/*  <link href={{asset('gentelella/css/select/select2.min.css')}} rel="stylesheet">*/
/* <center><h1><b>Edit Doctor Information</b></h1></center>*/
/*     <div class="x_content">*/
/*         <br />*/
/*         <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">*/
/*             {{ form_start(edit_form, {'attr' : {  'role' : 'form', 'novalidate' : 'novalidate', 'id':'editForm' } }) }}*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >First Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.firstName, { 'attr' : { 'id': 'firstName', 'name': 'firstName', 'placeholder' : 'First Name', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '1',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.firstName)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Last Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.lastName, { 'attr' : { 'name': 'lastName', 'placeholder' : 'Last Name', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                    'data-validate-length-range' : '2', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.lastName)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Middle Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.middleName, { 'attr' : { 'name': 'middleName', 'placeholder' : 'Middle Name', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                      'data-validate-length-range' : '2', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.middleName)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Address <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.emailAddress, { 'attr' : { 'name': 'emailAddress', 'placeholder' : 'Email Address', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                      'data-validate-length-range' : '2', 'required': 'required', 'type' : 'email' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.emailAddress)}}</div></i>*/
/*             <div class="item form-group ">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Username <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.username, { 'attr' : { 'name': 'username', 'placeholder' : 'Username', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '6', 'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.username)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                    {{  form_widget(edit_form.dateOfBirth, { 'attr' : { 'name': 'dateOfBirth', 'placeholder' : 'Date of Birth', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                         'data-validate-length-range' : '6',  'required': 'required', 'type' : 'password' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.dateOfBirth)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Field of Practice <span class="required">*</span></label>*/
/*                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                         {{  form_widget(edit_form.fieldOfPractice, { 'attr' : { 'name': 'fieldOfPractice', 'placeholder' : 'Field of Practice', 'class' : 'form-control col-md-7 col-xs-12'} }) }}*/
/*                     </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.fieldOfPractice)}}</div></i>*/
/*             <div id ="specialtyblock" class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Specialty <span class="required">*</span></label>*/
/*                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                         {{  form_widget(edit_form.specialties, { 'attr' : { 'name': 'specialty', 'placeholder' : 'Specialty', 'class' :'select2_multiple form-control'} }) }}*/
/*                     </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.specialties)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >Contact Number <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.contactNumber, { 'attr' : { 'id': 'cnum', 'name': 'cnum', 'placeholder' : 'ContactNumber', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                     'data-validate-length-range' : '1',  'required': 'required', 'type' : 'text' } }) }}*/
/*                 </div>*/
/*             </div><div class="item form-group" id="fellowshipblock">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Fellowship/s <span class="required">*</span></label>*/
/*                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                         {{  form_widget(edit_form.fellowships, { 'attr' : { 'name': 'fellowships', 'class' :'select2_multiple form-control', 'required':'required'} }) }}*/
/*                     </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.fellowships)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">PRC Number <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{  form_widget(edit_form.prcNumber, { 'attr' : { 'name': 'prcNumber', 'placeholder' : 'PRC Number', 'class' : 'form-control col-md-7 col-xs-12', */
/*                                                        'data-validate-length-range' : '7,7',  'required': 'required', 'type' : 'number' } }) }}  */
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.prcNumber)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Original Issue Date <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                    {{  form_widget(edit_form.origIssueDate, { 'attr' : { 'name': 'origIssueDate', 'placeholder' : 'Original Issue Date:', 'class' : 'form-control col-md-7 col-xs-12' } }) }}*/
/*                 </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.origIssueDate)}}</div></i>*/
/*             <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12">About My Practice <span class="required">*</span></label>*/
/*                     <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                         {{  form_widget(edit_form.aboutMyPractice, { 'attr' : { 'name': 'aboutMyPractice', 'placeholder' : 'About My Practice', 'class' : 'form-control col-md-7 col-xs-12',*/
/*                                                                  'data-validate-length-range' : '6',  'required': 'required', 'type' : 'text' } }) }}*/
/*                     </div>*/
/*             </div>*/
/*                 <i><div style = "color:#F0516C; font-family: Calibri; font-size: 16px; list-style-type: none;" >{{form_errors(edit_form.aboutMyPractice)}}</div></i>*/
/*              <div class="item form-group">*/
/*                 <label class="control-label col-md-3 col-sm-3 col-xs-12" >First Name <span class="required">*</span>*/
/*                 </label>*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12">*/
/*                     {{ form_widget(edit_form.file) }}*/
/*                 </div>*/
/*             </div>   */
/*             <div class="ln_solid"></div>*/
/*             */
/*             <div class ="form-group">*/
/*                 <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">*/
/*                     <a href="{{ path('doctor_show', {'id': entity.id})}}"> {{ form_widget(edit_form.submit, { 'attr' : { 'class' : 'btn btn-info', 'id' : 'submitbtn'} }) }}</a>*/
/*                     <button class="btn btn-dark" onclick="goBack()">Cancel</button>*/
/*                 </div>*/
/*             </div>*/
/*                 */
/*             {{ form_end(edit_form)}}*/
/*             </div>*/
/*     </div>*/
/*     */
/*     <div id="custom_notifications" class="custom-notifications dsp_none">*/
/*         <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">*/
/*         </ul>*/
/*         <div class="clearfix"></div>*/
/*         <div id="notif-group" class="tabbed_notifications"></div>*/
/*     </div>*/
/* */
/*     <script>*/
/*         function goBack() {*/
/*         window.history.back();*/
/*         }*/
/*     </script>*/
/*     <!-- icheck -->*/
/*     <script src={{asset('gentelella/js/icheck/icheck.min.js')}}></script>*/
/* */
/*     <script src={{asset('gentelella/js/custom.js')}}></script>*/
/*     <!-- form validation -->*/
/*     <script src={{asset('gentelella/js/validator/validator.js')}}></script>*/
/*      <script src={{asset('gentelella/js/select/select2.full.js')}}></script>*/
/*     <script>*/
/*         // initialize the validator function*/
/*         validator.message['date'] = 'not a real date';*/
/* */
/*         // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':*/
/*         $('#editForm')*/
/*             .on('blur', 'input[required], input.optional, select.required', validator.checkField)*/
/*             .on('change', 'select.required', validator.checkField)*/
/*             */
/* */
/*         // bind the validation to the form submit event*/
/*         //$('#send').click('submit');//.prop('disabled', true);*/
/* */
/*         $('#editForm').submit(function (e) {*/
/*             e.preventDefault();*/
/*             var submit = true;*/
/*             // evaluate the form using generic validaing*/
/*             if (!validator.checkAll($(this))) {*/
/*                 submit = false;*/
/*             }*/
/* */
/*             if (submit)*/
/*                 this.submit();*/
/*             return false;*/
/*         });*/
/*     </script>*/
/*     <script type="text/javascript">*/
/*             var choice = document.getElementById("researchproject_myprojectbundle_doctor_fieldOfPractice");*/
/*             choice.onchange=newChoice;*/
/*             function newChoice()*/
/*             {   */
/*                 var choice = document.getElementById("researchproject_myprojectbundle_doctor_fieldOfPractice");*/
/*                 var selectedValue = choice.options[choice.selectedIndex].value;*/
/* */
/* */
/*                 if (selectedValue == "Specialized")*/
/*                 {   document.getElementById("specialtyblock").style.display = "block";*/
/*                 }*/
/*                 else*/
/*                 {*/
/*                    document.getElementById("specialtyblock").style.display = "none";*/
/*                 }*/
/*             }*/
/*     </script>*/
/*      <script>*/
/*             $(document).ready(function () {*/
/*                 $(".select2_single").select2({*/
/*                     allowClear: true*/
/*                 });*/
/*                 $(".select2_group").select2({});*/
/*                 $(".select2_multiple").select2({*/
/*                     allowClear: true*/
/*                 });*/
/*                 */
/*                 */
/*             });*/
/*         </script>*/
/* */
/* {% endblock %}*/
/* */
