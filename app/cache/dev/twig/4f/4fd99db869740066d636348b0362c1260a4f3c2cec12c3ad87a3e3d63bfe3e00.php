<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_6ad1d5619978d643fc2067a971d46c3a49c44bc7129d453e266f48f27adff0cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f83c0769b52e51b71c13255fed5a8557d2d08777457fc9feab35eb0538182708 = $this->env->getExtension("native_profiler");
        $__internal_f83c0769b52e51b71c13255fed5a8557d2d08777457fc9feab35eb0538182708->enter($__internal_f83c0769b52e51b71c13255fed5a8557d2d08777457fc9feab35eb0538182708_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f83c0769b52e51b71c13255fed5a8557d2d08777457fc9feab35eb0538182708->leave($__internal_f83c0769b52e51b71c13255fed5a8557d2d08777457fc9feab35eb0538182708_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3bbf5852f1ff5c957c631cbfff249a44f6386b7153351b8732182df73b302d53 = $this->env->getExtension("native_profiler");
        $__internal_3bbf5852f1ff5c957c631cbfff249a44f6386b7153351b8732182df73b302d53->enter($__internal_3bbf5852f1ff5c957c631cbfff249a44f6386b7153351b8732182df73b302d53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_3bbf5852f1ff5c957c631cbfff249a44f6386b7153351b8732182df73b302d53->leave($__internal_3bbf5852f1ff5c957c631cbfff249a44f6386b7153351b8732182df73b302d53_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_0a1627736687264537e43321e9efcc77abe2f3f6ad50ee1de688783b51a2ec28 = $this->env->getExtension("native_profiler");
        $__internal_0a1627736687264537e43321e9efcc77abe2f3f6ad50ee1de688783b51a2ec28->enter($__internal_0a1627736687264537e43321e9efcc77abe2f3f6ad50ee1de688783b51a2ec28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_0a1627736687264537e43321e9efcc77abe2f3f6ad50ee1de688783b51a2ec28->leave($__internal_0a1627736687264537e43321e9efcc77abe2f3f6ad50ee1de688783b51a2ec28_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_43b1217cce39ca935fe608b0308b6e39cfe09e46b39f438303d2304b582f9028 = $this->env->getExtension("native_profiler");
        $__internal_43b1217cce39ca935fe608b0308b6e39cfe09e46b39f438303d2304b582f9028->enter($__internal_43b1217cce39ca935fe608b0308b6e39cfe09e46b39f438303d2304b582f9028_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_43b1217cce39ca935fe608b0308b6e39cfe09e46b39f438303d2304b582f9028->leave($__internal_43b1217cce39ca935fe608b0308b6e39cfe09e46b39f438303d2304b582f9028_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
