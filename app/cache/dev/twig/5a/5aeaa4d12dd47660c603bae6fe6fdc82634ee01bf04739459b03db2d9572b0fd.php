<?php

/* ::basenosidebar.html.twig */
class __TwigTemplate_ad7c5ee97beebf5f683f21dc7ad67a8777970fcd0b44cd8d6a9fb7e2853e63b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'home' => array($this, 'block_home'),
            'profile' => array($this, 'block_profile'),
            'doctor' => array($this, 'block_doctor'),
            'clinic' => array($this, 'block_clinic'),
            'illness' => array($this, 'block_illness'),
            'doctorgroup' => array($this, 'block_doctorgroup'),
            'settings' => array($this, 'block_settings'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_05a5be56e1820035f3ddc4bea2a62ab499854be945f844f30c0c7a4683482027 = $this->env->getExtension("native_profiler");
        $__internal_05a5be56e1820035f3ddc4bea2a62ab499854be945f844f30c0c7a4683482027->enter($__internal_05a5be56e1820035f3ddc4bea2a62ab499854be945f844f30c0c7a4683482027_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::basenosidebar.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
<head>
\t<meta charset=\"utf-8\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\" />
\t<title>Find Me A Doctor Iloilo</title>
\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("retina/default/css/images/favicon.ico"), "html", null, true);
        echo "\" />
\t
\t<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>
\t<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("retina/default/js/jquery-1.8.0.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<!--[if lt IE 9]>
\t\t<script src=\"js/modernizr.custom.js\"></script>
\t<![endif]-->
\t<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("retina/default/js/jquery.carouFredSel-5.5.0-packed.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
\t<script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("retina/default/js/functions.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <!-- Bootstrap core CSS -->
 <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/jquery-ui.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
     <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/jquery-ui.theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
     <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/jquery-ui/jquery-ui.structure.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/formerrornotify.css"), "html", null, true);
        echo " rel=\"stylesheet\">
    <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/fonts/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom styling plus plugins -->
    <link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/maps/jquery-jvectormap-2.0.1.css"), "html", null, true);
        echo "\" />
    <link href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/icheck/flat/green.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/floatexamples.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

    <script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/nprogress.js"), "html", null, true);
        echo "\"></script>
    <link rel=\"stylesheet\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("retina/default/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\" />
    <link href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
 <link href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/css/animate.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

</head>
<body>
<!-- wrapper -->
<div id=\"wrapper\">
\t<!-- shell -->
\t<div class =\"shell\">
\t\t<!-- container -->
\t\t<div class=\"container\">
\t\t\t<!-- header -->
\t\t\t<header id=\"header\">
\t\t\t\t<a href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("retina/default/logo.png"), "html", null, true);
        echo "\"></a>
                                   ";
        // line 49
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            echo " 
                                    <span class=\"right\"><a href=\"";
            // line 50
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\"><button class=\"btn btn-default\"> Log Out</button></a></span>
                                    ";
        } else {
            // line 52
            echo "                                    <span class = \"right\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("login");
            echo "\"><button class=\"btn btn-default\">Login</button></a></span>
                                    ";
        }
        // line 54
        echo "                                   
                                <!-- search -->
\t\t\t\t<div class=\"search\">
                                    
\t\t\t\t\t<form action=\"";
        // line 58
        echo $this->env->getExtension('routing')->getPath("search");
        echo "\" method=\"POST\" role=\"search\">
\t\t\t\t\t\t<div class=\"input-group\">
                                                  <input type=\"text\" id=\"searchfield\" name=\"searchfield\" class=\"field\" placeholder=\"Search something.\" value=\"\" />
\t\t\t\t\t\t<input type=\"submit\" class=\"search-btn\" value=\"\" />
\t\t\t\t\t\t<div class=\"cl\">&nbsp;</div>
                                                  </div>
                                        </form>
                                    
                                    
\t\t\t\t</div>
                               
\t\t\t\t<!-- end of search --> 
\t\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t</header>
\t\t\t<!-- end of header -->
\t\t\t<!-- navigaation -->
\t\t\t<nav id=\"navigation\">
\t\t\t\t<a href=\"#\" class=\"nav-btn\">HOME<span></span></a>
\t\t\t\t<ul>
\t\t\t\t\t<li ";
        // line 77
        $this->displayBlock('home', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\">Home</a></li>
                                        ";
        // line 78
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 79
            echo "                                            <li ";
            $this->displayBlock('profile', $context, $blocks);
            echo "><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
            echo "\">Profile</a></li>
                                        ";
        }
        // line 81
        echo "\t\t\t\t\t<li ";
        $this->displayBlock('doctor', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("doctorhomepage");
        echo "\">Doctors</a></li>
\t\t\t\t\t<li ";
        // line 82
        $this->displayBlock('clinic', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("clinichomepage");
        echo "\">Clinics</a></li>
\t\t\t\t\t<li ";
        // line 83
        $this->displayBlock('illness', $context, $blocks);
        echo "><a href=\"";
        echo $this->env->getExtension('routing')->getPath("illnesshomepage");
        echo "\">Illnesses</a></li>
                                        ";
        // line 84
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 85
            echo "                                        <li ";
            $this->displayBlock('doctorgroup', $context, $blocks);
            echo "><a href=\"";
            echo $this->env->getExtension('routing')->getPath("doctorgrouphomepage");
            echo "\">Doctor Group</a></li>
\t\t\t\t\t";
        }
        // line 87
        echo "                                        ";
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            // line 88
            echo "                                        <li";
            $this->displayBlock('settings', $context, $blocks);
            echo "><a href=\"";
            echo $this->env->getExtension('routing')->getPath("settings_page");
            echo "\">Settings</a></li>
\t\t\t\t\t";
        }
        // line 90
        echo "                                        
\t\t\t\t</ul>
\t\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t</nav>
\t\t\t<!-- end of navigation -->
\t\t\t
\t\t\t<!-- main -->
\t\t\t<div class=\"main\">

\t\t\t\t<section>
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t
                                                ";
        // line 102
        $this->displayBlock('body', $context, $blocks);
        // line 103
        echo "\t\t\t\t\t</div>

\t\t\t\t</section>

\t\t\t\t
\t\t\t</div>
\t\t\t<!-- end of main -->
\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t
\t\t\t<!-- footer -->
\t\t\t<div id=\"footer\">
\t\t\t\t<div class=\"footer-nav\">
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li><a href=\"#\">Home</a></li>
\t\t\t\t\t\t<li><a href=\"#\">About</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Services</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Projects</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Solutions</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Jobs</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Blog</a></li>
\t\t\t\t\t\t<li><a href=\"#\">Contacts</a></li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t\t</div>
\t\t\t\t<p class=\"copy\">&copy; Copyright 2012<span>|</span>Sitename. Design by <a href=\"http://chocotemplates.com\" target=\"_blank\">ChocoTemplates.com</a></p>
\t\t\t\t<div class=\"cl\">&nbsp;</div>
\t\t\t</div>
\t\t\t<!-- end of footer -->
\t\t</div>
\t\t<!-- end of container -->
\t</div>
\t<!-- end of shell -->
</div>
<!-- end of wrapper -->
<script src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("notify/notify.js"), "html", null, true);
        echo "\"></script>
    <!-- gauge js -->
    <script type=\"text/javascript\" src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/gauge/gauge.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/gauge/gauge_demo.js"), "html", null, true);
        echo "\"></script>
    <!-- chart js -->
    <script src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/chartjs/chart.min.js"), "html", null, true);
        echo "\"></script>
    <!-- bootstrap progress js -->
    <script src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/progressbar/bootstrap-progressbar.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/nicescroll/jquery.nicescroll.min.js"), "html", null, true);
        echo "\"></script>
    <!-- icheck -->
    <script src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/icheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
    <!-- daterangepicker -->
    <script type=\"text/javascript\" src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/moment.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/datepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/custom.js"), "html", null, true);
        echo "\"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type=\"text/javascript\" src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/excanvas.min.js"), "html", null, true);
        echo "\"></script><![endif]-->
    <script type=\"text/javascript\" src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.pie.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.orderBars.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.time.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/date.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.spline.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.stack.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/curvedLines.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/flot/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/notify/pnotify.core.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/notify/pnotify.buttons.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/js/notify/pnotify.nonblock.js"), "html", null, true);
        echo "\"></script>
</body>
</html>";
        
        $__internal_05a5be56e1820035f3ddc4bea2a62ab499854be945f844f30c0c7a4683482027->leave($__internal_05a5be56e1820035f3ddc4bea2a62ab499854be945f844f30c0c7a4683482027_prof);

    }

    // line 77
    public function block_home($context, array $blocks = array())
    {
        $__internal_f92fa937c2837336ac95a8a55860a3cbded1801fb831dd1f7f3e53dcdb7da0db = $this->env->getExtension("native_profiler");
        $__internal_f92fa937c2837336ac95a8a55860a3cbded1801fb831dd1f7f3e53dcdb7da0db->enter($__internal_f92fa937c2837336ac95a8a55860a3cbded1801fb831dd1f7f3e53dcdb7da0db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "home"));

        echo " ";
        
        $__internal_f92fa937c2837336ac95a8a55860a3cbded1801fb831dd1f7f3e53dcdb7da0db->leave($__internal_f92fa937c2837336ac95a8a55860a3cbded1801fb831dd1f7f3e53dcdb7da0db_prof);

    }

    // line 79
    public function block_profile($context, array $blocks = array())
    {
        $__internal_ef2013c4ac3642d248b0a0798be097b6554fa86ece2e505bf6b20ae5001b040c = $this->env->getExtension("native_profiler");
        $__internal_ef2013c4ac3642d248b0a0798be097b6554fa86ece2e505bf6b20ae5001b040c->enter($__internal_ef2013c4ac3642d248b0a0798be097b6554fa86ece2e505bf6b20ae5001b040c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "profile"));

        echo " ";
        
        $__internal_ef2013c4ac3642d248b0a0798be097b6554fa86ece2e505bf6b20ae5001b040c->leave($__internal_ef2013c4ac3642d248b0a0798be097b6554fa86ece2e505bf6b20ae5001b040c_prof);

    }

    // line 81
    public function block_doctor($context, array $blocks = array())
    {
        $__internal_94da5fdeb417bde4d1ae9c51554565ba806e1496a4e922c9f756465556cee006 = $this->env->getExtension("native_profiler");
        $__internal_94da5fdeb417bde4d1ae9c51554565ba806e1496a4e922c9f756465556cee006->enter($__internal_94da5fdeb417bde4d1ae9c51554565ba806e1496a4e922c9f756465556cee006_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctor"));

        echo " ";
        
        $__internal_94da5fdeb417bde4d1ae9c51554565ba806e1496a4e922c9f756465556cee006->leave($__internal_94da5fdeb417bde4d1ae9c51554565ba806e1496a4e922c9f756465556cee006_prof);

    }

    // line 82
    public function block_clinic($context, array $blocks = array())
    {
        $__internal_f2748cb00662b8ca94c45a42756fa8fba57a80124d84ff719c8f6c41d49216f0 = $this->env->getExtension("native_profiler");
        $__internal_f2748cb00662b8ca94c45a42756fa8fba57a80124d84ff719c8f6c41d49216f0->enter($__internal_f2748cb00662b8ca94c45a42756fa8fba57a80124d84ff719c8f6c41d49216f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "clinic"));

        echo " ";
        
        $__internal_f2748cb00662b8ca94c45a42756fa8fba57a80124d84ff719c8f6c41d49216f0->leave($__internal_f2748cb00662b8ca94c45a42756fa8fba57a80124d84ff719c8f6c41d49216f0_prof);

    }

    // line 83
    public function block_illness($context, array $blocks = array())
    {
        $__internal_bc47f94865ed745724407ec7b1e7db32c93e9071777a8072c662aeaa0ac199f6 = $this->env->getExtension("native_profiler");
        $__internal_bc47f94865ed745724407ec7b1e7db32c93e9071777a8072c662aeaa0ac199f6->enter($__internal_bc47f94865ed745724407ec7b1e7db32c93e9071777a8072c662aeaa0ac199f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "illness"));

        echo " ";
        
        $__internal_bc47f94865ed745724407ec7b1e7db32c93e9071777a8072c662aeaa0ac199f6->leave($__internal_bc47f94865ed745724407ec7b1e7db32c93e9071777a8072c662aeaa0ac199f6_prof);

    }

    // line 85
    public function block_doctorgroup($context, array $blocks = array())
    {
        $__internal_e4981cc7a9789e93ee471b85b52fef2ba7232108a1eac633a83908631deea06a = $this->env->getExtension("native_profiler");
        $__internal_e4981cc7a9789e93ee471b85b52fef2ba7232108a1eac633a83908631deea06a->enter($__internal_e4981cc7a9789e93ee471b85b52fef2ba7232108a1eac633a83908631deea06a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctorgroup"));

        echo " ";
        
        $__internal_e4981cc7a9789e93ee471b85b52fef2ba7232108a1eac633a83908631deea06a->leave($__internal_e4981cc7a9789e93ee471b85b52fef2ba7232108a1eac633a83908631deea06a_prof);

    }

    // line 88
    public function block_settings($context, array $blocks = array())
    {
        $__internal_f451d113b7f6746b3264859dc78a6d56827fb190a6607789e0517ab006f74d92 = $this->env->getExtension("native_profiler");
        $__internal_f451d113b7f6746b3264859dc78a6d56827fb190a6607789e0517ab006f74d92->enter($__internal_f451d113b7f6746b3264859dc78a6d56827fb190a6607789e0517ab006f74d92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "settings"));

        echo " ";
        
        $__internal_f451d113b7f6746b3264859dc78a6d56827fb190a6607789e0517ab006f74d92->leave($__internal_f451d113b7f6746b3264859dc78a6d56827fb190a6607789e0517ab006f74d92_prof);

    }

    // line 102
    public function block_body($context, array $blocks = array())
    {
        $__internal_f59e0b837ae3cff8accdcc5232509eea841f4dc646e0bf7ecc7c0b7ce837f1e9 = $this->env->getExtension("native_profiler");
        $__internal_f59e0b837ae3cff8accdcc5232509eea841f4dc646e0bf7ecc7c0b7ce837f1e9->enter($__internal_f59e0b837ae3cff8accdcc5232509eea841f4dc646e0bf7ecc7c0b7ce837f1e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_f59e0b837ae3cff8accdcc5232509eea841f4dc646e0bf7ecc7c0b7ce837f1e9->leave($__internal_f59e0b837ae3cff8accdcc5232509eea841f4dc646e0bf7ecc7c0b7ce837f1e9_prof);

    }

    public function getTemplateName()
    {
        return "::basenosidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  490 => 102,  478 => 88,  466 => 85,  454 => 83,  442 => 82,  430 => 81,  418 => 79,  406 => 77,  396 => 168,  392 => 167,  388 => 166,  384 => 165,  380 => 164,  376 => 163,  372 => 162,  368 => 161,  364 => 160,  360 => 159,  356 => 158,  352 => 157,  348 => 156,  342 => 153,  337 => 151,  333 => 150,  328 => 148,  323 => 146,  319 => 145,  314 => 143,  309 => 141,  305 => 140,  300 => 138,  296 => 137,  260 => 103,  258 => 102,  244 => 90,  236 => 88,  233 => 87,  225 => 85,  223 => 84,  217 => 83,  211 => 82,  204 => 81,  196 => 79,  194 => 78,  188 => 77,  166 => 58,  160 => 54,  154 => 52,  149 => 50,  145 => 49,  139 => 48,  124 => 36,  120 => 35,  116 => 34,  112 => 33,  108 => 32,  103 => 30,  99 => 29,  95 => 28,  91 => 27,  85 => 24,  81 => 23,  77 => 22,  73 => 21,  69 => 20,  65 => 19,  61 => 18,  56 => 16,  52 => 15,  45 => 11,  39 => 8,  30 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/*     */
/* <head>*/
/* 	<meta charset="utf-8" />*/
/* 	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />*/
/* 	<title>Find Me A Doctor Iloilo</title>*/
/* 	<link rel="shortcut icon" type="image/x-icon" href="{{asset('retina/default/css/images/favicon.ico')}}" />*/
/* 	*/
/* 	<link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' rel='stylesheet' type='text/css'>*/
/* 	<script src="{{asset('retina/default/js/jquery-1.8.0.min.js')}}" type="text/javascript"></script>*/
/* 	<!--[if lt IE 9]>*/
/* 		<script src="js/modernizr.custom.js"></script>*/
/* 	<![endif]-->*/
/* 	<script src="{{asset('retina/default/js/jquery.carouFredSel-5.5.0-packed.js')}}" type="text/javascript"></script>*/
/* 	<script src="{{asset('retina/default/js/functions.js')}}" type="text/javascript"></script>*/
/*         <!-- Bootstrap core CSS -->*/
/*  <link href="{{asset('gentelella/jquery-ui/jquery-ui.min.css')}}" rel="stylesheet">*/
/*      <link href="{{asset('gentelella/jquery-ui/jquery-ui.theme.css')}}" rel="stylesheet">*/
/*      <link href="{{asset('gentelella/jquery-ui/jquery-ui.structure.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('gentelella/css/bootstrap.min.css')}}" rel="stylesheet">*/
/* <link href={{asset('gentelella/css/formerrornotify.css')}} rel="stylesheet">*/
/*     <link href="{{asset('gentelella/fonts/css/font-awesome.min.css')}}" rel="stylesheet">*/
/*     <link href="{{asset('gentelella/css/animate.min.css')}}" rel="stylesheet">*/
/* */
/*     <!-- Custom styling plus plugins -->*/
/*     <link href="{{asset('gentelella/css/custom.css')}}" rel="stylesheet">*/
/*     <link rel="stylesheet" type="text/css" href="{{asset('gentelella/css/maps/jquery-jvectormap-2.0.1.css')}}" />*/
/*     <link href="{{asset('gentelella/css/icheck/flat/green.css')}}" rel="stylesheet" />*/
/*     <link href="{{asset('gentelella/css/floatexamples.css')}}" rel="stylesheet" type="text/css" />*/
/* */
/*     <script src="{{asset('gentelella/js/jquery.min.js')}}"></script>*/
/*     <script src="{{asset('gentelella/js/nprogress.js')}}"></script>*/
/*     <link rel="stylesheet" href="{{asset('retina/default/css/style.css')}}" type="text/css" media="all" />*/
/*     <link href="{{asset('gentelella/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*  <link href="{{asset('gentelella/css/animate.min.css')}}" rel="stylesheet">*/
/* */
/* </head>*/
/* <body>*/
/* <!-- wrapper -->*/
/* <div id="wrapper">*/
/* 	<!-- shell -->*/
/* 	<div class ="shell">*/
/* 		<!-- container -->*/
/* 		<div class="container">*/
/* 			<!-- header -->*/
/* 			<header id="header">*/
/* 				<a href="{{path('homepage')}}"><img src="{{asset('retina/default/logo.png')}}"></a>*/
/*                                    {% if app.user %} */
/*                                     <span class="right"><a href="{{ path('logout') }}"><button class="btn btn-default"> Log Out</button></a></span>*/
/*                                     {% else %}*/
/*                                     <span class = "right"><a href="{{path('login')}}"><button class="btn btn-default">Login</button></a></span>*/
/*                                     {% endif%}*/
/*                                    */
/*                                 <!-- search -->*/
/* 				<div class="search">*/
/*                                     */
/* 					<form action="{{path('search')}}" method="POST" role="search">*/
/* 						<div class="input-group">*/
/*                                                   <input type="text" id="searchfield" name="searchfield" class="field" placeholder="Search something." value="" />*/
/* 						<input type="submit" class="search-btn" value="" />*/
/* 						<div class="cl">&nbsp;</div>*/
/*                                                   </div>*/
/*                                         </form>*/
/*                                     */
/*                                     */
/* 				</div>*/
/*                                */
/* 				<!-- end of search --> */
/* 				<div class="cl">&nbsp;</div>*/
/* 			</header>*/
/* 			<!-- end of header -->*/
/* 			<!-- navigaation -->*/
/* 			<nav id="navigation">*/
/* 				<a href="#" class="nav-btn">HOME<span></span></a>*/
/* 				<ul>*/
/* 					<li {%block home%} {% endblock %}><a href="{{path('homepage')}}">Home</a></li>*/
/*                                         {% if app.user%}*/
/*                                             <li {%block profile%} {% endblock %}><a href="{{path('doctor_show', {'id' : app.user.id})}}">Profile</a></li>*/
/*                                         {% endif %}*/
/* 					<li {%block doctor%} {% endblock %}><a href="{{path('doctorhomepage')}}">Doctors</a></li>*/
/* 					<li {%block clinic%} {% endblock %}><a href="{{path('clinichomepage')}}">Clinics</a></li>*/
/* 					<li {%block illness%} {% endblock %}><a href="{{path('illnesshomepage')}}">Illnesses</a></li>*/
/*                                         {% if app.user%}*/
/*                                         <li {%block doctorgroup%} {% endblock %}><a href="{{path('doctorgrouphomepage')}}">Doctor Group</a></li>*/
/* 					{% endif %}*/
/*                                         {% if app.user%}*/
/*                                         <li{%block settings%} {% endblock %}><a href="{{path('settings_page')}}">Settings</a></li>*/
/* 					{% endif %}*/
/*                                         */
/* 				</ul>*/
/* 				<div class="cl">&nbsp;</div>*/
/* 			</nav>*/
/* 			<!-- end of navigation -->*/
/* 			*/
/* 			<!-- main -->*/
/* 			<div class="main">*/
/* */
/* 				<section>*/
/* 					<div class="col">*/
/* 						*/
/*                                                 {%block body%}{%endblock%}*/
/* 					</div>*/
/* */
/* 				</section>*/
/* */
/* 				*/
/* 			</div>*/
/* 			<!-- end of main -->*/
/* 			<div class="cl">&nbsp;</div>*/
/* 			*/
/* 			<!-- footer -->*/
/* 			<div id="footer">*/
/* 				<div class="footer-nav">*/
/* 					<ul>*/
/* 						<li><a href="#">Home</a></li>*/
/* 						<li><a href="#">About</a></li>*/
/* 						<li><a href="#">Services</a></li>*/
/* 						<li><a href="#">Projects</a></li>*/
/* 						<li><a href="#">Solutions</a></li>*/
/* 						<li><a href="#">Jobs</a></li>*/
/* 						<li><a href="#">Blog</a></li>*/
/* 						<li><a href="#">Contacts</a></li>*/
/* 					</ul>*/
/* 					<div class="cl">&nbsp;</div>*/
/* 				</div>*/
/* 				<p class="copy">&copy; Copyright 2012<span>|</span>Sitename. Design by <a href="http://chocotemplates.com" target="_blank">ChocoTemplates.com</a></p>*/
/* 				<div class="cl">&nbsp;</div>*/
/* 			</div>*/
/* 			<!-- end of footer -->*/
/* 		</div>*/
/* 		<!-- end of container -->*/
/* 	</div>*/
/* 	<!-- end of shell -->*/
/* </div>*/
/* <!-- end of wrapper -->*/
/* <script src="{{asset('gentelella/js/bootstrap.min.js')}}"></script>*/
/* <script type="text/javascript" src="{{asset('notify/notify.js')}}"></script>*/
/*     <!-- gauge js -->*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/gauge/gauge.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/gauge/gauge_demo.js')}}"></script>*/
/*     <!-- chart js -->*/
/*     <script src="{{asset('gentelella/js/chartjs/chart.min.js')}}"></script>*/
/*     <!-- bootstrap progress js -->*/
/*     <script src="{{asset('gentelella/js/progressbar/bootstrap-progressbar.min.js')}}"></script>*/
/*     <script src="{{asset('gentelella/js/nicescroll/jquery.nicescroll.min.js')}}"></script>*/
/*     <!-- icheck -->*/
/*     <script src="{{asset('gentelella/js/icheck/icheck.min.js')}}"></script>*/
/*     <!-- daterangepicker -->*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/moment.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/datepicker/daterangepicker.js')}}"></script>*/
/* */
/*     <script src="{{asset('gentelella/js/custom.js')}}"></script>*/
/* */
/*     <!-- flot js -->*/
/*     <!--[if lte IE 8]><script type="text/javascript" src="{{asset('gentelella/js/excanvas.min.js')}}"></script><![endif]-->*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.pie.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.orderBars.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.time.min.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/date.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.spline.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.stack.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/curvedLines.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/flot/jquery.flot.resize.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/notify/pnotify.core.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/notify/pnotify.buttons.js')}}"></script>*/
/*     <script type="text/javascript" src="{{asset('gentelella/js/notify/pnotify.nonblock.js')}}"></script>*/
/* </body>*/
/* </html>*/
