<?php

/* ResearchProjectMyProjectBundle:Search:searchSpecialty.html.twig */
class __TwigTemplate_3b58413d9963cde73d13c3ad15295a8a5e6acf45508d32326888df475b736f40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Search:searchSpecialty.html.twig", 1);
        $this->blocks = array(
            'doctor' => array($this, 'block_doctor'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28914f30fabf2331bb7522f72f8053cbec64dfaa4d975d59cd58724315b77923 = $this->env->getExtension("native_profiler");
        $__internal_28914f30fabf2331bb7522f72f8053cbec64dfaa4d975d59cd58724315b77923->enter($__internal_28914f30fabf2331bb7522f72f8053cbec64dfaa4d975d59cd58724315b77923_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Search:searchSpecialty.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_28914f30fabf2331bb7522f72f8053cbec64dfaa4d975d59cd58724315b77923->leave($__internal_28914f30fabf2331bb7522f72f8053cbec64dfaa4d975d59cd58724315b77923_prof);

    }

    // line 2
    public function block_doctor($context, array $blocks = array())
    {
        $__internal_ac3cda2979982e2a7149cb5dc70b20f7e1d9daed80b53e04b8ba59bfc7f1d9e5 = $this->env->getExtension("native_profiler");
        $__internal_ac3cda2979982e2a7149cb5dc70b20f7e1d9daed80b53e04b8ba59bfc7f1d9e5->enter($__internal_ac3cda2979982e2a7149cb5dc70b20f7e1d9daed80b53e04b8ba59bfc7f1d9e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctor"));

        echo " class=\"active\" ";
        
        $__internal_ac3cda2979982e2a7149cb5dc70b20f7e1d9daed80b53e04b8ba59bfc7f1d9e5->leave($__internal_ac3cda2979982e2a7149cb5dc70b20f7e1d9daed80b53e04b8ba59bfc7f1d9e5_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_c25709124f3ff5a6f4cb531f002c15336d1d552cf0fa5a99ccbac39f11db3445 = $this->env->getExtension("native_profiler");
        $__internal_c25709124f3ff5a6f4cb531f002c15336d1d552cf0fa5a99ccbac39f11db3445->enter($__internal_c25709124f3ff5a6f4cb531f002c15336d1d552cf0fa5a99ccbac39f11db3445_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_c25709124f3ff5a6f4cb531f002c15336d1d552cf0fa5a99ccbac39f11db3445->leave($__internal_c25709124f3ff5a6f4cb531f002c15336d1d552cf0fa5a99ccbac39f11db3445_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_df4cfbc0e9968c852f82bef794cd9b1342360511654f403c70e65adfa31aba65 = $this->env->getExtension("native_profiler");
        $__internal_df4cfbc0e9968c852f82bef794cd9b1342360511654f403c70e65adfa31aba65->enter($__internal_df4cfbc0e9968c852f82bef794cd9b1342360511654f403c70e65adfa31aba65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    <div class=\"\">
        <div class=\"page-title\">
            <div class=\"title\">
                <h3>&nbsp;DOCTOR - Specialty
                </h3>
            </div>
        </div>
        <div class=\"clearfix\"></div>
        
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\" >
                        <h3 style=\"font-size: 20px;\">";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["specialty"]) ? $context["specialty"] : $this->getContext($context, "specialty")), "html", null, true);
        echo "</h3>
                        <a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("doctorhomepage");
        echo "\"><button class=\"btn btn-primary btn-xs\">Back to Specialties</button></a> 
                    <div class=\"x_content\">
                        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["doctorsBySpecialty"]) ? $context["doctorsBySpecialty"] : $this->getContext($context, "doctorsBySpecialty")));
        foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
            // line 21
            echo "                            <div class=\"col-md-6 col-sm-6 col-xs-6\">
                                <center>
                                    <h3 style=\"font-size: 20px;\"> <a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute($context["doctor"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-user-md\"></i> &emsp;";
            echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["doctor"], "lastName", array()), "html", null, true);
            echo "</a></h3>
                                </center>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "                    </div>
                </div>
            </div>
        </div>
";
        
        $__internal_df4cfbc0e9968c852f82bef794cd9b1342360511654f403c70e65adfa31aba65->leave($__internal_df4cfbc0e9968c852f82bef794cd9b1342360511654f403c70e65adfa31aba65_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Search:searchSpecialty.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 27,  97 => 23,  93 => 21,  89 => 20,  84 => 18,  80 => 17,  66 => 5,  60 => 4,  48 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block doctor %} class="active" {% endblock %}*/
/* {% block title%} {% endblock %}*/
/* {% block body %}*/
/*     <div class="">*/
/*         <div class="page-title">*/
/*             <div class="title">*/
/*                 <h3>&nbsp;DOCTOR - Specialty*/
/*                 </h3>*/
/*             </div>*/
/*         </div>*/
/*         <div class="clearfix"></div>*/
/*         */
/*         <div class="row">*/
/*             <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                 <div class="x_panel" >*/
/*                         <h3 style="font-size: 20px;">{{specialty}}</h3>*/
/*                         <a href="{{path('doctorhomepage')}}"><button class="btn btn-primary btn-xs">Back to Specialties</button></a> */
/*                     <div class="x_content">*/
/*                         {% for doctor in doctorsBySpecialty%}*/
/*                             <div class="col-md-6 col-sm-6 col-xs-6">*/
/*                                 <center>*/
/*                                     <h3 style="font-size: 20px;"> <a href="{{path('doctor_show', {'id': doctor.id})}}"><i class="fa fa-user-md"></i> &emsp;{{doctor.firstName}} {{doctor.lastName}}</a></h3>*/
/*                                 </center>*/
/*                             </div>*/
/*                         {% endfor %}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* {% endblock %}*/
