<?php

/* ResearchProjectMyProjectBundle:Default:doctor.html.twig */
class __TwigTemplate_7224bb44dd04faab6cdac411e1512254451dc755feacbfd27560317f9ff0fa08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Default:doctor.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'doctor' => array($this, 'block_doctor'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78954128e61bafc0f7bbeb78550b47b489707da2e114c6ec6ecfa64ddaf9db4b = $this->env->getExtension("native_profiler");
        $__internal_78954128e61bafc0f7bbeb78550b47b489707da2e114c6ec6ecfa64ddaf9db4b->enter($__internal_78954128e61bafc0f7bbeb78550b47b489707da2e114c6ec6ecfa64ddaf9db4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Default:doctor.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_78954128e61bafc0f7bbeb78550b47b489707da2e114c6ec6ecfa64ddaf9db4b->leave($__internal_78954128e61bafc0f7bbeb78550b47b489707da2e114c6ec6ecfa64ddaf9db4b_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_d94b631428daec311ea85afa383b2ffaaed9cb7977f94392744959d381fbf057 = $this->env->getExtension("native_profiler");
        $__internal_d94b631428daec311ea85afa383b2ffaaed9cb7977f94392744959d381fbf057->enter($__internal_d94b631428daec311ea85afa383b2ffaaed9cb7977f94392744959d381fbf057_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_d94b631428daec311ea85afa383b2ffaaed9cb7977f94392744959d381fbf057->leave($__internal_d94b631428daec311ea85afa383b2ffaaed9cb7977f94392744959d381fbf057_prof);

    }

    // line 3
    public function block_doctor($context, array $blocks = array())
    {
        $__internal_36d0d794dcf383c55e882187c94273a0949e151be0e90216604cde5a17d3ee4a = $this->env->getExtension("native_profiler");
        $__internal_36d0d794dcf383c55e882187c94273a0949e151be0e90216604cde5a17d3ee4a->enter($__internal_36d0d794dcf383c55e882187c94273a0949e151be0e90216604cde5a17d3ee4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "doctor"));

        echo " class=\"active\" ";
        
        $__internal_36d0d794dcf383c55e882187c94273a0949e151be0e90216604cde5a17d3ee4a->leave($__internal_36d0d794dcf383c55e882187c94273a0949e151be0e90216604cde5a17d3ee4a_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_66b23066b376e101772fae7d1fb1534ba8aedd1545d609aa6fde94a238cfb8ba = $this->env->getExtension("native_profiler");
        $__internal_66b23066b376e101772fae7d1fb1534ba8aedd1545d609aa6fde94a238cfb8ba->enter($__internal_66b23066b376e101772fae7d1fb1534ba8aedd1545d609aa6fde94a238cfb8ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    ";
        if ( !$this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) {
            echo "    
    <div class=\"\">
            <div class=\"page-title\">
                <div class=\"title\">
                    <h3>&nbsp;DOCTOR</h3>
                </div>
            </div>
            <div class=\"clearfix\"></div>

            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12\">
                    <div class=\"x_panel\" >
                        <br/>
                 
                    ";
            // line 20
            $context["firstname1"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "firstName", array());
            // line 21
            echo "            ";
            $context["lastname1"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "lastName", array());
            // line 22
            echo "            ";
            $context["fop1"] = $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "fieldOfPractice", array());
            // line 23
            echo "            ";
            $context["firstname2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "firstName", array());
            // line 24
            echo "            ";
            $context["lastname2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "lastName", array());
            // line 25
            echo "            ";
            $context["fop2"] = $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "fieldOfPractice", array());
            // line 26
            echo "            ";
            $context["firstname3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "firstName", array());
            // line 27
            echo "            ";
            $context["lastname3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "lastName", array());
            // line 28
            echo "            ";
            $context["fop3"] = $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "fieldOfPractice", array());
            // line 29
            echo "                
            <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
            // line 34
            echo twig_escape_filter($this->env, (isset($context["firstname1"]) ? $context["firstname1"] : $this->getContext($context, "firstname1")), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["lastname1"]) ? $context["lastname1"] : $this->getContext($context, "lastname1")), "html", null, true);
            echo "</b></h3></i>
                                        
                                        ";
            // line 36
            if (((isset($context["fop1"]) ? $context["fop1"] : $this->getContext($context, "fop1")) == "General")) {
                // line 37
                echo "                                             <h4>";
                echo twig_escape_filter($this->env, (isset($context["fop1"]) ? $context["fop1"] : $this->getContext($context, "fop1")), "html", null, true);
                echo "</h4>
                                        ";
            } elseif ((            // line 38
(isset($context["fop1"]) ? $context["fop1"] : $this->getContext($context, "fop1")) == "Specialized")) {
                // line 39
                echo "                                            <h4>";
                echo twig_escape_filter($this->env, (isset($context["fop1"]) ? $context["fop1"] : $this->getContext($context, "fop1")), "html", null, true);
                echo "</h4>
                                            ";
                // line 40
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "specialties", array()), 0, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                    // line 41
                    echo "                                            <button class=\"btn-info btn-xs\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["specialty"], "specialtyName", array()), "html", null, true);
                    echo "\">See Specialties</button>
                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 43
                echo "                                        ";
            }
            // line 44
            echo "                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        ";
            // line 49
            if (($this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "getWebPath", array()) == null)) {
                // line 50
                echo "                                        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
                echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        <br/>
                                        <a href=\"";
                // line 52
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "id", array()))), "html", null, true);
                echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                        ";
            } else {
                // line 56
                echo "                                            <img style=\"margin-bottom: 17px;\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "getWebPath", array())), "html", null, true);
                echo "\"alt=\"\" class=\"img-circle img-responsive\">
                                            <a href=\"";
                // line 57
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc1"]) ? $context["randomdoc1"] : $this->getContext($context, "randomdoc1")), "id", array()))), "html", null, true);
                echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                        ";
            }
            // line 60
            echo "                                        <br/>
                                        
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
            // line 74
            echo twig_escape_filter($this->env, (isset($context["firstname2"]) ? $context["firstname2"] : $this->getContext($context, "firstname2")), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["lastname2"]) ? $context["lastname2"] : $this->getContext($context, "lastname2")), "html", null, true);
            echo "</b></h3></i>
                                        
                                        ";
            // line 76
            if (((isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")) == "General")) {
                // line 77
                echo "                                             <h4>";
                echo twig_escape_filter($this->env, (isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")), "html", null, true);
                echo "</h4>
                                        ";
            } elseif ((            // line 78
(isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")) == "Specialized")) {
                // line 79
                echo "                                            <h4>";
                echo twig_escape_filter($this->env, (isset($context["fop2"]) ? $context["fop2"] : $this->getContext($context, "fop2")), "html", null, true);
                echo "</h4>
                                            ";
                // line 80
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "specialties", array()), 0, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                    // line 81
                    echo "                                                <button class=\"btn-info btn-xs\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["specialty"], "specialtyName", array()), "html", null, true);
                    echo "\">See Specialties</button>
                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 83
                echo "                                        ";
            }
            // line 84
            echo "                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        ";
            // line 89
            if (($this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "getWebPath", array()) == null)) {
                // line 90
                echo "                                        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
                echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        <br/>
                                        <a href=\"";
                // line 92
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "id", array()))), "html", null, true);
                echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                        ";
            } else {
                // line 96
                echo "                                            <img style=\"margin-bottom: 17px;\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "getWebPath", array())), "html", null, true);
                echo "\"alt=\"\" class=\"img-circle img-responsive\">
                                             <a href=\"";
                // line 97
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc2"]) ? $context["randomdoc2"] : $this->getContext($context, "randomdoc2")), "id", array()))), "html", null, true);
                echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                            <i class=\"fa fa-user\"></i> View Profile </button></a>
                                        ";
            }
            // line 100
            echo "                                        <br/>
                                        
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-md-4 col-sm-4 col-xs-12 animated fadeInDown\">
                        <div class=\"well profile_view\">
                            <div class=\"col-sm-12\">
                                    <div class=\"left col-xs-7\">
                                        <i><h3><b>";
            // line 114
            echo twig_escape_filter($this->env, (isset($context["firstname3"]) ? $context["firstname3"] : $this->getContext($context, "firstname3")), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["lastname3"]) ? $context["lastname3"] : $this->getContext($context, "lastname3")), "html", null, true);
            echo "</b></h3></i>
                                        
                                        ";
            // line 116
            if (((isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")) == "General")) {
                // line 117
                echo "                                            <h4>";
                echo twig_escape_filter($this->env, (isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")), "html", null, true);
                echo "</h4>
                                        ";
            } elseif ((            // line 118
(isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")) == "Specialized")) {
                // line 119
                echo "                                            <h4>";
                echo twig_escape_filter($this->env, (isset($context["fop3"]) ? $context["fop3"] : $this->getContext($context, "fop3")), "html", null, true);
                echo "</h4>
                                            ";
                // line 120
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "specialties", array()), 0, 1));
                foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                    echo "              
                                            <button class=\"btn-info btn-xs\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                    // line 121
                    echo twig_escape_filter($this->env, $this->getAttribute($context["specialty"], "specialtyName", array()), "html", null, true);
                    echo "\">See Specialties</button>
                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 123
                echo "                                        ";
            }
            // line 124
            echo "                                        
                                    </div>
                                    <div class=\"right col-xs-5 text-center\">
                                        
                                        <br/>
                                        ";
            // line 129
            if (($this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "getWebPath", array()) == null)) {
                // line 130
                echo "                                        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gentelella/images/img.jpg"), "html", null, true);
                echo "\" alt=\"\" class=\"img-circle img-responsive\">
                                        <br/>
                                        <a href=\"";
                // line 132
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "id", array()))), "html", null, true);
                echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                               <i class=\"fa fa-user\"></i> View Profile </button></a>
                                            <br/>
                                            <br/>
                                        ";
            } else {
                // line 137
                echo "                                            <img style=\"margin-bottom: 17px;\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "getWebPath", array())), "html", null, true);
                echo "\"alt=\"\" class=\"img-circle img-responsive\">
                                             <a href=\"";
                // line 138
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute((isset($context["randomdoc3"]) ? $context["randomdoc3"] : $this->getContext($context, "randomdoc3")), "id", array()))), "html", null, true);
                echo "\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> 
                                               <i class=\"fa fa-user\"></i> View Profile </button></a>
                                        ";
            }
            // line 141
            echo "                                        
                                        
                                            
                                    </div>
                            </div>
                            <div class=\"col-xs-12 bottom text-center right\">
                                <div class=\"col-xs-12 col-sm-6 emphasis\">
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
                
            <div class=\"row\">
                <div class=\"col-md-6 col-xs-12\">
                    <div class=\"x_panel\" >
                        <center><h3 style =\"font-size: 25px;\">SPECIALTIES</h3></center>
                        <div style=\"overflow-y: scroll; height:400px;\">
                        ";
            // line 159
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["specialties"]) ? $context["specialties"] : $this->getContext($context, "specialties")));
            foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                echo "                
                            <a href=";
                // line 160
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("searchBySpecialty", array("specialty" => $this->getAttribute($context["specialty"], "specialtyName", array()))), "html", null, true);
                echo "><center><h4 style = \"font-family: Century Gothic; font-size: 15px;\"> <i class=\"fa fa-dot-circle-o\"></i> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["specialty"], "specialtyName", array()), "html", null, true);
                echo " </h4></center></a>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 162
            echo "                        </div>
                    </div>
                </div>
            
            
                <div class=\"col-md-6 col-xs-12\">
                    <div class=\"x_panel\" >
                        <center><h3 style=\"font-size: 25px;\">GENERAL DOCTORS</h3></center>
                        <div style=\"overflow-y: scroll; height:400px;\">
                        ";
            // line 171
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_escape_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                echo "                
                            ";
                // line 172
                if (($this->getAttribute($context["entity"], "fieldOfPractice", array()) == "General")) {
                    // line 173
                    echo "                                <center><i class=\"fa fa-user-md\" style=\"font-size: 20px;\"></i></center>  
                                <a href=\"";
                    // line 174
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("doctor_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                    echo "\"><center><h4 style=\"font-family: Century Gothic; font-size: 16px;\"> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "firstName", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "lastName", array()), "html", null, true);
                    echo "</h4></center></a> 
                                <div class=\"ln_solid\"></div>
                            ";
                }
                // line 177
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 178
            echo "                        </div>
                    </div>
                </div>
            </div>    
                
        </div>
     ";
        } else {
            // line 185
            echo "     <div class=\"row\">
                        <div class=\"col-md-8\">
                            <div class=\"x_panel\">
                                    <h3><b>Appointments Today (";
            // line 188
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "m/d/Y", "Asia/Manila"), "html", null, true);
            echo ")</b></h3>
                                <div class=\"x_content\">
                                    <div class=\"accordion\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
                                        ";
            // line 191
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clinics"]) ? $context["clinics"] : $this->getContext($context, "clinics")));
            foreach ($context['_seq'] as $context["_key"] => $context["clinic"]) {
                // line 192
                echo "                                        <div class=\"panel\">
                                            <a class=\"panel-heading\" role=\"tab\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                                                 <h4 class=\"panel-title\"style=\"font-family: Century Gothic; font-size: 16px; color:#379DFF;\"><b><i class=\"fa fa-hospital-o\"></i>&emsp;";
                // line 194
                echo twig_escape_filter($this->env, $this->getAttribute($context["clinic"], "clinicName", array()), "html", null, true);
                echo "</b></h4>
                                            </a>
                                            <div class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                                                <div class=\"panel-body\">
                                                    <!-- start project list -->
                                                <table class=\"table table-striped projects\">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Date Created</th>
                                                            <th><center>Appointment Code</center></th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        ";
                // line 209
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["appointments"]) ? $context["appointments"] : $this->getContext($context, "appointments")));
                foreach ($context['_seq'] as $context["_key"] => $context["appointment"]) {
                    // line 210
                    echo "                                                            ";
                    if (($this->getAttribute($context["clinic"], "id", array()) == $this->getAttribute($this->getAttribute($context["appointment"], "clinic", array()), "id", array()))) {
                        // line 211
                        echo "                                                                <tr>
                                                                    <td>
                                                                        <a id=\"appointment-";
                        // line 213
                        echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "id", array()), "html", null, true);
                        echo "\" class=\"appointment\" data-toggle=\"modal\" data-target=\"#appointmentmodal\" data-backdrop=\"static\" data-keyboard=\"false\">   <b style=\"color: #379DFF;\"><i>";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "firstName", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "lastName", array()), "html", null, true);
                        echo "</i></b> </a>                                            
                                                                    </td>
                                                                    <td>
                                                                        ";
                        // line 216
                        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["appointment"], "dateCreated", array()), "m/d/Y"), "html", null, true);
                        echo "
                                                                    </td>
                                                                    <td>
                                                                        <center>";
                        // line 219
                        echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "appointmentCode", array()), "html", null, true);
                        echo "</center>
                                                                    </td>
                                                                    <td>
                                                                        <div id =\"statusdiv-";
                        // line 222
                        echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "id", array()), "html", null, true);
                        echo "\">
                                                                            ";
                        // line 223
                        if ($this->getAttribute($context["appointment"], "isAssessed", array())) {
                            // line 224
                            echo "                                                                                <button id =\"status-";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "id", array()), "html", null, true);
                            echo "\" type=\"button\" class=\"btn btn-success btn-xs status-btn\">
                                                                                    <i id=\"iconclass\" class=\"fa fa-check\"></i>
                                                                                </button>
                                                                            ";
                        } else {
                            // line 227
                            echo " 
                                                                                <button id =\"status-";
                            // line 228
                            echo twig_escape_filter($this->env, $this->getAttribute($context["appointment"], "id", array()), "html", null, true);
                            echo "\" type=\"button\" class=\"btn btn-danger btn-xs status-btn\">
                                                                                    <i id=\"iconclass\" class=\"fa fa-minus\"></i>
                                                                                </button>
                                                                            ";
                        }
                        // line 232
                        echo "                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            ";
                    }
                    // line 236
                    echo "                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['appointment'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 237
                echo "                                                    </tbody>
                                                </table> 
                                                </div>
                                            </div>
                                        </div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['clinic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 243
            echo "                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"x_panel\">
                                    <h3><b>Group Invitations</b></h3>
                                <div class=\"x_content\">
                                <ul class=\"list-unstyled msg_list\">
                                   ";
            // line 252
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["invitations"]) ? $context["invitations"] : $this->getContext($context, "invitations")));
            foreach ($context['_seq'] as $context["_key"] => $context["inv"]) {
                // line 253
                echo "                                    <li>
                                          <span>
                                            <a>";
                // line 255
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["inv"], "doctorInviter", array()), "firstName", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["inv"], "doctorInviter", array()), "lastName", array()), "html", null, true);
                echo "</a>invited you to join the group
                                            <a>";
                // line 256
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["inv"], "group", array()), "groupName", array()), "html", null, true);
                echo "</a>
                                          </span>
                                          <a>
                                                 <button id=\"confirm-";
                // line 259
                echo twig_escape_filter($this->env, $this->getAttribute($context["inv"], "id", array()), "html", null, true);
                echo "\" title=\"Accept invitation\" data-toggle=\"tooltip\" data-placement=\"right\" type=\"button\" class=\"btn btn-primary btn-xs btn-confirm \"> <i class=\"fa fa-check-circle\"></i></button>
                                                 <button id=\"decline-";
                // line 260
                echo twig_escape_filter($this->env, $this->getAttribute($context["inv"], "id", array()), "html", null, true);
                echo "\" title=\"Decline invitation\" data-toggle=\"tooltip\" data-placement=\"right\" type=\"button\" class=\"btn btn-danger btn-xs btn-decline \"> <i class=\"fa fa-times-circle\"></i></button>
                                          </a>    
                                        </a>
                                    </li>
                                   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['inv'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 265
            echo "                                </ul>
                            </div>
                            </div>
                        </div>
                    </div>
                <div id=\"appointmentmodal\" class=\"modal fade bs-example-modal-sm\" align=\"left\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" data-backdrop=\"static\" >
                        <div class=\"modal-dialog modal-sm\">
                            <div class=\"modal-content\">
                                <div class=\"modal-header\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                        </button>
                                    <h4 class=\"modal-title\" style=\"font-family: Century Gothic;\" id=\"myModalLabel\">Appointment Details</h4>
                                </div>
                                <div class=\"modal-body\">
                                     <center><img hidden id=\"loading\" src=\"";
            // line 279
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("charisma/img/ajax-loaders/ajax-loader-1.gif"), "html", null, true);
            echo "\" ></center>
                                     <div hidden id=\"results\">
                                         <b>Name:</b>
                                         <div id=\"aname\"></div><br>
                                         <b>Contact Number:</b>
                                         <div id=\"acontact\"></div><br>
                                         <b>Email Address:</b>
                                         <div id=\"aemail\"></div><br>
                                         <b>Appointment Code:</b>
                                         <div id=\"acode\"></div><br>
                                         <b>Notes:</b>
                                         <div style=\"word-wrap: break-word;\" id=\"anote\"></div><br>
                                    </div>
                            </div>
                                
                        </div>
                   </div>

<script>    
                                    var confirmBtnId;
                                    \$(\".btn-confirm\").click(function(x) {
                                        x.preventDefault();
                                        confirmBtnId = this.id;
                                        confirmBtnId = confirmBtnId.replace('confirm-', '');
                                         \$.ajax({
                                            type: \"POST\",
                                            url: '";
            // line 305
            echo $this->env->getExtension('routing')->getPath("confirm_invite");
            echo "',
                                            data: {id: confirmBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                               location.reload();
                                                console.log(response);
                                            },
                                            error: function() {
                                                alert('error');
                                            }
                                        });
                                    });
                                    
                                    var declineBtnId;
                                    \$(\".btn-decline\").click(function(x) {
                                        x.preventDefault();
                                        declineBtnId = this.id;
                                        declineBtnId = declineBtnId.replace('decline-', '');
                                         \$.ajax({
                                            type: \"POST\",
                                            url: '";
            // line 325
            echo $this->env->getExtension('routing')->getPath("decline_invite");
            echo "',
                                            data: {id: declineBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                               location.reload();
                                                console.log(response);
                                            },
                                            error: function() {
                                                 location.reload();
                                                alert('error');
                                            }
                                        });
                                    });
                                   
                                   var statusBtnId;
                                    \$(\".status-btn\").click(function(x) {
                                        x.preventDefault();
                                        statusBtnId = this.id;
                                        statusBtnId = statusBtnId.replace('status-', '');
                                         \$.ajax({
                                            type: \"POST\",
                                            url: '";
            // line 346
            echo $this->env->getExtension('routing')->getPath("status_change");
            echo "',
                                            data: {id: statusBtnId},
                                            dataType: 'json',
                                            success: function(response) {
                                               
                                                console.log(response);
                                                \$('#statusdiv-'+statusBtnId).load(document.URL +  ' #statusdiv-'+statusBtnId);
                                                
                                                
                                            },
                                            error: function() {
                                                
                                                alert(statusBtnId);
                                            }
                                        });
                                    });
                                    
                                     var appointment;
                                    \$(\".appointment\").click(function(x) {
                                        x.preventDefault();
                                        appointment = this.id;
                                        appointment = appointment.replace('appointment-', '');
                                         \$.ajax({
                                            type: \"POST\",
                                            url: '";
            // line 370
            echo $this->env->getExtension('routing')->getPath("appointment_view");
            echo "',
                                            data: {id: appointment},
                                            dataType: 'json',
                                            beforeSend: function(){
                                                \$('#loading').show();
                                                 \$('#results').hide();
                                                },
                                            complete: function(){
                                                \$('#loading').hide();
                                                \$('#results').show();
                                                },
                                            success: function(data) {
                                                var len = data.length;
                                                var txt = \"\";
                                                  if(len > 0){
                                                    for(var i=0;i<len;i++){
                                                           \$(\"#aname\").html(data[i].firstName+\" \"+data[i].lastName);
                                                           \$(\"#acontact\").html(data[i].contactNumber);
                                                           \$(\"#aemail\").html(data[i].emailAddress);
                                                           \$(\"#acode\").html(data[i].appointmentCode);
                                                           \$(\"#anote\").html(data[i].note);
                                                           
                                                          }
                                                    
                                                }

                                                
                                                console.log(data);
                                            },error: function() {
                                             alert('error');
                                              }
                                            });
                                        
                                    });

                                   
  </script>
               
";
        }
        
        $__internal_66b23066b376e101772fae7d1fb1534ba8aedd1545d609aa6fde94a238cfb8ba->leave($__internal_66b23066b376e101772fae7d1fb1534ba8aedd1545d609aa6fde94a238cfb8ba_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Default:doctor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  726 => 370,  699 => 346,  675 => 325,  652 => 305,  623 => 279,  607 => 265,  596 => 260,  592 => 259,  586 => 256,  580 => 255,  576 => 253,  572 => 252,  561 => 243,  550 => 237,  544 => 236,  538 => 232,  531 => 228,  528 => 227,  520 => 224,  518 => 223,  514 => 222,  508 => 219,  502 => 216,  492 => 213,  488 => 211,  485 => 210,  481 => 209,  463 => 194,  459 => 192,  455 => 191,  449 => 188,  444 => 185,  435 => 178,  429 => 177,  419 => 174,  416 => 173,  414 => 172,  408 => 171,  397 => 162,  387 => 160,  381 => 159,  361 => 141,  355 => 138,  350 => 137,  342 => 132,  336 => 130,  334 => 129,  327 => 124,  324 => 123,  316 => 121,  310 => 120,  305 => 119,  303 => 118,  298 => 117,  296 => 116,  289 => 114,  273 => 100,  267 => 97,  262 => 96,  255 => 92,  249 => 90,  247 => 89,  240 => 84,  237 => 83,  228 => 81,  224 => 80,  219 => 79,  217 => 78,  212 => 77,  210 => 76,  203 => 74,  187 => 60,  181 => 57,  176 => 56,  169 => 52,  163 => 50,  161 => 49,  154 => 44,  151 => 43,  142 => 41,  138 => 40,  133 => 39,  131 => 38,  126 => 37,  124 => 36,  117 => 34,  110 => 29,  107 => 28,  104 => 27,  101 => 26,  98 => 25,  95 => 24,  92 => 23,  89 => 22,  86 => 21,  84 => 20,  66 => 6,  60 => 5,  48 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block title %} {% endblock %}*/
/* {%block doctor%} class="active" {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% if not app.user %}    */
/*     <div class="">*/
/*             <div class="page-title">*/
/*                 <div class="title">*/
/*                     <h3>&nbsp;DOCTOR</h3>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="clearfix"></div>*/
/* */
/*             <div class="row">*/
/*                 <div class="col-md-12 col-sm-12 col-xs-12">*/
/*                     <div class="x_panel" >*/
/*                         <br/>*/
/*                  */
/*                     {% set firstname1 = randomdoc1.firstName %}*/
/*             {% set lastname1 = randomdoc1.lastName %}*/
/*             {% set fop1 = randomdoc1.fieldOfPractice %}*/
/*             {% set firstname2 = randomdoc2.firstName %}*/
/*             {% set lastname2 = randomdoc2.lastName %}*/
/*             {% set fop2 = randomdoc2.fieldOfPractice %}*/
/*             {% set firstname3 = randomdoc3.firstName %}*/
/*             {% set lastname3 = randomdoc3.lastName %}*/
/*             {% set fop3 = randomdoc3.fieldOfPractice %}*/
/*                 */
/*             <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">*/
/*                         <div class="well profile_view">*/
/*                             <div class="col-sm-12">*/
/*                                     <div class="left col-xs-7">*/
/*                                         <i><h3><b>{{firstname1}} {{lastname1}}</b></h3></i>*/
/*                                         */
/*                                         {% if fop1 == "General"%}*/
/*                                              <h4>{{fop1}}</h4>*/
/*                                         {% elseif fop1 == "Specialized" %}*/
/*                                             <h4>{{fop1}}</h4>*/
/*                                             {% for specialty in randomdoc1.specialties | slice(0,1)%}*/
/*                                             <button class="btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="{{specialty.specialtyName}}">See Specialties</button>*/
/*                                             {% endfor %}*/
/*                                         {% endif %}*/
/*                                         */
/*                                     </div>*/
/*                                     <div class="right col-xs-5 text-center">*/
/*                                         */
/*                                         <br/>*/
/*                                         {% if randomdoc1.getWebPath == null%}*/
/*                                         <img src="{{asset('gentelella/images/img.jpg')}}" alt="" class="img-circle img-responsive">*/
/*                                         <br/>*/
/*                                         <a href="{{path('doctor_show', {'id': randomdoc1.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                         {% else %}*/
/*                                             <img style="margin-bottom: 17px;" src="{{asset(randomdoc1.getWebPath)}}"alt="" class="img-circle img-responsive">*/
/*                                             <a href="{{path('doctor_show', {'id': randomdoc1.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                         {% endif %}*/
/*                                         <br/>*/
/*                                         */
/*                                     </div>*/
/*                             </div>*/
/*                             <div class="col-xs-12 bottom text-center right">*/
/*                                 <div class="col-xs-12 col-sm-6 emphasis">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">*/
/*                         <div class="well profile_view">*/
/*                             <div class="col-sm-12">*/
/*                                     <div class="left col-xs-7">*/
/*                                         <i><h3><b>{{firstname2}} {{lastname2}}</b></h3></i>*/
/*                                         */
/*                                         {% if fop2 == "General"%}*/
/*                                              <h4>{{fop2}}</h4>*/
/*                                         {% elseif fop2 == "Specialized" %}*/
/*                                             <h4>{{fop2}}</h4>*/
/*                                             {% for specialty in randomdoc2.specialties | slice(0,1)%}*/
/*                                                 <button class="btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="{{specialty.specialtyName}}">See Specialties</button>*/
/*                                             {% endfor %}*/
/*                                         {% endif %}*/
/*                                         */
/*                                     </div>*/
/*                                     <div class="right col-xs-5 text-center">*/
/*                                         */
/*                                         <br/>*/
/*                                         {% if randomdoc2.getWebPath == null%}*/
/*                                         <img src="{{asset('gentelella/images/img.jpg')}}" alt="" class="img-circle img-responsive">*/
/*                                         <br/>*/
/*                                         <a href="{{path('doctor_show', {'id': randomdoc2.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                         {% else %}*/
/*                                             <img style="margin-bottom: 17px;" src="{{asset(randomdoc2.getWebPath)}}"alt="" class="img-circle img-responsive">*/
/*                                              <a href="{{path('doctor_show', {'id': randomdoc2.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                             <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                         {% endif %}*/
/*                                         <br/>*/
/*                                         */
/*                                     </div>*/
/*                             </div>*/
/*                             <div class="col-xs-12 bottom text-center right">*/
/*                                 <div class="col-xs-12 col-sm-6 emphasis">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">*/
/*                         <div class="well profile_view">*/
/*                             <div class="col-sm-12">*/
/*                                     <div class="left col-xs-7">*/
/*                                         <i><h3><b>{{firstname3}} {{lastname3}}</b></h3></i>*/
/*                                         */
/*                                         {% if fop3 == "General"%}*/
/*                                             <h4>{{fop3}}</h4>*/
/*                                         {% elseif fop3 == "Specialized" %}*/
/*                                             <h4>{{fop3}}</h4>*/
/*                                             {% for specialty in randomdoc3.specialties | slice(0, 1) %}              */
/*                                             <button class="btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="{{specialty.specialtyName}}">See Specialties</button>*/
/*                                             {% endfor %}*/
/*                                         {% endif %}*/
/*                                         */
/*                                     </div>*/
/*                                     <div class="right col-xs-5 text-center">*/
/*                                         */
/*                                         <br/>*/
/*                                         {% if randomdoc3.getWebPath == null%}*/
/*                                         <img src="{{asset('gentelella/images/img.jpg')}}" alt="" class="img-circle img-responsive">*/
/*                                         <br/>*/
/*                                         <a href="{{path('doctor_show', {'id': randomdoc3.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                                <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                             <br/>*/
/*                                             <br/>*/
/*                                         {% else %}*/
/*                                             <img style="margin-bottom: 17px;" src="{{asset(randomdoc3.getWebPath)}}"alt="" class="img-circle img-responsive">*/
/*                                              <a href="{{path('doctor_show', {'id': randomdoc3.id})}}"><button type="button" class="btn btn-primary btn-xs"> */
/*                                                <i class="fa fa-user"></i> View Profile </button></a>*/
/*                                         {% endif %}*/
/*                                         */
/*                                         */
/*                                             */
/*                                     </div>*/
/*                             </div>*/
/*                             <div class="col-xs-12 bottom text-center right">*/
/*                                 <div class="col-xs-12 col-sm-6 emphasis">*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*         </div>*/
/*                 */
/*             <div class="row">*/
/*                 <div class="col-md-6 col-xs-12">*/
/*                     <div class="x_panel" >*/
/*                         <center><h3 style ="font-size: 25px;">SPECIALTIES</h3></center>*/
/*                         <div style="overflow-y: scroll; height:400px;">*/
/*                         {% for specialty in specialties %}                */
/*                             <a href={{path('searchBySpecialty', {'specialty': specialty.specialtyName})}}><center><h4 style = "font-family: Century Gothic; font-size: 15px;"> <i class="fa fa-dot-circle-o"></i> {{ specialty.specialtyName }} </h4></center></a>*/
/*                         {% endfor %}*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             */
/*             */
/*                 <div class="col-md-6 col-xs-12">*/
/*                     <div class="x_panel" >*/
/*                         <center><h3 style="font-size: 25px;">GENERAL DOCTORS</h3></center>*/
/*                         <div style="overflow-y: scroll; height:400px;">*/
/*                         {% for entity in entities|e %}                */
/*                             {% if entity.fieldOfPractice == "General"%}*/
/*                                 <center><i class="fa fa-user-md" style="font-size: 20px;"></i></center>  */
/*                                 <a href="{{path('doctor_show', {'id': entity.id})}}"><center><h4 style="font-family: Century Gothic; font-size: 16px;"> {{entity.firstName}} {{entity.lastName}}</h4></center></a> */
/*                                 <div class="ln_solid"></div>*/
/*                             {% endif %}*/
/*                         {% endfor %}*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>    */
/*                 */
/*         </div>*/
/*      {% else  %}*/
/*      <div class="row">*/
/*                         <div class="col-md-8">*/
/*                             <div class="x_panel">*/
/*                                     <h3><b>Appointments Today ({{ "now"|date("m/d/Y", "Asia/Manila") }})</b></h3>*/
/*                                 <div class="x_content">*/
/*                                     <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">*/
/*                                         {% for clinic in clinics %}*/
/*                                         <div class="panel">*/
/*                                             <a class="panel-heading" role="tab" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">*/
/*                                                  <h4 class="panel-title"style="font-family: Century Gothic; font-size: 16px; color:#379DFF;"><b><i class="fa fa-hospital-o"></i>&emsp;{{clinic.clinicName}}</b></h4>*/
/*                                             </a>*/
/*                                             <div class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">*/
/*                                                 <div class="panel-body">*/
/*                                                     <!-- start project list -->*/
/*                                                 <table class="table table-striped projects">*/
/*                                                     <thead>*/
/*                                                         <tr>*/
/*                                                             <th>Name</th>*/
/*                                                             <th>Date Created</th>*/
/*                                                             <th><center>Appointment Code</center></th>*/
/*                                                             <th>Status</th>*/
/*                                                         </tr>*/
/*                                                     </thead>*/
/*                                                     <tbody>*/
/*                                                         {% for appointment in appointments %}*/
/*                                                             {% if clinic.id == appointment.clinic.id %}*/
/*                                                                 <tr>*/
/*                                                                     <td>*/
/*                                                                         <a id="appointment-{{appointment.id}}" class="appointment" data-toggle="modal" data-target="#appointmentmodal" data-backdrop="static" data-keyboard="false">   <b style="color: #379DFF;"><i>{{appointment.firstName}} {{appointment.lastName}}</i></b> </a>                                            */
/*                                                                     </td>*/
/*                                                                     <td>*/
/*                                                                         {{appointment.dateCreated | date("m/d/Y")}}*/
/*                                                                     </td>*/
/*                                                                     <td>*/
/*                                                                         <center>{{appointment.appointmentCode}}</center>*/
/*                                                                     </td>*/
/*                                                                     <td>*/
/*                                                                         <div id ="statusdiv-{{appointment.id}}">*/
/*                                                                             {% if appointment.isAssessed %}*/
/*                                                                                 <button id ="status-{{appointment.id}}" type="button" class="btn btn-success btn-xs status-btn">*/
/*                                                                                     <i id="iconclass" class="fa fa-check"></i>*/
/*                                                                                 </button>*/
/*                                                                             {% else %} */
/*                                                                                 <button id ="status-{{appointment.id}}" type="button" class="btn btn-danger btn-xs status-btn">*/
/*                                                                                     <i id="iconclass" class="fa fa-minus"></i>*/
/*                                                                                 </button>*/
/*                                                                             {% endif %}*/
/*                                                                         </div>*/
/*                                                                     </td>*/
/*                                                                 </tr>*/
/*                                                             {% endif %}*/
/*                                                         {% endfor %}*/
/*                                                     </tbody>*/
/*                                                 </table> */
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         {% endfor %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-4">*/
/*                             <div class="x_panel">*/
/*                                     <h3><b>Group Invitations</b></h3>*/
/*                                 <div class="x_content">*/
/*                                 <ul class="list-unstyled msg_list">*/
/*                                    {% for inv in invitations %}*/
/*                                     <li>*/
/*                                           <span>*/
/*                                             <a>{{inv.doctorInviter.firstName}} {{inv.doctorInviter.lastName}}</a>invited you to join the group*/
/*                                             <a>{{inv.group.groupName}}</a>*/
/*                                           </span>*/
/*                                           <a>*/
/*                                                  <button id="confirm-{{inv.id}}" title="Accept invitation" data-toggle="tooltip" data-placement="right" type="button" class="btn btn-primary btn-xs btn-confirm "> <i class="fa fa-check-circle"></i></button>*/
/*                                                  <button id="decline-{{inv.id}}" title="Decline invitation" data-toggle="tooltip" data-placement="right" type="button" class="btn btn-danger btn-xs btn-decline "> <i class="fa fa-times-circle"></i></button>*/
/*                                           </a>    */
/*                                         </a>*/
/*                                     </li>*/
/*                                    {% endfor %}*/
/*                                 </ul>*/
/*                             </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 <div id="appointmentmodal" class="modal fade bs-example-modal-sm" align="left" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" >*/
/*                         <div class="modal-dialog modal-sm">*/
/*                             <div class="modal-content">*/
/*                                 <div class="modal-header">*/
/*                                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>*/
/*                                         </button>*/
/*                                     <h4 class="modal-title" style="font-family: Century Gothic;" id="myModalLabel">Appointment Details</h4>*/
/*                                 </div>*/
/*                                 <div class="modal-body">*/
/*                                      <center><img hidden id="loading" src="{{asset('charisma/img/ajax-loaders/ajax-loader-1.gif')}}" ></center>*/
/*                                      <div hidden id="results">*/
/*                                          <b>Name:</b>*/
/*                                          <div id="aname"></div><br>*/
/*                                          <b>Contact Number:</b>*/
/*                                          <div id="acontact"></div><br>*/
/*                                          <b>Email Address:</b>*/
/*                                          <div id="aemail"></div><br>*/
/*                                          <b>Appointment Code:</b>*/
/*                                          <div id="acode"></div><br>*/
/*                                          <b>Notes:</b>*/
/*                                          <div style="word-wrap: break-word;" id="anote"></div><br>*/
/*                                     </div>*/
/*                             </div>*/
/*                                 */
/*                         </div>*/
/*                    </div>*/
/* */
/* <script>    */
/*                                     var confirmBtnId;*/
/*                                     $(".btn-confirm").click(function(x) {*/
/*                                         x.preventDefault();*/
/*                                         confirmBtnId = this.id;*/
/*                                         confirmBtnId = confirmBtnId.replace('confirm-', '');*/
/*                                          $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('confirm_invite') }}',*/
/*                                             data: {id: confirmBtnId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                location.reload();*/
/*                                                 console.log(response);*/
/*                                             },*/
/*                                             error: function() {*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*                                     */
/*                                     var declineBtnId;*/
/*                                     $(".btn-decline").click(function(x) {*/
/*                                         x.preventDefault();*/
/*                                         declineBtnId = this.id;*/
/*                                         declineBtnId = declineBtnId.replace('decline-', '');*/
/*                                          $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('decline_invite') }}',*/
/*                                             data: {id: declineBtnId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                location.reload();*/
/*                                                 console.log(response);*/
/*                                             },*/
/*                                             error: function() {*/
/*                                                  location.reload();*/
/*                                                 alert('error');*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*                                    */
/*                                    var statusBtnId;*/
/*                                     $(".status-btn").click(function(x) {*/
/*                                         x.preventDefault();*/
/*                                         statusBtnId = this.id;*/
/*                                         statusBtnId = statusBtnId.replace('status-', '');*/
/*                                          $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('status_change') }}',*/
/*                                             data: {id: statusBtnId},*/
/*                                             dataType: 'json',*/
/*                                             success: function(response) {*/
/*                                                */
/*                                                 console.log(response);*/
/*                                                 $('#statusdiv-'+statusBtnId).load(document.URL +  ' #statusdiv-'+statusBtnId);*/
/*                                                 */
/*                                                 */
/*                                             },*/
/*                                             error: function() {*/
/*                                                 */
/*                                                 alert(statusBtnId);*/
/*                                             }*/
/*                                         });*/
/*                                     });*/
/*                                     */
/*                                      var appointment;*/
/*                                     $(".appointment").click(function(x) {*/
/*                                         x.preventDefault();*/
/*                                         appointment = this.id;*/
/*                                         appointment = appointment.replace('appointment-', '');*/
/*                                          $.ajax({*/
/*                                             type: "POST",*/
/*                                             url: '{{ path('appointment_view') }}',*/
/*                                             data: {id: appointment},*/
/*                                             dataType: 'json',*/
/*                                             beforeSend: function(){*/
/*                                                 $('#loading').show();*/
/*                                                  $('#results').hide();*/
/*                                                 },*/
/*                                             complete: function(){*/
/*                                                 $('#loading').hide();*/
/*                                                 $('#results').show();*/
/*                                                 },*/
/*                                             success: function(data) {*/
/*                                                 var len = data.length;*/
/*                                                 var txt = "";*/
/*                                                   if(len > 0){*/
/*                                                     for(var i=0;i<len;i++){*/
/*                                                            $("#aname").html(data[i].firstName+" "+data[i].lastName);*/
/*                                                            $("#acontact").html(data[i].contactNumber);*/
/*                                                            $("#aemail").html(data[i].emailAddress);*/
/*                                                            $("#acode").html(data[i].appointmentCode);*/
/*                                                            $("#anote").html(data[i].note);*/
/*                                                            */
/*                                                           }*/
/*                                                     */
/*                                                 }*/
/* */
/*                                                 */
/*                                                 console.log(data);*/
/*                                             },error: function() {*/
/*                                              alert('error');*/
/*                                               }*/
/*                                             });*/
/*                                         */
/*                                     });*/
/* */
/*                                    */
/*   </script>*/
/*                */
/* {% endif %}*/
/* {% endblock %}*/
/* */
