<?php

/* ResearchProjectMyProjectBundle:Clinic:show.html.twig */
class __TwigTemplate_4e412e825f60fc7d99a04579d842a8162f07c94a3e28190908f05e9b4d5b96a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::basenosidebar.html.twig");

        $this->blocks = array(
            'clinic' => array($this, 'block_clinic'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_clinic($context, array $blocks = array())
    {
        echo " class=\"active\" ";
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "    <meta name=\"viewport\" content=\"initial-scale=1.0\">
    <meta charset=\"utf-8\">
    <title>Simple Map</title>
    <meta name=\"viewport\" content=\"initial-scale=1.0\">
    <meta charset=\"utf-8\">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
";
    }

    // line 18
    public function block_body($context, array $blocks = array())
    {
        // line 20
        echo "<div class=\"\">
                    <div class=\"page-title\">
                        <div class=\"title\">
                            <h3>&nbsp;CLINIC</h3>
                        </div>
                    </div>
                    <div class=\"clearfix\"></div>

                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\">
                                   <h3>Clinic Information</h3>
                               
                                <div class=\"x_content\">
                                    </br>
                                    </br>
                                    <div class=\"col-md-7 col-sm-7 col-xs-12\">
                                        <div class=\"product-image\">
                                            <div id=\"map\" style=\"width: 475px; height: 500px;\"></div>
                                        </div>
                                            <center><h3>Location of <i>";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicName"), "html", null, true);
        echo "</i></h3></center>
                                    </div>
                             
                                    
                                    
                                    <div class=\"col-md-5 col-sm-5 col-xs-12\" style=\"border:0px solid #e5e5e5;\">

                                        <div class=\"\" role=\"tabpanel\" data-example-id=\"togglable-tabs\">
                                        <ul id=\"myTab1\" class=\"nav nav-tabs bar_tabs right\" role=\"tablist\">
                                            <li role=\"presentation\" class=\"active\"><a href=\"#tab_content11\" id=\"home-tabb\" role=\"tab\" data-toggle=\"tab\" aria-controls=\"home\" aria-expanded=\"true\">Profile</a>
                                            </li>
                                            <li role=\"presentation\" class=\"\"><a href=\"#tab_content22\" role=\"tab\" id=\"profile-tabb\" data-toggle=\"tab\" aria-controls=\"profile\" aria-expanded=\"false\">Schedule</a>
                                            </li>
                                        </ul>
                                        <div id=\"myTabContent2\" class=\"tab-content\">
                                            <div role=\"tabpanel\" class=\"tab-pane fade active in\" id=\"tab_content11\" aria-labelledby=\"home-tab\">
                                                <center>
                                                    
                                                <div class=\"profile_img\">
                                                    <!-- end of image cropping -->
                                                <div id=\"crop-avatar\">
                                                    <!-- Current avatar -->
                                                <div class=\"avatar-view\" title=\"Change the avatar\">
                                                    <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/images/picture.jpg"), "html", null, true);
        echo "\" alt=\"Avatar\">
                                                </div>

                                                <!-- Cropping modal -->
                                                <div class=\"modal fade\" id=\"avatar-modal\" aria-hidden=\"true\" aria-labelledby=\"avatar-modal-label\" role=\"dialog\" tabindex=\"-1\">
                                                    <div class=\"modal-dialog modal-lg\">
                                                        <div class=\"modal-content\">
                                                            <form class=\"avatar-form\" action=\"crop.php\" enctype=\"multipart/form-data\" method=\"post\">
                                                                <div class=\"modal-header\">
                                                                    <button class=\"close\" data-dismiss=\"modal\" type=\"button\">&times;</button>
                                                                    <h4 class=\"modal-title\" id=\"avatar-modal-label\">Change Avatar</h4>
                                                                </div>
                                                                <div class=\"modal-body\">
                                                                    <div class=\"avatar-body\">

                                                                        <!-- Upload image and data -->
                                                                        <div class=\"avatar-upload\">
                                                                            <input class=\"avatar-src\" name=\"avatar_src\" type=\"hidden\">
                                                                            <input class=\"avatar-data\" name=\"avatar_data\" type=\"hidden\">
                                                                            <label for=\"avatarInput\">Local upload</label>
                                                                            <input class=\"avatar-input\" id=\"avatarInput\" name=\"avatar_file\" type=\"file\">
                                                                        </div>

                                                                        <!-- Crop and preview -->
                                                                        <div class=\"row\">
                                                                            <div class=\"col-md-9\">
                                                                                <div class=\"avatar-wrapper\"></div>
                                                                            </div>
                                                                            <div class=\"col-md-3\">
                                                                                <div class=\"avatar-preview preview-lg\"></div>
                                                                                <div class=\"avatar-preview preview-md\"></div>
                                                                                <div class=\"avatar-preview preview-sm\"></div>
                                                                            </div>
                                                                        </div>

                                                                        <div class=\"row avatar-btns\">
                                                                            <div class=\"col-md-9\">
                                                                                <div class=\"btn-group\">
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-90\" type=\"button\" title=\"Rotate -90 degrees\">Rotate Left</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-15\" type=\"button\">-15deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-30\" type=\"button\">-30deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"-45\" type=\"button\">-45deg</button>
                                                                                </div>
                                                                                <div class=\"btn-group\">
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"90\" type=\"button\" title=\"Rotate 90 degrees\">Rotate Right</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"15\" type=\"button\">15deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"30\" type=\"button\">30deg</button>
                                                                                    <button class=\"btn btn-primary\" data-method=\"rotate\" data-option=\"45\" type=\"button\">45deg</button>
                                                                                </div>
                                                                            </div>
                                                                            <div class=\"col-md-3\">
                                                                                <button class=\"btn btn-primary btn-block avatar-save\" type=\"submit\">Done</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class=\"modal-footer\">
                                                                <button class=\"btn btn-default\" data-dismiss=\"modal\" type=\"button\">Close</button>
                                                            </div> -->
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.modal -->

                                                <!-- Loading state -->
                                                <div class=\"loading\" aria-label=\"Loading\" role=\"img\" tabindex=\"-1\"></div>
                                                </div>
                                                <!-- end of image cropping -->
                                        
                                            </div>
                                              ";
        // line 134
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 135
            echo "
                                                <div hidden id='successmessage'>
                                                    ";
            // line 137
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
                                                </div>

                                              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 141
        echo "                                          ";
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user")) {
            // line 142
            echo "                                              ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "doctors"));
            foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
                // line 143
                echo "                                                 ";
                if (($this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "id") == $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user"), "id"))) {
                    // line 144
                    echo "                                                    <div id='addressError'>
                                                       .
                                                    </div>
                                                  ";
                }
                // line 147
                echo "  
                                              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 148
            echo "  
                                        ";
        }
        // line 150
        echo "                                            <h3 class=\"prod_title\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicName"), "html", null, true);
        echo "</h3>
                                            ";
        // line 151
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "getDoctors"));
        foreach ($context['_seq'] as $context["_key"] => $context["doctor"]) {
            // line 152
            echo "                                                      <ul>
                                                 <li>";
            // line 153
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["doctor"]) ? $context["doctor"] : $this->getContext($context, "doctor")), "firstName"), "html", null, true);
            echo "</a></li> 
                                                    </ul>
                                             ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['doctor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 156
        echo "                                            <h4><div id=\"address\"><i class=\"fa fa-map-marker\"></i>&emsp;";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicAddress"), "html", null, true);
        echo "</div></h4>
                                            <h4><i class=\"fa fa-mobile\"></i>&emsp;";
        // line 157
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicContact"), "html", null, true);
        echo "</h4>
                                            <p><i class=\"fa fa-pencil-square-o\"></i>&emsp;";
        // line 158
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicDescription"), "html", null, true);
        echo "</p>
                                            <div id=\"latitude\" hidden></div>
                                            <div id=\"longitude\" hidden></div>
                                            <form id=\"demo-form\" data-parsley-validate=\"\">

  

</form>
                                            <br/>
                                            
                                            <div class=\"\">
                                                <button id =\"amodal\" type=\"button\" class=\"btn btn-success btn-lg\" data-toggle=\"modal\" data-target=\"#appointmentmodal\" data-backdrop=\"static\" data-keyboard=\"false\">Set an Appointment</button>
                                            </div>
                                          
                                            </center>
                                        </div>
                                        <div role=\"tabpanel\" class=\"tab-pane fade\" id=\"tab_content22\" aria-labelledby=\"profile-tab\">
                                            <div class=\"\">
                                            <ul class=\"list-inline prod_color\">
                                                <center>
                                                <li><h3><i class=\"fa fa-calendar\"></i><b>&emsp;CLINIC SCHEDULE</b></h3></li>
                                                
                                                    <li>
                                                        <center>
                                                            <h3>Sunday</h3>
                                                            ";
        // line 183
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunOpenTime"), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunCloseTime"), "H:i") == "00:00"))) {
            // line 184
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 186
            echo "                                                                <div class=\"color bg-blue\"></div>
                                                                <h3><i><b>";
            // line 187
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunOpenTime"), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunCloseTime"), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 189
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Monday</h3>
                                                            ";
        // line 194
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monOpenTime"), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monCloseTime"), "H:i") == "00:00"))) {
            // line 195
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 197
            echo "                                                                <div class=\"color bg-blue\"></div>
                                                                <h3><i><b>";
            // line 198
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monOpenTime"), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monCloseTime"), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 200
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Tuesday</h3>
                                                            ";
        // line 205
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueOpenTime"), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueCloseTime"), "H:i") == "00:00"))) {
            // line 206
            echo "                                                               <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 208
            echo "                                                                <div class=\"color bg-blue\"></div>
                                                                <h3><i><b>";
            // line 209
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueOpenTime"), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tueCloseTime"), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 211
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Wednesday</h3> 
                                                            ";
        // line 216
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedOpenTime"), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedCloseTime"), "H:i") == "00:00"))) {
            // line 217
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 219
            echo "                                                                <div class=\"color bg-blue\"></div>
                                                                <h3><i><b>";
            // line 220
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedOpenTime"), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "wedCloseTime"), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 222
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Thursday </h3>
                                                            ";
        // line 227
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuOpenTime"), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuCloseTime"), "H:i") == "00:00"))) {
            // line 228
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 230
            echo "                                                                <div class=\"color bg-blue\"></div>
                                                                <h3><i><b>";
            // line 231
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuOpenTime"), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "thuCloseTime"), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 233
        echo "                                                        </center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Friday </h3>
                                                            ";
        // line 238
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friOpenTime"), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friCloseTime"), "H:i") == "00:00"))) {
            // line 239
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 241
            echo "                                                                <div class=\"color bg-blue\"></div>
                                                                <h3><i><b>";
            // line 242
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friOpenTime"), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "friCloseTime"), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 243
        echo "</h4>
                                                        <center>
                                                    </li>
                                                    <li>
                                                        <center>
                                                            <h3>Saturday </h3> 
                                                            ";
        // line 249
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satOpenTime"), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satCloseTime"), "H:i") == "00:00"))) {
            // line 250
            echo "                                                                <div class=\"color bg-red\"></div> <h3>CLOSED</h3>
                                                            ";
        } else {
            // line 252
            echo "                                                                <div class=\"color bg-blue\"></div>
                                                                <h3><i><b>";
            // line 253
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satOpenTime"), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "satCloseTime"), "H:i"), "html", null, true);
            echo "</b></i></h3>
                                                            ";
        }
        // line 254
        echo "</h4>
                                                        </center>
                                                    </li>
                                                    <div class=\"\">
                                                        <button type=\"button\" class=\"btn btn-success btn\">Set an Appointment</button>
                                                    </div>
                                                    </center>
                                                </ul>
                                            </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    
                    <div id=\"appointmentmodal\" class=\"modal fade bs-example-modal-lg\" align=\"left\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" data-backdrop=\"static\" >
                                    <div class=\"modal-dialog modal-lg\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                                </button>
                                                <h4 class=\"modal-title\" style=\"font-family: Century Gothic;\" id=\"myModalLabel\">Set an Appointment for ";
        // line 280
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "clinicName"), "html", null, true);
        echo "</h4>
                                            </div>
                                            <div class=\"modal-body\">
                                              
                                                
                                                 <!-- Smart Wizard -->
                                   
                                    <div id=\"wizard\" class=\"form_wizard wizard_horizontal\">
                                        <ul class=\"wizard_steps\">
                                            <li>
                                                <a href=\"#step-1\">
                                                    <span class=\"step_no\">1</span>
                                                    <span class=\"step_descr\">
                                           1<br />
                                            <small>Fill Up Information</small>
                                        </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href=\"#step-2\">
                                                    <span class=\"step_no\">2</span>
                                                    <span class=\"step_descr\">
                                            2<br />
                                            <small>Clinic Schedule</small>
                                        </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href=\"#step-3\">
                                                    <span class=\"step_no\">3</span>
                                                    <span class=\"step_descr\">
                                            3<br />
                                            <small>Confirm Appointment</small>
                                        </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href=\"#step-4\">
                                                    <span class=\"step_no\">4</span>
                                                    <span class=\"step_descr\">
                                            4<br />
                                            <small>Success!</small>
                                        </span>
                                                </a>
                                            </li>
                                        </ul>
                                        
                                           ";
        // line 327
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), 'form_start', array("attr" => array("role" => "form", "novalidate" => "novalidate", "id" => "appointmentForm", "class" => "form-horizontal form-label-left")));
        echo "
                                            <div id=\"step-1\">
                                            <h2 class=\"StepTitle\">Fill Up Information</h2>
                                                <div class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >First Name <span class=\"required\">*</span>
                                                    </label>
                                                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                        ";
        // line 334
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "firstName"), 'widget', array("attr" => array("id" => "firstName", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 335
        echo "
                                                    </div>
                                                </div>
                                                <div class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Last Name <span class=\"required\">*</span>
                                                    </label>
                                                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                        ";
        // line 342
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "lastName"), 'widget', array("attr" => array("id" => "lastName", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 343
        echo "
                                                    </div>
                                                </div>
                                                <div class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Email Address <span class=\"required\">*</span>
                                                    </label>
                                                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                        ";
        // line 350
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "emailAddress"), 'widget', array("attr" => array("id" => "emailAddress", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 351
        echo "
                                                    </div>
                                                </div>
                                                <div class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Contact Number  <span class=\"required\">*</span>
                                                    </label>
                                                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                        ";
        // line 358
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "contactNumber"), 'widget', array("attr" => array("id" => "emailAddress", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 359
        echo "
                                                    </div>
                                                </div>
                                                 <div class=\"item form-group\">
                                                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Note (if any)
                                                    </label>
                                                    <div class=\" col-sm-6 col-xs-12\">
                                                       ";
        // line 366
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "note"), 'widget', array("attr" => array("id" => "note", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control col-md-7 col-xs-12", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 367
        echo "
                                                    
                                                    </div>
                                                </div>
                                               
                                        </div>
                                        <div id=\"step-2\">
                                            <h2 class=\"StepTitle\">Clinic Schedule</h2>
                                                <div class=\"item form-group\">
                                                       <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Pick a Schedule  <span class=\"required\">*</span>
                                                       </label>
                                                        <div class=\" col-sm-6 col-xs-12\">
                                                               ";
        // line 379
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "dateAppointed"), 'widget', array("attr" => array("id" => "emailAddress", "name" => "firstName", "placeholder" => "First Name", "class" => "form-control has-feedback-left", "data-validate-length-range" => "1", "required" => "required", "type" => "text")));
        // line 380
        echo "
                                                               <span class=\"fa fa-calendar-o form-control-feedback left\" aria-hidden=\"true\"></span>
                                                           
                                                          
                                                      </div>
                                                   </div>
                                            <div class=\"item form-group\">
                                                 <div id=\"clinicsched\" class=\" col-sm-6 col-xs-12\"></div>
                                                
                                            </div>
                                        </div>
                                        <div id=\"step-3\">
                                            <h2 class=\"StepTitle\">Confirm Appointment</h2>
                                           
                                            <div>
                                                
                                                First Name: <div id='firstNameConfirm'></div>
                                                Last Name: <div id='lastNameConfirm'></div>
                                                Email Address: <div id='emailAddressConfirm'></div>
                                                Contact Number: <div id='contactNumberConfirm'></div>
                                                Appointment Date: <div id='appointmentDateConfirm'></div>
                                                Appointment Code: <div id='appointmentCodeConfirm'>";
        // line 401
        echo twig_escape_filter($this->env, (isset($context["appointmentCode"]) ? $context["appointmentCode"] : $this->getContext($context, "appointmentCode")), "html", null, true);
        echo "</div>
                                                Note: <div id='noteConfirm'></div>
                                                
                                                
                                            </div>
                                            
                                        </div>
                                        <div id=\"step-4\">
                                            <h2 class=\"StepTitle\">Step 4 Content</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                            </p>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                            </p>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                            </p>
                                            <a href=\"\">";
        // line 418
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), "Save"), 'widget', array("attr" => array("class" => "btn btn-info modal-submit", "id" => "submitbtn")));
        echo "</a> 
                                        </div>
                                         ";
        // line 420
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["appointmentForm"]) ? $context["appointmentForm"] : $this->getContext($context, "appointmentForm")), 'form_end');
        echo "
                                        
                                    </div>
                                    
                                    <!-- End SmartWizard Content -->

                                        
                                                
                                            </div>
                               
                                        </div>
                                    </div>
                                </div>
                    
                    <div id=\"schedulemodal\" class=\"modal fade bs-example-modal-lg\" align=\"left\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" data-backdrop=\"static\" >
                                    <div class=\"modal-dialog modal-lg\">
                                        <div class=\"modal-content\">

                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span>
                                                </button>
                                                <h4 class=\"modal-title\" style=\"font-family: Century Gothic;\" id=\"myModalLabel\">Clinic Schedule</h4>
                                            </div>
                                            <div class=\"modal-body\">
                                              k
                                                <div id=\"results\"></div>
                                                
                                            </div>
                               
                                        </div>
                                    </div>
                   </div>
                    
                    ";
        // line 454
        echo "                    <div id=\"appointmentsched\" hidden>
                        
                        <div id=\"sundaysched\">
                            ";
        // line 457
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunOpenTime"), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunCloseTime"), "H:i") == "00:00"))) {
            // line 458
            echo "                            <div class=\"color bg-red\"></div> <div id='sundaystatus'>CLOSED</div>
                            ";
        } else {
            // line 460
            echo "                              <div class=\"color bg-green\"></div><div id='sundaystatus'>OPEN</div>
                                   <i><b>";
            // line 461
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunOpenTime"), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sunCloseTime"), "H:i"), "html", null, true);
            echo "</b></i>
                            ";
        }
        // line 462
        echo "                                 
                        </div>
                        <div id=\"mondaysched\">
                            ";
        // line 465
        if (((twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monOpenTime"), "H:i") == "00:00") && (twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monCloseTime"), "H:i") == "00:00"))) {
            // line 466
            echo "                                  <div class=\"color bg-red\"></div> <div id='mondaystatus'>CLOSED</div>
                            ";
        } else {
            // line 468
            echo "                              <div class=\"color bg-green\"></div><div id='mondaystatus'>OPEN</div>
                                   <i><b>";
            // line 469
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monOpenTime"), "H:i"), "html", null, true);
            echo " up to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "monCloseTime"), "H:i"), "html", null, true);
            echo "</b></i>
                            ";
        }
        // line 470
        echo "                                 
                        </div>
                        
                        
                        
                    </div>
                    
                    
        </div>

    <script type=\"text/javascript\" src=\"";
        // line 480
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/jquery-ui/external/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
     <script type=\"text/javascript\" src=\"";
        // line 481
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/jquery-ui/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 482
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("notify/notify.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 483
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/parsley/parsley.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 484
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/moment.min2.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 485
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/datepicker/daterangepicker.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 486
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/jquery.geocomplete.js"), "html", null, true);
        echo "\"></script>
    <script src=\"http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places\"></script>
       <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDRJhfHphrYhN10mIoK_5CtgJQ0M9qbO14&callback=initMap\"
        async defer></script>
<script type=\"text/javascript\">
        \$(document).ready(function () {
            \$('#form_dateAppointed').datepicker({
                minDate: Date.now(),
                onSelect: function(dateText, evnt) {
                    
                    onSelectDate(dateText);
                   

                }
            });
        });
        
        function onSelectDate(dateText){ 
         var dateee = new Date(dateText);
         var weekday = new Array(7);
            weekday[0]=  \"Sunday\";
            weekday[1] = \"Monday\";
            weekday[2] = \"Tuesday\";
            weekday[3] = \"Wednesday\";
            weekday[4] = \"Thursday\";
            weekday[5] = \"Friday\";
            weekday[6] = \"Saturday\";
            
            var s = \$('#sundaysched').html();
            var m = \$('#mondaysched').html();
            
            var daydate = weekday[dateee.getDay()];
        
        
            if(daydate === 'Sunday'){
                 \$('#clinicsched').html(s);
            }
            if(daydate === 'Monday'){
                 \$('#clinicsched').html(m);
            }
        
        
        
        
       
        
        
        
    }
    </script>     
<script>
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 18,
    center: {lat: -34.397, lng: 150.644}
  });
  var geocoder = new google.maps.Geocoder();

   
    geocodeAddress(geocoder, map);
  
}

function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById('address').innerHTML;
  var latitude = document.getElementById('latitude');
  var longitude = document.getElementById('longitude');
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
        latitude.innerHTML=results[0].geometry.location.lat();
        longitude.innerHTML=results[0].geometry.location.lng();
      resultsMap.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location
      });
    } else {
        
    }
       
  });
}

var panorama;
function initialize() {
  var latitude = document.getElementById('latitude').innerHTML;
  var longitude = document.getElementById('longitude').innerHTML;
  panorama = new google.maps.StreetViewPanorama(
      document.getElementById('street-view'),
      {
        position: {lat: latitude, lng: longitude},
        pov: {heading: 165, pitch: 0},
        zoom: 1
      });
}
    </script>
 
<script>
    function isFutureDate(idate){
    var today = new Date().getTime(),
        idate = idate.split(\"/\");

    idate = new Date(idate[2], idate[1] - 1, idate[0]).getTime();
    return (today - idate) < 0 ? true : false;
}

function checkDate(){
    var idate = document.getElementById(\"form_dateAppointed\"),
        dateReg = /(0[1-9]|[12][0-9]|3[01])[\\/](0[1-9]|1[012])[\\/]201[4-9]|20[2-9][0-9]/;

    if(!dateReg.test(idate.value)){
       \$(\"#form_dateAppointed\").notify(\"Invalid Date\",  
  { position: \"right\", autohide: \"false\" });
    }
}

\$('#appointmentmodal').on('hidden.bs.modal', function (e) {
  \$(this)
    .find(\"input,textarea,select\")
       .val('')
       .end()
    .find(\"input[type=checkbox], input[type=radio]\")
       .prop(\"checked\", \"\")
       .end();
});









</script>
  <script type=\"text/javascript\" src=\"";
        // line 621
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("gentelella/js/wizard/jquery.smartWizard.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\">
\$(document).ready(function(){
    // Smart Wizard         
    \$('#wizard').smartWizard({
        onLeaveStep:leaveAStepCallback,
        onFinish:onFinishCallback
    });

    function leaveAStepCallback(obj, context){


        if(context.fromStep === 2){
           fillInfo();
        }


        return validateSteps(context.fromStep); // return false to stay on step and true to continue navigation 
    }

    function onFinishCallback(objs, context){
        if(validateAllSteps()){
            \$('form').submit();
            
        }
    }

    // Your Step validation logic
    function validateSteps(stepnumber){
        var isStepValid = true;
        // validate step 1
        if(stepnumber == 2){
            if(\$('#form_dateAppointed').val() == ''){
                \$(\"#form_dateAppointed\").notify(\"Plese pick a date\",  
                  { position: \"right\", autohide: \"false\" });
                  isStepValid = false;
                 }
                var idate = document.getElementById(\"form_dateAppointed\"),
                dateReg = /(0[1-9]|[12][0-9]|3[01])[\\/](0[1-9]|1[012])[\\/]201[4-9]|20[2-9][0-9]/;
                
                
                        if(dateReg.test(idate.value)){
                            \$(\"#form_dateAppointed\").notify(\"Invalid Date\",  
                            { position: \"right\", autohide: \"false\" });
                            isStepValid = false;
                            }
                            
                          
            var dateee = new Date( \$('#form_dateAppointed').val());
            var weekday = new Array(7);
            weekday[0]=  \"Sunday\";
            weekday[1] = \"Monday\";
            weekday[2] = \"Tuesday\";
            weekday[3] = \"Wednesday\";
            weekday[4] = \"Thursday\";
            weekday[5] = \"Friday\";
            weekday[6] = \"Saturday\";

            var daydate = weekday[dateee.getDay()];
            
            if(daydate === 'Sunday'){
                if(\$('#sundaystatus').text() === 'CLOSED'){
                   isStepValid = false;
                }
              } else if(daydate === 'Monday'){
                  if(\$('#mondaystatus').text() === 'CLOSED'){
                   isStepValid = false;
                }
              }
            
            
            
               

                
        }
        // ... 
        return isStepValid;
    }
    function validateAllSteps(){
        var isStepValid = true;
        // all step validation logic     
        return isStepValid;
    }
    
    function fillInfo(){
         \$('#firstNameConfirm').html(\$(\"#form_firstName\").val());
         \$('#lastNameConfirm').html(\$(\"#form_lastName\").val());
         \$('#contactNumberConfirm').html(\$(\"#form_contactNumber\").val());
         \$('#emailAddressConfirm').html(\$(\"#form_emailAddress\").val());
         \$('#noteConfirm').html(\$(\"#form_note\").val());
         \$('#appointmentDateConfirm').html(\$(\"#form_dateAppointed\").val());
        
    }
    
    
    
});
</script>

<script type=\"text/javascript\">
\$(document).ready(function(){
    
    if (\$('#successmessage').length){
        
            new PNotify({
                                title: 'Success!',
                                text: 'You have successfully created an appointment!',
                                type: 'success'
                            });
    }
    if (\$('#addressError').length){
        
            new PNotify({
                                title: 'Sorry!',
                                text: 'It seems that we can\\'t map your address. Try to edit your clinic address.',
                                type: 'error',
                                hide: false
                            });
            }
    
    
});
</script>
   
";
    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Clinic:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  889 => 621,  751 => 486,  747 => 485,  743 => 484,  739 => 483,  735 => 482,  731 => 481,  727 => 480,  715 => 470,  708 => 469,  705 => 468,  701 => 466,  699 => 465,  694 => 462,  687 => 461,  684 => 460,  680 => 458,  678 => 457,  673 => 454,  637 => 420,  632 => 418,  612 => 401,  589 => 380,  587 => 379,  573 => 367,  571 => 366,  562 => 359,  560 => 358,  551 => 351,  549 => 350,  540 => 343,  538 => 342,  529 => 335,  527 => 334,  517 => 327,  467 => 280,  439 => 254,  432 => 253,  429 => 252,  425 => 250,  423 => 249,  415 => 243,  408 => 242,  405 => 241,  401 => 239,  399 => 238,  392 => 233,  385 => 231,  382 => 230,  378 => 228,  376 => 227,  369 => 222,  362 => 220,  359 => 219,  355 => 217,  353 => 216,  346 => 211,  339 => 209,  336 => 208,  332 => 206,  330 => 205,  323 => 200,  316 => 198,  313 => 197,  309 => 195,  307 => 194,  300 => 189,  293 => 187,  290 => 186,  286 => 184,  284 => 183,  256 => 158,  252 => 157,  247 => 156,  238 => 153,  235 => 152,  231 => 151,  226 => 150,  222 => 148,  215 => 147,  209 => 144,  206 => 143,  201 => 142,  198 => 141,  188 => 137,  184 => 135,  180 => 134,  106 => 63,  80 => 40,  58 => 20,  55 => 18,  39 => 4,  36 => 3,  30 => 2,);
    }
}
