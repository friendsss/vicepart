<?php

/* ResearchProjectMyProjectBundle:Doctor:home.html.twig */
class __TwigTemplate_bd63639b80f224ead177c8743a561e8b891ebb2e916cfe7450fb1e045204fe38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::basenosidebar.html.twig", "ResearchProjectMyProjectBundle:Doctor:home.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::basenosidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b4a810ab2838e87d1e8445225c01d5829b4d968cf79102a62f40a6994e60261 = $this->env->getExtension("native_profiler");
        $__internal_5b4a810ab2838e87d1e8445225c01d5829b4d968cf79102a62f40a6994e60261->enter($__internal_5b4a810ab2838e87d1e8445225c01d5829b4d968cf79102a62f40a6994e60261_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ResearchProjectMyProjectBundle:Doctor:home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5b4a810ab2838e87d1e8445225c01d5829b4d968cf79102a62f40a6994e60261->leave($__internal_5b4a810ab2838e87d1e8445225c01d5829b4d968cf79102a62f40a6994e60261_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_7577cd23bd123ce886132f20e7485110aa0320782db8d5f95712059fc3a6e30a = $this->env->getExtension("native_profiler");
        $__internal_7577cd23bd123ce886132f20e7485110aa0320782db8d5f95712059fc3a6e30a->enter($__internal_7577cd23bd123ce886132f20e7485110aa0320782db8d5f95712059fc3a6e30a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        
        $__internal_7577cd23bd123ce886132f20e7485110aa0320782db8d5f95712059fc3a6e30a->leave($__internal_7577cd23bd123ce886132f20e7485110aa0320782db8d5f95712059fc3a6e30a_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_8fed8d0e7917bd8f251ebd50043aa0ece5fdd481a929f08e0646f6119408c7b9 = $this->env->getExtension("native_profiler");
        $__internal_8fed8d0e7917bd8f251ebd50043aa0ece5fdd481a929f08e0646f6119408c7b9->enter($__internal_8fed8d0e7917bd8f251ebd50043aa0ece5fdd481a929f08e0646f6119408c7b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "       
";
        
        $__internal_8fed8d0e7917bd8f251ebd50043aa0ece5fdd481a929f08e0646f6119408c7b9->leave($__internal_8fed8d0e7917bd8f251ebd50043aa0ece5fdd481a929f08e0646f6119408c7b9_prof);

    }

    public function getTemplateName()
    {
        return "ResearchProjectMyProjectBundle:Doctor:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 5,  47 => 4,  35 => 2,  11 => 1,);
    }
}
/* {% extends '::basenosidebar.html.twig' %}*/
/* {% block title %} {% endblock %}*/
/* */
/* {% block body %}*/
/*        */
/* {% endblock %}*/
/* */
