<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/admin')) {
                if (0 === strpos($pathinfo, '/admin/delete')) {
                    // admin_delete_doctor
                    if ($pathinfo === '/admin/delete') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteDoctorAction',  '_route' => 'admin_delete_doctor',);
                    }

                    // admin_delete_doctor_click
                    if ($pathinfo === '/admin/delete/user') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteDoctorClickAction',  '_route' => 'admin_delete_doctor_click',);
                    }

                    // admin_delete_specialty
                    if ($pathinfo === '/admin/deletespecialty') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteSpecialtyAction',  '_route' => 'admin_delete_specialty',);
                    }

                    // admin_delete_specialty_click
                    if ($pathinfo === '/admin/delete/specialty') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteSpecialtyClickAction',  '_route' => 'admin_delete_specialty_click',);
                    }

                    // admin_delete_illness
                    if ($pathinfo === '/admin/deleteillness') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteIllnessAction',  '_route' => 'admin_delete_illness',);
                    }

                    // admin_delete_illness_click
                    if ($pathinfo === '/admin/delete/illness') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteIllnessClickAction',  '_route' => 'admin_delete_illness_click',);
                    }

                    // admin_delete_clinic
                    if ($pathinfo === '/admin/deleteclinic') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteClinicAction',  '_route' => 'admin_delete_clinic',);
                    }

                    // admin_delete_clinic_click
                    if ($pathinfo === '/admin/delete/clinic') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteClinicClickAction',  '_route' => 'admin_delete_clinic_click',);
                    }

                    // admin_delete_fellowship
                    if ($pathinfo === '/admin/deletefellowship') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteFellowshipAction',  '_route' => 'admin_delete_fellowship',);
                    }

                    // admin_delete_fellowship_click
                    if ($pathinfo === '/admin/delete/fellowship') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteFellowshipClickAction',  '_route' => 'admin_delete_fellowship_click',);
                    }

                }

                // admin
                if ($pathinfo === '/admin/home') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::indexAction',  '_route' => 'admin',);
                }
                not_admin:

                // researchproject_myproject_admin_login
                if ($pathinfo === '/admin/login') {
                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::loginAction',  '_route' => 'researchproject_myproject_admin_login',);
                }

                // admin_create
                if ($pathinfo === '/admin/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_create;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::createAction',  '_route' => 'admin_create',);
                }
                not_admin_create:

                // admin_new
                if ($pathinfo === '/admin/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_new;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::newAction',  '_route' => 'admin_new',);
                }
                not_admin_new:

                // admin_edit
                if (preg_match('#^/admin/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::editAction',));
                }
                not_admin_edit:

                // admin_delete
                if (preg_match('#^/admin/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_admin_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::deleteAction',));
                }
                not_admin_delete:

                // admin_doctor_create
                if ($pathinfo === '/admin/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_doctor_create;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::createDoctorAction',  '_route' => 'admin_doctor_create',);
                }
                not_admin_doctor_create:

                // admin_doctor_new
                if ($pathinfo === '/admin/newDoctor') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_doctor_new;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::newDoctorAction',  '_route' => 'admin_doctor_new',);
                }
                not_admin_doctor_new:

                // admin_specialty_create
                if ($pathinfo === '/admin/createspec') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_specialty_create;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::createSpecialtyAction',  '_route' => 'admin_specialty_create',);
                }
                not_admin_specialty_create:

                // admin_specialty_new
                if ($pathinfo === '/admin/newSpecialty') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_specialty_new;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::newSpecialtyAction',  '_route' => 'admin_specialty_new',);
                }
                not_admin_specialty_new:

                // admin_fellowship_create
                if ($pathinfo === '/admin/createfellowship') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_fellowship_create;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::createFellowshipAction',  '_route' => 'admin_fellowship_create',);
                }
                not_admin_fellowship_create:

                // admin_fellowship_new
                if ($pathinfo === '/admin/newFellowship') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_fellowship_new;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::newFellowshipAction',  '_route' => 'admin_fellowship_new',);
                }
                not_admin_fellowship_new:

                // admin_doctor_edit
                if (0 === strpos($pathinfo, '/admin/editDoctor') && preg_match('#^/admin/editDoctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_doctor_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_doctor_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::editDoctorAction',));
                }
                not_admin_doctor_edit:

                // admin_doctor_update
                if (0 === strpos($pathinfo, '/admin/updatedoctor') && preg_match('#^/admin/updatedoctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_admin_doctor_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_doctor_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::updateDoctorAction',));
                }
                not_admin_doctor_update:

                if (0 === strpos($pathinfo, '/admin/edit')) {
                    // admin_edit_doctor_search
                    if ($pathinfo === '/admin/editDoc') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::editDoctorSearchAction',  '_route' => 'admin_edit_doctor_search',);
                    }

                    // admin_edit_doctor_search_click
                    if ($pathinfo === '/admin/edit/doctor') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::editDoctorSearchClickAction',  '_route' => 'admin_edit_doctor_search_click',);
                    }

                    // admin_clinic_edit
                    if (0 === strpos($pathinfo, '/admin/editClinic') && preg_match('#^/admin/editClinic/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_clinic_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_clinic_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::editClinicAction',));
                    }
                    not_admin_clinic_edit:

                }

                // admin_clinic_update
                if (preg_match('#^/admin/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_admin_clinic_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_clinic_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::updateClinicAction',));
                }
                not_admin_clinic_update:

                if (0 === strpos($pathinfo, '/admin/edit')) {
                    // admin_edit_clinic_search
                    if ($pathinfo === '/admin/editClin') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::editClinicSearchAction',  '_route' => 'admin_edit_clinic_search',);
                    }

                    // admin_illness_edit
                    if (0 === strpos($pathinfo, '/admin/editIllness') && preg_match('#^/admin/editIllness/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_illness_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_illness_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::editIllnessAction',));
                    }
                    not_admin_illness_edit:

                }

                // admin_illness_update
                if (0 === strpos($pathinfo, '/admin/updateIllness') && preg_match('#^/admin/updateIllness/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_admin_illness_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_illness_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::updateIllnessAction',));
                }
                not_admin_illness_update:

                // admin_edit_illness_search
                if ($pathinfo === '/admin/editIllness') {
                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AdminController::editIllnessSearchAction',  '_route' => 'admin_edit_illness_search',);
                }

            }

            if (0 === strpos($pathinfo, '/appointment')) {
                // appointment
                if (rtrim($pathinfo, '/') === '/appointment') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_appointment;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'appointment');
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AppointmentController::indexAction',  '_route' => 'appointment',);
                }
                not_appointment:

                // appointment_create
                if ($pathinfo === '/appointment/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_appointment_create;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AppointmentController::createAction',  '_route' => 'appointment_create',);
                }
                not_appointment_create:

                // appointment_new
                if ($pathinfo === '/appointment/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_appointment_new;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AppointmentController::newAction',  '_route' => 'appointment_new',);
                }
                not_appointment_new:

                // appointment_show
                if (preg_match('#^/appointment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_appointment_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'appointment_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AppointmentController::showAction',));
                }
                not_appointment_show:

                // appointment_edit
                if (preg_match('#^/appointment/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_appointment_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'appointment_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AppointmentController::editAction',));
                }
                not_appointment_edit:

                // appointment_update
                if (preg_match('#^/appointment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_appointment_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'appointment_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AppointmentController::updateAction',));
                }
                not_appointment_update:

                // appointment_delete
                if (preg_match('#^/appointment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_appointment_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'appointment_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\AppointmentController::deleteAction',));
                }
                not_appointment_delete:

            }

        }

        if (0 === strpos($pathinfo, '/c')) {
            if (0 === strpos($pathinfo, '/clinic')) {
                // clinic
                if (rtrim($pathinfo, '/') === '/clinic') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_clinic;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'clinic');
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::indexAction',  '_route' => 'clinic',);
                }
                not_clinic:

                // clinic_create
                if ($pathinfo === '/clinic/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_clinic_create;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::createAction',  '_route' => 'clinic_create',);
                }
                not_clinic_create:

                // clinic_new
                if ($pathinfo === '/clinic/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_clinic_new;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::newAction',  '_route' => 'clinic_new',);
                }
                not_clinic_new:

                // clinic_edit
                if (preg_match('#^/clinic/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_clinic_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'clinic_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::editAction',));
                }
                not_clinic_edit:

                // clinic_update
                if (preg_match('#^/clinic/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_clinic_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'clinic_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::updateAction',));
                }
                not_clinic_update:

                // clinic_delete
                if (preg_match('#^/clinic/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_clinic_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'clinic_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::deleteAction',));
                }
                not_clinic_delete:

            }

            if (0 === strpos($pathinfo, '/co')) {
                if (0 === strpos($pathinfo, '/comment')) {
                    // comment
                    if (rtrim($pathinfo, '/') === '/comment') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_comment;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'comment');
                        }

                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\CommentController::indexAction',  '_route' => 'comment',);
                    }
                    not_comment:

                    // comment_create
                    if ($pathinfo === '/comment/') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_comment_create;
                        }

                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\CommentController::createAction',  '_route' => 'comment_create',);
                    }
                    not_comment_create:

                    // comment_new
                    if ($pathinfo === '/comment/new') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_comment_new;
                        }

                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\CommentController::newAction',  '_route' => 'comment_new',);
                    }
                    not_comment_new:

                    // comment_show
                    if (preg_match('#^/comment(?P<post_id>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_comment_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'comment_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\CommentController::showAction',));
                    }
                    not_comment_show:

                    // comment_edit
                    if (preg_match('#^/comment/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_comment_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'comment_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\CommentController::editAction',));
                    }
                    not_comment_edit:

                    // comment_update
                    if (preg_match('#^/comment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_comment_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'comment_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\CommentController::updateAction',));
                    }
                    not_comment_update:

                }

                if (0 === strpos($pathinfo, '/contactnumber')) {
                    // contactnumber
                    if (rtrim($pathinfo, '/') === '/contactnumber') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_contactnumber;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'contactnumber');
                        }

                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ContactNumberController::indexAction',  '_route' => 'contactnumber',);
                    }
                    not_contactnumber:

                    // contactnumber_create
                    if ($pathinfo === '/contactnumber/') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_contactnumber_create;
                        }

                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ContactNumberController::createAction',  '_route' => 'contactnumber_create',);
                    }
                    not_contactnumber_create:

                    // contactnumber_new
                    if ($pathinfo === '/contactnumber/new') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_contactnumber_new;
                        }

                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ContactNumberController::newAction',  '_route' => 'contactnumber_new',);
                    }
                    not_contactnumber_new:

                    // contactnumber_show
                    if (preg_match('#^/contactnumber/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_contactnumber_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contactnumber_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ContactNumberController::showAction',));
                    }
                    not_contactnumber_show:

                    // contactnumber_edit
                    if (preg_match('#^/contactnumber/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_contactnumber_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contactnumber_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ContactNumberController::editAction',));
                    }
                    not_contactnumber_edit:

                    // contactnumber_update
                    if (preg_match('#^/contactnumber/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_contactnumber_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contactnumber_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ContactNumberController::updateAction',));
                    }
                    not_contactnumber_update:

                    // contactnumber_delete
                    if (preg_match('#^/contactnumber/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_contactnumber_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contactnumber_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ContactNumberController::deleteAction',));
                    }
                    not_contactnumber_delete:

                }

            }

        }

        if (0 === strpos($pathinfo, '/home')) {
            // homepage
            if ($pathinfo === '/home') {
                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            }

            // doctorhomepage
            if ($pathinfo === '/home/doctor') {
                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::doctorAction',  '_route' => 'doctorhomepage',);
            }

            // clinichomepage
            if ($pathinfo === '/home/clinic') {
                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::clinicAction',  '_route' => 'clinichomepage',);
            }

            // doctorgrouphomepage
            if ($pathinfo === '/home/doctorgroup') {
                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::doctorgroupAction',  '_route' => 'doctorgrouphomepage',);
            }

            // illnesshomepage
            if ($pathinfo === '/home/illness') {
                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::illnessAction',  '_route' => 'illnesshomepage',);
            }

        }

        // researchproject_myproject_default_login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::loginAction',  '_route' => 'researchproject_myproject_default_login',);
        }

        // confirm_invite
        if ($pathinfo === '/confirmInvite') {
            return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::confirmInviteAction',  '_route' => 'confirm_invite',);
        }

        // decline_invite
        if ($pathinfo === '/declineInvite') {
            return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::declineInviteAction',  '_route' => 'decline_invite',);
        }

        // status_change
        if ($pathinfo === '/statusChange') {
            return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::statusChangeAction',  '_route' => 'status_change',);
        }

        // appointment_view
        if ($pathinfo === '/appointmentview') {
            return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::appointmentViewAction',  '_route' => 'appointment_view',);
        }

        // settings_page
        if ($pathinfo === '/settings') {
            return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::changePasswdAction',  '_route' => 'settings_page',);
        }

        // remove_doctor_click
        if ($pathinfo === '/remove/user') {
            return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::removeDoctorClickAction',  '_route' => 'remove_doctor_click',);
        }

        if (0 === strpos($pathinfo, '/doctor')) {
            // doctor_home
            if ($pathinfo === '/doctor/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_home;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::homeAction',  '_route' => 'doctor_home',);
            }
            not_doctor_home:

            // doctor_create
            if ($pathinfo === '/doctor/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_doctor_create;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::createAction',  '_route' => 'doctor_create',);
            }
            not_doctor_create:

            // doctor_new
            if ($pathinfo === '/doctor/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_new;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::newAction',  '_route' => 'doctor_new',);
            }
            not_doctor_new:

            // doctor_show
            if (0 === strpos($pathinfo, '/doctor/show') && preg_match('#^/doctor/show/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::showAction',));
            }
            not_doctor_show:

            // doctor_edit
            if ($pathinfo === '/doctor/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_edit;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::editAction',  '_route' => 'doctor_edit',);
            }
            not_doctor_edit:

            // doctor_update
            if (preg_match('#^/doctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_doctor_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::updateAction',));
            }
            not_doctor_update:

            // doctor_delete
            if (preg_match('#^/doctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_doctor_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::deleteAction',));
            }
            not_doctor_delete:

            if (0 === strpos($pathinfo, '/doctor/u')) {
                // username_check
                if ($pathinfo === '/doctor/username-check') {
                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::usernameCheckAction',  '_route' => 'username_check',);
                }

                // upload
                if ($pathinfo === '/doctor/upload/file') {
                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::uploadAction',  '_route' => 'upload',);
                }

            }

            if (0 === strpos($pathinfo, '/doctorgroup')) {
                // doctorgroup
                if (rtrim($pathinfo, '/') === '/doctorgroup') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_doctorgroup;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'doctorgroup');
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::indexAction',  '_route' => 'doctorgroup',);
                }
                not_doctorgroup:

                if (0 === strpos($pathinfo, '/doctorgroup/invite')) {
                    // invite_doctor
                    if ($pathinfo === '/doctorgroup/invite/m') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::liveSearchAction',  '_route' => 'invite_doctor',);
                    }

                    // doctorgroup_invite
                    if ($pathinfo === '/doctorgroup/invite') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::inviteAction',  '_route' => 'doctorgroup_invite',);
                    }

                }

                // doctorgroup_create
                if ($pathinfo === '/doctorgroup/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_doctorgroup_create;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::createAction',  '_route' => 'doctorgroup_create',);
                }
                not_doctorgroup_create:

                // doctorgroup_new
                if ($pathinfo === '/doctorgroup/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_doctorgroup_new;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::newAction',  '_route' => 'doctorgroup_new',);
                }
                not_doctorgroup_new:

                // doctorgroup_edit
                if (preg_match('#^/doctorgroup/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_doctorgroup_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorgroup_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::editAction',));
                }
                not_doctorgroup_edit:

                // doctorgroup_update
                if (preg_match('#^/doctorgroup/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_doctorgroup_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorgroup_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::updateAction',));
                }
                not_doctorgroup_update:

                // doctorgroup_delete
                if (preg_match('#^/doctorgroup/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_doctorgroup_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorgroup_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::deleteAction',));
                }
                not_doctorgroup_delete:

                // post_delete
                if ($pathinfo === '/doctorgroup/delete_post') {
                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::deletePost',  '_route' => 'post_delete',);
                }

                // post_edit
                if ($pathinfo === '/doctorgroup/edit_post') {
                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::editPost',  '_route' => 'post_edit',);
                }

                if (0 === strpos($pathinfo, '/doctorgroup/delete_')) {
                    // delete_group
                    if ($pathinfo === '/doctorgroup/delete_group') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::deleteGroupAction',  '_route' => 'delete_group',);
                    }

                    // delete_member
                    if ($pathinfo === '/doctorgroup/delete_member') {
                        return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::deleteMemberAction',  '_route' => 'delete_member',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/fellowship')) {
            // fellowship
            if (rtrim($pathinfo, '/') === '/fellowship') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fellowship;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fellowship');
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\FellowshipController::indexAction',  '_route' => 'fellowship',);
            }
            not_fellowship:

            // fellowship_create
            if ($pathinfo === '/fellowship/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fellowship_create;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\FellowshipController::createAction',  '_route' => 'fellowship_create',);
            }
            not_fellowship_create:

            // fellowship_new
            if ($pathinfo === '/fellowship/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fellowship_new;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\FellowshipController::newAction',  '_route' => 'fellowship_new',);
            }
            not_fellowship_new:

            // fellowship_show
            if (preg_match('#^/fellowship/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fellowship_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fellowship_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\FellowshipController::showAction',));
            }
            not_fellowship_show:

            // fellowship_edit
            if (preg_match('#^/fellowship/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fellowship_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fellowship_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\FellowshipController::editAction',));
            }
            not_fellowship_edit:

            // fellowship_update
            if (preg_match('#^/fellowship/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_fellowship_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fellowship_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\FellowshipController::updateAction',));
            }
            not_fellowship_update:

            // fellowship_delete
            if (preg_match('#^/fellowship/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_fellowship_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fellowship_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\FellowshipController::deleteAction',));
            }
            not_fellowship_delete:

        }

        if (0 === strpos($pathinfo, '/grouppost')) {
            // grouppost
            if (rtrim($pathinfo, '/') === '/grouppost') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_grouppost;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'grouppost');
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\GroupPostController::indexAction',  '_route' => 'grouppost',);
            }
            not_grouppost:

            // grouppost_create
            if ($pathinfo === '/grouppost/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_grouppost_create;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\GroupPostController::createAction',  '_route' => 'grouppost_create',);
            }
            not_grouppost_create:

            // grouppost_new
            if ($pathinfo === '/grouppost/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_grouppost_new;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\GroupPostController::newAction',  '_route' => 'grouppost_new',);
            }
            not_grouppost_new:

            // grouppost_edit
            if (preg_match('#^/grouppost/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_grouppost_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'grouppost_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\GroupPostController::editAction',));
            }
            not_grouppost_edit:

            // grouppost_update
            if (preg_match('#^/grouppost/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_grouppost_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'grouppost_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\GroupPostController::updateAction',));
            }
            not_grouppost_update:

            // grouppost_delete
            if (preg_match('#^/grouppost/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_grouppost_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'grouppost_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\GroupPostController::deleteAction',));
            }
            not_grouppost_delete:

            // comment_delete
            if ($pathinfo === '/grouppost/delete_comment') {
                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\GroupPostController::deleteComment',  '_route' => 'comment_delete',);
            }

        }

        if (0 === strpos($pathinfo, '/illness')) {
            // illness_home
            if ($pathinfo === '/illness/home') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_illness_home;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::indexAction',  '_route' => 'illness_home',);
            }
            not_illness_home:

            // illness_create
            if ($pathinfo === '/illness/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_illness_create;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::createAction',  '_route' => 'illness_create',);
            }
            not_illness_create:

            // illness_new
            if ($pathinfo === '/illness/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_illness_new;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::newAction',  '_route' => 'illness_new',);
            }
            not_illness_new:

            // illness_show
            if (preg_match('#^/illness/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_illness_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'illness_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::showAction',));
            }
            not_illness_show:

            // illness_edit
            if (preg_match('#^/illness/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_illness_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'illness_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::editAction',));
            }
            not_illness_edit:

            // illness_update
            if (preg_match('#^/illness/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_illness_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'illness_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::updateAction',));
            }
            not_illness_update:

            // illness_delete
            if (preg_match('#^/illness/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_illness_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'illness_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::deleteAction',));
            }
            not_illness_delete:

        }

        if (0 === strpos($pathinfo, '/s')) {
            if (0 === strpos($pathinfo, '/search')) {
                // search
                if ($pathinfo === '/search/result') {
                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\SearchController::searchResultAction',  '_route' => 'search',);
                }

                // searchBySpecialty
                if (0 === strpos($pathinfo, '/search/searchBySpecialty') && preg_match('#^/search/searchBySpecialty/(?P<specialty>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'searchBySpecialty')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\SearchController::searchSpecialtyAction',));
                }

            }

            if (0 === strpos($pathinfo, '/specialty')) {
                // specialty
                if (rtrim($pathinfo, '/') === '/specialty') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_specialty;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'specialty');
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\SpecialtyController::indexAction',  '_route' => 'specialty',);
                }
                not_specialty:

                // specialty_create
                if ($pathinfo === '/specialty/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_specialty_create;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\SpecialtyController::createAction',  '_route' => 'specialty_create',);
                }
                not_specialty_create:

                // specialty_new
                if ($pathinfo === '/specialty/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_specialty_new;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\SpecialtyController::newAction',  '_route' => 'specialty_new',);
                }
                not_specialty_new:

                // specialty_show
                if (preg_match('#^/specialty/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_specialty_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'specialty_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\SpecialtyController::showAction',));
                }
                not_specialty_show:

                // specialty_edit
                if (preg_match('#^/specialty/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_specialty_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'specialty_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\SpecialtyController::editAction',));
                }
                not_specialty_edit:

                // specialty_update
                if (preg_match('#^/specialty/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_specialty_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'specialty_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\SpecialtyController::updateAction',));
                }
                not_specialty_update:

                // specialty_delete
                if (preg_match('#^/specialty/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_specialty_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'specialty_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\SpecialtyController::deleteAction',));
                }
                not_specialty_delete:

            }

        }

        if (0 === strpos($pathinfo, '/login')) {
            // login
            if ($pathinfo === '/login') {
                return array (  '_controller' => 'MyProject:Default:login',  '_route' => 'login',);
            }

            // login_check
            if ($pathinfo === '/login_check') {
                return array('_route' => 'login_check');
            }

        }

        if (0 === strpos($pathinfo, '/admin/login')) {
            // admin_login
            if ($pathinfo === '/admin/login') {
                return array (  '_controller' => 'MyProject:Admin:login',  '_route' => 'admin_login',);
            }

            // admin_login_check
            if ($pathinfo === '/admin/login_check') {
                return array('_route' => 'admin_login_check');
            }

        }

        // logout
        if ($pathinfo === '/logout') {
            return array('_route' => 'logout');
        }

        // admin_logout
        if ($pathinfo === '/admin/logout') {
            return array('_route' => 'admin_logout');
        }

        // doctorgroup_show
        if (0 === strpos($pathinfo, '/doctorgroup') && preg_match('#^/doctorgroup/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                goto not_doctorgroup_show;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorgroup_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::showAction',));
        }
        not_doctorgroup_show:

        // grouppost_show
        if (0 === strpos($pathinfo, '/grouppost') && preg_match('#^/grouppost/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                goto not_grouppost_show;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'grouppost_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\GroupPostController::showAction',));
        }
        not_grouppost_show:

        // clinic_show
        if (0 === strpos($pathinfo, '/clinic') && preg_match('#^/clinic/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                goto not_clinic_show;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'clinic_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::showAction',));
        }
        not_clinic_show:

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
