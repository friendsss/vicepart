<?php

namespace Proxies\__CG__\ResearchProject\MyProjectBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Doctor extends \ResearchProject\MyProjectBundle\Entity\Doctor implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', 'id', 'firstName', 'middleName', 'lastName', 'username', 'password', 'dateOfBirth', 'fieldOfPractice', 'prcNumber', 'origIssueDate', 'specialty', 'subspecialty', 'residencyTrainingHospital', 'residencyYearCompleted', 'areasOfExpertise', 'aboutMyPractice', 'userRoles', 'doctorGroups');
        }

        return array('__isInitialized__', 'id', 'firstName', 'middleName', 'lastName', 'username', 'password', 'dateOfBirth', 'fieldOfPractice', 'prcNumber', 'origIssueDate', 'specialty', 'subspecialty', 'residencyTrainingHospital', 'residencyYearCompleted', 'areasOfExpertise', 'aboutMyPractice', 'userRoles', 'doctorGroups');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Doctor $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getUserRoles()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUserRoles', array());

        return parent::getUserRoles();
    }

    /**
     * {@inheritDoc}
     */
    public function getDoctorGroups()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDoctorGroups', array());

        return parent::getDoctorGroups();
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setFirstName($firstName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFirstName', array($firstName));

        return parent::setFirstName($firstName);
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFirstName', array());

        return parent::getFirstName();
    }

    /**
     * {@inheritDoc}
     */
    public function setMiddleName($middleName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMiddleName', array($middleName));

        return parent::setMiddleName($middleName);
    }

    /**
     * {@inheritDoc}
     */
    public function getMiddleName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMiddleName', array());

        return parent::getMiddleName();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastName($lastName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastName', array($lastName));

        return parent::setLastName($lastName);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastName', array());

        return parent::getLastName();
    }

    /**
     * {@inheritDoc}
     */
    public function setUsername($username)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUsername', array($username));

        return parent::setUsername($username);
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUsername', array());

        return parent::getUsername();
    }

    /**
     * {@inheritDoc}
     */
    public function setPassword($password)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPassword', array($password));

        return parent::setPassword($password);
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPassword', array());

        return parent::getPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateOfBirth($dateOfBirth)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateOfBirth', array($dateOfBirth));

        return parent::setDateOfBirth($dateOfBirth);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateOfBirth()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateOfBirth', array());

        return parent::getDateOfBirth();
    }

    /**
     * {@inheritDoc}
     */
    public function setFieldOfPractice($fieldOfPractice)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFieldOfPractice', array($fieldOfPractice));

        return parent::setFieldOfPractice($fieldOfPractice);
    }

    /**
     * {@inheritDoc}
     */
    public function getFieldOfPractice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFieldOfPractice', array());

        return parent::getFieldOfPractice();
    }

    /**
     * {@inheritDoc}
     */
    public function setPrcNumber($prcNumber)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrcNumber', array($prcNumber));

        return parent::setPrcNumber($prcNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getPrcNumber()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrcNumber', array());

        return parent::getPrcNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function setOrigIssueDate($origIssueDate)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOrigIssueDate', array($origIssueDate));

        return parent::setOrigIssueDate($origIssueDate);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrigIssueDate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOrigIssueDate', array());

        return parent::getOrigIssueDate();
    }

    /**
     * {@inheritDoc}
     */
    public function setSpecialty($specialty)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSpecialty', array($specialty));

        return parent::setSpecialty($specialty);
    }

    /**
     * {@inheritDoc}
     */
    public function getSpecialty()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSpecialty', array());

        return parent::getSpecialty();
    }

    /**
     * {@inheritDoc}
     */
    public function setSubspecialty($subspecialty)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSubspecialty', array($subspecialty));

        return parent::setSubspecialty($subspecialty);
    }

    /**
     * {@inheritDoc}
     */
    public function getSubspecialty()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSubspecialty', array());

        return parent::getSubspecialty();
    }

    /**
     * {@inheritDoc}
     */
    public function setResidencyTrainingHospital($residencyTrainingHospital)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setResidencyTrainingHospital', array($residencyTrainingHospital));

        return parent::setResidencyTrainingHospital($residencyTrainingHospital);
    }

    /**
     * {@inheritDoc}
     */
    public function getResidencyTrainingHospital()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getResidencyTrainingHospital', array());

        return parent::getResidencyTrainingHospital();
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'serialize', array());

        return parent::serialize();
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'unserialize', array($serialized));

        return parent::unserialize($serialized);
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRoles', array());

        return parent::getRoles();
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSalt', array());

        return parent::getSalt();
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'eraseCredentials', array());

        return parent::eraseCredentials();
    }

    /**
     * {@inheritDoc}
     */
    public function setResidencyYearCompleted($residencyYearCompleted)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setResidencyYearCompleted', array($residencyYearCompleted));

        return parent::setResidencyYearCompleted($residencyYearCompleted);
    }

    /**
     * {@inheritDoc}
     */
    public function getResidencyYearCompleted()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getResidencyYearCompleted', array());

        return parent::getResidencyYearCompleted();
    }

    /**
     * {@inheritDoc}
     */
    public function setAreasOfExpertise($areasOfExpertise)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAreasOfExpertise', array($areasOfExpertise));

        return parent::setAreasOfExpertise($areasOfExpertise);
    }

    /**
     * {@inheritDoc}
     */
    public function getAreasOfExpertise()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAreasOfExpertise', array());

        return parent::getAreasOfExpertise();
    }

    /**
     * {@inheritDoc}
     */
    public function setAboutMyPractice($aboutMyPractice)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAboutMyPractice', array($aboutMyPractice));

        return parent::setAboutMyPractice($aboutMyPractice);
    }

    /**
     * {@inheritDoc}
     */
    public function getAboutMyPractice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAboutMyPractice', array());

        return parent::getAboutMyPractice();
    }

    /**
     * {@inheritDoc}
     */
    public function setUserRoles(\ResearchProject\MyProjectBundle\Entity\Role $role)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUserRoles', array($role));

        return parent::setUserRoles($role);
    }

    /**
     * {@inheritDoc}
     */
    public function setDoctorGroups(\ResearchProject\MyProjectBundle\Entity\DoctorGroup $doctorgroup)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDoctorGroups', array($doctorgroup));

        return parent::setDoctorGroups($doctorgroup);
    }

    /**
     * {@inheritDoc}
     */
    public function addUserRole(\ResearchProject\MyProjectBundle\Entity\Role $userRoles)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addUserRole', array($userRoles));

        return parent::addUserRole($userRoles);
    }

    /**
     * {@inheritDoc}
     */
    public function removeUserRole(\ResearchProject\MyProjectBundle\Entity\Role $userRoles)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeUserRole', array($userRoles));

        return parent::removeUserRole($userRoles);
    }

    /**
     * {@inheritDoc}
     */
    public function addDoctorGroup(\ResearchProject\MyProjectBundle\Entity\DoctorGroup $doctorGroups)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addDoctorGroup', array($doctorGroups));

        return parent::addDoctorGroup($doctorGroups);
    }

    /**
     * {@inheritDoc}
     */
    public function removeDoctorGroup(\ResearchProject\MyProjectBundle\Entity\DoctorGroup $doctorGroups)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeDoctorGroup', array($doctorGroups));

        return parent::removeDoctorGroup($doctorGroups);
    }

}
