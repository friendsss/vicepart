<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/clinic')) {
            // clinic
            if (rtrim($pathinfo, '/') === '/clinic') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_clinic;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'clinic');
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::indexAction',  '_route' => 'clinic',);
            }
            not_clinic:

            // clinic_create
            if ($pathinfo === '/clinic/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_clinic_create;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::createAction',  '_route' => 'clinic_create',);
            }
            not_clinic_create:

            // clinic_new
            if ($pathinfo === '/clinic/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_clinic_new;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::newAction',  '_route' => 'clinic_new',);
            }
            not_clinic_new:

            // clinic_show
            if (preg_match('#^/clinic/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_clinic_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'clinic_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::showAction',));
            }
            not_clinic_show:

            // clinic_edit
            if (preg_match('#^/clinic/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_clinic_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'clinic_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::editAction',));
            }
            not_clinic_edit:

            // clinic_update
            if (preg_match('#^/clinic/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_clinic_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'clinic_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::updateAction',));
            }
            not_clinic_update:

            // clinic_delete
            if (preg_match('#^/clinic/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_clinic_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'clinic_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\ClinicController::deleteAction',));
            }
            not_clinic_delete:

        }

        // researchproject_myproject_default_index
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'researchproject_myproject_default_index')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::indexAction',));
        }

        // researchproject_myproject_default_login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DefaultController::loginAction',  '_route' => 'researchproject_myproject_default_login',);
        }

        if (0 === strpos($pathinfo, '/doctor')) {
            // doctor_home
            if ($pathinfo === '/doctor/home') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_home;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::homeAction',  '_route' => 'doctor_home',);
            }
            not_doctor_home:

            // doctor
            if (rtrim($pathinfo, '/') === '/doctor') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'doctor');
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::indexAction',  '_route' => 'doctor',);
            }
            not_doctor:

            // doctor_create
            if ($pathinfo === '/doctor/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_doctor_create;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::createAction',  '_route' => 'doctor_create',);
            }
            not_doctor_create:

            // doctor_new
            if ($pathinfo === '/doctor/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_new;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::newAction',  '_route' => 'doctor_new',);
            }
            not_doctor_new:

            // doctor_show
            if (preg_match('#^/doctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::showAction',));
            }
            not_doctor_show:

            // doctor_edit
            if (preg_match('#^/doctor/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_doctor_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::editAction',));
            }
            not_doctor_edit:

            // doctor_update
            if (preg_match('#^/doctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_doctor_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::updateAction',));
            }
            not_doctor_update:

            // doctor_delete
            if (preg_match('#^/doctor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_doctor_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctor_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorController::deleteAction',));
            }
            not_doctor_delete:

            if (0 === strpos($pathinfo, '/doctorgroup')) {
                // doctorgroup
                if (rtrim($pathinfo, '/') === '/doctorgroup') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_doctorgroup;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'doctorgroup');
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::indexAction',  '_route' => 'doctorgroup',);
                }
                not_doctorgroup:

                // doctorgroup_create
                if ($pathinfo === '/doctorgroup/') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_doctorgroup_create;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::createAction',  '_route' => 'doctorgroup_create',);
                }
                not_doctorgroup_create:

                // doctorgroup_new
                if ($pathinfo === '/doctorgroup/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_doctorgroup_new;
                    }

                    return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::newAction',  '_route' => 'doctorgroup_new',);
                }
                not_doctorgroup_new:

                // doctorgroup_show
                if (preg_match('#^/doctorgroup/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_doctorgroup_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorgroup_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::showAction',));
                }
                not_doctorgroup_show:

                // doctorgroup_edit
                if (preg_match('#^/doctorgroup/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_doctorgroup_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorgroup_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::editAction',));
                }
                not_doctorgroup_edit:

                // doctorgroup_update
                if (preg_match('#^/doctorgroup/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_doctorgroup_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorgroup_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::updateAction',));
                }
                not_doctorgroup_update:

                // doctorgroup_delete
                if (preg_match('#^/doctorgroup/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_doctorgroup_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'doctorgroup_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\DoctorGroupController::deleteAction',));
                }
                not_doctorgroup_delete:

            }

        }

        if (0 === strpos($pathinfo, '/illness')) {
            // illness
            if (rtrim($pathinfo, '/') === '/illness') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_illness;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'illness');
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::indexAction',  '_route' => 'illness',);
            }
            not_illness:

            // illness_create
            if ($pathinfo === '/illness/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_illness_create;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::createAction',  '_route' => 'illness_create',);
            }
            not_illness_create:

            // illness_new
            if ($pathinfo === '/illness/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_illness_new;
                }

                return array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::newAction',  '_route' => 'illness_new',);
            }
            not_illness_new:

            // illness_show
            if (preg_match('#^/illness/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_illness_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'illness_show')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::showAction',));
            }
            not_illness_show:

            // illness_edit
            if (preg_match('#^/illness/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_illness_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'illness_edit')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::editAction',));
            }
            not_illness_edit:

            // illness_update
            if (preg_match('#^/illness/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_illness_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'illness_update')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::updateAction',));
            }
            not_illness_update:

            // illness_delete
            if (preg_match('#^/illness/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_illness_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'illness_delete')), array (  '_controller' => 'ResearchProject\\MyProjectBundle\\Controller\\IllnessController::deleteAction',));
            }
            not_illness_delete:

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => 'MyProject:Default:login',  '_route' => 'login',);
                }

                // login_check
                if ($pathinfo === '/login_check') {
                    return array('_route' => 'login_check');
                }

            }

            // logout
            if ($pathinfo === '/logout') {
                return array('_route' => 'logout');
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
