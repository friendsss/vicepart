<?php

namespace ResearchProject\MyProjectBundle\Controller;

use DateTime;
use ResearchProject\MyProjectBundle\Entity\Comment;
use ResearchProject\MyProjectBundle\Entity\GroupPost;
use ResearchProject\MyProjectBundle\Form\GroupPostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * GroupPost controller.
 *
 * @Route("/grouppost")
 */
class GroupPostController extends Controller
{

    /**
     * Lists all GroupPost entities.
     *
     * @Route("/", name="grouppost")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResearchProjectMyProjectBundle:GroupPost')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new GroupPost entity.
     *
     * @Route("/", name="grouppost_create")
     * @Method("POST")
     * @Template("ResearchProjectMyProjectBundle:GroupPost:new.html.twig")
     */
    public function createAction(Request $request)
    {
       
        $entity = new GroupPost();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $gid = $request->request->get('groupId');
        $group = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->findOneById($gid);
        $entity->addGroup($group);
        
        $entity->setDoctorAuthor($user);
        if ($form->isValid()) {
           
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grouppost_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a GroupPost entity.
    *
    * @param GroupPost $entity The entity
    *
    * @return Form The form
    */
    private function createCreateForm(GroupPost $entity)
    {
        $form = $this->createForm(new GroupPostType(), $entity, array(
            'action' => $this->generateUrl('grouppost_create'),
            'method' => 'POST', 
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new GroupPost entity.
     *
     * @Route("/new", name="grouppost_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $request = $this->getRequest();
        $entity = new GroupPost();
        $form   = $this->createCreateForm($entity);
       
        
        return array(
            
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a GroupPost entity.
     *
     * @Route("{post_id}/{id}", name="grouppost_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request)
    {
        
        
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:GroupPost')->find($id);
        $groupid = $entity->getGroup();
        $entity1 = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->find($groupid);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GroupPost entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        
        
        $comment = new Comment();

        $formBuilder = $this->createFormBuilder($comment);
        /* @var $formBuilder FormBuilder */


        $form = $formBuilder ->add('commentContent')
                             ->add('Comment', 'submit')
                             ->getForm();

        /* @var $form Form */

       
        $form->handleRequest($request);
       if ($form->isValid()) {
            $user = $this->getUser();
            $comment->setCommentAuthor($user);
            $comment->setPost($entity);
            $comment->setDateCommented(new DateTime());
       
 
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
              $em->flush();

        }
        

        return array(
            'entity'      => $entity,
            'entity1'     => $entity1,
            'delete_form' => $deleteForm->createView(),
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing GroupPost entity.
     *
     * @Route("/{id}/edit", name="grouppost_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:GroupPost')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GroupPost entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a GroupPost entity.
    *
    * @param GroupPost $entity The entity
    *
    * @return Form The form
    */
    private function createEditForm(GroupPost $entity)
    {
        $form = $this->createForm(new GroupPostType(), $entity, array(
            'action' => $this->generateUrl('grouppost_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing GroupPost entity.
     *
     * @Route("/{id}", name="grouppost_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:GroupPost:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:GroupPost')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GroupPost entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grouppost_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a GroupPost entity.
     *
     * @Route("/{id}", name="grouppost_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:GroupPost')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GroupPost entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grouppost'));
    }

    /**
     * Creates a form to delete a GroupPost entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grouppost_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    /**
     * Deletes a Post entity.
     *
     * @Route("/delete_comment", name="comment_delete")
     */
    public function deleteComment(Request $request) {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            
            $id = $request->request->get('id');
        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Comment')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Comment entity.');
            }
            
            $em->remove($entity);
            $em->flush();
      
                $response = ['message' => 'oks']; 
         /* @var $entity Patient */
           
           } else {
            $response = ['errors' => 'not ajax'];
        }
           return new Response(json_encode($response));
    }
    

    
    
}
