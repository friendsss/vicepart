<?php

namespace ResearchProject\MyProjectBundle\Controller;

use DateTime;
use ResearchProject\MyProjectBundle\Entity\Doctor;
use ResearchProject\MyProjectBundle\Entity\DoctorGroup;
use ResearchProject\MyProjectBundle\Entity\GroupPost;
use ResearchProject\MyProjectBundle\Entity\Illness;
use ResearchProject\MyProjectBundle\Entity\Invitation;
use ResearchProject\MyProjectBundle\Form\DoctorGroupType;
use ResearchProject\MyProjectBundle\Form\GroupPostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * DoctorGroup controller.
 *
 * @Route("/doctorgroup")
 * @Security("has_role('ROLE_USER')")
 */
class DoctorGroupController extends Controller
{

    /**
     * Lists all DoctorGroup entities.
     *
     * @Route("/", name="doctorgroup")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    
    
    /**
     * 
     *@Route("/invite/m", name="invite_doctor")
     * 
     */
    public function liveSearchAction(Request $request)  
    {
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
        $string = $this->getRequest()->request->get('searchText');
        $groupId = $this->getRequest()->request->get('groupId');
        //$string = "alfa";
        $users = $this->getDoctrine()
                     ->getRepository('ResearchProjectMyProjectBundle:Doctor')
                     ->findAll();

        //return users on json format

     //   $encoders = array(new XmlEncoder(), new JsonEncoder());
    // /   $normalizers = array(new GetSetMethodNormalizer());
     //   $serializer = new Serializer($normalizers, $encoders);

    //    $jsonContent = $serializer->serialize($users, 'json');

       // $result = ['k' => 'k'];
      //  $response = new Response($jsonContent);
       //  return $response;
         
       /* 
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

         $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $serializer = new Serializer(array($normalizer), array($encoder));
        $jsonContent = var_dump($serializer->serialize($users, 'json'));
        */
         $em = $this->getDoctrine()->getManager();
         $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Doctor c WHERE 
             (c.firstName LIKE :string OR c.lastName LIKE :string) AND c.id NOT IN
                (SELECT d FROM ResearchProjectMyProjectBundle:Doctor d
                INNER JOIN d.doctorGroups g
                WHERE g.id = :groupId)')
                ->setParameter('string','%'.$string.'%')
                ->setParameter('groupId',"{$groupId}");
        
        //AND u.firstName LIKE :string OR u.lastName LIKE :string
         $result = $query->getArrayResult();
         $response = new Response(json_encode($result));
         $response->headers->set('Content-Type', 'application/json');

         return $response;
        
       // $response = new Response($jsonContent);
     //   return $response;
       
        /*
        
        
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($users, 'json');

        // $jsonContent contains {"name":"foo","age":99,"sportsman":false}
        $response = new Response($jsonContent);
        return $response; 
        
        */
        
       
        
        }
    }
    
    
    /**
     * 
     *
     * @Route("/invite", name="doctorgroup_invite")
     * @Template()
     */
    public function inviteAction(Request $request)  
    {
           $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $groupId = $request->request->get('groupId');
        
            $em = $this->getDoctrine()->getEntityManager();
            $doctorEntity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($id);
            $doctorGroupEntity = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->find($groupId);
          
            if (!$doctorEntity) {
                throw $this->createNotFoundException('Unable to find GroupPost entity.');
            }
            
            $invitation = new Invitation();
            $user = $this->getUser();
            $invitation->setDoctorInviter($user);
            $invitation->setDoctorInvitee($doctorEntity);
            $invitation->setIsAssessed(false);
            $invitation->setGroup($doctorGroupEntity);
            
            $em->persist($invitation);
            $em->flush();
            
      
            $response = ['message' => 'oks']; 
         /* @var $entity Patient */
           
           } else {
            $response = ['errors' => 'not ajax'];
        }
           return new Response(json_encode($response));
        
     }
    
    
    
    
    /**
     * Creates a new DoctorGroup entity.
     *
     * @Route("/", name="doctorgroup_create")
     * @Method("POST")
     * @Template("ResearchProjectMyProjectBundle:DoctorGroup:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new DoctorGroup();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
             date_default_timezone_set('Asia/Manila');
            //$doctor = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findOneById(4);
            $user = $this->getUser();
            $entity->addDoctor($user);
            $entity->addAdminDoctor($user);
             $entity->setDateCreated(new DateTime());
            
            $em->persist($entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('doctorgroup_show', array('id' => $entity->getId())));
        }
        
        

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a DoctorGroup entity.
    *
    * @param DoctorGroup $entity The entity
    *
    * @return Form The form
    */
    private function createCreateForm(DoctorGroup $entity)
    {
        $form = $this->createForm(new DoctorGroupType(), $entity, array(
            'action' => $this->generateUrl('doctorgroup_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new DoctorGroup entity.
     *
     * @Route("/new", name="doctorgroup_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new DoctorGroup();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a DoctorGroup entity.
     *
     * @Route("/{id}", name="doctorgroup_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DoctorGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        //groupPost
        $post = new GroupPost();

        $formBuilder = $this->createFormBuilder($post);
        
        /* @var $formBuilder FormBuilder */


        $form = $formBuilder ->add('postTitle')
                             ->add('postContent')
                             ->add('Post', 'submit')
                             ->getForm();

        /* @var $form Form */

       
        $form->handleRequest($request);
       if ($form->isValid()) {
            $user = $this->getUser();
            $post->setDoctorAuthor($user);
            $post->setGroup($entity);
            $post->setDatePosted(new DateTime());
       
 
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
              $em->flush();

        }
        
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
        $string = $this->getRequest()->request->get('searchText');
        //$string = "alfa";
        

        //return users on json format

     //   $encoders = array(new XmlEncoder(), new JsonEncoder());
    // /   $normalizers = array(new GetSetMethodNormalizer());
     //   $serializer = new Serializer($normalizers, $encoders);

    //    $jsonContent = $serializer->serialize($users, 'json');

       // $result = ['k' => 'k'];
      //  $response = new Response($jsonContent);
       //  return $response;
         
       /* 
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

         $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $serializer = new Serializer(array($normalizer), array($encoder));
        $jsonContent = var_dump($serializer->serialize($users, 'json'));
        */
         $em = $this->getDoctrine()->getManager();
         $query = $em->createQuery('SELECT u FROM ResearchProjectMyProjectBundle:Doctor u  
                WHERE u.firstName LIKE :string OR u.lastName LIKE :string')
                ->setParameter('string','%'.$string.'%');
        
        
         $result = $query->getArrayResult();
         $response = new Response(json_encode($result));
         $response->headers->set('Content-Type', 'application/json');

         return $response;
        
       // $response = new Response($jsonContent);
     //   return $response;
       
        /*
        
        
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($users, 'json');

        // $jsonContent contains {"name":"foo","age":99,"sportsman":false}
        $response = new Response($jsonContent);
        return $response; 
        
        */
        
       
        
        }
        
        
       
        

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'form' => $form->createView(),
            
        );
    }

    /**
     * Displays a form to edit an existing DoctorGroup entity.
     *
     * @Route("/{id}/edit", name="doctorgroup_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DoctorGroup entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a DoctorGroup entity.
    *
    * @param DoctorGroup $entity The entity
    *
    * @return Form The form
    */
    private function createEditForm(DoctorGroup $entity)
    {
        $form = $this->createForm(new DoctorGroupType(), $entity, array(
            'action' => $this->generateUrl('doctorgroup_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing DoctorGroup entity.
     *
     * @Route("/{id}", name="doctorgroup_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:DoctorGroup:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DoctorGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('doctorgroup_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a DoctorGroup entity.
     *
     * @Route("/{id}", name="doctorgroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find DoctorGroup entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('doctorgroup'));
    }

    /**
     * Creates a form to delete a DoctorGroup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('doctorgroup_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    /**
     * @Template("ResearchProjectMyProjectBundle:DoctorGroup:show.html.twig")
     * 
     */
    public function createPostAction(Request $request) {
        $post = new GroupPost();

        $formBuilder = $this->createFormBuilder($post);
        /* @var $formBuilder FormBuilder */


        $form = $formBuilder ->add('postTitle')
                             ->add('postContent')
                             ->add('datePosted')
                             ->add('Save', 'submit')
                             ->getForm();

        /* @var $form Form */

        $form->handleRequest($request);

        if ($form->isValid()) {
 
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            
        }

        return [ 'form' => $form->createView()];
    }

    
    /**
     * Deletes a Post entity.
     *
     * @Route("/delete_post", name="post_delete")
     */
    public function deletePost(Request $request) {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:GroupPost')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GroupPost entity.');
            }
            
            $em->remove($entity);
            $em->flush();
      
                $response = ['message' => 'oks']; 
         /* @var $entity Patient */
           
           } else {
            $response = ['errors' => 'not ajax'];
        }
           return new Response(json_encode($response));
    }
    
     /**
     * Edit a Post entity.
     *
     * @Route("/edit_post", name="post_edit")
     */
    public function editPost(Request $request) {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:GroupPost')->find($id);
            
            $editForm = $this->createForm(new GroupPostType(), $entity);
            
             
             return['editForm' => $editForm->createView()];
   
        }
    }
    
    /**
     * Edit a Post entity.
     *
     * @Route("/delete_group", name="delete_group")
     */
    
    public function deleteGroupAction(Request $request) {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find DoctorGroup entity.');
            }
            
            $em->remove($entity);
            $em->flush();
            
            return new JsonResponse($this->generateUrl('doctorhomepage'));
        }
    }
        
        /**
     * Edit a Post entity.
     *
     * @Route("/delete_member", name="delete_member")
     */
    public function deleteMemberAction(Request $request) {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $groupId = $request->request->get('groupId');
        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($id);
            $groupEntity = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->find($groupId);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Doctor entity.');
            }
            
            $groupEntity->removeDoctor($entity);
            $em->persist($groupEntity);
            $em->flush();
      
                $response = ['message' => 'oks']; 
         /* @var $entity Patient */
           
           } else {
            $response = ['errors' => 'not ajax'];
            }
           return new Response(json_encode($response));
        }
    
}