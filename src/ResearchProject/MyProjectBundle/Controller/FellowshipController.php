<?php

namespace ResearchProject\MyProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ResearchProject\MyProjectBundle\Entity\Fellowship;
use ResearchProject\MyProjectBundle\Form\FellowshipType;

/**
 * Fellowship controller.
 *
 * @Route("/fellowship")
 */
class FellowshipController extends Controller
{

    /**
     * Lists all Fellowship entities.
     *
     * @Route("/", name="fellowship")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResearchProjectMyProjectBundle:Fellowship')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Fellowship entity.
     *
     * @Route("/", name="fellowship_create")
     * @Method("POST")
     * @Template("ResearchProjectMyProjectBundle:Fellowship:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Fellowship();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('fellowship_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Fellowship entity.
     *
     * @param Fellowship $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Fellowship $entity)
    {
        $form = $this->createForm(new FellowshipType(), $entity, array(
            'action' => $this->generateUrl('fellowship_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Fellowship entity.
     *
     * @Route("/new", name="fellowship_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Fellowship();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Fellowship entity.
     *
     * @Route("/{id}", name="fellowship_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Fellowship')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fellowship entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Fellowship entity.
     *
     * @Route("/{id}/edit", name="fellowship_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Fellowship')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fellowship entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Fellowship entity.
    *
    * @param Fellowship $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Fellowship $entity)
    {
        $form = $this->createForm(new FellowshipType(), $entity, array(
            'action' => $this->generateUrl('fellowship_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Fellowship entity.
     *
     * @Route("/{id}", name="fellowship_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:Fellowship:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Fellowship')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fellowship entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('fellowship_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Fellowship entity.
     *
     * @Route("/{id}", name="fellowship_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Fellowship')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Fellowship entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('fellowship'));
    }

    /**
     * Creates a form to delete a Fellowship entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fellowship_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
