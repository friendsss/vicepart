<?php

namespace ResearchProject\MyProjectBundle\Controller;

use ResearchProject\MyProjectBundle\Entity\Doctor;
use ResearchProject\MyProjectBundle\Form\DoctorType;
use ResearchProject\MyProjectBundle\Form\UserEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\SecurityContext;


/**
 * Doctor controller.
 *
 * @Route("/doctor")
 * 
 */
class DoctorController extends Controller
{

    private function authenticateUser(Doctor $doctor)
    {
    $providerKey = 'secured_area'; // your firewall name
    $token = new UsernamePasswordToken($doctor, null, $providerKey, $doctor->getRoles());

    $this->container->get('security.context')->setToken($token);
    }
    /**
     * Lists all Doctor entities.
     *
     * @Route("/profile", name="doctor_home")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities =  $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    
    
    /**
     * Creates a new Doctor entity.
     *
     * @Route("/", name="doctor_create")
     * @Method("POST")
     * 
     * @Template("ResearchProjectMyProjectBundle:Doctor:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Doctor();
        //$entity->setUserRole("ROLE_USER");
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $validator = $this->get('validator');
        $errors = count($validator->validate($entity));

        if ($form->isValid()) {
            
             $factory = $this->get('security.encoder_factory');
            
            $encoder = $factory->getEncoder($entity);
            $encryptedPass = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
            $entity->setPassword($encryptedPass);
            
            $em = $this->getDoctrine()->getManager();
            $role = $em->getRepository('ResearchProjectMyProjectBundle:Role')->findOneByRole('ROLE_USER');
            
            //$userRole = new Role();
           // $userRole->setRole($role);
            
            
            //$roles = new \Doctrine\Common\Collections\ArrayCollection;
           // $roles->add($role);
            $entity->setUserRoles($role);
            
            
            //$role = $this->getDoctrine()
            // ->getRepository('ResearchProjectMyProjectBundle:Role')
            // ->find(2);
            
            
            
   
            
            $em->persist($entity);
            $em->flush();
            $this->authenticateUser($entity);
            return $this->redirect($this->generateUrl('doctor_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'errors' => $errors,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Doctor entity.
    *
    * @param Doctor $entity The entity
    *
    * @return Form The form
    */
    private function createCreateForm(Doctor $entity)
    {
        $form = $this->createForm(new DoctorType(), $entity, array(
            'action' => $this->generateUrl('doctor_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Doctor entity.
     *
     * @Route("/new", name="doctor_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction()
    {
         if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))
         {
        // redirect authenticated users to homepage
        return $this->redirect($this->generateUrl('doctor_home'));
         }
    
        $entity = new Doctor();
        $form   = $this->createCreateForm($entity);
        $validator = $this->get('validator');
        $errors = count($validator->validate($entity));

        return array(
            'entity' => $entity,
            'errors' => 0,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Doctor entity.
     *
     * @Route("/show/{id}", name="doctor_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findOneById($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doctor entity.');
        }

        $deleteForm = $this->createDeleteForm($entity->getId());

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Doctor entity.
     *
     * @Route("/edit", name="doctor_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getUser();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doctor entity.');
        }

        $editForm = $this->createEditForm($entity);
        

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            
        );
    }

    /**
    * Creates a form to edit a Doctor entity.
    *
    * @param Doctor $entity The entity
    *
    * @return Form The form
    */
    private function createEditForm(Doctor $entity)
    {
        $form = $this->createForm(new UserEditType(), $entity, array(
            'action' => $this->generateUrl('doctor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Doctor entity.
     *
     * @Route("/{id}", name="doctor_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:Doctor:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doctor entity.');
        }

         $form = $this->createForm(new UserEditType(), $entity);
         $form->bind($request);
              if ($form->isValid()) {
                  $em->persist($entity);
                  $em->flush();

            return $this->redirect($this->generateUrl('doctor_show', array('id' => $entity->getId())));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $form->createView(),
            
        );
    }
    /**
     * Deletes a Doctor entity.
     *
     * @Route("/{id}", name="doctor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Doctor entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('doctor'));
    }

    /**
     * Creates a form to delete a Doctor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('doctor_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    /**
     * 
     *@Route("/username-check", name="username_check")
     * 
     */
    public function usernameCheckAction(Request $request)  
    {
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
        $string = $this->getRequest()->request->get('usernameText');
        

         $em = $this->getDoctrine()->getEntityManager();
         $doctors = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findAll();
         $duplicate = 'false';
         
        
         foreach ($doctors as $doctor) {
            if ($doctor->getUsername() == trim($string)) {
                 $duplicate = 'true'; 
             }
         }
        
           $response = ['duplicate' => $duplicate];
        
           
           } else {
            $response = ['errors' => 'not ajax'];
        }
           return new Response(json_encode($response));
      
        
    }
    
    
    /**
     * @Route("/upload/file", name="upload")
 * @Template()
 */
public function uploadAction(Request $request)
{
    $doctor = new Doctor();
    $form = $this->createFormBuilder($doctor)
        ->add('file')
        ->add('submit', 'submit')
        ->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();

        $doctor->upload();

        $em->persist($doctor);
        $em->flush();

        return $this->redirect($this->generateUrl('doctor_show', array('id' => $entity->getId())));
    }

    return array('form' => $form->createView());
}
    
}
