<?php

namespace ResearchProject\MyProjectBundle\Controller;

use ResearchProject\MyProjectBundle\Entity\Admin;
use ResearchProject\MyProjectBundle\Entity\Clinic;
use ResearchProject\MyProjectBundle\Entity\Doctor;
use ResearchProject\MyProjectBundle\Entity\Fellowship;
use ResearchProject\MyProjectBundle\Entity\Illness;
use ResearchProject\MyProjectBundle\Entity\Specialty;
use ResearchProject\MyProjectBundle\Form\AdminType;
use ResearchProject\MyProjectBundle\Form\ClinicType;
use ResearchProject\MyProjectBundle\Form\DoctorType;
use ResearchProject\MyProjectBundle\Form\FellowshipType;
use ResearchProject\MyProjectBundle\Form\IllnessType;
use ResearchProject\MyProjectBundle\Form\SpecialtyType;
use ResearchProject\MyProjectBundle\Form\UserEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Admin controller.
 *
 * @Route("/admin")
 */
class AdminController extends Controller
{

    /**
     * @Route("/delete", name="admin_delete_doctor")
     * @Template()
     */
    public function deleteDoctorAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $string = $this->getRequest()->request->get('searchText');
            
        
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Doctor c WHERE 
             (c.firstName LIKE :string OR c.lastName LIKE :string)')
                ->setParameter('string','%'.$string.'%');
                
        
            //AND u.firstName LIKE :string OR u.lastName LIKE :string
             $result = $query->getArrayResult();
             $response = new Response(json_encode($result));
             $response->headers->set('Content-Type', 'application/json');

             return $response;
        }
        
    }
    
    /**
     * @Route("/delete/user", name="admin_delete_doctor_click")
     * 
     */
    public function deleteDoctorClickAction(Request $request)
    {
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Doctor entity.');
            }
            
            $em->remove($entity);
            $em->flush();
            
            return new JsonResponse($this->generateUrl('admin_delete_doctor'));
        }
    }
     /**
     * @Route("/deletespecialty", name="admin_delete_specialty")
     * @Template()
     */
    public function deleteSpecialtyAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $string = $this->getRequest()->request->get('searchText');
            
        
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Specialty c WHERE 
             c.specialtyName LIKE :string')
                ->setParameter('string','%'.$string.'%');
                
        
            //AND u.firstName LIKE :string OR u.lastName LIKE :string
             $result = $query->getArrayResult();
             $response = new Response(json_encode($result));
             $response->headers->set('Content-Type', 'application/json');

             return $response;
        }
        
    }
    
    /**
     * @Route("/delete/specialty", name="admin_delete_specialty_click")
     * 
     */
    public function deleteSpecialtyClickAction(Request $request)
    {
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Specialty')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Specialty entity.');
            }
            
            $em->remove($entity);
            $em->flush();
            
            return new JsonResponse($this->generateUrl('admin_delete_specialty'));
        }
    }  
    /**
     * @Route("/deleteillness", name="admin_delete_illness")
     * @Template()
     */
    public function deleteIllnessAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $string = $this->getRequest()->request->get('searchText');
            
        
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Illness c WHERE 
             c.illnessName LIKE :string')
                ->setParameter('string','%'.$string.'%');
                
        
            //AND u.firstName LIKE :string OR u.lastName LIKE :string
             $result = $query->getArrayResult();
             $response = new Response(json_encode($result));
             $response->headers->set('Content-Type', 'application/json');

             return $response;
        }
        
    }
    
    /**
     * @Route("/delete/illness", name="admin_delete_illness_click")
     * 
     */
    public function deleteIllnessClickAction(Request $request)
    {
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Illness')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Illness entity.');
            }
            
            $em->remove($entity);
            $em->flush();
            
            return new JsonResponse($this->generateUrl('admin_delete_illness'));
        }
    }  
    
     /**
     * @Route("/deleteclinic", name="admin_delete_clinic")
     * @Template()
     */
    public function deleteClinicAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $string = $this->getRequest()->request->get('searchText');
            
        
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Clinic c WHERE 
             c.clinicName LIKE :string')
                ->setParameter('string','%'.$string.'%');
                
        
            //AND u.firstName LIKE :string OR u.lastName LIKE :string
             $result = $query->getArrayResult();
             $response = new Response(json_encode($result));
             $response->headers->set('Content-Type', 'application/json');

             return $response;
        }
        
    }
    
    /**
     * @Route("/delete/clinic", name="admin_delete_clinic_click")
     * 
     */
    public function deleteClinicClickAction(Request $request)
    {
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Clinic entity.');
            }
            
            $em->remove($entity);
            $em->flush();
            
            return new JsonResponse($this->generateUrl('admin_delete_clinic'));
        }
    }   
    
     /**
     * @Route("/deletefellowship", name="admin_delete_fellowship")
     * @Template()
     */
    public function deleteFellowshipAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $string = $this->getRequest()->request->get('searchText');
            
        
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Fellowship c WHERE 
             c.fellowshipName LIKE :string')
                ->setParameter('string','%'.$string.'%');
                
        
            //AND u.firstName LIKE :string OR u.lastName LIKE :string
             $result = $query->getArrayResult();
             $response = new Response(json_encode($result));
             $response->headers->set('Content-Type', 'application/json');

             return $response;
        }
        
    }
    
    /**
     * @Route("/delete/fellowship", name="admin_delete_fellowship_click")
     * 
     */
    public function deleteFellowshipClickAction(Request $request)
    {
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Fellowship')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Fellowship entity.');
            }
            
            $em->remove($entity);
            $em->flush();
            
            return new JsonResponse($this->generateUrl('admin_delete_fellowship'));
        }
    }   
    
     
    /**
     * Lists all Admin entities.
     *
     * @Route("/home", name="admin")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Admin')->find($user);

        return array(
            'entity' => $entity,
        );
    }
    
     /**
     * @Route("/login")
     * @Template()
     */
    public function loginAction(Request $request) {
        $session = $request->getSession();

        // get the login error if there is onewqre
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return [
            // last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ];
    } 
    
    
    /**
     * Creates a new Admin entity.
     *
     * @Route("/", name="admin_create")
     * @Method("POST")
     * @Template("ResearchProjectMyProjectBundle:Admin:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Admin();
        //$entity->setUserRole("ROLE_USER");
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $validator = $this->get('validator');
        $errors = count($validator->validate($entity));

        if ($form->isValid()) {
            
             $factory = $this->get('security.encoder_factory');
            
            $encoder = $factory->getEncoder($entity);
            $encryptedPass = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
            $entity->setPassword($encryptedPass);
            
            $em = $this->getDoctrine()->getManager();
           
            
            
            //$role = $this->getDoctrine()
            // ->getRepository('ResearchProjectMyProjectBundle:Role')
            // ->find(2);
            
            
            
   
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'errors' => $errors,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Admin entity.
    *
    * @param Admin $entity The entity
    *
    * @return Form The form
    */
    private function createCreateForm(Admin $entity)
    {
        $form = $this->createForm(new AdminType(), $entity, array(
            'action' => $this->generateUrl('admin_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Admin entity.
     *
     * @Route("/new", name="admin_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Admin();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    

    /**
     * Displays a form to edit an existing Admin entity.
     *
     * @Route("/{id}/edit", name="admin_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Admin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Admin entity.
    *
    * @param Admin $entity The entity
    *
    * @return Form The form
    */
    private function createEditForm(Admin $entity)
    {
        $form = $this->createForm(new AdminType(), $entity, array(
            'action' => $this->generateUrl('admin_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
   /*
    /**
     * Edits an existing Admin entity.
     *
     * @Route("/{id}", name="admin_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:Admin:edit.html.twig")
     *
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Admin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    */
    /**
     * Deletes a Admin entity.
     *
     * @Route("/{id}", name="admin_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Admin')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Admin entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin'));
    }

    /**
     * Creates a form to delete a Admin entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    /**
     * Creates a new Doctor entity.
     *
     * @Route("/", name="admin_doctor_create")
     * @Method("POST")
     * 
     * @Template()
     */
    public function createDoctorAction(Request $request)
    {
        $entity = new Doctor();
        //$entity->setUserRole("ROLE_USER");
        $form = $this->createCreateDoctorForm($entity);
        $form->handleRequest($request);
        $validator = $this->get('validator');
        $errors = count($validator->validate($entity));

        if ($form->isValid()) {
            
             $factory = $this->get('security.encoder_factory');
            
            $encoder = $factory->getEncoder($entity);
            $encryptedPass = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
            $entity->setPassword($encryptedPass);
            
            $em = $this->getDoctrine()->getManager();
            $role = $em->getRepository('ResearchProjectMyProjectBundle:Role')->findOneByRole('ROLE_USER');
            
            //$userRole = new Role();
           // $userRole->setRole($role);
            
            
            //$roles = new \Doctrine\Common\Collections\ArrayCollection;
           // $roles->add($role);
            $entity->setUserRoles($role);
            
            
            //$role = $this->getDoctrine()
            // ->getRepository('ResearchProjectMyProjectBundle:Role')
            // ->find(2);
            
            
            
   
            
            $em->persist($entity);
            $em->flush();
            $this->authenticateUser($entity);
            return $this->redirect($this->generateUrl('admin_doctor_create'));
        }

        return array(
            'entity' => $entity,
            'errors' => $errors,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Doctor entity.
    *
    * @param Doctor $entity The entity
    *
    * @return Form The form
    */
    private function createCreateDoctorForm(Doctor $entity)
    {
        $form = $this->createForm(new DoctorType(), $entity, array(
            'action' => $this->generateUrl('admin_doctor_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Doctor entity.
     *
     * @Route("/newDoctor", name="admin_doctor_new")
     * @Method("GET")
     * @Template()
     */
    public function newDoctorAction()
    {
         
    
        $entity = new Doctor();
        $form   = $this->createCreateDoctorForm($entity);
        $validator = $this->get('validator');
        $errors = count($validator->validate($entity));

        return array(
            'entity' => $entity,
            'errors' => 0,
            'form'   => $form->createView(),
        );
    }
    
    /**
     * Creates a new Specialty entity.
     *
     * @Route("/createspec", name="admin_specialty_create")
     * @Method("POST")
     * @Template()
     */
    public function createSpecialtyAction(Request $request)
    {
        $entity = new Specialty();
        $form = $this->createCreateSpecialtyForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_specialty_new'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Specialty entity.
     *
     * @param Specialty $entity The entity
     *
     * @return Form The form
     */
    private function createCreateSpecialtyForm(Specialty $entity)
    {
        $form = $this->createForm(new SpecialtyType(), $entity, array(
            'action' => $this->generateUrl('admin_specialty_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Specialty entity.
     *
     * @Route("/newSpecialty", name="admin_specialty_new")
     * @Method("GET")
     * @Template()
     */
    public function newSpecialtyAction()
    {
        $entity = new Specialty();
        $form   = $this->createCreateSpecialtyForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new Fellowship entity.
     *
     * @Route("/createfellowship", name="admin_fellowship_create")
     * @Method("POST")
     * @Template()
     */
    public function createFellowshipAction(Request $request)
    {
        $entity = new Fellowship();
        $form = $this->createCreateFellowshipForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_fellowship_new'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Fellowship entity.
     *
     * @param Fellowship $entity The entity
     *
     * @return Form The form
     */
    private function createCreateFellowshipForm(Fellowship $entity)
    {
        $form = $this->createForm(new FellowshipType(), $entity, array(
            'action' => $this->generateUrl('admin_fellowship_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Fellowship entity.
     *
     * @Route("/newFellowship", name="admin_fellowship_new")
     * @Method("GET")
     * @Template()
     */
    public function newFellowshipAction()
    {
        $entity = new Fellowship();
        $form   = $this->createCreateFellowshipForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
    
    /**
     * Displays a form to edit an existing Doctor entity.
     *
     * @Route("/editDoctor/{id}", name="admin_doctor_edit")
     * @Method("GET")
     * @Template()
     * 
     */
    public function editDoctorAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doctor entity.');
        }

        $editForm = $this->createEditDoctorForm($entity);
        

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            
        );
    }

    /**
    * Creates a form to edit a Doctor entity.
    *
    * @param Doctor $entity The entity
    *
    * @return Form The form
    */
    private function createEditDoctorForm(Doctor $entity)
    {
        $form = $this->createForm(new UserEditType(), $entity, array(
            'action' => $this->generateUrl('admin_doctor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Doctor entity.
     *
     * @Route("/updatedoctor/{id}", name="admin_doctor_update")
     * @Method("PUT")
     * @Template()
     */
    public function updateDoctorAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Doctor entity.');
        }

         $form = $this->createForm(new UserEditType(), $entity);
         $form->bind($request);
              if ($form->isValid()) {
                  $em->persist($entity);
                  $em->flush();

            return $this->redirect($this->generateUrl('admin'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $form->createView(),
            
        );
    }
    
    
     /**
     * @Route("/editDoc", name="admin_edit_doctor_search")
     * @Template()
     */
    public function editDoctorSearchAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $string = $this->getRequest()->request->get('searchText');
            
        
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Doctor c WHERE 
             (c.firstName LIKE :string OR c.lastName LIKE :string)')
                ->setParameter('string','%'.$string.'%');
                
        
            //AND u.firstName LIKE :string OR u.lastName LIKE :string
             $result = $query->getArrayResult();
             $response = new Response(json_encode($result));
             $response->headers->set('Content-Type', 'application/json');

             return $response;
        }
        
    }
    
    /**
     * @Route("/edit/doctor", name="admin_edit_doctor_search_click")
     * 
     */
    public function editDoctorSearchClickAction(Request $request)
    {
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Doctor entity.');
            }
            
           
            
            return new JsonResponse($this->generateUrl('admin_doctor_edit', array('id' => $entity->getId())));
        }
    }
    
    
      /**
     * Displays a form to edit an existing Clinic entity.
     *
     * @Route("/editClinic/{id}", name="admin_clinic_edit")
     * @Method("GET")
     * @Template()
     */
    public function editClinicAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clinic entity.');
        }

        $editForm = $this->createEditClinicForm($entity);
        

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
           
        );
    }

    /**
    * Creates a form to edit a Clinic entity.
    *
    * @param Clinic $entity The entity
    *
    * @return Form The form
    */
    private function createEditClinicForm(Clinic $entity)
    {
        $form = $this->createForm(new ClinicType(), $entity, array(
            'action' => $this->generateUrl('admin_clinic_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Clinic entity.
     *
     * @Route("/{id}", name="admin_clinic_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:Clinic:editClinic.html.twig")
     */
    public function updateClinicAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clinic entity.');
        }

       
        $editForm = $this->createEditClinicForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
           
        );
    }
    
     /**
     * @Route("/editClin", name="admin_edit_clinic_search")
     * @Template()
     */
    public function editClinicSearchAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $string = $this->getRequest()->request->get('searchText');
            
        
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Clinic c WHERE 
             c.clinicName LIKE :string')
                ->setParameter('string','%'.$string.'%');
                
        
            //AND u.firstName LIKE :string OR u.lastName LIKE :string
             $result = $query->getArrayResult();
             $response = new Response(json_encode($result));
             $response->headers->set('Content-Type', 'application/json');

             return $response;
        }
        
    }
    
    
      /**
     * Displays a form to edit an existing Illness entity.
     *
     * @Route("/editIllness/{id}", name="admin_illness_edit")
     * @Method("GET")
     * @Template()
     */
    public function editIllnessAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Illness')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Illness entity.');
        }

        $editForm = $this->createEditIllnessForm($entity);
        

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
           
        );
    }

    /**
    * Creates a form to edit a Illness entity.
    *
    * @param Illness $entity The entity
    *
    * @return Form The form
    */
    private function createEditIllnessForm(Illness $entity)
    {
        $form = $this->createForm(new IllnessType(), $entity, array(
            'action' => $this->generateUrl('admin_illness_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Illness entity.
     *
     * @Route("/updateIllness/{id}", name="admin_illness_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:Clinic:editClinic.html.twig")
     */
    public function updateIllnessAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Illness')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Illness entity.');
        }

       
        $editForm = $this->createEditIllnessForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
           
        );
    }
    
     /**
     * @Route("/editIllness", name="admin_edit_illness_search")
     * @Template()
     */
    public function editIllnessSearchAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');
            $string = $this->getRequest()->request->get('searchText');
            
        
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Illness c WHERE 
             c.illnessName LIKE :string')
                ->setParameter('string','%'.$string.'%');
                
        
            //AND u.firstName LIKE :string OR u.lastName LIKE :string
             $result = $query->getArrayResult();
             $response = new Response(json_encode($result));
             $response->headers->set('Content-Type', 'application/json');

             return $response;
        }
        
    }
    
}
