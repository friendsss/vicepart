<?php

namespace ResearchProject\MyProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ResearchProject\MyProjectBundle\Entity\Illness;
use ResearchProject\MyProjectBundle\Form\IllnessType;

/**
 * Illness controller.
 *
 * @Route("/illness")
 */
class IllnessController extends Controller
{

    /**
     * Lists all Illness entities.
     *
     * @Route("/home", name="illness_home")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResearchProjectMyProjectBundle:Illness')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Illness entity.
     *
     * @Route("/", name="illness_create")
     * @Method("POST")
     * @Template("ResearchProjectMyProjectBundle:Illness:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Illness();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('illness_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Illness entity.
    *
    * @param Illness $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Illness $entity)
    {
        $form = $this->createForm(new IllnessType(), $entity, array(
            'action' => $this->generateUrl('illness_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Illness entity.
     *
     * @Route("/new", name="illness_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Illness();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Illness entity.
     *
     * @Route("/{id}", name="illness_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Illness')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Illness entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Illness entity.
     *
     * @Route("/{id}/edit", name="illness_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Illness')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Illness entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        );
    }

    /**
    * Creates a form to edit a Illness entity.
    *
    * @param Illness $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Illness $entity)
    {
        $form = $this->createForm(new IllnessType(), $entity, array(
            'action' => $this->generateUrl('illness_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Illness entity.
     *
     * @Route("/{id}", name="illness_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:Illness:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Illness')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Illness entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('illness_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Illness entity.
     *
     * @Route("/{id}", name="illness_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Illness')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Illness entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('illness'));
    }

    /**
     * Creates a form to delete a Illness entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('illness_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
