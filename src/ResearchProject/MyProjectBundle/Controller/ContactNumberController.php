<?php

namespace ResearchProject\MyProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use ResearchProject\MyProjectBundle\Entity\ContactNumber;
use ResearchProject\MyProjectBundle\Form\ContactNumberType;

/**
 * ContactNumber controller.
 *
 * @Route("/contactnumber")
 */
class ContactNumberController extends Controller
{

    /**
     * Lists all ContactNumber entities.
     *
     * @Route("/", name="contactnumber")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResearchProjectMyProjectBundle:ContactNumber')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new ContactNumber entity.
     *
     * @Route("/", name="contactnumber_create")
     * @Method("POST")
     * @Template("ResearchProjectMyProjectBundle:ContactNumber:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new ContactNumber();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('contactnumber_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a ContactNumber entity.
    *
    * @param ContactNumber $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(ContactNumber $entity)
    {
        $form = $this->createForm(new ContactNumberType(), $entity, array(
            'action' => $this->generateUrl('contactnumber_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ContactNumber entity.
     *
     * @Route("/new", name="contactnumber_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new ContactNumber();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a ContactNumber entity.
     *
     * @Route("/{id}", name="contactnumber_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:ContactNumber')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactNumber entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ContactNumber entity.
     *
     * @Route("/{id}/edit", name="contactnumber_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:ContactNumber')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactNumber entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a ContactNumber entity.
    *
    * @param ContactNumber $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ContactNumber $entity)
    {
        $form = $this->createForm(new ContactNumberType(), $entity, array(
            'action' => $this->generateUrl('contactnumber_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ContactNumber entity.
     *
     * @Route("/{id}", name="contactnumber_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:ContactNumber:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:ContactNumber')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactNumber entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('contactnumber_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a ContactNumber entity.
     *
     * @Route("/{id}", name="contactnumber_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:ContactNumber')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ContactNumber entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('contactnumber'));
    }

    /**
     * Creates a form to delete a ContactNumber entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contactnumber_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
