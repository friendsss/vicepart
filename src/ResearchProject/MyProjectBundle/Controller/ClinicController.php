<?php

namespace ResearchProject\MyProjectBundle\Controller;

use ResearchProject\MyProjectBundle\Entity\Appointment;
use ResearchProject\MyProjectBundle\Entity\Clinic;
use ResearchProject\MyProjectBundle\Form\ClinicType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\Validator\Constraints\DateTime;
/**
 * Clinic controller.
 *
 * @Route("/clinic")
 */
class ClinicController extends Controller
{

    /**
     * Lists all Clinic entities.
     *
     * @Route("/", name="clinic")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Clinic entity.
     *
     * @Route("/", name="clinic_create")
     * @Method("POST")
     * @Template("ResearchProjectMyProjectBundle:Clinic:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Clinic();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $entity->addDoctor($user);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('clinic_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Clinic entity.
    *
    * @param Clinic $entity The entity
    *
    * @return Form The form
    */
    private function createCreateForm(Clinic $entity)
    {
        $form = $this->createForm(new ClinicType(), $entity, array(
            'action' => $this->generateUrl('clinic_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Clinic entity.
     *
     * @Route("/new", name="clinic_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_USER')")
     */
    public function newAction()
    {
        $entity = new Clinic();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Clinic entity.
     *
     * @Route("/{id}", name="clinic_show")
     * @Method("GET")
     * @Template()
     */
   public function showAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clinic entity.');
        }

       
        $generator = new SecureRandom();
        $code = $generator->nextBytes(2);
        $appointmentCode = bin2hex($code);
       //appointment
        $appointment = new Appointment();

        $formBuilder = $this->createFormBuilder($appointment);
        /* @var $formBuilder FormBuilder */


        $appointmentForm = $formBuilder ->add('firstName')
                             ->add('lastName')
                             ->add('emailAddress', 'email')
                             ->add('contactNumber')
                             
                             ->add('note')
                             ->add('dateAppointed', 'datetime', array('widget' => 'single_text'))
                             ->add('Save', 'submit')
                             ->getForm();

        /* @var $form Form */

       
        $appointmentForm->handleRequest($request);
       if ($appointmentForm->isValid()) {
            $appointment->setClinic($entity);
           // $appointment->setDateCreated(new DateTime());
            $appointment->setAppointmentCode($appointmentCode);
            $appointment->setIsAssessed(false);
 
            $em = $this->getDoctrine()->getManager();
            $em->persist($appointment);
              $em->flush();
              
           //return $this->redirect($this->generateUrl('clinic_show', array('id' => $entity->getId()))); 
              $this->get('session')->getFlashBag()->add(
                'notice',
                'You have sucessfully created an appointment!'
        );
        }
        
        
        

        return array(
            'entity'      => $entity,
            'appointmentCode' => $appointmentCode,
            'appointmentForm' => $appointmentForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Clinic entity.
     *
     * @Route("/{id}/edit", name="clinic_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clinic entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Clinic entity.
    *
    * @param Clinic $entity The entity
    *
    * @return Form The form
    */
    private function createEditForm(Clinic $entity)
    {
        $form = $this->createForm(new ClinicType(), $entity, array(
            'action' => $this->generateUrl('clinic_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Clinic entity.
     *
     * @Route("/{id}", name="clinic_update")
     * @Method("PUT")
     * @Template("ResearchProjectMyProjectBundle:Clinic:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clinic entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('clinic_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Clinic entity.
     *
     * @Route("/{id}", name="clinic_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Clinic entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('clinic'));
    }

    /**
     * Creates a form to delete a Clinic entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clinic_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
