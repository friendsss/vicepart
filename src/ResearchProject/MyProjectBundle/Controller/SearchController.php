<?php

namespace ResearchProject\MyProjectBundle\Controller;

use ResearchProject\MyProjectBundle\Entity\Doctor;
use ResearchProject\MyProjectBundle\Entity\Clinic;
use ResearchProject\MyProjectBundle\Entity\Illness;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ResearchProject\MyProjectBundle\Entity\Specialty;



/**
 * Search controller.
 * @Route("/search")
 */
class SearchController extends Controller
{
    /**
     * @Route("/result", name="search" )
     * @Template()
     */
     public function searchResultAction(Request $request) {
      
        
            $em = $this->getDoctrine()->getManager(); 
            //$searchword = $request->query->get('searchfield');
            $request = $this->getRequest();
            $searchword = $request->request->get('searchfield');
            $repo = $this->getDoctrine()->getRepository("ResearchProjectMyProjectBundle:Doctor");
            $repo1 = $this->getDoctrine()->getRepository("ResearchProjectMyProjectBundle:Clinic");
            $repo2 = $this->getDoctrine()->getRepository("ResearchProjectMyProjectBundle:Illness");
            $repo3 = $this->getDoctrine()->getRepository("ResearchProjectMyProjectBundle:DoctorGroup");
            $doctors = $repo->findByDoctorKeyword($searchword);
            $clinics = $repo1->findByClinicKeyword($searchword);
            $illnesses = $repo2->findByIllnessKeyword($searchword);
            $doctorgroups = $repo3->findByDoctorGroupKeyword($searchword);
            $specialties =  $em->getRepository('ResearchProjectMyProjectBundle:Specialty')->findAll();
        
        return ['doctors' => $doctors, 'clinics' => $clinics, 'illnesses' => $illnesses, 'searchword' => $searchword,
                'specialties' => $specialties, 'doctorgroups' => $doctorgroups];
    }
    
    /**
     * @Route("/searchBySpecialty/{specialty}", name="searchBySpecialty" )
     * @Template()
     */
     public function searchSpecialtyAction($specialty) {
      
      
        $repo3 = $this->getDoctrine()->getRepository("ResearchProjectMyProjectBundle:Specialty");
        $doctorsBySpecialty = $repo3->findBySpecialty($specialty);
        
        return ['doctorsBySpecialty' => $doctorsBySpecialty , 'specialty' => $specialty ];
    }   

}
