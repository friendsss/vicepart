<?php

namespace ResearchProject\MyProjectBundle\Controller;

use ResearchProject\MyProjectBundle\Form\ChangePasswordType;
use ResearchProject\MyProjectBundle\Form\Model\ChangePassword;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends Controller
{
    
    /**
     * @Route("/home", name="homepage")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();        

        $entities =  $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findAll();
        $clinics = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findAll();
        $illnesses = $em->getRepository('ResearchProjectMyProjectBundle:Illness')->findAll();
        $randomdoc1= $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findByRandomDoctor();
        $randomdoc2= $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findByRandomDoctor();
        $randomdoc3= $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findByRandomDoctor();
        $randomcli1= $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findByRandomClinic();
        $randomcli2= $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findByRandomClinic();
        $randomcli3= $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findByRandomClinic();
        $specialties =  $em->getRepository('ResearchProjectMyProjectBundle:Specialty')->findAll();

        return array(
            'entities' => $entities, 'clinics' => $clinics, 'illnesses' => $illnesses, 'randomdoc1' => $randomdoc1,
            'randomdoc2' => $randomdoc2, 'randomdoc3' => $randomdoc3, 'specialties' => $specialties, 'randomcli1' => $randomcli1,
            'randomcli2' => $randomcli2, 'randomcli3' => $randomcli3,
        );
    }
    
    /**
     * @Route("/home/doctor", name="doctorhomepage")
     * @Template()
     */
    public function doctorAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities =  $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findAll();
        $randomdoc1= $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findByRandomDoctor();
        $randomdoc2= $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findByRandomDoctor();
        $randomdoc3= $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->findByRandomDoctor();
        $specialty = $em->getRepository('ResearchProjectMyProjectBundle:Specialty')->findAll();
        
       
        if( $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
   
        date_default_timezone_set('Asia/Manila');
        $user = $this->getUser()->getId();
        $clinics = $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findByClinicDoctor($user);
        $date1 = new \DateTime();
        
        $datenow = $date1->format('Y-m-d');
        $date2 = new \DateTime("tomorrow");
        $datetom = $date2->format('Y-m-d');
        
        $appointments = $em->getRepository('ResearchProjectMyProjectBundle:Appointment')->findAllAppointmentsToday($datetom, $user );
        
        $invitations = $em->getRepository('ResearchProjectMyProjectBundle:Invitation')->findByInvitation($user);
        
        return array(
            'entities' => $entities, 'randomdoc1' => $randomdoc1, 'randomdoc2' => $randomdoc2, 'randomdoc3' => $randomdoc3, 'specialties' => $specialty, 'appointments' => $appointments,
            'clinics' => $clinics, 'invitations' => $invitations
        );
        
        }else{
             return array(
            'entities' => $entities, 'randomdoc1' => $randomdoc1, 'randomdoc2' => $randomdoc2, 'randomdoc3' => $randomdoc3, 'specialties' => $specialty
            
        );
        }
         
    }
    
    /**
     * @Route("/home/clinic", name="clinichomepage")
     * @Template()
     */
    public function clinicAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities =  $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findAll();
        $randomclinic1= $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findByRandomClinic();
        $randomclinic2= $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findByRandomClinic();
        $randomclinic3= $em->getRepository('ResearchProjectMyProjectBundle:Clinic')->findByRandomClinic();

        return array(
            'entities' => $entities, 'randomclinic1' => $randomclinic1, 'randomclinic2' => $randomclinic2,
            'randomclinic3' => $randomclinic3,
        );
        
    }
    
    /**
     * @Route("/home/doctorgroup", name="doctorgrouphomepage")
     * @Template()
     */
    public function doctorgroupAction(Request $request)
    {
        $request = $this->getRequest();
        $searchword = $request->request->get('searchfield');
        $em = $this->getDoctrine()->getManager();
        $repo3 = $this->getDoctrine()->getRepository("ResearchProjectMyProjectBundle:DoctorGroup");
        $doctorgroups = $repo3->findByDoctorGroupKeyword($searchword);
        $entities =  $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->findAll();
       

        return array(
            'entities' => $entities, 'doctorgroups' => $doctorgroups, 'searchword' => $searchword,
        );
        
    }
    
    /**
     * @Route("/home/illness", name="illnesshomepage")
     * @Template()
     */
    public function illnessAction()
    {
         $em = $this->getDoctrine()->getManager();

        $entities =  $em->getRepository('ResearchProjectMyProjectBundle:Illness')->findAll();

        return array(
            'entities' => $entities,
        );
        
    }
    
    
    /**
     * @Route("/login")
     * @Template()
     */
    public function loginAction(Request $request) {
        $session = $request->getSession();

        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
        // redirect authenticated users to homepage
        return $this->redirect($this->generateUrl('homepage'));
    }

        
        
        // get the login error if there is onewqre
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return [
            // last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ];
    }
    
     /**
     * @Route("/confirmInvite", name="confirm_invite")
     * 
     */
    public function confirmInviteAction(Request $request) {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $invitation = $em->getRepository('ResearchProjectMyProjectBundle:Invitation')->find($id);
            
          
            if (!$invitation) {
                throw $this->createNotFoundException('Unable to find GroupPost entity.');
            }
            
            
            $groupToAdd = $invitation->getGroup();
            $group = $em->getRepository('ResearchProjectMyProjectBundle:DoctorGroup')->find($groupToAdd);
            $invitation->setIsAssessed(true);
            $user = $this->getUser();
            $group->addDoctor($user);
            
            $em->persist($group);
            $em->persist($invitation);
            $em->flush();
            
      
            $response = ['message' => 'oks']; 
         /* @var $entity Patient */
           
           } else {
            $response = ['errors' => 'not ajax'];
        }
           return new Response(json_encode($response));
        
     }
     
     /**
     * @Route("/declineInvite", name="decline_invite")
     * 
     */
    public function declineInviteAction(Request $request) {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $invitation = $em->getRepository('ResearchProjectMyProjectBundle:Invitation')->find($id);
            
          
            if (!$invitation) {
                throw $this->createNotFoundException('Unable to find GroupPost entity.');
            }
            
            
            
            $em->remove($invitation);
            $em->flush();
            
      
            $response = ['message' => 'oks']; 
         /* @var $entity Patient */
           
           } else {
            $response = ['errors' => 'not ajax'];
        }
           return new Response(json_encode($response));
        
     }
     
     /**
     * @Route("/statusChange", name="status_change")
     * 
     */
    public function statusChangeAction(Request $request) {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $invitation = $em->getRepository('ResearchProjectMyProjectBundle:Appointment')->find($id);
            
          
            if (!$invitation) {
                throw $this->createNotFoundException('Unable to find Appointment entity.');
            }
            
            
            $invitation->setIsAssessed(true);
            $em->persist($invitation);
            $em->flush();
            
      
            $response = ['message' => 'oks']; 
         /* @var $entity Patient */
           
           } else {
            $response = ['errors' => 'not ajax'];
        }
           return new Response(json_encode($response));
        
     }
     
     /**
     * @Route("/appointmentview", name="appointment_view")
     * 
     */
    public function appointmentViewAction(Request $request) {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $id = $request->request->get('id');

        
            $em = $this->getDoctrine()->getEntityManager();
            $appointment = $em->getRepository('ResearchProjectMyProjectBundle:Appointment')->find($id);
            
          
            if (!$appointment) {
                throw $this->createNotFoundException('Unable to find Appointment entity.');
            }
            
    
           
            $query = $em->createQuery('SELECT c from ResearchProjectMyProjectBundle:Appointment c  WHERE c.id = :id')
                     ->setParameter('id',"{$id}");
        
        //AND u.firstName LIKE :string OR u.lastName LIKE :string
         $result = $query->getArrayResult();
         $response = new Response(json_encode($result));
         $response->headers->set('Content-Type', 'application/json');

         return $response;
        }
    }
    
    /**
     * @Route("/settings", name="settings_page")
     * @Template()
     */
    
         public function changePasswdAction(Request $request)
     {
      $changePasswordModel = new ChangePassword();
     
      $userId = $this->getUser()->getId();
      $em = $this->getDoctrine()->getEntityManager();
      $doctorentity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($userId);
     
      $form = $this->createForm(new ChangePasswordType(), $changePasswordModel);
      $changePasswordModel->setDoctor($doctorentity);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $changePassword = $form->getData();

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($changePassword->getDoctor());

        //var_dump($encoder);
        $password = $encoder->encodePassword($changePassword->getNewPassword(), $changePassword->getDoctor()->getSalt());

        $doctorentity->setPassword($password);

        $em->persist($doctorentity);
        $em->flush();

        
          return $this->redirect($this->generateUrl('settings_page'));
      }

      return $this->render('ResearchProjectMyProjectBundle:Default:settings.html.twig', array(
          'form' => $form->createView(),
      ));      
    }
        
    /**
     * @Route("/remove/user", name="remove_doctor_click")
     * 
     */
    public function removeDoctorClickAction(Request $request)
    {
         $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
           
            $id = $request->request->get('id');
        
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('ResearchProjectMyProjectBundle:Doctor')->find($id);
            
          
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Doctor entity.');
            }
            
            $em->remove($entity);
            $em->flush();
            
            $this->get('security.token_storage')->setToken(null);
            $this->get('request')->getSession()->invalidate();
            return new JsonResponse($this->generateUrl('homepage'));
        }
    }
}
