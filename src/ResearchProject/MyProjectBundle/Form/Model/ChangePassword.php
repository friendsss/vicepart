<?php

namespace ResearchProject\MyProjectBundle\Form\Model;

use ResearchProject\MyProjectBundle\Entity\Doctor;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

class ChangePassword
{

      /**
     * 
     * 
     */
    protected $doctor;
    /**
     * @SecurityAssert\UserPassword(
     *     message = "Wrong value for your current password"
     * )
     */
     protected $oldPassword;

    /**
     * @Assert\Length(
     *     min = 6,
     *     minMessage = "Password should by at least 6 chars long"
     * )
     */
     protected $newPassword;
     
     public function setDoctor(Doctor $doctor)
{
    $this->doctor = $doctor;
}

public function getdoctor()
{
    return $this->doctor;
}
     /**
     * Set oldPassword
     *
     * @param string $oldPassword
     * @return ChangePassword
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }
    
    /**
     * Set oldPassword
     *
     * @param string $oldPassword
     * @return ChangePassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }
     
}