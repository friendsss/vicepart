<?php
namespace ResearchProject\MyProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserEditType extends AbstractType {
    
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('firstName')
            ->add('middleName')
            ->add('lastName')
            ->add('emailAddress')   
            ->add('username')
            ->add('dateOfBirth', 'birthday', [ 'invalid_message' => 'Date is invalid', 'widget' => 'single_text',
                  ])
            ->add('fieldOfPractice', 'choice', 
                    array('choices' => array('General' => 'General', 'Specialized' => 'Specialized' )))
             ->add('specialties', 'entity', array(
                            'class' => 'ResearchProjectMyProjectBundle:Specialty',
                            'property'     => 'specialtyName',
                            'multiple'     => true,
                            'expanded' => false,  
                        'query_builder' => function(\ResearchProject\MyProjectBundle\Entity\SpecialtyRepository $repository) { 
                         return $repository->createQueryBuilder('u')->orderBy('u.specialtyName', 'ASC');
        }
                        ))
            ->add('fellowships', 'entity', array(
                            'class' => 'ResearchProjectMyProjectBundle:Fellowship',
                            'property'     => 'fellowshipName',
                            'multiple'     => true,
                            'expanded' => false,  
                        'query_builder' => function(\ResearchProject\MyProjectBundle\Entity\FellowshipRepository $repository) { 
                         return $repository->createQueryBuilder('u')->orderBy('u.fellowshipName', 'ASC');
        }
                        ))
            
            ->add('prcNumber')
            ->add('origIssueDate', 'birthday', [ 'invalid_message' => 'Date is invalid', 'widget' => 'single_text'])
            ->add('aboutMyPractice')
            ->add('contactNumber')
            
            ->add('file')                    
            ->add('submit', 'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ResearchProject\MyProjectBundle\Entity\Doctor',
            'validation_groups' => array('edit'),
        ));
    }

    public function getName() {
        return 'researchproject_myprojectbundle_doctor';
    }

}