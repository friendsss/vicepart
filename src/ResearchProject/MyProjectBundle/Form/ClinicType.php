<?php

namespace ResearchProject\MyProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClinicType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clinicName')
            ->add('monOpenTime')
            ->add('monCloseTime')
            ->add('tueOpenTime')
            ->add('tueCloseTime')
                ->add('wedOpenTime')
            ->add('wedCloseTime')
                ->add('thuOpenTime')
            ->add('thuCloseTime')
                ->add('friOpenTime')
            ->add('friCloseTime')
                ->add('satOpenTime')
            ->add('satCloseTime')
                ->add('sunOpenTime')
            ->add('sunCloseTime')
                
            ->add('clinicAddress')
            
            ->add('clinicDescription')
             ->add('clinicLong')
             ->add('clinicLat')
             ->add('contactNumber')
             ->add('file');
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ResearchProject\MyProjectBundle\Entity\Clinic'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'researchproject_myprojectbundle_clinic';
    }
}
