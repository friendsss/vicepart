<?php

namespace ResearchProject\MyProjectBundle\Form;

use ResearchProject\MyProjectBundle\Entity\ContactNumber;
use ResearchProject\MyProjectBundle\Entity\FellowshipRepository;
use ResearchProject\MyProjectBundle\Entity\SpecialtyRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilder;


class DoctorType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('middleName')
            ->add('lastName')
            ->add('emailAddress' , 'email')   
            ->add('username')
            ->add('password', 'repeated', ['type' => 'password',
                         'invalid_message' => 'The password fields must match.',
                         'options' => ['attr' => ['class' => 'password']],
                         'required' => true,
                         'first_options'  => ['label' => 'Password'],
                         'second_options' => ['label' => 'Confirm Password'],
                         'first_name' => 'first',
                         'second_name' => 'second'])
            ->add('dateOfBirth', 'date', [ 'widget' => 'single_text', 'html5'   => false, 'format' => 'MM-dd-yyyy'
                  ])
            ->add('fieldOfPractice', 'choice', 
                    array('choices' => array('Specialized' => 'Specialized','General' => 'General',  )))
            ->add('specialties', 'entity', array(
                            'class' => 'ResearchProjectMyProjectBundle:Specialty',
                            'property'     => 'specialtyName',
                            'multiple'     => true,
                            'expanded' => false,  
                        'query_builder' => function(SpecialtyRepository $repository) { 
                         return $repository->createQueryBuilder('u')->orderBy('u.specialtyName', 'ASC');
        }
                        ))
            ->add('fellowships', 'entity', array(
                            'class' => 'ResearchProjectMyProjectBundle:Fellowship',
                            'property'     => 'fellowshipName',
                            'multiple'     => true,
                            'expanded' => false,  
                        'query_builder' => function(FellowshipRepository $repository) { 
                         return $repository->createQueryBuilder('u')->orderBy('u.fellowshipName', 'ASC');
        }
                        ))
            ->add('prcNumber')
            ->add('origIssueDate', 'date', [ 'widget' => 'single_text', 'html5'   => false, 'format' => 'MM-dd-yyyy' 
                  ])
            ->add('aboutMyPractice')
            ->add('contactNumber')
            
            ->add('file')
           /* ->add('contactNumbers', 'collection', array(
                'type' => new ContactNumberType(),
                'allow_add' => true,
                'by_reference' => false,
            'allow_delete' => true
                
               ))
            */
            ;
        
                        
                
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ResearchProject\MyProjectBundle\Entity\Doctor'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'researchproject_myprojectbundle_doctor';
    }
}

