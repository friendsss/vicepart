<?php

namespace ResearchProject\MyProjectBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Clinic
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClinicRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Clinic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="clinicName", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "1",
     *      minMessage = "Must be at least {{ limit }} characters."
     *      )
     * *  @Assert\Regex(pattern="/^[\p{L}\dÑñ\s .'-\/!?@\$\^\&\*\(\)]+$/i",
     *                  message = "Alphanumeric and some Special Characters are allowed. ")
     */
    private $clinicName;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="clinicDescription", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $clinicDescription;
    
     /**
     * @ORM\Column(name="contactNumber", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex("/^(\d{7}|\d{10}|09\d{9})$/",
     *               message= "Only telephone with or without local numbers and mobile numbers are allowed.")
     */
    private $contactNumber;
    
     /**
     * @var string
     *
     * @ORM\Column(name="clinicLong", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $clinicLong;
    
     /**
     * @var string
     *
     * @ORM\Column(name="clinicLat", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $clinicLat;

    /**
     * @var string
     *
     * @ORM\Column(name="monOpenTime", type="time", length=255)
     */
    private $monOpenTime;
    /**
     * @var string
     *
     * @ORM\Column(name="monCloseTime", type="time", length=255)
     */
    private $monCloseTime;
    /**
     * @var string
     *
     * @ORM\Column(name="tueOpenTime", type="time", length=255)
     */
    private $tueOpenTime;
    /**
     * @var string
     *
     * @ORM\Column(name="tueCloseTime", type="time", length=255)
     */
    private $tueCloseTime;
     /**
     * @var string
     *
     * @ORM\Column(name="wedOpenTime", type="time", length=255)
     */
    private $wedOpenTime;
    /**
     * @var string
     *
     * @ORM\Column(name="wedCloseTime", type="time", length=255)
     */
    private $wedCloseTime;
     /**
     * @var string
     *
     * @ORM\Column(name="thuOpenTime", type="time", length=255)
     */
    private $thuOpenTime;
    /**
     * @var string
     *
     * @ORM\Column(name="thuCloseTime", type="time", length=255)
     */
    private $thuCloseTime;
     /**
     * @var string
     *
     * @ORM\Column(name="friOpenTime", type="time", length=255)
     */
    private $friOpenTime;
    /**
     * @var string
     *
     * @ORM\Column(name="friCloseTime", type="time", length=255)
     */
    private $friCloseTime;
    /**
     * @var string
     *
     * @ORM\Column(name="satOpenTime", type="time", length=255)
     */
    private $satOpenTime;
    /**
     * @var string
     *
     * @ORM\Column(name="satCloseTime", type="time", length=255)
     */
    private $satCloseTime;
    /**
     * @var string
     *
     * @ORM\Column(name="sunOpenTime", type="time", length=255)
     */
    private $sunOpenTime;
    /**
     * @var string
     *
     * @ORM\Column(name="sunCloseTime", type="time", length=255)
     */
    private $sunCloseTime;

    /**
     * @var string
     *
     * @ORM\Column(name="clinicAddress", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $clinicAddress;

    
    
   

     /**
     * 
     * 
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Doctor", inversedBy="doctorClinics")
     * 
     */
    private $doctors;
    
    /**
     * 
     * Res
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Appointment", mappedBy="clinic", cascade={"remove"})
     */
    private $appointments;
   
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;
    
     /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;
    
     private $temp;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clinicName
     *
     * @param string $clinicName
     * @return Clinic
     */
    public function setClinicName($clinicName)
    {
        $this->clinicName = $clinicName;

        return $this;
    }

    /**
     * Get clinicName
     *
     * @return string 
     */
    public function getClinicName()
    {
        return $this->clinicName;
    }

   
    

    /**
     * Set clinicAddress
     *
     * @param string $clinicAddress
     * @return Clinic
     */
    public function setClinicAddress($clinicAddress)
    {
        $this->clinicAddress = $clinicAddress;

        return $this;
    }

    /**
     * Get clinicAddress
     *
     * @return string 
     */
    public function getClinicAddress()
    {
        return $this->clinicAddress;
    }
    
     

    /**
     * Set clinicDescription
     *
     * @param string $clinicDescription
     * @return Clinic
     */
    public function setClinicDescription($clinicDescription)
    {
        $this->clinicDescription = $clinicDescription;
    
        return $this;
    }

    /**
     * Get clinicDescription
     *
     * @return string 
     */
    public function getClinicDescription()
    {
        return $this->clinicDescription;
    }

    

    /**
     * Set monOpenTime
     *
     * @param DateTime $monOpenTime
     * @return Clinic
     */
    public function setMonOpenTime($monOpenTime)
    {
        $this->monOpenTime = $monOpenTime;
    
        return $this;
    }

    /**
     * Get monOpenTime
     *
     * @return DateTime 
     */
    public function getMonOpenTime()
    {
        return $this->monOpenTime;
    }

    /**
     * Set monCloseTime
     *
     * @param DateTime $monCloseTime
     * @return Clinic
     */
    public function setMonCloseTime($monCloseTime)
    {
        $this->monCloseTime = $monCloseTime;
    
        return $this;
    }

    /**
     * Get monCloseTime
     *
     * @return DateTime 
     */
    public function getMonCloseTime()
    {
        return $this->monCloseTime;
    }

    /**
     * Set tueOpenTime
     *
     * @param DateTime $tueOpenTime
     * @return Clinic
     */
    public function setTueOpenTime($tueOpenTime)
    {
        $this->tueOpenTime = $tueOpenTime;
    
        return $this;
    }

    /**
     * Get tueOpenTime
     *
     * @return DateTime 
     */
    public function getTueOpenTime()
    {
        return $this->tueOpenTime;
    }

    /**
     * Set tueCloseTime
     *
     * @param DateTime $tueCloseTime
     * @return Clinic
     */
    public function setTueCloseTime($tueCloseTime)
    {
        $this->tueCloseTime = $tueCloseTime;
    
        return $this;
    }

    /**
     * Get tueCloseTime
     *
     * @return DateTime 
     */
    public function getTueCloseTime()
    {
        return $this->tueCloseTime;
    }

    /**
     * Set wedOpenTime
     *
     * @param DateTime $wedOpenTime
     * @return Clinic
     */
    public function setWedOpenTime($wedOpenTime)
    {
        $this->wedOpenTime = $wedOpenTime;
    
        return $this;
    }

    /**
     * Get wedOpenTime
     *
     * @return DateTime 
     */
    public function getWedOpenTime()
    {
        return $this->wedOpenTime;
    }

    /**
     * Set wedCloseTime
     *
     * @param DateTime $wedCloseTime
     * @return Clinic
     */
    public function setWedCloseTime($wedCloseTime)
    {
        $this->wedCloseTime = $wedCloseTime;
    
        return $this;
    }

    /**
     * Get wedCloseTime
     *
     * @return DateTime 
     */
    public function getWedCloseTime()
    {
        return $this->wedCloseTime;
    }

    /**
     * Set thuOpenTime
     *
     * @param DateTime $thuOpenTime
     * @return Clinic
     */
    public function setThuOpenTime($thuOpenTime)
    {
        $this->thuOpenTime = $thuOpenTime;
    
        return $this;
    }

    /**
     * Get thuOpenTime
     *
     * @return DateTime 
     */
    public function getThuOpenTime()
    {
        return $this->thuOpenTime;
    }

    /**
     * Set thuCloseTime
     *
     * @param DateTime $thuCloseTime
     * @return Clinic
     */
    public function setThuCloseTime($thuCloseTime)
    {
        $this->thuCloseTime = $thuCloseTime;
    
        return $this;
    }

    /**
     * Get thuCloseTime
     *
     * @return DateTime 
     */
    public function getThuCloseTime()
    {
        return $this->thuCloseTime;
    }

    /**
     * Set friOpenTime
     *
     * @param DateTime $friOpenTime
     * @return Clinic
     */
    public function setFriOpenTime($friOpenTime)
    {
        $this->friOpenTime = $friOpenTime;
    
        return $this;
    }

    /**
     * Get friOpenTime
     *
     * @return DateTime 
     */
    public function getFriOpenTime()
    {
        return $this->friOpenTime;
    }

    /**
     * Set friCloseTime
     *
     * @param DateTime $friCloseTime
     * @return Clinic
     */
    public function setFriCloseTime($friCloseTime)
    {
        $this->friCloseTime = $friCloseTime;
    
        return $this;
    }

    /**
     * Get friCloseTime
     *
     * @return DateTime 
     */
    public function getFriCloseTime()
    {
        return $this->friCloseTime;
    }

    /**
     * Set satOpenTime
     *
     * @param DateTime $satOpenTime
     * @return Clinic
     */
    public function setSatOpenTime($satOpenTime)
    {
        $this->satOpenTime = $satOpenTime;
    
        return $this;
    }

    /**
     * Get satOpenTime
     *
     * @return DateTime 
     */
    public function getSatOpenTime()
    {
        return $this->satOpenTime;
    }

    /**
     * Set satCloseTime
     *
     * @param DateTime $satCloseTime
     * @return Clinic
     */
    public function setSatCloseTime($satCloseTime)
    {
        $this->satCloseTime = $satCloseTime;
    
        return $this;
    }

    /**
     * Get satCloseTime
     *
     * @return DateTime 
     */
    public function getSatCloseTime()
    {
        return $this->satCloseTime;
    }

    /**
     * Set sunOpenTime
     *
     * @param DateTime $sunOpenTime
     * @return Clinic
     */
    public function setSunOpenTime($sunOpenTime)
    {
        $this->sunOpenTime = $sunOpenTime;
    
        return $this;
    }

    /**
     * Get sunOpenTime
     *
     * @return DateTime 
     */
    public function getSunOpenTime()
    {
        return $this->sunOpenTime;
    }

    /**
     * Set sunCloseTime
     *
     * @param DateTime $sunCloseTime
     * @return Clinic
     */
    public function setSunCloseTime($sunCloseTime)
    {
        $this->sunCloseTime = $sunCloseTime;
    
        return $this;
    }

    /**
     * Get sunCloseTime
     *
     * @return DateTime 
     */
    public function getSunCloseTime()
    {
        return $this->sunCloseTime;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->doctors = new ArrayCollection();
    }

    /**
     * Add doctors
     *
     * @param Doctor $doctors
     * @return Clinic
     */
    public function addDoctor(Doctor $doctors)
    {
        $this->doctors[] = $doctors;
    
        return $this;
    }

    /**
     * Remove doctors
     *
     * @param Doctor $doctors
     */
    public function removeDoctor(Doctor $doctors)
    {
        $this->doctors->removeElement($doctors);
    }

    /**
     * Get doctors
     *
     * @return Collection 
     */
    public function getDoctors()
    {
        return $this->doctors;
    }

    

   
    public function __toString()
{
    return $this->clinicName;
}

    /**
     * Add appointment
     *
     * @param Appointment $appointment
     *
     * @return Clinic
     */
    public function addAppointment(Appointment $appointment)
    {
        $this->appointments[] = $appointment;

        return $this;
    }

    /**
     * Remove appointment
     *
     * @param Appointment $appointment
     */
    public function removeAppointment(Appointment $appointment)
    {
        $this->appointments->removeElement($appointment);
    }

    /**
     * Get appointments
     *
     * @return Collection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * Set clinicLong
     *
     * @param string $clinicLong
     *
     * @return Clinic
     */
    public function setClinicLong($clinicLong)
    {
        $this->clinicLong = $clinicLong;

        return $this;
    }

    /**
     * Get clinicLong
     *
     * @return string
     */
    public function getClinicLong()
    {
        return $this->clinicLong;
    }

    /**
     * Set clinicLat
     *
     * @param string $clinicLat
     *
     * @return Clinic
     */
    public function setClinicLat($clinicLat)
    {
        $this->clinicLat = $clinicLat;

        return $this;
    }

    /**
     * Get clinicLat
     *
     * @return string
     */
    public function getClinicLat()
    {
        return $this->clinicLat;
    }

    /**
     * Set contactNumber
     *
     * @param string $contactNumber
     *
     * @return Clinic
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    /**
     * Get contactNumber
     *
     * @return string
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }
    
     public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }
    
     /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getFile()->guessExtension();
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
     /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Doctor
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
