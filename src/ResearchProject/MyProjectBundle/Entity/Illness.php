<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Illness
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ResearchProject\MyProjectBundle\Entity\IllnessRepository")
 */
class Illness
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="illnessName", type="string", length=255)
     */
    private $illnessName;

    /**
     * @var string
     *
     * @ORM\Column(name="illnessCommonName", type="string", length=255)
     */
    private $illnessCommonName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="illnessDefinition", type="text")
     */
    private $illnessDefinition;
    
    /**
     * @var string
     *
     * @ORM\Column(name="symptoms", type="text")
     */
    private $symptoms;
    
    /**
     * @var string
     *
     * @ORM\Column(name="homeremedies", type="text")
     */
    private $homeRemedies;
    
    /**
     * @var string
     *
     * @ORM\Column(name="commonlyPrescribedDrugs", type="text")
     */
    private $commonlyPrescribedDrugs;
    
    /**
     * @var string
     *
     * @ORM\Column(name="illnessDescription", type="text")
     */
    private $illnessDescription;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
  

    /**
     * Set illnessName
     *
     * @param string $illnessName
     * @return Illness
     */
    public function setIllnessName($illnessName)
    {
        $this->illnessName = $illnessName;

        return $this;
    }

    /**
     * Get illnessName
     *
     * @return string 
     */
    public function getIllnessName()
    {
        return $this->illnessName;
    }

    /**
     * Set illnessDescription
     *
     * @param text $illnessDescription
     * @return Illness
     */
    public function setIllnessDescription($illnessDescription)
    {
        $this->illnessDescription = $illnessDescription;

        return $this;
    }

    /**
     * Get illnessDescription
     *
     * @return text 
     */
    public function getIllnessDescription()
    {
        return $this->illnessDescription;
    }
    
        /**
     * Get illnessCommonName
     *
     * @return string 
     */
    public function getIllnessCommonName()
    {
        return $this->illnessCommonName;
    }
    
    /**
     * Set illnessCommonName
     *
     * @param string $illnessCommonName
     * @return Illness
     */
    public function setIllnessCommonName($illnessCommonName)
    {
        $this->illnessCommonName = $illnessCommonName;

        return $this;
    }
    
        /**
     * Get illnessDefinition
     *
     * @return text 
     */
    public function getIllnessDefinition()
    {
        return $this->illnessDefinition;
    }
    
    /**
     * Set illnessDefinition
     *
     * @param text $illnessDefinition
     * @return Illness
     */
    public function setIllnessDefinition($illnessDefinition)
    {
        $this->illnessDefinition = $illnessDefinition;

        return $this;
    }
    
    /**
     * Get symptoms
     *
     * @return text
     */
    public function getSymptoms()
    {
        return $this->symptoms;
    }
    
    /**
     * Set symptoms
     *
     * @param string $symptoms
     * @return Illness
     */
    public function setSymptoms($symptoms)
    {
        $this->symptoms = $symptoms;

        return $this;
    }
    
    /**
     * Get homeRemedies
     *
     * @return text
     */
    public function getHomeRemedies()
    {
        return $this->homeRemedies;
    }
    
    /**
     * Set homeRemedies
     *
     * @param text $homeRemedies
     * @return Illness
     */
    public function setHomeRemedies($homeRemedies)
    {
        $this->homeRemedies = $homeRemedies;

        return $this;
    }
    
    /**
     * Get commonlyPrescribedDrugs
     *
     * @return text
     */
    public function getCommonlyPrescribedDrugs()
    {
        return $this->commonlyPrescribedDrugs;
    }
    
    /**
     * Set commonlyPrescribedDrugs
     *
     * @param text $commonlyPrescribedDrugs
     * @return Illness
     */
    public function setCommonlyPrescribedDrugs($commonlyPrescribedDrugs)
    {
        $this->commonlyPrescribedDrugs = $commonlyPrescribedDrugs;

        return $this;
    }
}
