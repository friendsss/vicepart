<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ClinicRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ClinicRepository extends EntityRepository
{
    function findByClinicKeyword($keyword) {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
        SELECT c FROM ResearchProjectMyProjectBundle:Clinic c
        WHERE c.clinicName LIKE :searchedName');
        $query->setParameter('searchedName', "%{$keyword}%");

        return $query->getResult();
    }
    
    function findByRandomClinic() {
        $count = $this->createQueryBuilder('u')
             ->select('COUNT(u)')
             ->getQuery()
             ->getSingleScalarResult();
        
        return $this->createQueryBuilder('u')
                    ->setFirstResult(rand(0, $count - 1))
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getSingleResult();
    }
    
    function findByClinicDoctor($doctorId) {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
        SELECT c FROM ResearchProjectMyProjectBundle:Clinic c
        INNER JOIN c.doctors d
        WHERE d.id = :doctorId');
        $query->setParameter('doctorId', "{$doctorId}");

        return $query->getResult();
    }
}
