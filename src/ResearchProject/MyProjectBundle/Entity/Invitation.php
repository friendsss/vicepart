<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invitation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ResearchProject\MyProjectBundle\Entity\InvitationRepository")
 */
class Invitation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isAssessed", type="boolean")
     */
    private $isAssessed;
    
     /**
     * @ORM\ManyToOne(targetEntity="Doctor", inversedBy="inviter")
     * @ORM\JoinColumn(name="doctor_inviter", referencedColumnName="id")
     */
    private $doctorInviter;
    
    /**
     * @ORM\ManyToOne(targetEntity="Doctor", inversedBy="invitee")
     * @ORM\JoinColumn(name="doctor_invitee", referencedColumnName="id")
     */
    private $doctorInvitee;
    
    /**
     * @ORM\ManyToOne(targetEntity="DoctorGroup", inversedBy="invitations")
     * @ORM\JoinColumn(name="invitation_group", referencedColumnName="id")
     */
    private $group;
    
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isAssessed
     *
     * @param boolean $isAssessed
     * @return Invitation
     */
    public function setIsAssessed($isAssessed)
    {
        $this->isAssessed = $isAssessed;
    
        return $this;
    }

    /**
     * Get isAssessed
     *
     * @return boolean 
     */
    public function getIsAssessed()
    {
        return $this->isAssessed;
    }

    /**
     * Set doctorInviter
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Doctor $doctorInviter
     * @return Invitation
     */
    public function setDoctorInviter(\ResearchProject\MyProjectBundle\Entity\Doctor $doctorInviter = null)
    {
        $this->doctorInviter = $doctorInviter;
    
        return $this;
    }

    /**
     * Get doctorInviter
     *
     * @return \ResearchProject\MyProjectBundle\Entity\Doctor 
     */
    public function getDoctorInviter()
    {
        return $this->doctorInviter;
    }

    /**
     * Set doctorInvitee
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Doctor $doctorInvitee
     * @return Invitation
     */
    public function setDoctorInvitee(\ResearchProject\MyProjectBundle\Entity\Doctor $doctorInvitee = null)
    {
        $this->doctorInvitee = $doctorInvitee;
    
        return $this;
    }

    /**
     * Get doctorInvitee
     *
     * @return \ResearchProject\MyProjectBundle\Entity\Doctor 
     */
    public function getDoctorInvitee()
    {
        return $this->doctorInvitee;
    }

    /**
     * Set group
     *
     * @param \ResearchProject\MyProjectBundle\Entity\DoctorGroup $group
     *
     * @return Invitation
     */
    public function setGroup(\ResearchProject\MyProjectBundle\Entity\DoctorGroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \ResearchProject\MyProjectBundle\Entity\DoctorGroup
     */
    public function getGroup()
    {
        return $this->group;
    }
}
