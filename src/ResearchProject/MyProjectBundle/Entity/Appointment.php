<?php

namespace ResearchProject\MyProjectBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Appointment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppointmentRepository")
 */
class Appointment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="dateAppointed", type="datetime")
     */
    private $dateAppointed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isAssessed", type="boolean")
     */
    private $isAssessed;

    /**
     * @var string
     *
     * @ORM\Column(name="emailAddress", type="string", length=255)
     */
    private $emailAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="appointmentCode", type="string", length=255)
     */
    private $appointmentCode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contactNumber", type="string", length=255)
     */
    private $contactNumber;
    
    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255)
     */
    private $note;
    
    /**
     * @ORM\ManyToOne(targetEntity="Clinic", inversedBy="appointments")
     * @ORM\JoinColumn(name="clinic_appointment", referencedColumnName="id")
     */
    private $clinic;

     public function __construct()
    {
        $this->dateCreated = new DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Appointment
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Appointment
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set dateCreated
     *
     * @param DateTime $dateCreated
     * @return Appointment
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    
        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateAppointed
     *
     * @param DateTime $dateAppointed
     * @return Appointment
     */
    public function setDateAppointed($dateAppointed)
    {
        $this->dateAppointed = $dateAppointed;
    
        return $this;
    }

    /**
     * Get dateAppointed
     *
     * @return DateTime 
     */
    public function getDateAppointed()
    {
        return $this->dateAppointed;
    }

    /**
     * Set isAssessed
     *
     * @param boolean $isAssessed
     * @return Appointment
     */
    public function setIsAssessed($isAssessed)
    {
        $this->isAssessed = $isAssessed;
    
        return $this;
    }

    /**
     * Get isAssessed
     *
     * @return boolean 
     */
    public function getIsAssessed()
    {
        return $this->isAssessed;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     * @return Appointment
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    
        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string 
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set appointmentCode
     *
     * @param string $appointmentCode
     * @return Appointment
     */
    public function setAppointmentCode($appointmentCode)
    {
        $this->appointmentCode = $appointmentCode;
    
        return $this;
    }

    /**
     * Get appointmentCode
     *
     * @return string 
     */
    public function getAppointmentCode()
    {
        return $this->appointmentCode;
    }

    /**
     * Set contactNumber
     *
     * @param string $contactNumber
     * @return Appointment
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;
    
        return $this;
    }

    /**
     * Get contactNumber
     *
     * @return string 
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Set clinic
     *
     * @param Clinic $clinic
     * @return Appointment
     */
    public function setClinic(Clinic $clinic = null)
    {
        $this->clinic = $clinic;
    
        return $this;
    }

    /**
     * Get clinic
     *
     * @return Clinic 
     */
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Appointment
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }
}
