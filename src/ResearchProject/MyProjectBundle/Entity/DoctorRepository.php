<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * DoctorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DoctorRepository extends EntityRepository
{
    
    function findOneByDoctorUsername($keyword) {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
        SELECT r FROM ResearchProjectMyProjectBundle:Doctor r 
        WHERE r.username LIKE :therole');
        $query->setParameter('therole', "%{$keyword}%");
                    

        return $query->getResult();
    }
    
    function findByDoctorKeyword($keyword) {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
        SELECT d FROM ResearchProjectMyProjectBundle:Doctor d
        WHERE d.firstName LIKE :searchedName OR d.middleName LIKE :searchedName
        OR d.lastName LIKE :searchedName');
        $query->setParameter('searchedName', "%{$keyword}%");

        return $query->getResult();
    }
    
    function findByRandomDoctor() {
        $count = $this->createQueryBuilder('u')
             ->select('COUNT(u)')
             ->getQuery()
             ->getSingleScalarResult();
        
        return $this->createQueryBuilder('u')
                    ->setFirstResult(rand(0, $count - 1))
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getSingleResult();
    }
    
    public function findByLetters($string){
        return $this->getEntityManager()->createQuery('SELECT u FROM ResearchProjectMyProjectBundle:Doctor u  
                WHERE u.firstName LIKE :string OR u.lastName LIKE :string')
                ->setParameter('string','%'.$string.'%')
                ->getResult();
    }
    
}
