<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * DoctorGroup
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DoctorGroupRepository")
 */
class DoctorGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="groupName", type="string", length=255)
     */
    private $groupName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     *@var DateTime
     *
     * @ORM\Column(name="dateCreated", type="date")
     */
    private $dateCreated;
    
    /**
     * 
     * 
     * 
     * 
     * @ORM\ManyToMany(targetEntity="Doctor", mappedBy="groupsAdministrating")
     * 
     */
    private $adminDoctors;
    
    /**
     * 
     * 
     * 
     * 
     * @ORM\ManyToMany(targetEntity="Doctor", mappedBy="doctorGroups")
     */
    private $doctors;
    
    /**
     * 
     * 
     * 
     * 
     * @ORM\OneToMany(targetEntity="GroupPost", mappedBy="group", cascade={"remove"}, orphanRemoval=true)
     */
    private $groupPosts;
    
    /**
     * 
     * 
     * 
     * 
     * @ORM\OneToMany(targetEntity="Invitation", mappedBy="group", cascade={"remove"}, orphanRemoval=true)
     */
    private $invitations;
    
        
    function getDoctors() {
        return $this->doctors;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set groupName
     *
     * @param string $groupName
     * @return DoctorGroup
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;

        return $this;
    }

    /**
     * Get groupName
     *
     * @return string 
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return DoctorGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateCreated
     *
     * @param string $dateCreated
     * @return DoctorGroup
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return string 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->doctors = new ArrayCollection();
        $this->groupPosts = new ArrayCollection();
    }

    /**
     * Add doctors
     *
     * @param Doctor $doctors
     * @return DoctorGroup
     */
    public function addDoctor(Doctor $doctor)
    {
        $this->doctors[] = $doctor;

    
       if (!$doctor->getDoctorGroups()->contains($this)){
           $doctor->setDoctorGroups($this);
       }
        
        return $this;
        
        
    }

    /**
     * Remove doctors
     *
     * @param Doctor $doctors
     */
    public function removeDoctor(Doctor $doctors)
    {
        $this->doctors->removeElement($doctors);
        $doctors->getDoctorGroups()->removeElement($this);
    }

   

    /**
     * Add groupPosts
     *
     * @param GroupPost $groupPosts
     * @return DoctorGroup
     */
    public function addGroupPost(GroupPost $groupPosts)
    {
        $this->groupPosts[] = $groupPosts;
    
        return $this;
    }

    /**
     * Remove groupPosts
     *
     * @param GroupPost $groupPosts
     */
    public function removeGroupPost(GroupPost $groupPosts)
    {
        $this->groupPosts->removeElement($groupPosts);
    }

    /**
     * Get groupPosts
     *
     * @return Collection 
     */
    public function getGroupPosts()
    {
        return $this->groupPosts;
    }

    /**
     * Add adminDoctors
     *
     * @param Doctor $adminDoctors
     * @return DoctorGroup
     */
    public function addAdminDoctor(Doctor $adminDoctors)
    {
       $this->adminDoctors[] = $adminDoctors;

    
       if (!$adminDoctors->getGroupsAdministrating()->contains($this)){
           $adminDoctors->addGroupsAdministrating($this);
       }
        
        return $this;
    }

    /**
     * Remove adminDoctors
     *
     * @param Doctor $adminDoctors
     */
    public function removeAdminDoctor(Doctor $adminDoctors)
    {
        $this->adminDoctors->removeElement($adminDoctors);
    }

    /**
     * Get adminDoctors
     *
     * @return Collection 
     */
    public function getAdminDoctors()
    {
        return $this->adminDoctors;
    }
   
    
    

    /**
     * Add invitation
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Invitation $invitation
     *
     * @return DoctorGroup
     */
    public function addInvitation(\ResearchProject\MyProjectBundle\Entity\Invitation $invitation)
    {
        $this->invitations[] = $invitation;

        return $this;
    }

    /**
     * Remove invitation
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Invitation $invitation
     */
    public function removeInvitation(\ResearchProject\MyProjectBundle\Entity\Invitation $invitation)
    {
        $this->invitations->removeElement($invitation);
    }

    /**
     * Get invitations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvitations()
    {
        return $this->invitations;
    }
}
