<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContactNumber
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ResearchProject\MyProjectBundle\Entity\ContactNumberRepository")
 */
class ContactNumber
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255)
     */
    private $number;
    
    /**
     * @ORM\ManyToOne(targetEntity="Clinic", inversedBy="contactNumbers", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="clinic_contact", referencedColumnName="id")
     */
    private $clinic;
    
    /**
     * @ORM\ManyToOne(targetEntity="Doctor", inversedBy="contactNumbers", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="doctor_contact", referencedColumnName="id")
     */
    private $doctor;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return ContactNumber
     */
    public function setNumber($number)
    {
        $this->number = $number;
    
        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set clinic
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Clinic $clinic
     * @return ContactNumber
     */
    public function setClinic(\ResearchProject\MyProjectBundle\Entity\Clinic $clinic = null)
    {
        $this->clinic = $clinic;
    
        return $this;
    }

    /**
     * Get clinic
     *
     * @return \ResearchProject\MyProjectBundle\Entity\Clinic 
     */
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * Set doctor
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Doctor $doctor
     * @return ContactNumber
     */
    public function setDoctor(\ResearchProject\MyProjectBundle\Entity\Doctor $doctor = null)
    {
        $this->doctor = $doctor;
    
        return $this;
    }

    /**
     * Get doctor
     *
     * @return \ResearchProject\MyProjectBundle\Entity\Doctor 
     */
    public function getDoctor()
    {
        return $this->doctor;
    }
}
