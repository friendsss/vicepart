<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="CommentRepository")
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="commentContent", type="string", length=255)
     */
    private $commentContent;

    /**
     * @var date
     *
     * @ORM\Column(name="dateCommented", type="datetime")
     */
    private $dateCommented;
    
     /**
     * @ORM\ManyToOne(targetEntity="GroupPost", inversedBy="postComments")
     * @ORM\JoinColumn(name="post_comments", referencedColumnName="id")
     */
    private $post;
    
    /**
     * @ORM\ManyToOne(targetEntity="Doctor", inversedBy="comment")
     * @ORM\JoinColumn(name="doctor_comment", referencedColumnName="id")
     */
    private $commentAuthor;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commentContent
     *
     * @param string $commentContent
     * @return Comment
     */
    public function setCommentContent($commentContent)
    {
        $this->commentContent = $commentContent;
    
        return $this;
    }

    /**
     * Get commentContent
     *
     * @return string 
     */
    public function getCommentContent()
    {
        return $this->commentContent;
    }

    /**
     * Set dateCommented
     *
     * @param string $dateCommented
     * @return Comment
     */
    public function setDateCommented($dateCommented)
    {
        $this->dateCommented = $dateCommented;
    
        return $this;
    }

    /**
     * Get dateCommented
     *
     * @return string 
     */
    public function getDateCommented()
    {
        return $this->dateCommented;
    }

    /**
     * Set post
     *
     * @param GroupPost $post
     * @return Comment
     */
    public function setPost(GroupPost $post = null)
    {
        $this->post = $post;
    
        return $this;
    }

    /**
     * Get post
     *
     * @return GroupPost 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set commentAuthor
     *
     * @param Doctor $commentAuthor
     * @return Comment
     */
    public function setCommentAuthor(Doctor $commentAuthor = null)
    {
        $this->commentAuthor = $commentAuthor;
    
        return $this;
    }

    /**
     * Get commentAuthor
     *
     * @return Doctor 
     */
    public function getCommentAuthor()
    {
        return $this->commentAuthor;
    }
}
