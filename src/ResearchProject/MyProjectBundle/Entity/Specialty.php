<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Specialty
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ResearchProject\MyProjectBundle\Entity\SpecialtyRepository")
 */
class Specialty
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="specialtyName", type="string", length=255)
     */
    private $specialtyName;
    
    /**
     * 
     * Res
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Doctor", mappedBy="specialties")
     */
    private $doctors;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set specialtyName
     *
     * @param string $specialtyName
     * @return Specialty
     */
    public function setSpecialtyName($specialtyName)
    {
        $this->specialtyName = $specialtyName;
    
        return $this;
    }

    /**
     * Get specialtyName
     *
     * @return string 
     */
    public function getSpecialtyName()
    {
        return $this->specialtyName;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->doctors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add doctors
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Doctor $doctors
     * @return Specialty
     */
    public function addDoctor(\ResearchProject\MyProjectBundle\Entity\Doctor $doctors)
    {
        $this->doctors[] = $doctors;
        
       
    
        return $this;
    }

    /**
     * Remove doctors
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Doctor $doctors
     */
    public function removeDoctor(\ResearchProject\MyProjectBundle\Entity\Doctor $doctors)
    {
        $this->doctors->removeElement($doctors);
    }

    /**
     * Get doctors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDoctors()
    {
        return $this->doctors;
    }
}
