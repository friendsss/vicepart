<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * InvitationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class InvitationRepository extends EntityRepository
{
     function findByInvitation($doctorId) {
        $em = $this->getEntityManager();
        $query = $em->createQuery('
        SELECT i FROM ResearchProjectMyProjectBundle:Invitation i
        INNER JOIN i.doctorInvitee t
        WHERE t.id = :doctorId AND i.isAssessed = false');
        $query->setParameter('doctorId', "{$doctorId}");

        return $query->getResult();
    }
    
}
