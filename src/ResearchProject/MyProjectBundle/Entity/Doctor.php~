<?php

namespace ResearchProject\MyProjectBundle\Entity;
//use Symfony\Component\Security\Core\User\User;


use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Serializable;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Doctor
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DoctorRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Doctor implements UserInterface, Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "1",
     *      minMessage = "Must be at least {{ limit }} characters."
     *      )
     *  @Assert\Regex(pattern="/^[\p{L}\dÑñ\s .'-]+$/i",
     *                  message = "Alphanumeric and Special ( - . ' ) Characters are allowed. ")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="middleName", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[\p{L}Ññ .'-]+$/i",
     *               message="Alphanumeric and Special ( - . ' ) Characters are allowed.")
     *      )
     * @Assert\Length(
     *      min = "2",
     *      minMessage = "Must be at least {{ limit }} characters."
     *      )
     */
    private $middleName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[\p{L}Ññ .'-]+$/i",
     *               message="Alphanumeric and Special ( - . ' ) Characters are allowed.")
     *      )
     * @Assert\Length(
     *      min = "2",
     *      minMessage = "Must be at least {{ limit }} characters."
     *      )
     */
    private $lastName;

     /**
     * @var string
     *
     * @ORM\Column(name="emailAddress", type="string", length=255)
     * 
     * @Assert\NotBlank()
     * 
     */
    private $emailAddress;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     * @ORM\Column(name="username", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[\pL\pN\p{Pc}]*$/",
     *               message="Alphanumeric and ( _ ) characters only.")
     * @Assert\Length(
     *      min = "5",
     *      max = "12",
     *      minMessage = "Must be at least {{ limit }} characters",
     *      maxMessage = "Your username cannot be longer than {{ limit }} characters length
                          and can only be a combination of letters and numbers."
     *      )
     */
    
    private $username;
    
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
        * @Assert\NotBlank(message=" Password must not be empty.")
        * @Assert\Length(
        *      min = "5",
        *      max = "12",
        *      minMessage = "Must be at least 5 characters long.",
        *      maxMessage = "Cannot be longer than 12 characters.",
        *      groups = {"Default"}
 * )
     */
    private $password;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="dateOfBirth", type="date")
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $dateOfBirth;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldOfPractice", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $fieldOfPractice;

    /**
     * @var float
     *
     * @ORM\Column(name="prcNumber", type="float")
     * @Assert\NotBlank()
     * @Assert\Regex("/\d{7}/",
     *               message= "Numbers only are allowed.")
     */
    private $prcNumber;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="origIssueDate", type="date")
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $origIssueDate;

    /**
     * 
     * 
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Specialty", inversedBy="doctors")
     */
    private $specialties;
    
    /**
     * 
     * 
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Fellowship", inversedBy="doctors")
     */
    private $fellowships;

     /**
     * @var string
     *
     * @ORM\Column(name="aboutMyPractice", type="string", length=255)
      * @Assert\NotBlank()
     */
    private $aboutMyPractice;
    
    /**
     * @ORM\Column(name="contactNumber", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex("/^(\d{7}|\d{10}|09\d{9})$/",
     *               message= "Only telephone with or without local numbers and mobile numbers are allowed.")
     */
    private $contactNumber;
    
   

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;
    
     /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;
    
     private $temp;
    
   
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="userRole", type="string", length=255)
     */
    
    /**
     * User's roles. (Owning Side)
     * 
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     */
    private $userRoles;
    
    /**
     * 
     * 
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="DoctorGroup", inversedBy="doctors", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="doctor_doctorGroupsMembers")
     * 
     */
    private $doctorGroups;
    
     /**
     * 
     * Res
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Clinic", mappedBy="doctors")
     */
    private $doctorClinics;
    
    /**
     * 
     * Res
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="DoctorGroup", inversedBy="adminDoctors")
     * @ORM\JoinTable(name="doctor_doctorGroupsAdmins" )
     */
    private $groupsAdministrating;
    
    
     /**
     * 
     * Res
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="GroupPost", mappedBy="doctorAuthor", cascade={"remove"}, orphanRemoval=true)
     */
    private $groupPost;
    
    
    /**
     * 
     * Res
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Invitation", mappedBy="doctorInviter", cascade={"remove"}, orphanRemoval=true)
     */
    private $inviter;
    
    /**
     * 
     * Res
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Invitation", mappedBy="doctorInvitee", cascade={"remove"}, orphanRemoval=true)
     */
    private $invitee;
    
    
     /**
     * 
     * Res
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="commentAuthor", cascade={"remove"}, orphanRemoval=true)
     */
    private $comment;
    
    
    function getUserRoles() {
        $roles = array();
             foreach ($this->userRoles as $role) {
        $roles[] = $role->getRole();
        }
    }
    
    function getDoctorGroups() {
        return $this->doctorGroups;
    }
  
   
   
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Doctor
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return Doctor
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Doctor
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Doctor
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
    
    
    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set dateOfBirth
     *
     * @param DateTime $dateOfBirth
     * @return Doctor
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return DateTime 
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set fieldOfPractice
     *
     * @param string $fieldOfPractice
     * @return Doctor
     */
    public function setFieldOfPractice($fieldOfPractice)
    {
        $this->fieldOfPractice = $fieldOfPractice;

        return $this;
    }

    /**
     * Get fieldOfPractice
     *
     * @return string 
     */
    public function getFieldOfPractice()
    {
        return $this->fieldOfPractice;
    }

    /**
     * Set prcNumber
     *
     * @param float $prcNumber
     * @return Doctor
     */
    public function setPrcNumber($prcNumber)
    {
        $this->prcNumber = $prcNumber;

        return $this;
    }

    /**
     * Get prcNumber
     *
     * @return float 
     */
    public function getPrcNumber()
    {
        return $this->prcNumber;
    }

    /**
     * Set origIssueDate
     *
     * @param DateTime $origIssueDate
     * @return Doctor
     */
    public function setOrigIssueDate($origIssueDate)
    {
        $this->origIssueDate = $origIssueDate;

        return $this;
    }

    /**
     * Get origIssueDate
     *
     * @return DateTime 
     */
    public function getOrigIssueDate()
    {
        return $this->origIssueDate;
    }

 


    public function serialize() {
         return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    public function unserialize($serialized) {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
    
    public function getRoles() {
         $roles = array();
             foreach ($this->userRoles as $role) {
        $roles[] = $role->getRole();
        }

        return $roles;
    }
    
     public function getSalt()
    {
        
        return null;
    }
    
    public function eraseCredentials() {
        return null;
    }

 

    /**
     * Set aboutMyPractice
     *
     * @param string $aboutMyPractice
     * @return Doctor
     */
    public function setAboutMyPractice($aboutMyPractice)
    {
        $this->aboutMyPractice = $aboutMyPractice;

        return $this;
    }

    /**
     * Get aboutMyPractice
     *
     * @return string 
     */
    public function getAboutMyPractice()
    {
        return $this->aboutMyPractice;
    }
    
    
    public function setUserRoles(Role $role)
    {
        $this->userRoles[] = $role;
 
        //if (!$this->getUserRoles()->contains($role)){
          // $this->setUserRoles($role);
       //}
        
        return $this;
        
    }
    
    public function setDoctorGroups(DoctorGroup $doctorgroup)
    {
        $this->doctorGroups[] = $doctorgroup;
 
        return $this;
        
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userRoles = new ArrayCollection();
        $this->doctorGroups = new ArrayCollection();
    }

    /**
     * Add userRoles
     *
     * @param Role $userRoles
     * @return Doctor
     */
    public function addUserRole(Role $userRoles)
    {
        $this->userRoles[] = $userRoles;

        return $this;
    }

    /**
     * Remove userRoles
     *
     * @param Role $userRoles
     */
    public function removeUserRole(Role $userRoles)
    {
        $this->userRoles->removeElement($userRoles);
    }

    /**
     * Add doctorGroups
     *
     * @param DoctorGroup $doctorGroups
     * @return Doctor
     */
    public function addDoctorGroup(DoctorGroup $doctorGroups)
    {
        $this->doctorGroups[] = $doctorGroups;

        return $this;
    }

    /**
     * Remove doctorGroups
     *
     * @param DoctorGroup $doctorGroups
     */
    public function removeDoctorGroup(DoctorGroup $doctorGroups)
    {
        $this->doctorGroups->removeElement($doctorGroups);
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     * @return Doctor
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    
        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string 
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Add doctorClinics
     *
     * @param Clinic $doctorClinics
     * @return Doctor
     */
    public function addDoctorClinic(Clinic $doctorClinics)
    {
        $this->doctorClinics[] = $doctorClinics;
    
        return $this;
    }

    /**
     * Remove doctorClinics
     *
     * @param Clinic $doctorClinics
     */
    public function removeDoctorClinic(Clinic $doctorClinics)
    {
        $this->doctorClinics->removeElement($doctorClinics);
    }

    /**
     * Get doctorClinics
     *
     * @return Collection 
     */
    public function getDoctorClinics()
    {
        return $this->doctorClinics;
    }

    /**
     * Add specialties
     *
     * @param Specialty $specialties
     * @return Doctor
     */
    public function addSpecialty(Specialty $specialties)
    {
        $this->specialties[] = $specialties;
    
        if (!$specialties->getDoctors()->contains($this)){
            $specialties->addDoctor($this);
        }
        
        return $this;
    }

    /**
     * Remove specialties
     *
     * @param Specialty $specialties
     */
    public function removeSpecialty(Specialty $specialties)
    {
        $this->specialties->removeElement($specialties);
    }

    /**
     * Get specialties
     *
     * @return Collection 
     */
    public function getSpecialties()
    {
        return $this->specialties;
    }

    /**
     * Add groupsAdministrating
     *
     * @param DoctorGroup $groupsAdministrating
     * @return Doctor
     */
    public function addGroupsAdministrating(DoctorGroup $groupsAdministrating)
    {
        $this->groupsAdministrating[] = $groupsAdministrating;
    
        return $this;
    }

    /**
     * Remove groupsAdministrating
     *
     * @param DoctorGroup $groupsAdministrating
     */
    public function removeGroupsAdministrating(DoctorGroup $groupsAdministrating)
    {
        $this->groupsAdministrating->removeElement($groupsAdministrating);
    }

    /**
     * Get groupsAdministrating
     *
     * @return Collection 
     */
    public function getGroupsAdministrating()
    {
        return $this->groupsAdministrating;
    }

    /**
     * Set groupPost
     *
     * @param GroupPost $groupPost
     * @return Doctor
     */
    public function setGroupPost(GroupPost $groupPost = null)
    {
        $this->groupPost = $groupPost;
    
        return $this;
    }

    /**
     * Get groupPost
     *
     * @return GroupPost 
     */
    public function getGroupPost()
    {
        return $this->groupPost;
    }

    /**
     * Add groupPost
     *
     * @param DoctorGroup $groupPost
     * @return Doctor
     */
    public function addGroupPost(DoctorGroup $groupPost)
    {
        $this->groupPost[] = $groupPost;
    
        return $this;
    }

    /**
     * Remove groupPost
     *
     * @param DoctorGroup $groupPost
     */
    public function removeGroupPost(DoctorGroup $groupPost)
    {
        $this->groupPost->removeElement($groupPost);
    }

    /**
     * Add comment
     *
     * @param Comment $comment
     * @return Doctor
     */
    public function addComment(Comment $comment)
    {
        $this->comment[] = $comment;
    
        return $this;
    }

    /**
     * Remove comment
     *
     * @param Comment $comment
     */
    public function removeComment(Comment $comment)
    {
        $this->comment->removeElement($comment);
    }

    /**
     * Get comment
     *
     * @return Collection 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add fellowships
     *
     * @param Fellowship $fellowships
     * @return Doctor
     */
    public function addFellowship(Fellowship $fellowships)
    {
        $this->fellowships[] = $fellowships;
    
        return $this;
    }

    /**
     * Remove fellowships
     *
     * @param Fellowship $fellowships
     */
    public function removeFellowship(Fellowship $fellowships)
    {
        $this->fellowships->removeElement($fellowships);
    }

    /**
     * Get fellowships
     *
     * @return Collection 
     */
    public function getFellowships()
    {
        return $this->fellowships;
    }

   

    /**
     * Add inviter
     *
     * @param Invitation $inviter
     * @return Doctor
     */
    public function addInviter(Invitation $inviter)
    {
        $this->inviter[] = $inviter;
    
        return $this;
    }

    /**
     * Remove inviter
     *
     * @param Invitation $inviter
     */
    public function removeInviter(Invitation $inviter)
    {
        $this->inviter->removeElement($inviter);
    }

    /**
     * Get inviter
     *
     * @return Collection 
     */
    public function getInviter()
    {
        return $this->inviter;
    }

    /**
     * Add invitee
     *
     * @param Invitation $invitee
     * @return Doctor
     */
    public function addInvitee(Invitation $invitee)
    {
        $this->invitee[] = $invitee;
    
        return $this;
    }

    /**
     * Remove invitee
     *
     * @param Invitation $invitee
     */
    public function removeInvitee(Invitation $invitee)
    {
        $this->invitee->removeElement($invitee);
    }

    /**
     * Get invitee
     *
     * @return Collection 
     */
    public function getInvitee()
    {
        return $this->invitee;
    }
    
    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }
    
     /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getFile()->guessExtension();
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
     /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Doctor
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set contactNumber
     *
     * @param string $contactNumber
     *
     * @return Doctor
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    /**
     * Get contactNumber
     *
     * @return string
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

   
}
