<?php

namespace ResearchProject\MyProjectBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * GroupPost
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GroupPostRepository")
 */
class GroupPost
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="postTitle", type="string", length=255)
     */
    private $postTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="postContent", type="string", length=255)
     */
    private $postContent;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="datePosted", type="datetime")
     */
    private $datePosted;
    
    /**
     * 
     * 
     * 
     * 
     *@ORM\ManyToOne(targetEntity="DoctorGroup", inversedBy="groupPosts")
     *@ORM\JoinColumn(name="group_posts", referencedColumnName="id")
     */
     private $group;
     
     /**
     * @ORM\ManyToOne(targetEntity="Doctor", inversedBy="groupPost")
     * @ORM\JoinColumn(name="doctor_post", referencedColumnName="id")
     */
    private $doctorAuthor;
    
    
   
    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="post", cascade={"remove"}, orphanRemoval=true)
     */
    private $postComments;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postTitle
     *
     * @param string $postTitle
     * @return GroupPost
     */
    public function setPostTitle($postTitle)
    {
        $this->postTitle = $postTitle;
    
        return $this;
    }

    /**
     * Get postTitle
     *
     * @return string 
     */
    public function getPostTitle()
    {
        return $this->postTitle;
    }

    /**
     * Set postContent
     *
     * @param string $postContent
     * @return GroupPost
     */
    public function setPostContent($postContent)
    {
        $this->postContent = $postContent;
    
        return $this;
    }

    /**
     * Get postContent
     *
     * @return string 
     */
    public function getPostContent()
    {
        return $this->postContent;
    }

    /**
     * Set datePosted
     *
     * @param DateTime $datePosted
     * @return GroupPost
     */
    public function setDatePosted($datePosted)
    {
        $this->datePosted = $datePosted;
    
        return $this;
    }

    /**
     * Get datePosted
     *
     * @return DateTime 
     */
    public function getDatePosted()
    {
        return $this->datePosted;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
       
    }

    /**
     * Add group
     *
     * @param DoctorGroup $group
     * @return GroupPost
     */
    public function addGroup(DoctorGroup $group)
    {
        $this->group[] = $group;
    
      

    
       if (!$group->getGroupPosts()->contains($this)){
           $group->addGroupPost($this);
       }
        
       
        
        
        return $this;
    }

    /**
     * Remove group
     *
     * @param DoctorGroup $group
     */
    public function removeGroup(DoctorGroup $group)
    {
        $this->group->removeElement($group);
    }

    /**
     * Get group
     *
     * @return Collection 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set doctorAuthor
     *
     * @param Doctor $doctorAuthor
     * @return GroupPost
     */
    public function setDoctorAuthor(Doctor $doctorAuthor = null)
    {
        $this->doctorAuthor = $doctorAuthor;
    
        return $this;
    }

    /**
     * Get doctorAuthor
     *
     * @return Doctor 
     */
    public function getDoctorAuthor()
    {
        return $this->doctorAuthor;
    }

    /**
     * Set group
     *
     * @param \ResearchProject\MyProjectBundle\Entity\DoctorGroup $group
     * @return GroupPost
     */
    public function setGroup(\ResearchProject\MyProjectBundle\Entity\DoctorGroup $group = null)
    {
        $this->group = $group;
    
        return $this;
    }

    /**
     * Add postComments
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Comment $postComments
     * @return GroupPost
     */
    public function addPostComment(\ResearchProject\MyProjectBundle\Entity\Comment $postComments)
    {
        $this->postComments[] = $postComments;
    
        return $this;
    }

    /**
     * Remove postComments
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Comment $postComments
     */
    public function removePostComment(\ResearchProject\MyProjectBundle\Entity\Comment $postComments)
    {
        $this->postComments->removeElement($postComments);
    }

    /**
     * Get postComments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPostComments()
    {
        return $this->postComments;
    }
}
