<?php

namespace ResearchProject\MyProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fellowship
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ResearchProject\MyProjectBundle\Entity\FellowshipRepository")
 */
class Fellowship
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fellowshipName", type="string", length=255)
     */
    private $fellowshipName;

    /**
     * 
     * Res
     * @var ArrayCollection
     * 
     * @ORM\ManyToMany(targetEntity="Doctor", mappedBy="fellowships")
     */
    private $doctors;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fellowshipName
     *
     * @param string $fellowshipName
     * @return Fellowship
     */
    public function setFellowshipName($fellowshipName)
    {
        $this->fellowshipName = $fellowshipName;
    
        return $this;
    }

    /**
     * Get fellowshipName
     *
     * @return string 
     */
    public function getFellowshipName()
    {
        return $this->fellowshipName;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->doctors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add doctors
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Doctor $doctors
     * @return Fellowship
     */
    public function addDoctor(\ResearchProject\MyProjectBundle\Entity\Doctor $doctors)
    {
        $this->doctors[] = $doctors;
    
        return $this;
    }

    /**
     * Remove doctors
     *
     * @param \ResearchProject\MyProjectBundle\Entity\Doctor $doctors
     */
    public function removeDoctor(\ResearchProject\MyProjectBundle\Entity\Doctor $doctors)
    {
        $this->doctors->removeElement($doctors);
    }

    /**
     * Get doctors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDoctors()
    {
        return $this->doctors;
    }
}
