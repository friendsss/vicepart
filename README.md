# **Find Me A Doctor Iloilo (FIMADILO) Doctor Directory and Network Application** 
#

1.) Change directory to the project and update composer

```
#!cmd
$ cd your/path/ResearchProject
$ composer update
```
#
2.) Create the project's database

```
#!cmd

$ php app/console doctrine:database:create
```

#
3.) Execute command
```
#!cmd

$ php app/console doctrine:schema:update --force
```