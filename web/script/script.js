$(document).ready(function() {

    $('.form-signin').submit(function(e) {
        if ($('#username').val() == "" || $('#password').val() == "") {
            $('#errDiv').html('<p>Please fill in all fields.</p>');
            e.preventDefault();

            if ($('#username').val() == "") {
                $('#usernameLoginDiv').html($('#usernameLoginDiv').html() + '<i class="glyphicon glyphicon-exclamation-sign"></i>');
                $('#usernameLoginDiv').attr('class', 'right-inner-addon has-error');
            }
            if ($('#password').val() == "") {
                $('#passwordLoginDiv').html($('#passwordLoginDiv').html() + '<i class="glyphicon glyphicon-exclamation-sign"></i>');
                $('#passwordLoginDiv').attr('class', 'right-inner-addon has-error');
            }
        }
    });
    

    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '100', // Distance from top before showing element (px)
        topSpeed: 300, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // Animation in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: '', // Text for element
        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });

    $("#infoToggle").click(function() {
        $("#info").slideToggle();
    });


//    $('#admintable').dataTable();

});



